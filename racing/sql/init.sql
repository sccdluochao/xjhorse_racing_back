-- 加载初始数据
-- makemigrations racing
-- makemigrations racing --empty
-- 编辑makemigrations racing --empty生成的迁移文件，添加以下内容
-- def load_data_from_sql():
--     from xjhorse.settings import BASE_DIR
--     import os
--     sql_statements = open(os.path.join(BASE_DIR, 'racing/sql/init.sql'), 'r').read()
--     return sql_statements
--
-- class Migration(migrations.Migration):
--     dependencies = [
--         ('racing', '0001_initial'),
--     ]
--
--     operations = [
--         migrations.RunSQL(load_data_from_sql()),
--    ]


-- 插入比赛视频记录
INSERT INTO racing_video (title, ranking, duration, url, enabled, poster) VALUES
  ('盛装舞步', 0, '2:47', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Dressage.mp4', '1',
   'racing/racing_dressage.png'),
  ('障碍赛', 1, '2:36', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Show_Jumping.mp4', '1', 'racing/racing_show_jumping.jpg'),
  ('越野障碍赛', 2, '1:38', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Cross_Country.mp4', '1', 'racing/racing_cross_country_jumping.png'),
  ('联合驾驶赛', 3, '2:38', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Combined_Driving.mp4', '1', 'racing/racing_Combined_driving.jpg'),
  ('耐力骑乘赛', 4, '1:00', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Endurance_Riding.mp4', '1', 'racing/racing_endurance_riding.png'),
  ('马球比赛', 5, '2:38', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Horse-Ball.mp4', '1', 'racing/racing_horseball.png'),
  ('马背体操', 6, '3:00', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Vaulting.mp4', '1', 'racing/racing_vaulting.png'),
  ('骑士赛（跑马挑桩）', 7, '8:14', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Tent_Pegging.mp4', '1', 'racing/racing_tent_pegging.png'),
  ('驾驭比赛', 8, '7:04', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Reining.mp4', '1', 'racing/racing_reining.png'),
  ('残疾人马术', 9, '2:12', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Paralympic_Games.mp4', '1', 'racing/racing_paralympic.png'),
  ('越野障碍赛骑手视角', 10, '8:19', 'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Cross_Country_2.mp4', '1', 'racing/racing_cross_country_jumping.png'),
  ('越野障碍赛道详解 ', 11, '5:58',
   'http://zth-horse.oss-cn-beijing.aliyuncs.com/xjhorse/racing/Cross_Country_Course_Walk.mp4', '1',
   'racing/racing_cross_country_jumping.png');

-- 插入轮播图片记录
INSERT INTO racing_carousel (title, slogan, ranking, photo, enabled, active) VALUES
  ('好客人远迎君，果子沟高架桥', '昭苏欢迎您', 0, 'racing/Fruit_Valley_Viaduct.png', '1', '1'),
  ('蓝天白云风止步，碧水青草马落蹄 ', '大美昭苏等你来', 1, 'racing/zhaosu1.png', '1', '0'),
  ('黄萼裳裳绿叶稠，不是闲花野草流', '闻香识昭苏', 2, 'racing/zhaosu2.png', '1', '0'),
  ('此马非凡马，房星本是星', '天马弄里故事多', 3, 'racing/horses.png', '1', '0'),
  ('鸣驺辞凤苑，赤骥最承恩', '马蹄急，人心稳', 4, 'racing/race1.png', '1', '0'),
  ('吾闻果下马，羁策任蛮儿', '驭马之乐，非鱼所知', 5, 'racing/race2.png', '1', '0'),
  ('龙背铁连钱，银蹄白踏烟', '昭苏激情 ', 6, 'racing/race3.png', '1', '0'),
  ('一朝沟陇出，看取拂云飞', '起跑各争先', 7, 'racing/race4.png', '1', '0'),
  ('伯乐向前看，旋毛在腹间', '识马如识人', 8, 'racing/race5.png', '1', '0');

-- 插入赛事公告记录
INSERT INTO racing_eventsannouncement (title, content, created, enabled) VALUES
  ('赛事平台设计开发进入关键阶段', '课题组成员计划2018天马节之前能够让平台上线运行', '2018-03-01 12:36:00', '1');

-- 插入角色记录
INSERT INTO racing_role (name, description, min_certification) VALUES
  ('管理员', '1、赛事平台的超级用户<br>2、拥有所有权限和并承担相应义务', 5),
  ('裁判员', '1、查看自己执裁之比赛项目的详细信息<br>2、录入自己执裁之比赛项目的比赛结果和比赛成绩', 3),
  ('购票观众', '1、提交购票订单<br>2、查看订单信息<br>3、查看所购门票对应的所有比赛项目的详细信息', 3),
  ('单位参赛队伍', '1、提交参赛报名信息<br>2、查看本单位比赛项目详细信息', 5),
  ('个人参赛队伍', '1、提交参赛报名信息<br>2、查看本比赛项目详细信息', 3),
  ('单位马主', '1、提交单位参赛马匹报名信息<br>2、查看单位马匹比赛项目详细信息', 5),
  ('个人马主', '1、提交参赛马匹报名信息<br>2、查看马匹比赛项目详细信息', 3),
  ('骑师', '1、提交骑师参赛报名信息<br>2、查看自己参赛项目的详细信息', 3);

-- 插入协会记录
INSERT INTO racing_association (name) VALUES
  ('中国马业协会'),
  ('新疆马业协会'),
  ('伊犁哈萨克自治州马业协会');

-- 插入赛事规程记录
INSERT INTO racing_regulation (year,name,host,organizer,coorganizer,executive,support,participant,time,venue,
eligibility,method,administration,ranking_award,
prize_remark,analeptic_inspection,enroll_checkin,facility_finance,other,active) VALUES
  (2018, '2018中国新疆伊犁天马国际旅游节马术赛', '中国马业协会', '新疆昭苏县人民政府
新疆马业协会
伊犁州马业协会', '国家马属动物安全福利中心
中国纯血马登记管理委员会
国家马业传媒中心
昭苏马场
伊犁种马场
昭苏县西域马业有限责任公司', '中国马会赛马委员会（China Racing Authority）', '澳大利亚雅士牧场( Aquis Farm )
澳大利亚神奇百万马匹拍卖公司( Magic Millions Sales PTY LTD )', '纯血马参赛单位须为中国马业协会团体会员或马主会员
国产马参赛单位须为新疆马业协会会员、伊犁哈萨克自治州马业协会会员', '2017年7月16日-18日', '新疆昭苏·中国西域赛马场 ', '（一）参赛纯血马须持有中国马业协会纯血马登记管理委员会（CSBC）颁发的马匹护照，国产马必须持有新疆马业协会颁发的护照或登记备案，经资格审查委员会验证确认后方可参赛。
（二）参赛马匹在赛前按照规定要求进行验马，未参加验马的马匹不得参加比赛。
（三）纯血马的单场比赛，各单位每场可报2匹参赛马及1匹后备马，单场报满14匹即止。若有马匹退出比赛，根据剩余闸位数量及报名顺序补位，并交纳宣布出赛费。
（四）国产马比赛，每个项目每单位参赛马匹不超过3匹；单项参赛马匹超过14匹，按马匹数分闸比赛。每参赛马匹只能参加一个单项，不得兼项。
（五）各单位参赛的骑师数不得超过马匹数。
（六）参赛骑师性别不限，年龄需满16周岁。
（七）参赛骑师须持有县级以上医院出具的适合参加高强度赛马比赛的健康证明。
（八）参赛马匹须持有县级兽医站检疫合格证明书,检疫证明随马匹运输并交由承办单位办理回程检疫手续。
（九）本次为期三天的比赛要求所有参赛马匹（纯血马、国产马）必须提供马流感免疫证明或在赛前7-14天接种马流感疫苗，没有按照要求接种马流感疫苗的马匹不得参赛。
（十）“中国马会·雅士神奇百万杯”（G2）为邀请赛制。参赛马匹须为从神奇百万公司公开拍卖会购买的马匹、通过神奇百万在牧场私人交易中购买的马匹、父系或母系购自神奇百万公开拍卖会或私人交易。', '（一）本次比赛速度赛执行中国马业协会制定的《速度赛马竞赛规则（试行）》，速步赛执行伊犁哈萨克自治州马业协会制定的《速步赛马竞赛规则（试行）》，特殊情况遵照中国马业协会补充规定执行。

（二）纯血马赛事的赛制为条件赛。
1. “丝绸之路杯”（G1） 3岁马匹负重：
北半球马负重：若马匹在2015年1月1日（含）至12月31日（含）出生，公马负重56.0kg，母马负重54.5kg。骟马负重同公马。
南半球马负重：若马匹在2015年1月1日（含）至2015年7月31日（含）出生，公马负重56.0kg，母马负重54.5kg。若马匹在2015年8月1日（含）至2015年12月31日（含）出生，则公马负重54.5kg，母马负重53.0kg。骟马负重同公马。

2. “中国马会·雅士神奇百万杯”（G2）2岁及以上马匹负重：
2岁马匹负重：若马匹在2016年1月1日（含）至2016年12月31日（含）出生公马负重53.0kg，母马负重51.0kg。骟马负重同公马。
3岁马匹负重：若马匹在2015年1月1日（含）至2015年12月31日（含）出生公马负重55.0kg，母马负重53.0kg。骟马负重同公马。
4岁马匹负重：若马匹在2014年1月1日（含）至2014年12月31日（含）出生公马负重57.5kg，母马负重55.5kg。骟马负重同公马。
5岁及以上马匹负重：若马匹在2013年1月1日（含）之前出生公马负重58.0kg，母马负重56.0kg。骟马负重同公马。

3.国产马赛事马匹负重。
2岁马匹负重：马匹2016年出生，公马负重52.0kg，母马负重50.0kg。骟马负重同公马。
3及以上马匹负重：马匹2015年及以前出生，公马负重55.0kg，母马负重53.0kg。骟马负重同公马。

4.公开马组赛事马匹负重。
2岁马匹负重：马匹2016年出生，公马负重53.0kg，母马负重51.0kg。骟马负重同公马。
3岁及以上马匹负重：马匹2015年及以前出生，公马负重56.0kg，母马负重54.0kg。骟马负重同公马。

（三）免报名费，宣布出赛费每匹马按总奖金的1%收取。
（四）骑师和其所策骑的马匹必须在抽签前确定，不得随意更改。
（五）参赛马匹由抽签决定各组马号和闸位，且马号与闸位号一致。
（六）比赛在沙地举行,使用起跑马闸,沿顺时针方向行进。
（七）使用电子计时（千分之一秒）。', '（一）赛事董事组负责整个赛事的公平性、公正性、公开性。负责赛事过程的研讯和仲裁。
（二）资格审查组对参赛马匹的身份进行验证，决定其是否能够参赛；对骑师参赛证、彩衣、马匹装备进行验证，决定其是否能够参加比赛。
（三）赛事干事组负责整个赛事的运作，包括报名、检录、负磅与复磅、司闸、计时、赛事兽医、尿（血）样采集、马房管理、名次判定、成绩公布等工作。
（四）赛事董事组、资格审查组及赛事干事组的主要工作人员由中国马业协会选派，其他工作人员由承办单位提出推荐名单，报中国马业协会审批确定。', '（一）向获得头马的马主、骑师和马匹颁发奖杯、奖牌、奖盘、荣誉证书和奖金牌。
（二）奖金分配比例', '备注：“中国马会·雅士神奇百万杯”奖励获奖前八名：第一名：50,000元；第二名：20,000元；第三名：10,000元；第四名：6,000元；第五名：5,000元；第六名：4,000元；第七名：3,000元；第八名：2,000元。
其余赛事奖金为税前。“伊犁马会杯”相关赛事，参赛马匹14匹（含14匹）取前五名，奖金分配比例同“新疆育马者杯”，参赛马匹14匹以上取前八名，奖金分配：第一名：40%，第二名:18%，第三名：12%，第四名：10%，第五名：8%，第六名：6%，第七名：4%，第八名：2%。“伊犁马会杯”5000米马主挑战赛，2万元奖金为基础奖金，奖金分配比例由马主协商而定。

（三）违禁药物检测结果在赛后3个月之内公布，同时根据实际检测情况发放奖金。
（四）如果出现马匹名次并列的情况，将并列马匹所获奖金总和进行平均分配。', '参加比赛的单位必须与中国马会签订不使用违禁物质协议，并接受由赛马组委会指定的实验室进行违禁物质检测，比赛结束后，每场前三名马匹必须配合检测，其他抽样马匹由赛事董事确定配合检测。
若有名次的马匹违禁物质检测结果呈阳性，则取消其名次和奖金，头马贬为最低名次，罚款2万元，并对马主和参赛单位进行6个月的禁赛，其余马匹名次和奖金依次向上顺延。
若对检测结果有异议，可向赛事组委会提出复检申请，复检产生的所有费用由提出方承担。', '（一）纯血马组报名
登录中国国家马业网“重要发布”《2018年中华民族大赛马“丝绸之路杯”赛事规程》，下载附件并填写《纯血马参赛报名信息表》及《骑师登记信息表》，以电子邮件或传真形式发送至中国马会赛马委员会。参赛骑师务必认真完整填写报名表，附1寸蓝底免冠电子照片。

报名时间：即日起至2018年7月3日下午15:00（北京时间）。
相关详细信息，请致电中国马业协会赛马委员会。
联系人：曾敏     电  话：13146298401
邮  箱：cra@chinahorse.org   传  真：010-59195017

（二）国产马组、公开组报名
国产马组及公开组报名：参赛单位及个人必须携带马主身份证及身份证复印件和一寸照片、骑师身份证及身份证复印件和一寸照片，参赛马匹在西域赛马场现场报名，经检验合格后方可参加比赛。
国产马组及共公开组报名时间：2018年7月5日早上12:00至2018年7月12日下午18：00截止，报名截止当天对所有马匹以抽签的方式确定马闸号（全部时间为北京时间）。
资格审查委员会对报名的参赛马匹进行初步核查，对于符合参赛条件的马匹收取出赛费，费用为该项比赛总奖金的1%、由当场收缴参赛费用。
国产马及伊犁马报名联系人：
联系人：王守磊    电  话：15292771582   0999-6025929
邮 箱：1505186830@qq.com
联系人：吴云巴沙  电 话：18899572806   0999-6025929
各参赛单位在以收到中国马会赛马委员会（纯血马报名）或伊犁州马业协会（国产马、公开组报名）确认邮件确认报名成功，报名不成功或逾期报名，按不参赛论。
（三）报到
1、报到时间2018年7月12日下午18：00截止。
2、报到同时请提交马匹护照和马匹检疫证明，未提交相关证明的马匹不得进入马厩。马厩50元/天，水电免费，饲草自理。
3、所有参赛马主、骑师须在验马之后参加赛马委员会召开的赛前技术会。由于技术会涉及到重要的参赛技术事项，如缺席出现的任何失误由马主和骑师自行负责。
4、报到及入厩
联系人：巴音别勒克  15292790899
巴依斯哈     13619973958
5、工作人员食宿费用自理
（四）报到时各参赛单位必须携带参赛马匹的护照原件并交验有效的健康证明，必须遵守赛事组委会安排的时间和场地进行训练。', '（一）骑师彩衣由马主自备，并在中国马业协会注册，彩衣一经注册，不得随意更改；若没有彩衣，可使用中国马会指定的彩衣。
（二）中国马业协会统一配备汗垫和号码布，其它装备自理并符合赛事规则。
（三）承办方负责比赛期间编内人员的食宿费用。
（四）参赛骑师和马匹比赛期间的意外保险由各单位自行办理，参赛骑师和马匹在比赛期间及往返赛场路途中所发生的任何事故与意外伤害,主办方和承办方不承担任何责任。', '本赛事规程最终解释权归中国马业协会赛马委员会（常务委员会）所有。未尽事宜，另行通知。', '1');

-- 插入比赛日记录
INSERT INTO racing_gameday (name, year_id) VALUES
  ('7月16日', 1),
  ('7月17日', 1),
  ('7月18日', 1);

-- 插入马匹类型记录
INSERT INTO racing_horsetype (type_name) VALUES
  ('纯血马'),
  ('伊犁马'),
  ('国产马');

-- 插入比赛项目记录
INSERT INTO racing_game (name, time, prize, prize_comment, active, game_day_id, year_id, horse_type_id, fare_rate,horse_birthday_begin, horse_birthday_end, racing_horse_limit, backup_horse_limit) VALUES
  ('“丝绸之路杯”G1（3岁纯血马2000米速度赛）', '10:30', 20, '前五名：40%、24%、18%、12%、6%', '1', 1, 1, 1, 0.01 ,'2016-01-01', '2016-12-31', 2, 1),
  ('“丝绸之路杯”G1（3岁及以上国产马5000米速度赛）', '12:30', 20,'前五名：40%、24%、18%、12%、6%', '1', 1, 1, 3, 0.01 ,'2010-01-01', '2016-12-31', 2, 1),
  ('“中国马会·雅士神奇百万杯”G2（2岁及以上纯血马1600米速度赛）', '14:30', 10, '前八名，详见备注', '1', 1, 1, 1, 0.01 ,'2010-01-01', '2017-12-31', 3, 0),
  ('“新疆育马者杯”（2岁伊犁马1600米速度赛）', '16:30', 10, '前五名：40%、24%、18%、12%、6%', '1', 1, 1, 2, 0.01 ,'2017-01-01', '2017-12-31', 3, 0),
  ('“中国马会国家仪仗马杯”（3岁及以上伊犁马2000米速度赛）', '10:30', 10, '前五名：40%、24%、18%、12%、6% ', '1', 2, 1, 2, 0.01 ,'2010-01-01', '2016-12-31', 3, 0),
  ('“伊犁马会杯”（2岁伊犁马3600米速度赛）', '12:30', 8, '单组前五名，分组前八名，详见备注', '1', 2, 1, 2, 0.01 ,'2017-01-01', '2017-12-31', 3, 0),
  ('“伊犁马会杯”（3岁及以上公开组3000米 走马）', '14:30', 8, '单组前五名，分组前八名，详见备注', '1', 2, 1, 3, 0.01 ,'2010-01-01', '2016-12-31', 3, 0),
  ('“伊犁马会杯”（4岁及以上伊犁马10000米速度赛）', '16:30', 8, '单组前五名，分组前八名，详见备注', '1', 2, 1, 2, 0.01 ,'2010-01-01', '2015-12-31', 3, 0),
  ('“伊犁马会杯”（4岁及以上伊犁 3000米速度赛）', '10:30', 8, '单组前五名，分组前八名，详见备注', '1', 3, 1, 2, 0.01 ,'2010-01-01', '2015-12-31', 3, 0),
  ('“伊犁马会杯”（4岁及以上公开组15000米速度赛）', '12:30', 8, '单组前五名，分组前八名，详见备注', '1', 3, 1, 3, 0.01 ,'2010-01-01', '2015-12-31', 3, 0),
  ('“伊犁马会杯”（3岁及以上公开组3000米速度赛', '14:30', 8, '单组前五名，分组前八名，详见备注', '1', 3, 1, 3, 0.01 ,'2010-01-01', '2016-12-31', 3, 0),
  ('“伊犁马会杯”（3岁及以上公开组5000米马主挑战赛）', '16:30', 2, '单组前五名，分组前八名，详见备注', '1', 3, 1, 3, 0.01 ,'2010-01-01', '2016-12-31', 3, 0);

-- 插入门票产品记录
INSERT INTO racing_product (ticket_type, price, sold, game_day_id, available, name, stock, description, image) VALUES
  (1, 80, 0, 1, '1', '天马赛事门票', 10000, '当日有效，仅限壹人！', 'racing/ticket/第一天成人票.png'),
  (2, 40, 0, 1, '1', '天马赛事门票', 10000, '当日有效，仅限壹人！', 'racing/ticket/第一天儿童票.png'),
  (1, 80, 0, 2, '1', '天马赛事门票', 10000, '当日有效，仅限壹人！', 'racing/ticket/第二天成人票.png'),
  (2, 40, 0, 2, '1', '天马赛事门票', 10000, '当日有效，仅限壹人！', 'racing/ticket/第二天儿童票.png'),
  (1, 80, 0, 3, '1', '天马赛事门票', 10000, '当日有效，仅限壹人！', 'racing/ticket/第三天成人票.png'),
  (2, 40, 0, 3, '1', '天马赛事门票', 10000, '当日有效，仅限壹人！', 'racing/ticket/第三天儿童票.png');

--插入注册流程记录
INSERT INTO  racing_enrollmentworkflow (role_id, step, step_name, url, step_description, step_comment) VALUES
  (2, 1, '登录【新疆马业】', '/accounts/login', '1、如果您已经是【马产业科技创新平台】的注册用户，可以直接<a href="/accounts/login?next=/racing/enrollment">登录</a>
2、如果您尚未在【马产业科技创新平台】创建账户，请先进行<a href="/accounts/signup?next=/racing/enrollment">注册</a>', '任何人均可注册登录【新疆马业】'),
  (2, 2, '手机短信实名认证', '/certification', '裁判员必须完成【手机短信实名认证】', '登录【新疆马业】'),
  (2, 3, '启用【天马赛事】平台', '/my_application', '1、您必须亲自决定使用哪一个平台。
2、我们允许一个用户使用多个平台，您必须明白这意味着更多的责任和义务！
3、该步骤您必须选择【天马赛事】应用平台，否则无法执行后续的步骤！！！', '手机短信实名认证'),
  (2, 4, '选择【裁判员】角色', '/racing/my_racing_role', '1、您必须亲自决定自己的角色。
2、为了保证竞赛的公平性，裁判员不能兼任其他任何角色！
3、该步骤要求您选择【骑师】角色，否则无法执行后续的步骤！！', '启用【天马赛事】平台'),
  (2, 5, '裁判员登记', '/racing/enrollment_referee_apply', '提交裁判员信息', '选择【裁判员】角色'),
  (4, 1, '登录【新疆马业】', '/accounts/login', '1、如果您已经是【马产业科技创新平台】的注册用户，可以直接<a href="/accounts/login?next=/racing/enrollment">登录</a>
2、如果您尚未在【马产业科技创新平台】创建账户，请先进行<a href="/accounts/signup?next=/racing/enrollment">注册</a>', '任何人均可注册登录【新疆马业】'),
  (4, 2, '单位授权委托实名认证', '/certification', '单位参赛队伍必须完成【单位授权委托实名认证】', '登录【新疆马业】'),
  (4, 3, '启用【天马赛事】平台', '/my_application', '1、您必须亲自决定使用哪一个平台。
2、我们允许一个用户使用多个平台，您必须明白这意味着更多的责任和义务！
3、该步骤您必须选择【天马赛事】应用平台，否则无法执行后续的步骤！！！', '单位授权委托实名认证'),
  (4, 4, '选择【单位参赛队伍】角色', '/racing/my_racing_role', '1、您必须亲自决定自己的角色。
2、我们允许一个用户拥有多个角色，您必须明白这意味着更多的责任和义务！
3、该步骤要求您选择【单位参赛队伍】角色，否则无法执行后续的步骤！！！', '启用【天马赛事】平台'),
  (4, 5, '申请会员资格', '/racing/enrollment_member_apply', '2018中国新疆伊犁天马国际旅游节马术赛《赛事规程》对参赛实体的要求：
1、纯血马参赛单位须为中国马业协会团体会员或马主会员
2、国产马参赛单位须为新疆马业协会会员、伊犁哈萨克自治州马业协会会员', '选择【单位参赛队伍】角色'),
  (4, 6, '参赛项目报名', '/racing/team_game_enrollment', '1、选择参赛项目
2、指定参赛马匹
3、指定参赛骑手
4、支付出赛费', '1、赛会审核并批准您的会员资格
2、马主已经提交参赛马匹信息
3、赛会审核并批准参赛马匹资格
4、骑师已经提交个人参赛信息
5、赛会审核并批准骑师资格'),
  (5, 1, '登录【新疆马业】', '/accounts/login', '1、如果您已经是【马产业科技创新平台】的注册用户，可以直接<a href="/accounts/login?next=/racing/enrollment">登录</a>
2、如果您尚未在【马产业科技创新平台】创建账户，请先进行<a href="/accounts/signup?next=/racing/enrollment">注册</a>', '任何人均可注册登录【新疆马业】'),
  (5, 2, '手机短信实名认证', '/certification', '个人参赛队伍必须完成【手机短信实名认证】', '登录【新疆马业】'),
  (5, 3, '启用【天马赛事】平台', '/my_application', '1、您必须亲自决定使用哪一个平台。),
2、我们允许一个用户使用多个平台，您必须明白这意味着更多的责任和义务！
3、该步骤您必须选择【天马赛事】应用平台，否则无法执行后续的步骤！！！', '手机短信实名认证'),
  (5, 4, '选择【个人参赛队伍】角色', '/racing/my_racing_role', '1、您必须亲自决定自己的角色。
2、我们允许一个用户拥有多个角色，您必须明白这意味着更多的责任和义务！
3、该步骤要求您选择【个人参赛队伍】角色，否则无法执行后续的步骤！！', '启用【天马赛事】平台'),
  (5, 5, '申请个人会员资格', '/racing/enrollment_member_apply', '2018中国新疆伊犁天马国际旅游节马术赛《赛事规程》对参赛实体的要求：
1、纯血马参赛单位须为中国马业协会团体会员或马主会员
2、国产马参赛单位须为新疆马业协会会员、伊犁哈萨克自治州马业协会会员', '选择【个人参赛队伍】角色'),
  (5, 6, '参赛项目报名', '/racing/team_game_enrollment', '1、选择参赛项目
2、指定参赛马匹
3、指定参赛骑手
4、支付出赛费', '1、赛会审核并批准您的会员资格
2、马主已经提交参赛马匹信息
3、赛会审核并批准参赛马匹资格
4、骑师已经提交个人参赛信息
5、赛会审核并批准骑师资格'),
  (6, 1, '登录【新疆马业】', '/accounts/login', '1、如果您已经是【马产业科技创新平台】的注册用户，可以直接<a href="/accounts/login?next=/racing/enrollment">登录</a>
2、如果您尚未在【马产业科技创新平台】创建账户，请先进行<a href="/accounts/signup?next=/racing/enrollment">注册</a>', '任何人均可注册登录【新疆马业】'),
  (6, 2, '单位授权委托实名认证', '/certification', '单位马主必须完成【单位授权委托实名认证】', '登录【新疆马业】'),
  (6, 3, '启用【天马赛事】平台', '/my_application', '1、您必须亲自决定使用哪一个平台。
2、我们允许一个用户使用多个平台，您必须明白这意味着更多的责任和义务！
3、该步骤您必须选择【天马赛事】应用平台，否则无法执行后续的步骤！！！', '单位授权委托实名认证'),
  (6, 4, '选择【单位马主】角色', '/racing/my_racing_role', '1、您必须亲自决定自己的角色。
2、我们允许一个用户拥有多个角色，您必须明白这意味着更多的责任和义务！
3、该步骤要求您选择【单位马主】角色，否则无法执行后续的步骤！！！', '启用【天马赛事】平台'),
  (6, 5, '参赛马匹登记', '/racing/enrollment_horse_owner_list', '1、纯血马须持有中国马业协会纯血马登记管理委员会（CSBC）颁发的马匹护照。
2、国产马必须持有新疆马业协会颁发的护照或登记备案。
3、经资格审查委员会验证确认后方可参赛。', '前提：选择【单位马主】角色
后果：该步骤直接会影响到参赛队伍的报名'),
  (7, 1, '登录【新疆马业】', '/accounts/login', '1、如果您已经是【马产业科技创新平台】的注册用户，可以直接<a href="/accounts/login?next=/racing/enrollment">登录</a>
2、如果您尚未在【马产业科技创新平台】创建账户，请先进行<a href="/accounts/signup?next=/racing/enrollment">注册</a>', '任何人均可注册登录【新疆马业】'),
  (7, 2, '手机短信实名认证', '/certification', '个人马主必须完成【手机短信实名认证】', '登录【新疆马业'),
  (7, 3, '启用【天马赛事】平台', '/my_application', '1、您必须亲自决定使用哪一个平台。
2、我们允许一个用户使用多个平台，您必须明白这意味着更多的责任和义务！
3、该步骤您必须选择【天马赛事】应用平台，否则无法执行后续的步骤！！', '手机短信实名认证'),
  (7, 4, '选择【个人马主】角色', '/racing/my_racing_role', '1、您必须亲自决定自己的角色。
2、我们允许一个用户拥有多个角色，您必须明白这意味着更多的责任和义务！
3、该步骤要求您选择【个人马主】角色，否则无法执行后续的步骤！！！', '启用【天马赛事】平台'),
  (7, 5, '参赛马匹登记', '/racing/enrollment_horse_owner_list', '1、纯血马须持有中国马业协会纯血马登记管理委员会（CSBC）颁发的马匹护照。
2、国产马必须持有新疆马业协会颁发的护照或登记备案。
3、经资格审查委员会验证确认后方可参赛。', '前提：选择【个人马主】角色
后果：该步骤直接会影响到参赛队伍的报名'),
  (8, 1, '登录【新疆马业】', '/accounts/login', '1、如果您已经是【马产业科技创新平台】的注册用户，可以直接<a href="/accounts/login?next=/racing/enrollment">登录</a>
2、如果您尚未在【马产业科技创新平台】创建账户，请先进行<a href="/accounts/signup?next=/racing/enrollment">注册</a>', '任何人均可注册登录【新疆马业】'),
  (8, 2, '手机短信实名认证', '/certification', '骑师必须完成【手机短信实名认证】', '登录【新疆马业】'),
  (8, 3, '启用【天马赛事】平台', '/my_application', '1、您必须亲自决定使用哪一个平台。
2、我们允许一个用户使用多个平台，您必须明白这意味着更多的责任和义务！
3、该步骤您必须选择【天马赛事】应用平台，否则无法执行后续的步骤！！！', '手机短信实名认证'),
  (8, 4, '选择【骑师】角色', '/racing/my_racing_role', '1、您必须亲自决定自己的角色。
2、我们允许一个用户拥有多个角色，您必须明白这意味着更多的责任和义务！
3、该步骤要求您选择【骑师】角色，否则无法执行后续的步骤！！！', '启用【天马赛事】平台'),
  (8, 5, '参赛骑师登记', '/racing/enrollment_horseman_apply', '1、各参赛队伍的骑师数不得超过马匹数。
2、参赛骑师性别不限，年龄需满16周岁。
3、参赛骑师须持有县级以上医院出具的适合参加高强度赛马比赛的健康证明。', '前提：选择【骑师】角色
后果：该步骤直接会影响到参赛队伍的报名');


--插入收费项目记录
INSERT INTO  racing_charge (charge_name, charge_description) VALUES
  ('Ticket', '门票费'),
  ('Racing', '马匹出赛费');


--插入支付宝配置记录
INSERT INTO  racing_alipaysetting (charge_id, gateway, app_id, sign_type, product_code, app_private_key, alipay_public_key, return_url, debug_mode, notify_url) VALUES
  (1, 'https://openapi.alipaydev.com/gateway.do', '2016091100485292', 'RSA2', 'FAST_INSTANT_TRADE_PAY', '-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDYUN9JPwV55NJx
b+yNL4wA7eaMrGoW+ZbPctrXWsfr0nE9koDC7JSmBFvoXat0ezGIcxWNu/P7RX3i
yjQj3BQ6IEuE3em331C4CDF6BSi+2m0MZZBI3mkbkmW3D12l94TF58gsG5hcNraY
kP8OoqQTQRwTDt30htncdbS8KnVwzAh4s7fL4HcQG0JDRhqsa0rtfnz+hR5qlOSz
Z3hFSp7d/veKsZO5gFcbKDnYTvXX0LY7QpCCjpU9Ly+DkZytXFtaT3dI4wK4GmTv
rh5Nn0HGzwEnF741zLEyvFs3uz85xvgbrJNnK8ztsut4athiuLyRy44D+NUhCLHt
9a2Qh6AlAgMBAAECggEAApqupX4ZvcbwdacoTObWo6g5j1YADPjCRQsX8WHJaGBn
NpuN60rX20vQI3MFPmjBRuhDySoLHOpQQqu2hvv+TljXL+lQCAOrQBvxJIvtWqbc
izqpywbKcyeHC/YmZXoeYheRRF6sreeGuT19pJp2qTiucthJ+cip1Mqh+2XzdgEp
KiJhjYIswYGUb7DoX5EmEqv2aGdme31MwpTAyiDMwjP5mjeGRAAeO3sBsBvUm9zH
s1F9YGlbDxk8b7p6JtmXL3Texikf0DxmzRjIgQhj+q2Alco0MmSiIzemtZGlMOHX
PgcbnbPhaUUMjGVQfiXXbKT0PXKw4K45F+2Obgry3QKBgQDv5nfmOSuntSoOvROf
8DkrxXSrggZM4QDxsU/xN67LryvSizIbXpPPRF/pDa8QbSMYg7U+2Vktr20SajCg
tFgp/l/KaLqHrMYj1Jr/9seMNWX5wk22i5wVXzH0NfJXKuFeWt0GZx+Id3/DDqok
JNhqzbo+V/GyqdXT9WrjGa0v9wKBgQDm1ThZ4faXAKINJl6V1fu6obMpS+NwPBox
/ggoWVdmfMgtDBxaVZuqCaU4GFO9SHvKdfL5cIPFgAj/Qp0MagM1PMUYljbD7mHe
odxp/5jChjjsXl5QJoN/pYkg2hXhuOeBySIUXK7JHh8ZqwOUS2UpsxKlhhaaZVIW
s9611CzhwwKBgQDKxJC5Hb9hTAXVWUjaGXApKAcZ45exqUhkAMuiBb+PV2sB1Tcv
3Puv54ntcOx0RNqyILiz+3+d1rWpJ+ocGvKd4+xRJ73paUY8CLpN+ObnAkdEGFAZ
J91wTvrpACPqO0srtj805zrG3MI3OxDh5eaqN2LgyPJ4YYZjVf7XHt+8NwKBgGBe
ylz0MsAOOpmk2D5UDPl5AtSN41F7NjCC7yLxUcTu+Z+c+8VXElsmFb1aorNWrJ/1
KFCtgcYV0yvGwsxpAhfa3CqMpCaewmtxRhVW6Y7k/KTqLRPnUGUdVifKBPKtALI1
p626mBpNIgzUUfU6mOyU34cNeShSfrgNtkBRab+DAoGAB2g41Mw7HEqW0midBfjy
qdSg++ZFmHhKrzLDNctOwb+5nD5sfz08ytbLPY3gDjaHkZnJUukp28PC5SR2tziu
f5b8mVj3H+zePfjz4FtGrH1q+zJADPrNi4ys5B899CVazq6yLyzaM/cI7sakyE/b
T12ni0NsTwGgd/Pa3six78A=
-----END PRIVATE KEY-----', '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxa6SysdUeZf9N2DsLq8xlqJW4hRUgkaPFs93JWgXVk+cBwMtTFo8x1cONdmsOGb18aMcru3ICnDVYkktNPrBJa+iskRwOPeuCUdwrhP28cZIT15A7Ri1tNQEkWusWhCVCNQry8JhStUPV8IkXd38XGHy6BXxlwc1geVMzdyZ9aNZT+iRdW0HlOur4P9g3qVG97ibQYEcJLLAUCWHhq2KS0MaYQrXs5zvPx/exoWiLsIWag44im68auMmw7X65ypYvxd6THeK/1PgLUAOyskK6mAR8tR31OUG+nhnc2ZjUZyTl+nqhzBHm0ica686P9kEC5INgoR8PFIhzqI69jakwwIDAQAB
-----END PUBLIC KEY-----', 'http://localhost:8000/racing/ticket/alipay/return_url', '1', 'http://localhost:8000/racing/ticket/alipay/return_url'),
  (2, 'https://openapi.alipaydev.com/gateway.do', '2016091100485292', 'RSA2', 'FAST_INSTANT_TRADE_PAY', '-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDYUN9JPwV55NJx
b+yNL4wA7eaMrGoW+ZbPctrXWsfr0nE9koDC7JSmBFvoXat0ezGIcxWNu/P7RX3i
yjQj3BQ6IEuE3em331C4CDF6BSi+2m0MZZBI3mkbkmW3D12l94TF58gsG5hcNraY
kP8OoqQTQRwTDt30htncdbS8KnVwzAh4s7fL4HcQG0JDRhqsa0rtfnz+hR5qlOSz
Z3hFSp7d/veKsZO5gFcbKDnYTvXX0LY7QpCCjpU9Ly+DkZytXFtaT3dI4wK4GmTv
rh5Nn0HGzwEnF741zLEyvFs3uz85xvgbrJNnK8ztsut4athiuLyRy44D+NUhCLHt
9a2Qh6AlAgMBAAECggEAApqupX4ZvcbwdacoTObWo6g5j1YADPjCRQsX8WHJaGBn
NpuN60rX20vQI3MFPmjBRuhDySoLHOpQQqu2hvv+TljXL+lQCAOrQBvxJIvtWqbc
izqpywbKcyeHC/YmZXoeYheRRF6sreeGuT19pJp2qTiucthJ+cip1Mqh+2XzdgEp
KiJhjYIswYGUb7DoX5EmEqv2aGdme31MwpTAyiDMwjP5mjeGRAAeO3sBsBvUm9zH
s1F9YGlbDxk8b7p6JtmXL3Texikf0DxmzRjIgQhj+q2Alco0MmSiIzemtZGlMOHX
PgcbnbPhaUUMjGVQfiXXbKT0PXKw4K45F+2Obgry3QKBgQDv5nfmOSuntSoOvROf
8DkrxXSrggZM4QDxsU/xN67LryvSizIbXpPPRF/pDa8QbSMYg7U+2Vktr20SajCg
tFgp/l/KaLqHrMYj1Jr/9seMNWX5wk22i5wVXzH0NfJXKuFeWt0GZx+Id3/DDqok
JNhqzbo+V/GyqdXT9WrjGa0v9wKBgQDm1ThZ4faXAKINJl6V1fu6obMpS+NwPBox
/ggoWVdmfMgtDBxaVZuqCaU4GFO9SHvKdfL5cIPFgAj/Qp0MagM1PMUYljbD7mHe
odxp/5jChjjsXl5QJoN/pYkg2hXhuOeBySIUXK7JHh8ZqwOUS2UpsxKlhhaaZVIW
s9611CzhwwKBgQDKxJC5Hb9hTAXVWUjaGXApKAcZ45exqUhkAMuiBb+PV2sB1Tcv
3Puv54ntcOx0RNqyILiz+3+d1rWpJ+ocGvKd4+xRJ73paUY8CLpN+ObnAkdEGFAZ
J91wTvrpACPqO0srtj805zrG3MI3OxDh5eaqN2LgyPJ4YYZjVf7XHt+8NwKBgGBe
ylz0MsAOOpmk2D5UDPl5AtSN41F7NjCC7yLxUcTu+Z+c+8VXElsmFb1aorNWrJ/1
KFCtgcYV0yvGwsxpAhfa3CqMpCaewmtxRhVW6Y7k/KTqLRPnUGUdVifKBPKtALI1
p626mBpNIgzUUfU6mOyU34cNeShSfrgNtkBRab+DAoGAB2g41Mw7HEqW0midBfjy
qdSg++ZFmHhKrzLDNctOwb+5nD5sfz08ytbLPY3gDjaHkZnJUukp28PC5SR2tziu
f5b8mVj3H+zePfjz4FtGrH1q+zJADPrNi4ys5B899CVazq6yLyzaM/cI7sakyE/b
T12ni0NsTwGgd/Pa3six78A=
-----END PRIVATE KEY-----', '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxa6SysdUeZf9N2DsLq8xlqJW4hRUgkaPFs93JWgXVk+cBwMtTFo8x1cONdmsOGb18aMcru3ICnDVYkktNPrBJa+iskRwOPeuCUdwrhP28cZIT15A7Ri1tNQEkWusWhCVCNQry8JhStUPV8IkXd38XGHy6BXxlwc1geVMzdyZ9aNZT+iRdW0HlOur4P9g3qVG97ibQYEcJLLAUCWHhq2KS0MaYQrXs5zvPx/exoWiLsIWag44im68auMmw7X65ypYvxd6THeK/1PgLUAOyskK6mAR8tR31OUG+nhnc2ZjUZyTl+nqhzBHm0ica686P9kEC5INgoR8PFIhzqI69jakwwIDAQAB
-----END PUBLIC KEY-----', 'http://localhost:8000/racing/enrollment/return_url', '1', 'http://localhost:8000/racing/enrollment/return_url');
