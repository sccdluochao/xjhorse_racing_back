from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from certification.models import Certification, Corporation
from racing.models_regulation import Regulation
from racing.models_enrollment import Game, GameDay


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 角色类别
# 赛事平台用户角色
class Role(models.Model):
    name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        unique=True,  # 角色名称必须唯一
        verbose_name=_('Role Name')  # 角色名称
    )
    description = models.TextField(
        null=False,
        blank=False,
        verbose_name=_('Role Description')  # 角色说明
    )
    min_certification = models.CharField(  # 最低实名认证要求
        max_length=1,
        null=False,
        blank=False,
        default='3',
        verbose_name=_("Minimum Certification")
    )

    class Meta:
        verbose_name = _('Role')
        verbose_name_plural = _('Roles')

    def __str__(self):
        return self.name

    def get_enrollment_workflow(self):
        return EnrollmentWorkflow.objects.filter(role=self).order_by('step')


# 赛事平台用户权限
class Permission(models.Model):
    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
        verbose_name=_('User')
    )
    role = models.ForeignKey(
        Role,
        null=False,
        blank=False,
        verbose_name=_('Role')
    )

    class Meta:
        verbose_name = _('Permission')
        verbose_name_plural = _('Permissions')
        unique_together = (("user", "role"),)  # 一个用户拥有重复角色是不允许的


# 参赛报名流程
class EnrollmentWorkflow(models.Model):
    role = models.ForeignKey(
        Role,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("Role")
    )
    step = models.SmallIntegerField(
        null=False,
        blank=False,
        default=1,
        verbose_name=_("Enrollment Step")
    )
    step_name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Step Name")
    )
    url = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        default='/racing',
        verbose_name=_("Step URL")
    )
    step_description = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Step Description")
    )
    step_comment = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Premise & Comment")
    )

    class Meta:
        verbose_name = _('Enrollment Workflow')
        verbose_name_plural = _('Enrollment Workflow')


# 收费项目
class Charge(models.Model):
    charge_name = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        verbose_name=_("Charge Name")
    )
    charge_description = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Charge Description")
    )

    class Meta:
        verbose_name = _('Charge')
        verbose_name_plural = _('Charges')

    def __str__(self):
        return self.charge_description


# 支付宝接口参数
class AliPaySetting(models.Model):
    charge = models.OneToOneField(
        Charge,
        verbose_name=("Charge")
    )
    gateway = models.CharField(  # 支付宝网关地址
        max_length=200,
        blank=False,
        null=False,
        verbose_name=_("AliPay Gateway")
    )
    app_id = models.CharField(  # 支付宝分配给开发者的应用ID
        max_length=32,
        blank=False,
        null=False,
        verbose_name=_("AliPay App ID")
    )
    sign_type = models.CharField(  # 签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
        max_length=32,
        blank=False,
        null=False,
        default='RSA2',
        verbose_name=_("AliPay Sign Type")
    )
    product_code = models.CharField(  # 与支付宝签约的产品码名称，这里使用【FAST_INSTANT_TRADE_PAY】
        max_length=64,
        blank=False,
        null=False,
        default='FAST_INSTANT_TRADE_PAY',
        verbose_name=_("AliPay Product Code")
    )
    app_private_key = models.TextField(  # 应用私钥
        blank=False,
        null=False,
        verbose_name=_("AliPay App Private Key")
    )
    alipay_public_key = models.TextField(  # 支付宝应用公钥
        blank=False,
        null=False,
        verbose_name=_("AliPay Public Key")
    )
    notify_url = models.CharField(  # 异步通知返回地址，以HTTP/HTTPS开头的网页地址（处理支付宝返回结果的URL）
        max_length=256,
        blank=False,
        null=False,
        verbose_name=_("AliPay Notify URL")
    )
    return_url = models.CharField(  # 同步返回地址，以HTTP/HTTPS开头的网页地址（支付宝操作结束后跳转的页面）
        max_length=256,
        blank=False,
        null=False,
        verbose_name=_("AliPay Return URL")
    )
    debug_mode = models.BooleanField( # 是否启用调试模式
        blank=False,
        null=False,
        default=True,
        verbose_name=_("AliPay Debug Mode Enabled")
    )

    class Meta:
        verbose_name = _('AliPay Setting')
        verbose_name_plural = _('AliPay Setting')

    def __str__(self):
        return self.charge.charge_description
