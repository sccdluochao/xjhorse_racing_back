from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST

from certification.decorator import certificate_required
from racing.models_ticket import Product
from racing.cart import Cart
from racing.forms_cart import CartAddProductForm
from racing.models import Regulation


@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        clean_data = form.cleaned_data
        cart.add(product=product,
                 quantity=clean_data['quantity'],
                 update_quantity=clean_data['update'])
    return redirect('cart:cart_detail')


def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')


def cart_detail(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    cart = Cart(request)

    context={
        'regulation': regulation,
        'cart': cart,
    }
    return render(request, 'racing/ticket/cart_detail.html', context)
