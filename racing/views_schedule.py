import copy
import os
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from io import BytesIO

from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Spacer, Paragraph, Table, TableStyle

from racing.models_enrollment import Regulation, Game


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 比赛日程(游客)
from xjhorse import settings


def schedule_list(request):
    all_starting_gate_no = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]  # 有效的马闸号
    games = Game.objects.filter(active=True,year__active=1).order_by('game_day')
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    context = {
        "regulation": regulation,
        "games": games,
        "all_starting_gate_no": all_starting_gate_no,
    }
    return render(request, "racing/schedule/schedule_list.html", context)

# print schedule打印日程
class SchedulePrinter:
    def __init__(self, pk):
        self.gameid = pk

    def getpdf(self):

        games = Game.objects.filter(id=self.gameid)

        if games.count() > 0:
            self.game = games[0]
        else:
            return None

        # 注册字体，支持中文
        pdfmetrics.registerFont(TTFont('song', open(os.path.join(settings.BASE_DIR, "media/fonts/simsun.ttf"), 'rb')))
        buffer = BytesIO()
        # 获取简单样式集合
        styles = getSampleStyleSheet()
        # 简单文档模板
        doc = SimpleDocTemplate(buffer, pagesize=A4)
        # 垫片(Spacer：间隔)
        Story = [Spacer(1, 0)]
        # 标题
        title_style = copy.deepcopy(styles['Normal'])
        title_style.fontName = 'song'  # 使用的字体
        title_style.fontSize = 16
        title_style.textColor = colors.red
        title_style.alignment = 1
        title_style_p = Paragraph(self.game.name, title_style)
        Story.append(title_style_p)
        Story.append(Spacer(1, 0.2 * inch))

        # 时间
        time_style = copy.deepcopy(styles['Normal'])
        time_style.fontName = 'song'  # 使用的字体
        time_style.fontSize = 10
        time_style.alignment = 1
        time_style.textColor = colors.black
        time_style_p = Paragraph(self.game.game_day.name + self.game.time, time_style)
        Story.append(time_style_p)
        Story.append(Spacer(1, 0.2 * inch))

        # 获取数据
        data = self.getdata()

        t = Table(data)
        t.setStyle(TableStyle([
            ('GRID', (0, 0), (-1, -1), 0.5, colors.darkgray),  # 表格内部的网格线
            ('BOX', (0, 0), (-1, -1), 1, colors.black),  # 表格四周边框线
            ('FONT', (0, 0), (-1, -1), 'song'),  # 表格所有单元格均使用宋体
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),  # 表格所有单元格均使用居中对齐
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),  # 对齐
            ('FONTSIZE', (0, 0), (-1, 0), 12),  # 字体大小
        ]))

        t._argW[0] = 0.5 * inch  # 第一列宽度
        t._argW[1] = 0.5 * inch  # 第二列宽度
        t._argW[2] = 1.50 * inch  # 第三列宽度
        t._argW[3] = 1.50 * inch  # 第四列宽度
        t._argW[4] = 1.50 * inch  # 第五列宽度
        t._argW[5] = 1 * inch  # 第六列宽度
        t._argW[6] = 0.5 * inch  # 第七列宽度

        t._rowHeights[0] = 0.5 * inch  # 第一行高度
        Story.append(t)
        Story.append(Spacer(1, 0.5 * inch))

        # 落款
        inscribe_style = copy.deepcopy(styles['Normal'])
        inscribe_style.fontName = 'song'  # 使用的字体
        inscribe_style.fontSize = 10
        inscribe_style.alignment = 2
        inscribe_style.textColor = colors.black
        inscribe_style_p = Paragraph(
            '裁判员&nbsp:&nbsp;' + self.game.get_referees() + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;裁判员签字&nbsp:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp',
            inscribe_style)
        Story.append(inscribe_style_p)
        Story.append(Spacer(1, 0.5 * inch))

        # 落款时间
        inscribe_time_style = copy.deepcopy(styles['Normal'])
        inscribe_time_style.fontName = 'song'  # 使用的字体
        inscribe_time_style.fontSize = 10
        inscribe_time_style.alignment = 2
        inscribe_time_style.textColor = colors.black
        inscribe_time_style_p = Paragraph(
            '年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日',
            inscribe_time_style)
        Story.append(inscribe_time_style_p)

        doc.build(Story)

        return buffer

    # 获取数据
    def getdata(self):
        data = [['场次', '马闸号', '马匹', '骑师', '参赛队伍', '时间', '排名'], ]
        # 没有参赛单位
        if self.game.enrollmentdetail_set.count() == 0:
            for i in range(0, 14):
                subdata = ['', '', '', '', '', '', '']
                # 场次号
                subdata[0] = 1
                # 马闸号
                subdata[1] = i + 1
                data.append(subdata)
        else:
            # 获取组号最大
            group_no = self.game.enrollmentdetail_set.all().order_by("-group_no")[0].group_no
            for i in range(0, group_no):
                for j in range(0, 14):
                    subdata = ['', '', '', '', '', '', '']
                    eds = self.game.enrollmentdetail_set.filter(group_no=i + 1, starting_gate_no=j + 1)
                    # 存在这个比赛
                    if eds.count() > 0:
                        subdata[2] = eds[0].horse.name
                        subdata[3] = eds[0].horseman.user.userName
                        subdata[4] = eds[0].enrollment.member
                        if eds[0].ranking != 0:
                            subdata[6] = eds[0].ranking
                    subdata[0] = i + 1
                    subdata[1] = j + 1
                    data.append(subdata)
        return data


# 获取日程报表
def get_schedule_pdf(request, pk):
    # 判断是否有这个记录
    get_object_or_404(Game, pk=pk)

    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = "filename='schedule.pdf'"

    # 实例化
    sch_pdf = SchedulePrinter(pk)
    buffer = sch_pdf.getpdf()

    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response