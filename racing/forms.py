# 创 建 人：张太红
# 创建日期：2018年03月17日

from django import forms
from racing.models import Permission, Role
from racing.models_enrollment import Member


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 角色权限
class PermissionForm(forms.ModelForm):
    class Meta:
        model = Permission
        fields = ('user', 'role')

        widgets = {'user': forms.HiddenInput(),
                   'role': forms.Select(attrs={'onchange': 'get_description();'}),
                   }

    def __init__(self, user, *args, **kwargs):
        super(PermissionForm, self).__init__(*args, **kwargs)
        if user.has_perm('permission.corporation_certificated'):
            # 通过单位委托授权实名认证的用户可以选择任意角色
            roles = Role.objects.all()
        elif user.has_perm('permission.individual_certificated'):
            # 通过手机微信实名认证的用户可以选择以下角色：
            '''
            裁判员
            购票观众
            个人参赛队伍
            个人马主
            骑师
            '''
            roles = Role.objects.filter(min_certification='3')
        else:
            # 非实名认证用户不能选择任何角色
            roles = Role.objects.filter(id=-1)

        # 赛事平台只有1个管理员
        try:
            perm = Permission.objects.get(role_id=1)
            roles = roles.exclude(id__in=[perm.role.id])
        except Permission.DoesNotExist:
            pass

        # 下拉列别中排除该用户已经拥有的角色
        # 获取该用户已经拥有的角色
        role_id_list = Permission.objects.filter(user=user).values_list('role_id', flat=True)
        roles = roles.exclude(id__in=role_id_list)

        # 如果用户已经拥有【管理员】则不能再同时拥有其它角色
        if 1 in role_id_list:
            roles = roles.filter(id=-1)

        # 如果用户已经拥有【裁判员】则不能再同时拥有以下角色：
        '''
        管理员
        裁判员
        单位参赛队伍
        个人参赛队伍
        单位马主
        个人马主
        骑师
        '''
        if 2 in role_id_list:
            roles = roles.exclude(id__in=[1, 2, 4, 5, 6, 7, 8])

        # 如果用户已经拥有管理员、裁判员、单位参赛队伍、个人参赛队伍、单位马主、个人马主、骑师角色，则不能再同时拥有裁判角色
        can_be_referee = True
        for role_id in role_id_list:
            if role_id in [1, 2, 4, 5, 6, 7, 8]:
                can_be_referee = False
        if not can_be_referee:
            roles = roles.exclude(id__in=[2])

        # 如果用户已经拥有任何角色，则不能再同时拥有【管理员】
        if len(role_id_list) > 0:
            roles = roles.exclude(id__in=[1])

        # 不能同时为【单位参赛队伍和个人参赛队伍】
        if 4 in role_id_list:
            roles = roles.exclude(id__in=[5])
        if 5 in role_id_list:
            roles = roles.exclude(id__in=[4])
        self.fields['role'].queryset = roles


# 协会会员资格
class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ('user',
                  'member_type',
                  'contact',
                  'contact_number',
                  'association',
                  'trainer',
                  'trainer_number',
                  'veterinarian',
                  'veterinarian_number')

        widgets = {'user': forms.HiddenInput()}
