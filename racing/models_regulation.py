from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 2、赛事规程
class Regulation(models.Model):
    year = models.SmallIntegerField(
        null=False,
        blank=False,
        verbose_name=_("Year")  # 比赛项目名称
    )

    name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Regulation Name")  # 比赛项目名称
    )

    host = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Host")  # 主办单位
    )
    organizer = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Organizer")  # 承办单位
    )
    coorganizer = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("CoOrganizer")  # 协办单位
    )
    executive = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Executive")  # 执行单位
    )
    support = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Support")  # 支持单位
    )
    participant = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Game Participant")  # 参赛单位
    )
    time = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        verbose_name=_("Time")  # 比赛时间
    )
    venue = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Venue")  # 比赛场地
    )
    eligibility = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Eligibility")  # 参赛资格
    )
    method = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Method")  # 竞赛办法
    )

    administration = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Game Administration")  # 赛事机构
    )

    ranking_award = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Ranking & Award")  # 录取名次和奖励
    )
    prize_remark = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Prize Remark")  # 奖励备注
    )

    analeptic_inspection = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Analeptic Inspection")  # 违禁物质检测
    )

    enroll_checkin = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Enroll Checkin")  # 报名及报到
    )

    facility_finance = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Facility Finance")  # 器材和经费
    )

    other = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Other")  # 其它
    )

    active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("Active Game Year")  # 有效状态
    )

    class Meta:
        verbose_name = _('Game Year')
        verbose_name_plural = _('Game Year')

    def __str__(self):
        return str(self.year)+"年度"+str(self.name)

    def get_absolute_url(self):
        return reverse('regulation_manage_detail', kwargs={'pk': self.id})
