from django.http import HttpResponse, JsonResponse, QueryDict
from django.template.loader import render_to_string
from django.views.generic import ListView, DeleteView, UpdateView, CreateView, DetailView
from django.urls import reverse_lazy
from rules.contrib.views import PermissionRequiredMixin
from django.shortcuts import render

from racing.forms_regulation import GameDayModelForm
from racing.models_regulation import Regulation
from racing.models_enrollment import Game, GameDay


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 赛事规程(游客)
def regulation(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    games = Game.objects.all().filter(active=True, year=regulation).order_by('id')
    context = {
        "regulation": regulation,
        "games": games,
    }
    return render(request, "racing/regulation/regulation.html", context)


# ********************************************************
# 赛事规程管理 model：Regulation
# ********************************************************
# 赛事规程管理列表
class RegulationManageListView(PermissionRequiredMixin, ListView):
    model = Regulation
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/regulation/regulation_manage_list.html'

    def get_permission_object(self):
        return "天马赛事"


regulation_manage_list = RegulationManageListView.as_view()


# 创建赛事规程
class RegulationManageCreateView(PermissionRequiredMixin, CreateView):
    model = Regulation
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/regulation/regulation_manage_create.html'
    fields = ['year',
              'name',
              'host',
              'organizer',
              'coorganizer',
              'executive',
              'support',
              'participant',
              'time',
              'venue',
              'eligibility',
              'method',
              'administration',
              'ranking_award',
              'prize_remark',
              'analeptic_inspection',
              'enroll_checkin',
              'facility_finance',
              'other',
              'active',
              ]

    def get_permission_object(self):
        return "天马赛事"

    #表单验证通过调用的方法
    def form_valid(self, form):
        form = super(RegulationManageCreateView, self).form_valid(form)
        # 当前赛程为激活状态时，将其余赛程设为非激活
        if self.object.active:
            regulation_list = Regulation.objects.filter(active=1)
            for item in regulation_list:
                #当前赛程排除掉
                if item.id != self.object.id:
                    item.active = 0
                    item.save()
        return form


    def get_initial(self):
        regulation = Regulation.objects.get(active=True)
        return {
            'year': regulation.year + 1,
            'name': regulation.name,
            'host': regulation.host,
            'organizer': regulation.organizer,
            'coorganizer': regulation.coorganizer,
            'executive': regulation.executive,
            'support': regulation.support,
            'participant': regulation.participant,
            'time': regulation.time,
            'venue': regulation.venue,
            'eligibility': regulation.eligibility,
            'method': regulation.method,
            'administration': regulation.administration,
            'ranking_award': regulation.ranking_award,
            'prize_remark': regulation.prize_remark,
            'analeptic_inspection': regulation.analeptic_inspection,
            'enroll_checkin': regulation.enroll_checkin,
            'facility_finance': regulation.facility_finance,
            'other': regulation.other,
            'active': False,
        }


regulation_manage_create = RegulationManageCreateView.as_view()


# 赛事规程明细
class RegulationManageDetailView(PermissionRequiredMixin, DetailView):
    model = Regulation
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/regulation/regulation_manage_detail.html'

    def get_permission_object(self):
        return "天马赛事"


regulation_manage_detail = RegulationManageDetailView.as_view()


# 赛事规程修改
class RegulationManageUpdateView(PermissionRequiredMixin, UpdateView):
    model = Regulation
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True

    template_name = 'racing/regulation/regulation_manage_update.html'
    fields = ['year',
              'name',
              'host',
              'organizer',
              'coorganizer',
              'executive',
              'support',
              'participant',
              'time',
              'venue',
              'eligibility',
              'method',
              'administration',
              'ranking_award',
              'prize_remark',
              'analeptic_inspection',
              'enroll_checkin',
              'facility_finance',
              'other',
              'active',
              ]

    #验证通过访问的时间
    def form_valid(self, form):
        form=super(RegulationManageUpdateView, self).form_valid(form)
        # 当前赛程为激活状态时，将其余赛程设为非激活
        if self.object.active:
            regulation_list = Regulation.objects.filter(active=1)
            for item in regulation_list:
                #排除掉当前赛程
                if item.id != self.object.id:
                    item.active = 0
                    item.save()

        return form

    def get_permission_object(self):
        return "天马赛事"


regulation_manage_update = RegulationManageUpdateView.as_view()


# 赛事规程删除
class RegulationManageDeleteView(PermissionRequiredMixin, DeleteView):
    model = Regulation
    success_url = reverse_lazy('regulation_manage_list')
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/regulation/regulation_manage_delete.html'

    def get_permission_object(self):
        return "天马赛事"


regulation_manage_delete = RegulationManageDeleteView.as_view()

#编辑比赛日
def edit_gameday(request,r_id):
    rs={"status":0,"msg":"操作失败！"}
    regulation = Regulation.objects.get(id=r_id)
    gameday_list = GameDay.objects.filter(year=regulation)
    #处理GET请求
    if request.method == "GET":
        gameday_form=GameDayModelForm(initial={"year":regulation})
        context={
            'gameday_list':gameday_list,
            "gameday_form":gameday_form,
            "regulation":regulation
        }
        rs['html_form']=render_to_string("racing/regulation/render_gameday_form.html",\
                                         context,request)
        rs['status']=1
        return JsonResponse(rs)
    #处理POST请求
    elif request.method == "POST":
        gameday_form = GameDayModelForm(request.POST)
        if gameday_form.is_valid():
            gameday_form.save()
            rs['status'] = 1
        context = {
            'gameday_list': gameday_list,
            "gameday_form": gameday_form,
            "regulation":regulation
        }
        rs['html_form'] = render_to_string("racing/regulation/render_gameday_form.html", \
                                           context, request)

        return JsonResponse(rs)
    #删除某个比赛日
    elif request.method == "DELETE":
        delete_dict=QueryDict(request.body)
        gdid=delete_dict.get("id")
        gd=GameDay.objects.get(id=gdid)
        gameday_form = GameDayModelForm(initial={"year": regulation})
        context = {
            'gameday_list': gameday_list,
            "gameday_form": gameday_form,
            "regulation": regulation
        }
        if gd.game_set.count() == 0:
            gd.delete()
            rs['html_form'] = render_to_string("racing/regulation/render_gameday_form.html", \
                                               context, request)
            rs['status']=1
        else:
            rs['msg']="有比赛项目的比赛日无法删除！"
        return JsonResponse(rs)
    return HttpResponse("Do not support protocol!")