from django.template.defaultfilters import stringfilter, register

#给网页中字符右侧补空格
@register.filter(is_safe=True)
@stringfilter
def myrjust(value, arg):

    if len(value)>=int(arg):
        return value

    for i in range(0,int(arg)-len(value)):
        value=value+"&nbsp;&nbsp;"

    return value