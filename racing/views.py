import hashlib
import json
import random

import requests
import time
import urllib3
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.generic import ListView
from django.utils.translation import ugettext as _
from rules.contrib.views import PermissionRequiredMixin
from django.shortcuts import render, get_object_or_404
from certification.decorator import certificate_required
from racing.forms import PermissionForm
from racing.models import Permission, Role


# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 角色权限管理 model：Permission
# ********************************************************


# 我的角色
@login_required  # 首先必须是登录用户
@certificate_required('3')  # 必须通过手机短信认证
def my_racing_role(request):
    my_permissions = Permission.objects.filter(user=request.user)
    context = {
        "my_permissions": my_permissions,
    }
    return render(request, "racing/user_role_list.html", context)


# 添加角色
def my_role_add(request):
    data = dict()

    if request.method == 'POST':
        form = PermissionForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            my_permissions = Permission.objects.filter(user=request.user)
            data['html_role_list'] = render_to_string('racing/user_role_partial.html',
                                                      {'my_permissions': my_permissions},
                                                      request=request)
            data['html_add_role_btn'] = render_to_string('racing/user_role_add_btn.html', {
                'my_permissions': my_permissions
            })
        else:
            data['form_is_valid'] = False
    else:
        # 通过 initial={'user': request.user} 将新对象的user字段初始设置为当前登录用户
        form = PermissionForm(request.user, initial={'user': request.user})

    context = {'form': form}
    data['html_form'] = render_to_string('racing/user_role_add.html',
                                         context,
                                         request=request
                                         )

    return JsonResponse(data)


# 删除角色
def my_role_abandon(request, pk):
    permission = get_object_or_404(Permission, pk=pk)
    data = dict()
    if request.method == 'POST':
        permission.delete()
        data['form_is_valid'] = True
        my_permissions = Permission.objects.filter(user=request.user)
        data['html_role_list'] = render_to_string('racing/user_role_partial.html',
                                                  {'my_permissions': my_permissions},
                                                  request=request)
        data['html_add_role_btn'] = render_to_string('racing/user_role_add_btn.html', {
            'my_permissions': my_permissions
        })
    else:
        context = {'permission': permission}
        data['html_form'] = render_to_string('racing/user_role_abandon.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# 获取每个角色的描述
def get_description(request):
    data = dict()
    if request.is_ajax() and request.method == 'GET':
        role_id_str = request.GET.get('role', '')
        if role_id_str is not None and role_id_str != '':
            role_id = int(role_id_str)
            role = Role.objects.get(id=role_id)
            data['description'] = role.description
        else:
            data['description'] = _("Please choose a Role!")

    return JsonResponse(data)


# 管理员查看全部角色权限
class RoleListView(PermissionRequiredMixin, ListView):
    model = Permission

    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/role_list.html'

    def get_permission_object(self):
        return "天马赛事"


role_list = RoleListView.as_view()


#发送短信
def send_text_msg(phone,msg):
    requests.packages.urllib3.disable_warnings()
    # 一个PoolManager实例来生成请求, 由该实例对象处理与线程池的连接以及线程安全的所有细节
    http = urllib3.PoolManager()

    appkey = "e3f663cb9bbf0aa9f39450d111d1f72e"
    # 开发者的AppID
    current = int(round(time.time()))
    # 当前时间戳，为符合UNIX Epoch时间戳规范的数值，单位为秒
    code = str(random.randint(1000,990000))

    # <!-- 拼接签名串 -->
    srcStr = 'appkey=' + str(appkey) + '&random=' + str(code) + '&time=' + str(current) + '&mobile='+phone

    #生成加密字符串
    hash = hashlib.sha256()
    hash.update(srcStr.encode('utf-8'))
    hashstr = hash.hexdigest()

    data = {
        "ext": "",
        "extend": "",
        "msg": "您的验证码为：" + msg + "，请于1分钟内填写。如非本人操作，请忽略本短信。",
        "sig": hashstr,
        "tel": {
            "mobile": phone,
            "nationcode": "86"
        },
        "time": current,
        "type": 0
    }

    encoded_data = json.dumps(data).encode('utf-8')

    appid = "1400055665";  # 腾讯短信AppID
    url = "https://yun.tim.qq.com/v5/tlssmssvr/sendsms?sdkappid=" + appid + "&random=" + code;

    # 通过request()方法创建一个请求：
    r = http.request('POST',
                     url,
                     body=encoded_data,
                     headers={'Content-Type': 'application/json'})

    #print(r.status)  # 200
    # 获得html源码,utf-8解码
    #print(r.data.decode('utf-8'))

    return r.data.decode('utf-8')