import json
import datetime
import hashlib
import urllib3
import time
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import transaction, IntegrityError, DatabaseError, Error
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from io import BytesIO

from django.urls import reverse
from rules.contrib.views import permission_required
from django.views.decorators.http import require_http_methods
from allauth.account.models import EmailAddress
from certification.models import Certification
from permission.models import Application, UserApplication
from racing.models_enrollment import Horse, Horseman, Referee, Member, Enrollment, EnrollmentDetail
from racing.models_regulation import Regulation
from .models import Permission as Role_Permission, Role
from racing.forms_enrollment import HorsemanForm, RefereeForm
from racing.forms_enrollment_admin import UserModelForm, IndividualCertificationModelForm, MemberCertificationModelForm, \
    CorporationModelForm, HorseModelForm, MemberForm, EnrollmentDetailModelForm, PermissionModelForm
from .models import Permission as User_Permission


# 注册首页

# 处理异常
def handle_errors(errors):
    errors_dict = eval(errors)
    errors_keys = []
    errors_values = []
    for k, v in errors_dict.items():
        errors_keys.append(k)
        errors_values.append(v[0]['message'])
    return errors_keys, errors_values

# 注册中心首页
def enrolment_admin_index(request):
    return render(request, "racing/enrollment_admin/index.html")

# 识别身份证
@require_http_methods(["POST"])
@login_required
def enrolment_admin_idcardrecognize(request):
    rs = {"status": 0, "msg": ""}
    if "idcardimg" in request.FILES:
        buffer = BytesIO()

        for chunk in request.FILES['idcardimg'].chunks():
            buffer.write(chunk)

        current_datetime = str(datetime.datetime.now())
        dev_key = "55a7042d338dff78107d7b7599e26ba6"
        headers = {
            "x-app-key": "c45d5465",
            "x-request-date": current_datetime,
            "x-session-key": hashlib.md5((current_datetime + dev_key).encode('utf-8')).hexdigest(),
            # 身份证正面
            "x-task-config": "capkey=ocr.cloud.template,property=idcard,templateIndex=0,templatePageIndex=0",
            # 身份证反面
            # "x-task-config": "capkey=ocr.cloud.template,property=idcard,templateIndex=0,templatePageIndex=1",
            "x-sdk-version": "5.2"
        }
        http = urllib3.PoolManager()
        urllib3.disable_warnings()
        try:
            r = http.request('POST',
                             "http://ocr.aicloud.com:8541/ocr",
                             body=buffer.getvalue(),
                             headers=headers)
        except Exception:
            rs['msg'] = "出现了一个问题，请稍后再试，或检查网络是否通畅！"
            return JsonResponse(rs)
        r_msg = json.loads(r.data.decode('utf-8'))

        # 请求成功
        if r.status == 200 and r_msg['error'] == 0:
            r_id_msg = r_msg['data']['forms']['form']['page']['cell']
            rs["identity"] = r_id_msg[0]['result']['result']
            rs["userName"] = r_id_msg[2]['result']['result']
            rs["ethnicity"] = r_id_msg[1]['result']['result']
            rs["address"] = r_id_msg[5]['result']['result']
            rs["gender"] = r_id_msg[3]['result']['result']
            rs["birthday"] = r_id_msg[4]['cell'][0]['result']['result'] + "-" + r_id_msg[4]['cell'][1]['result'][
                'result'] + "-" + r_id_msg[4]['cell'][2]['result']['result']
            rs['status'] = 1
        else:
            rs['msg'] = "识别错误，请更换照片！"
        request.FILES['idcardimg'].close()
        buffer.close()
    return JsonResponse(rs)

# 骑师列表
@login_required
def enrollment_admin_horseman_list(request):
    context={}
    # 搜索时
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        horseman_list = Horseman.objects \
            .filter((Q(user__identity__contains=request.GET["search_text"]) \
                     | Q(user__mobile__contains=request.GET["search_text"]) \
                     | Q(user__userName__contains=request.GET["search_text"]) \
                     | Q(user__user__username__contains=request.GET["search_text"])))
        context["search_text"] = request.GET["search_text"]
    # 未搜索时
    else:
        horseman_list = Horseman.objects.all()

    horseman_list_instance=list(horseman_list)
    #for item in horseman_list:
     #   horseman_list_instance.append(item)
    horseman_list_instance.sort(key=lambda horseman:horseman.get_all_prize(),reverse=True)

    context["horseman_list"] = horseman_list_instance
    return render(request, "racing/enrollment_admin/horseman_list.html", context)

# 骑师注册
@login_required
@permission_required('racing.admin', raise_exception=True)
@transaction.atomic
def enrollment_admin_horseman(request):
    # 处理GET请求
    if request.method == "GET":
        # 修改
        if "id" in request.GET:
            horseman = Horseman.objects.get(user__user_id=request.GET['id'])
            user = horseman.user.user
            user.password = ""
            userform = UserModelForm(instance=user)
            horsemanform = HorsemanForm(instance=horseman)
            icform = IndividualCertificationModelForm(instance=horseman.user)
            context = {
                "horsemanform": horsemanform,
                "icform": icform,
                "userform": userform
            }
        # 创建
        else:
            userform = UserModelForm()
            horsemanform = HorsemanForm()
            icform = IndividualCertificationModelForm()
            context = {
                "horsemanform": horsemanform,
                "icform": icform,
                "userform": userform,
                "iscreating": True
            }

        context['html_form'] = render_to_string("racing/enrollment_admin/render_horseman_enrollment_form.html",
                                                context, request)
        return render(request,
                      "racing/enrollment_admin/horseman_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            hm_modelform = HorsemanForm(request.POST, request.FILES)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            user = User.objects.get(id=request.GET['id'])
            user_modelform = UserModelForm(instance=user, data=request.POST)
            ic_modelform = IndividualCertificationModelForm(instance=user.certification, \
                                                            data=request.POST, files=request.FILES)
            hm_modelform = HorsemanForm(instance=user.certification.horseman, \
                                        data=request.POST, files=request.FILES)
            print("测试", user.certification.horseman.qualified)
            rs['msg'] = "修改成功！"
        context["horsemanform"] = hm_modelform
        context["icform"] = ic_modelform
        context["userform"] = user_modelform

        # 数据验证，然后更新
        if (is_update and not is_create) \
                or user_modelform.is_valid_custom(request):
            if ic_modelform.is_valid():
                if hm_modelform.is_valid():
                    try:
                        with transaction.atomic():
                            # 直接创建
                            if is_create:
                                # 创建用户信息
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()
                                # 是邮箱处于被验证成功状态
                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()
                                # 选择应用
                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()

                                # 设置为马主
                                role = Role.objects.get(id=8)
                                rp = Role_Permission.objects.create(user=user, role=role)
                                rp.save()

                                # 2.创建实名信息
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.user = user
                                ic_model.id_photo_certificated = True
                                ic_model.mobile_certificated = True
                                ic_model.user_photo_certificated = True
                                ic_model.save()

                                # 3.创建骑师信息
                                hm_model = hm_modelform.save(commit=False)
                                hm_model.user = ic_model
                                hm_model.qualified = 1
                                hm_model.save()
                                rs['status'] = 1
                            elif is_update:
                                # 修改用户信息
                                user.username = request.POST['username']
                                user.email = request.POST['email']
                                if request.POST['password'] != None \
                                        and request.POST['password'] != "" \
                                        and len(request.POST['password']) > 8:
                                    user.set_password(request.POST['password'])
                                user.save()

                                # 修改认证信息
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.save()

                                # 修改骑师信息
                                hm_model = hm_modelform.save(commit=False)
                                hm_model.qualified = 1
                                hm_model.save()

                                rs['status'] = 1
                    except BaseException:
                        rs['status'] = -1
                    except Error:
                        rs['status'] = -1
        return JsonResponse(rs)

"""
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":

            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            hm_modelform = HorsemanForm(request.POST, request.FILES)

            context = {
                "horsemanform": hm_modelform,
                "icform": ic_modelform,
                "userform": user_modelform,
                "iscreating": True
            }
            rs['html_form'] = render_to_string("racing/enrollment_admin/render_horseman_enrollment_form.html",
                                               context, request)

            # 先验证
            if user_modelform.is_valid_custom(request):
                if ic_modelform.is_valid():
                    if hm_modelform.is_valid():
                        # 1.创建账号
                        # user_dict=user_modelform.cleaned_data
                        user = User.objects.create_user(username=request.POST['username'],
                                                        email=request.POST['email'],
                                                        password=request.POST['password'])
                        user.save()

                        email_address = EmailAddress(user=user, email=request.POST['email'], verified=1, primary=1)
                        email_address.save()

                        application = Application.objects.get(id=5)
                        ua = UserApplication.objects.create(user=user, application=application)
                        ua.save()

                        # 2.创建实名文件
                        ic_model = ic_modelform.save(commit=False)
                        ic_model.user = user
                        ic_model.id_photo_certificated = True
                        ic_model.mobile_certificated = True
                        ic_model.user_photo_certificated = True
                        ic_model.save()

                        # 3.创建骑师信息
                        hm_model = hm_modelform.save(commit=False)
                        hm_model.user = ic_model
                        hm_model.qualified = 1
                        hm_model.save()

                        rs['status'] = 1

                    else:
                        return JsonResponse(rs)
                else:
                    return JsonResponse(rs)
            else:
                return JsonResponse(rs)
        # 更新
        else:
            pass
"""


# 单位马主注册
@login_required
@permission_required('racing.admin', raise_exception=True)
@transaction.atomic
def enrollment_admin_horsememberowner(request):
    # 处理GET请求
    if request.method == "GET":
        if "id" in request.GET:
            user = User.objects.get(id=request.GET['id'])
            user.password = ""
            userform = UserModelForm(instance=user)
            mcform = MemberCertificationModelForm(instance=user.certification)
            cpform = CorporationModelForm(instance=user.certification.uscc)
            context = {
                "cpform": cpform,
                "mcform": mcform,
                "userform": userform
            }
        else:
            userform = UserModelForm()
            mcform = MemberCertificationModelForm()
            cpform = CorporationModelForm()

            context = {
                "cpform": cpform,
                "mcform": mcform,
                "userform": userform,
                "iscreating": True
            }

        context['html_form'] = render_to_string("racing/enrollment_admin/render_horsememberowner_enrollment_form.html", \
                                                context, request)
        return render(request,
                      "racing/enrollment_admin/horse_memberowner_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            user_modelform = UserModelForm(request.POST)
            mcform = MemberCertificationModelForm(request.POST, request.FILES)
            cpform = CorporationModelForm(request.POST, request.FILES)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            user = User.objects.get(id=request.GET['id'])
            user_modelform = UserModelForm(instance=user, data=request.POST)
            mcform = MemberCertificationModelForm(instance=user.certification \
                                                  , data=request.POST, files=request.FILES)
            cpform = CorporationModelForm(instance=user.certification.uscc \
                                          , data=request.POST, files=request.FILES)
            rs['msg'] = "修改成功！"

        context["cpform"] = cpform
        context["mcform"] = mcform
        context["userform"] = user_modelform

        # 数据验证，然后更新
        if (is_update and not is_create) \
                or user_modelform.is_valid_custom(request):
            if mcform.is_valid():
                if cpform.is_valid():
                    try:
                        with transaction.atomic():
                            if is_create:
                                # 1.创建账号
                                # user_dict=user_modelform.cleaned_data
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()
                                #创建邮箱验证信息
                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()
                                #选择应用信息
                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()
                                # 设置为马主
                                role = Role.objects.get(id=6)
                                rp = Role_Permission.objects.create(user=user, role=role)
                                rp.save()
                                # 2.创建单位信息
                                cp_model = cpform.save(commit=False)
                                cp_model.admin = user
                                cp_model.save()
                                # 3.创建实名文件
                                mc_model = mcform.save(commit=False)
                                mc_model.user = user
                                mc_model.id_photo_certificated = True
                                mc_model.mobile_certificated = True
                                mc_model.user_photo_certificated = True
                                mc_model.corporation_photo_certificate_status = 4
                                mc_model.authorization_photo_certificate_status = 4
                                mc_model.uscc = cp_model
                                mc_model.save()

                                rs['status'] = 1

                            elif is_update:
                                # 修改用户信息
                                user.username = request.POST['username']
                                user.email = request.POST['email']
                                if request.POST['password'] != None \
                                        and request.POST['password'] != "" \
                                        and len(request.POST['password']) > 8:
                                    user.set_password(request.POST['password'])
                                user.save()

                                # 2.创建单位信息
                                cp_model = cpform.save(commit=False)
                                cp_model.save()

                                # 3.创建实名信息
                                mc_model = mcform.save(commit=False)
                                mc_model.save()

                                rs['status'] = 1

                    except BaseException as e:
                        print(e)
                        rs['status'] = -1
                    except Error as e1:
                        print(e1)
                        rs['status'] = -1
        return JsonResponse(rs)

        """
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":

            user_modelform = UserModelForm(request.POST)
            mcform = MemberCertificationModelForm(request.POST, request.FILES)
            cpform = CorporationModelForm(request.POST, request.FILES)

            context = {
                "cpform": cpform,
                "mcform": mcform,
                "userform": user_modelform,
                "iscreating": True
            }
            rs['html_form'] = render_to_string(
                "racing/enrollment_admin/render_horsememberowner_enrollment_form.html", \
                context, request)

            # 先验证
            if user_modelform.is_valid_custom(request):
                if mcform.is_valid():
                    if cpform.is_valid():
                        try:
                            with transaction.atomic():
                                # 1.创建账号
                                # user_dict=user_modelform.cleaned_data
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()

                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()

                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()

                                # 设置为马主
                                role = Role.objects.get(id=6)
                                rp = Role_Permission.objects.create(user=user, role=role)
                                rp.save()

                                # 2.创建单位信息
                                cp_model = cpform.save(commit=False)
                                cp_model.admin = user
                                cp_model.save()

                                # 3.创建实名文件
                                mc_model = mcform.save(commit=False)
                                mc_model.user = user
                                mc_model.id_photo_certificated = True
                                mc_model.mobile_certificated = True
                                mc_model.user_photo_certificated = True
                                mc_model.corporation_photo_certificate_status = 4
                                mc_model.authorization_photo_certificate_status = 4
                                mc_model.uscc = cp_model
                                mc_model.save()
                                rs['status'] = 1
                        except Error:
                            rs['status'] = -1
                            return JsonResponse(rs)
                        except BaseException:
                            rs['status'] = -1
                            return JsonResponse(rs)
                    else:
                        return JsonResponse(rs)
                else:
                    return JsonResponse(rs)
            else:
                return JsonResponse(rs)
        # 更新
        else:
            pass

        return JsonResponse(rs)
"""

# 个人马主注册
@login_required
@permission_required('racing.admin', raise_exception=True)
@transaction.atomic
def enrollment_admin_horseowner(request):
    # 处理GET请求
    if request.method == "GET":
        if "id" in request.GET:
            user = User.objects.get(id=request.GET['id'])
            user.password = ""
            userform = UserModelForm(instance=user)
            icform = IndividualCertificationModelForm(instance=user.certification)
            context = {
                "icform": icform,
                "userform": userform
            }
        else:
            userform = UserModelForm()
            icform = IndividualCertificationModelForm()
            context = {
                "icform": icform,
                "userform": userform,
                "iscreating": True
            }
        context['html_form'] = render_to_string(
            "racing/enrollment_admin/render_horseowner_enrollment_form.html", \
            context, request)
        return render(request,
                      "racing/enrollment_admin/horse_owner_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            user = User.objects.get(id=request.GET['id'])
            user_modelform = UserModelForm(instance=user, data=request.POST)
            ic_modelform = IndividualCertificationModelForm(instance=user.certification, \
                                                            data=request.POST, files=request.FILES)
            rs['msg'] = "修改成功！"

        context["icform"] = ic_modelform
        context["userform"] = user_modelform

        # 数据验证，然后更新
        if (is_update and not is_create) \
                or user_modelform.is_valid_custom(request):
            if ic_modelform.is_valid():
                try:
                    with transaction.atomic():
                        if is_create:
                            # 1.创建账号
                            # user_dict=user_modelform.cleaned_data
                            user = User.objects.create_user(username=request.POST['username'],
                                                            email=request.POST['email'],
                                                            password=request.POST['password'])
                            user.save()

                            email_address = EmailAddress(user=user, email=request.POST['email'], verified=1, primary=1)
                            email_address.save()

                            application = Application.objects.get(id=5)
                            ua = UserApplication.objects.create(user=user, application=application)
                            ua.save()

                            # 设置为个人马主
                            role = Role.objects.get(id=7)
                            rp = Role_Permission.objects.create(user=user, role=role)
                            rp.save()

                            # 2.创建实名文件
                            ic_model = ic_modelform.save(commit=False)
                            ic_model.user = user
                            ic_model.id_photo_certificated = True
                            ic_model.mobile_certificated = True
                            ic_model.user_photo_certificated = True
                            ic_model.save()
                            rs['status'] = 1

                        elif is_update:
                            # 修改用户信息
                            user.username = request.POST['username']
                            user.email = request.POST['email']
                            if request.POST['password'] != None \
                                    and request.POST['password'] != "" \
                                    and len(request.POST['password']) > 8:
                                user.set_password(request.POST['password'])
                            user.save()

                            # 2.创建实名文件
                            ic_model = ic_modelform.save(commit=False)
                            ic_model.save()

                            rs['status'] = 1

                except BaseException:
                    rs['status'] = -1
                except Error:
                    rs['status'] = -1

        return JsonResponse(rs)


"""
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":

            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)

            context = {
                "icform": ic_modelform,
                "userform": user_modelform,
                "iscreating": True
            }
            rs['html_form'] = render_to_string(
                "racing/enrollment_admin/render_horseowner_enrollment_form.html", \
                context, request)

            # 先验证
            if user_modelform.is_valid_custom(request):
                if ic_modelform.is_valid():

                    # 1.创建账号
                    # user_dict=user_modelform.cleaned_data
                    user = User.objects.create_user(username=request.POST['username'],
                                                    email=request.POST['email'],
                                                    password=request.POST['password'])
                    user.save()

                    email_address = EmailAddress(user=user, email=request.POST['email'], verified=1, primary=1)
                    email_address.save()

                    application = Application.objects.get(id=5)
                    ua = UserApplication.objects.create(user=user, application=application)
                    ua.save()

                    # 设置为马主
                    role = Role.objects.get(id=7)
                    rp = Role_Permission.objects.create(user=user, role=role)
                    rp.save()

                    # 2.创建实名文件
                    ic_model = ic_modelform.save(commit=False)
                    ic_model.user = user
                    ic_model.id_photo_certificated = True
                    ic_model.mobile_certificated = True
                    ic_model.user_photo_certificated = True
                    ic_model.save()
                    rs['status'] = 1
                else:
                    return JsonResponse(rs)
            else:
                return JsonResponse(rs)
        # 更新
        else:
            pass

        return JsonResponse(rs)
"""


# 马主列表
@login_required
def enrollment_admin_horseowner_list(request):
    # 搜索时
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        horseowner_p_list = Role_Permission.objects \
            .filter((Q(role_id=6) | Q(role_id=7)) \
                    & (Q(user__certification__identity__contains=request.GET["search_text"]) \
                       | Q(user__certification__mobile__contains=request.GET["search_text"]) \
                       | Q(user__certification__userName__contains=request.GET["search_text"]) \
                       | Q(user__username__contains=request.GET["search_text"])))
        context = {
            "h_p_list": horseowner_p_list,
            "search_text": request.GET["search_text"]
        }
        return render(request, "racing/enrollment_admin/horse_owner_list.html", context)
    # 未搜索时
    else:
        horseowner_p_list = Role_Permission.objects.filter(Q(role_id=6) | Q(role_id=7)).order_by("id")
        context = {
            "h_p_list": horseowner_p_list
        }
        return render(request, "racing/enrollment_admin/horse_owner_list.html", context)


# 马匹注册
@login_required
@permission_required('racing.admin', raise_exception=True)
def enrollment_admin_horse(request):
    context = {}
    # 处理GET请求
    if request.method == "GET":
        if "id" in request.GET:
            horse = Horse.objects.get(id=request.GET['id'])
            horseform = HorseModelForm(instance=horse)
        else:
            horseform = HorseModelForm()
            context['iscreating'] = True

        context['horseform'] = horseform

        context['html_form'] = render_to_string("racing/enrollment_admin/render_horse_enrollment_form.html",
                                                context=context,
                                                request=request)

        return render(request,
                      "racing/enrollment_admin/horse_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            horseform = HorseModelForm(request.POST, request.FILES)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            horse = Horse.objects.get(id=request.GET['id'])
            horseform = HorseModelForm(instance=horse, data=request.POST, files=request.FILES)
            rs['msg'] = "修改成功！"

        context['horseform'] = horseform

        rs['html_form'] = render_to_string("racing/enrollment_admin/render_horse_enrollment_form.html",
                                           context=context,
                                           request=request)
        # 先验证
        if horseform.is_valid():
            h_model = horseform.save(commit=False)
            h_model.qualified = 1
            h_model.save()
            rs['status'] = 1

        return JsonResponse(rs)


"""
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":
            horseform = HorseModelForm(request.POST, request.FILES)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        elif "id" in request.POST:
            horse = Horse.objects.get(id=request.POST['id'])
            horseform = HorseModelForm(request.POST, request.FILES, instance=horse)
            rs['msg'] = "修改成功！"

        context['horseform'] = horseform

        rs['html_form'] = render_to_string("racing/enrollment_admin/render_horse_enrollment_form.html",
                                           context=context,
                                           request=request)
        # 先验证
        if horseform.is_valid():
            h_model = horseform.save(commit=False)
            h_model.qualified = 1
            h_model.save()
            rs['status'] = 1

        return JsonResponse(rs)
"""


# 马匹列表
@login_required
def enrollment_admin_horse_list(request):
    context={}
    # 搜索时
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        horse_list = Horse.objects \
            .filter(Q(user__identity__contains=request.GET["search_text"]) \
                    | Q(user__mobile__contains=request.GET["search_text"]) \
                    | Q(user__userName__contains=request.GET["search_text"]) \
                    | Q(user__user__username__contains=request.GET["search_text"]) \
                    | Q(name__contains=request.GET["search_text"]) \
                    | Q(chip_no__contains=request.GET["search_text"]))
        context['search_text'] = request.GET["search_text"]
    # 未搜索时
    else:
        horse_list = Horse.objects.all().order_by("id")

    horse_list_instance=list(horse_list)
    #horse_list_instance=[]
    #for item in horse_list:
    #    horse_list_instance.append(item)
    horse_list_instance.sort(key=lambda horse:horse.get_all_prize(),reverse=True)

    context['horse_list'] = horse_list_instance
    return render(request, "racing/enrollment_admin/horse_list.html", context)


# 获取马主列表
@login_required
def enrollment_admin_search_horseowner(request):
    rs = {"status": 0, "msg": "未知错误！"}

    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        horseowner_list = Role_Permission.objects \
                              .filter((Q(role_id=6) | Q(role_id=7)) \
                                      & (Q(user__certification__identity__contains=request.GET["search_text"]) \
                                         | Q(user__certification__mobile__contains=request.GET["search_text"]) \
                                         | Q(user__certification__userName__contains=request.GET["search_text"]) \
                                         | Q(user__username__contains=request.GET["search_text"])))[:5]

    if horseowner_list.count() > 0:
        rs["status"] = 1
        horse_list_html = render_to_string("racing/enrollment_admin/horseowner_list_render.html", \
                                           {"horseowner_list": horseowner_list}, request)
        rs["horse_list_html"] = horse_list_html

        horseowner_list_json = []

        for hoitem in horseowner_list:
            horseowner_json = {}
            try:
                hoitem.user.certification
            except Exception:
                Certification.objects.create(user=hoitem.user)
            horseowner_json['name'] = hoitem.user.certification.userName
            horseowner_json['identity'] = hoitem.user.certification.identity
            horseowner_json['mobile'] = hoitem.user.certification.mobile
            horseowner_json['id'] = hoitem.user.certification.user.id
            horseowner_list_json.append(horseowner_json)

        rs["horseowner_list"] = horseowner_list_json
    else:
        horse_list_html = render_to_string("racing/enrollment_admin/horseowner_list_render.html", \
                                           {"horseowner_list": horseowner_list}, request)
        rs["horse_list_html"] = horse_list_html
        rs["status"] = -1

    return JsonResponse(rs)


# 裁判列表
@login_required
def enrollment_admin_referee_list(request):
    # 搜索时
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        referee_list = Referee.objects \
            .filter((Q(user__identity__contains=request.GET["search_text"]) \
                     | Q(user__mobile__contains=request.GET["search_text"]) \
                     | Q(user__userName__contains=request.GET["search_text"]) \
                     | Q(user__user__username__contains=request.GET["search_text"])))
        context = {
            "referee_list": referee_list,
            "search_text": request.GET["search_text"]
        }
        return render(request, "racing/enrollment_admin/referee_list.html", context)
    # 未搜索时
    else:
        referee_list = Referee.objects.all()
        for item in referee_list:
            print(item.referee_qualified)
        context = {
            "referee_list": referee_list
        }
        return render(request, "racing/enrollment_admin/referee_list.html", context)


# 裁判注册
@login_required
@permission_required('racing.admin', raise_exception=True)
@transaction.atomic
def enrollment_admin_referee(request):
    # 处理GET请求
    if request.method == "GET":
        if "id" in request.GET:
            referee = Referee.objects.get(user__user_id=request.GET['id'])
            user = referee.user.user
            user.password = ""
            userform = UserModelForm(instance=user)
            refereeform = RefereeForm(instance=referee)
            icform = IndividualCertificationModelForm(instance=referee.user)
            context = {
                "refereeform": refereeform,
                "icform": icform,
                "userform": userform
            }
        else:
            userform = UserModelForm()
            refereeform = RefereeForm()
            icform = IndividualCertificationModelForm()
            context = {
                "refereeform": refereeform,
                "icform": icform,
                "userform": userform,
                "iscreating": True
            }

        context['html_form'] = render_to_string("racing/enrollment_admin/render_referee_enrollment_form.html",
                                                context=context, request=request)

        return render(request,
                      "racing/enrollment_admin/referee_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            rf_modelform = RefereeForm(request.POST, request.FILES)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            user = User.objects.get(id=request.GET['id'])
            user_modelform = UserModelForm(instance=user, data=request.POST)
            ic_modelform = IndividualCertificationModelForm(instance=user.certification, \
                                                            data=request.POST, files=request.FILES)
            rf_modelform = RefereeForm(instance=user.certification.referee, \
                                       data=request.POST, files=request.FILES)
            rs['msg'] = "修改成功！"

        context["refereeform"] = rf_modelform
        context["icform"] = ic_modelform
        context["userform"] = user_modelform
        rs['html_form'] = render_to_string("racing/enrollment_admin/render_referee_enrollment_form.html",
                                           context=context, request=request)

        # 数据验证，然后更新
        if (is_update and not is_create) \
                or user_modelform.is_valid_custom(request):
            if ic_modelform.is_valid():
                if rf_modelform.is_valid():
                    try:
                        with transaction.atomic():
                            # 直接创建
                            if is_create:
                                # 1.创建账号
                                # user_dict=user_modelform.cleaned_data
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()
                                # 创建邮箱验证信息
                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()

                                # 创建应用信息
                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()

                                # 设置为马主
                                role = Role.objects.get(id=2)
                                rp = Role_Permission.objects.create(user=user, role=role)
                                rp.save()

                                # 2.创建实名文件
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.user = user
                                ic_model.id_photo_certificated = True
                                ic_model.mobile_certificated = True
                                ic_model.user_photo_certificated = True
                                ic_model.save()

                                # 3.创建裁判信息
                                rf_model = rf_modelform.save(commit=False)
                                rf_model.user = ic_model
                                rf_model.referee_qualified = 1
                                rf_model.save()

                                rs['status'] = 1

                            elif is_update:

                                user.username = request.POST['username']
                                user.email = request.POST['email']
                                if request.POST['password'] != None \
                                        and request.POST['password'] != "" \
                                        and len(request.POST['password']) > 8:
                                    user.set_password(request.POST['password'])
                                user.save()

                                # 2.修改实名信息
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.save()

                                # 3.修改裁判信息
                                rf_model = rf_modelform.save(commit=False)
                                rf_model.referee_qualified = 1
                                rf_model.save()

                                rs['status'] = 1

                    except BaseException:
                        rs['status'] = -1
                    except Error:
                        rs['status'] = -1

        return JsonResponse(rs)


"""
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":

            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            rf_modelform = RefereeForm(request.POST, request.FILES)

            context = {
                "refereeform": rf_modelform,
                "icform": ic_modelform,
                "userform": user_modelform,
                "iscreating": True
            }
            rs['html_form'] = render_to_string("racing/enrollment_admin/render_referee_enrollment_form.html",
                                               context=context, request=request)
            # 先验证
            if user_modelform.is_valid_custom(request):
                if ic_modelform.is_valid():
                    if rf_modelform.is_valid():
                        try:
                            with transaction.atomic():
                                # 1.创建账号
                                # user_dict=user_modelform.cleaned_data
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()

                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()

                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()

                                # 2.创建实名文件
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.user = user
                                ic_model.id_photo_certificated = True
                                ic_model.mobile_certificated = True
                                ic_model.user_photo_certificated = True
                                ic_model.save()

                                # 3.创建裁判信息
                                hm_model = rf_modelform.save(commit=False)
                                hm_model.user = ic_model
                                hm_model.qualified = 1
                                hm_model.save()

                            rs['status'] = 1

                        except Error:
                            rs['status'] = -1
                            return JsonResponse(rs)
                        except BaseException:
                            rs['status'] = -1
                            return JsonResponse(rs)

                    else:
                        return JsonResponse(rs)
                else:
                    return JsonResponse(rs)
            else:
                return JsonResponse(rs)
        # 更新
        else:
            pass

        return JsonResponse(rs)
"""


# 队伍列表
@login_required
def enrollment_admin_member_list(request):
    # 搜索时
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":

        search_text=str(request.GET["search_text"]).replace(" ","")

        member_list = Member.objects \
            .filter(Q(user__identity__contains=search_text) \
                    | Q(user__mobile__contains=search_text) \
                    | Q(user__userName__contains=search_text) \
                    | Q(user__user__username__contains=search_text) \
                    | Q(user__uscc__name__contains=search_text)
                    | Q(member_name__contains=search_text))
        context = {
            "member_list": member_list,
            "search_text": request.GET["search_text"]
        }
        return render(request, "racing/enrollment_admin/member_list.html", context)
    # 未搜索时
    else:
        member_list = Member.objects.all()
        context = {
            "member_list": member_list
        }
        return render(request, "racing/enrollment_admin/member_list.html", context)


# 个人会员注册
@login_required
@permission_required('racing.admin', raise_exception=True)
@transaction.atomic
def enrollment_admin_indivdualmember(request):
    # 处理GET请求
    if request.method == "GET":
        if "id" in request.GET:
            user = User.objects.get(id=request.GET['id'])
            user.password = ""
            userform = UserModelForm(instance=user)
            icform = IndividualCertificationModelForm(instance=user.certification)
            memberform = MemberForm(initial={"member_type": 1}, instance=user.certification.member)

            context = {
                "icform": icform,
                "userform": userform,
                "memberform": memberform
            }
        else:
            userform = UserModelForm()
            icform = IndividualCertificationModelForm()
            memberform = MemberForm(initial={"member_type": 1})

            context = {
                "icform": icform,
                "userform": userform,
                "memberform": memberform,
                "iscreating": True
            }

        context['html_form'] = render_to_string("racing/enrollment_admin/render_individual_member_enrollment_form.html",
                                                context, request)
        return render(request,
                      "racing/enrollment_admin/individual_member_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            memberform = MemberForm(request.POST)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            user = User.objects.get(id=request.GET['id'])
            user_modelform = UserModelForm(instance=user,data=request.POST)
            ic_modelform = IndividualCertificationModelForm(instance=user.certification\
                                                            ,data=request.POST, files=request.FILES)
            memberform = MemberForm(instance=user.certification.member,data=request.POST)
            rs['msg'] = "修改成功！"

        context["memberform"] = memberform
        context["icform"] = ic_modelform
        context["userform"] = user_modelform

        rs['html_form'] = render_to_string(
            "racing/enrollment_admin/render_individual_member_enrollment_form.html",
            context, request)

        #先验证数据
        if (is_update and not is_create) \
                or user_modelform.is_valid_custom(request):
            if ic_modelform.is_valid():
                if memberform.is_valid():
                    try:
                        with transaction.atomic():
                            if is_create:
                                # 1.创建账号
                                # user_dict=user_modelform.cleaned_data
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()

                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()

                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()

                                # 设置为个人参赛队伍
                                role = Role.objects.get(id=5)
                                rp = Role_Permission.objects.create(user=user, role=role)
                                rp.save()

                                # 2.创建实名文件
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.user = user
                                ic_model.id_photo_certificated = True
                                ic_model.mobile_certificated = True
                                ic_model.user_photo_certificated = True
                                ic_model.save()

                                # 3.创建队伍信息
                                member = memberform.save(commit=False)
                                member.user = ic_model
                                member.qualified = 1
                                member.save()

                                rs['status'] = 1
                            elif is_update:
                                # 修改用户信息
                                user.username = request.POST['username']
                                user.email = request.POST['email']
                                if request.POST['password'] != None \
                                        and request.POST['password'] != "" \
                                        and len(request.POST['password']) > 8:
                                    user.set_password(request.POST['password'])
                                user.save()

                                # 2.创建实名文件
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.save()

                                # 3.创建队伍信息
                                member = memberform.save(commit=False)
                                member.qualified = 1
                                member.save()
                                rs['status'] = 1

                    except BaseException:
                        rs['status'] = -1
                    except Error:
                        rs['status'] = -1
        return JsonResponse(rs)

        """
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":

            user_modelform = UserModelForm(request.POST)
            ic_modelform = IndividualCertificationModelForm(request.POST, request.FILES)
            memberform = MemberForm(request.POST)

            context = {
                "icform": ic_modelform,
                "userform": user_modelform,
                "memberform": memberform,
                "iscreating": True
            }

            rs['html_form'] = render_to_string(
                "racing/enrollment_admin/render_individual_member_enrollment_form.html",
                context, request)

            # 先验证
            if user_modelform.is_valid_custom(request):
                if ic_modelform.is_valid():
                    if memberform.is_valid():
                        try:
                            with transaction.atomic():
                                # 1.创建账号
                                # user_dict=user_modelform.cleaned_data
                                user = User.objects.create_user(username=request.POST['username'],
                                                                email=request.POST['email'],
                                                                password=request.POST['password'])
                                user.save()

                                email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                             primary=1)
                                email_address.save()

                                application = Application.objects.get(id=5)
                                ua = UserApplication.objects.create(user=user, application=application)
                                ua.save()

                                # 设置为个人参赛队伍
                                role = Role.objects.get(id=5)
                                rp = Role_Permission.objects.create(user=user, role=role)
                                rp.save()

                                # 2.创建实名文件
                                ic_model = ic_modelform.save(commit=False)
                                ic_model.user = user
                                ic_model.id_photo_certificated = True
                                ic_model.mobile_certificated = True
                                ic_model.user_photo_certificated = True
                                ic_model.save()

                                # 3.创建队伍信息
                                member = memberform.save(commit=False)
                                member.user = ic_model
                                member.qualified = 1
                                member.save()

                                rs['status'] = 1

                        except Error:
                            rs['status'] = -1
                            return JsonResponse(rs)
                        except BaseException:
                            rs['status'] = -1
                            return JsonResponse(rs)
                    else:
                        return JsonResponse(rs)
                else:
                    return JsonResponse(rs)
            else:
                return JsonResponse(rs)
        # 更新
        else:
            pass
        return JsonResponse(rs)
"""


# 组织会员注册
@login_required
@transaction.atomic
def enrollment_admin_organizationmember(request):
    # 处理GET请求
    if request.method == "GET":
        if "id" in request.GET:
            user = User.objects.get(id=request.GET['id'])
            user.password = ""
            userform = UserModelForm(instance=user)
            mcform = MemberCertificationModelForm(instance=user.certification)
            cpform = CorporationModelForm(instance=user.certification.uscc)
            memberform = MemberForm(initial={"member_type": 2}, instance=user.certification.member)

            context = {
                "cpform": cpform,
                "mcform": mcform,
                "userform": userform,
                "memberform": memberform
            }
        else:
            userform = UserModelForm()
            mcform = MemberCertificationModelForm()
            cpform = CorporationModelForm()
            memberform = MemberForm(initial={"member_type": 2})

            context = {
                "cpform": cpform,
                "mcform": mcform,
                "userform": userform,
                "memberform": memberform,
                "iscreating": True
            }

        context['html_form'] = render_to_string(
            "racing/enrollment_admin/render_organization_member_enrollment_form.html",
            context,
            request)

        return render(request,
                      "racing/enrollment_admin/organization_member_enrollment.html",
                      context)
    # 处理POST请求
    elif request.method == "POST":
        rs = {"status": 0, "msg": ""}
        context = {}
        # 是否正在创建
        is_create = "iscreating" in request.POST and request.POST["iscreating"] == "on"
        # 正在更新
        is_update = "id" in request.GET and request.GET['id'] != ""
        # 正在新建
        if is_create:
            user_modelform = UserModelForm(request.POST)
            mcform = MemberCertificationModelForm(request.POST, request.FILES)
            cpform = CorporationModelForm(request.POST, request.FILES)
            memberform = MemberForm(request.POST)
            context['iscreating'] = True
            rs['msg'] = "创建成功！"
        # 修改信息
        elif is_update:
            user = User.objects.get(id=request.GET['id'])
            user_modelform = UserModelForm(instance=user,data=request.POST)
            mcform = MemberCertificationModelForm(instance=user.certification\
                                                  ,data=request.POST, files=request.FILES)
            cpform = CorporationModelForm(instance=user.certification.uscc\
                                          ,data=request.POST, files=request.FILES)
            memberform = MemberForm(instance=user.certification.member,data=request.POST)
            rs['msg'] = "修改成功！"

        context["cpform"] = cpform
        context["mcform"] = mcform
        context["memberform"] = memberform
        context["userform"] = user_modelform

        rs['html_form'] = render_to_string(
            "racing/enrollment_admin/render_organization_member_enrollment_form.html",
            context,
            request)

        if (is_update and not is_create) \
                or user_modelform.is_valid_custom(request):
            if cpform.is_valid():
                if mcform.is_valid():
                    if memberform.is_valid():
                        try:
                            with transaction.atomic():
                                if is_create:
                                    # 1.创建账号
                                    # user_dict=user_modelform.cleaned_data
                                    user = User.objects.create_user(username=request.POST['username'],
                                                                    email=request.POST['email'],
                                                                    password=request.POST['password'])
                                    user.save()

                                    email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                                 primary=1)
                                    email_address.save()

                                    application = Application.objects.get(id=5)
                                    ua = UserApplication.objects.create(user=user, application=application)
                                    ua.save()

                                    # 设置为单位会员
                                    role = Role.objects.get(id=4)
                                    rp = Role_Permission.objects.create(user=user, role=role)
                                    rp.save()

                                    # 2.创建单位信息
                                    cp_model = cpform.save(commit=False)
                                    cp_model.admin = user
                                    cp_model.save()

                                    # 3.创建实名文件
                                    mc_model = mcform.save(commit=False)
                                    mc_model.user = user
                                    mc_model.id_photo_certificated = True
                                    mc_model.mobile_certificated = True
                                    mc_model.user_photo_certificated = True
                                    mc_model.corporation_photo_certificate_status = 4
                                    mc_model.authorization_photo_certificate_status = 4
                                    mc_model.uscc = cp_model
                                    mc_model.save()

                                    # 4.创建队伍信息
                                    member = memberform.save(commit=False)
                                    member.user = mc_model
                                    member.qualified = 1
                                    member.save()

                                    rs['status'] = 1
                                elif is_update:
                                    # 修改用户信息
                                    user.username = request.POST['username']
                                    user.email = request.POST['email']
                                    if request.POST['password'] != None \
                                            and request.POST['password'] != "" \
                                            and len(request.POST['password']) > 8:
                                        user.set_password(request.POST['password'])
                                    user.save()

                                    # 2.创建单位信息
                                    cp_model = cpform.save(commit=False)
                                    cp_model.save()

                                    # 3.创建实名文件
                                    mc_model = mcform.save(commit=False)
                                    mc_model.save()

                                    # 4.创建队伍信息
                                    member = memberform.save(commit=False)
                                    member.qualified = 1
                                    member.save()

                                    rs['status'] = 1

                        except BaseException:
                            rs['status'] = -1
                        except Error:
                            rs['status'] = -1
        return JsonResponse(rs)

        """
        # 正在新建
        if "iscreating" in request.POST \
                and request.POST["iscreating"] == "on":

            user_modelform = UserModelForm(request.POST)
            mcform = MemberCertificationModelForm(request.POST, request.FILES)
            cpform = CorporationModelForm(request.POST, request.FILES)
            memberform = MemberForm(request.POST)

            context = {
                "cpform": cpform,
                "mcform": mcform,
                "userform": user_modelform,
                "memberform": memberform,
                "iscreating": True
            }

            rs['html_form'] = render_to_string(
                "racing/enrollment_admin/render_organization_member_enrollment_form.html",
                context,
                request)

            # 先验证
            if user_modelform.is_valid_custom(request):
                if mcform.is_valid():
                    if cpform.is_valid():
                        if memberform.is_valid():
                            try:
                                with transaction.atomic():
                                    # 1.创建账号
                                    # user_dict=user_modelform.cleaned_data
                                    user = User.objects.create_user(username=request.POST['username'],
                                                                    email=request.POST['email'],
                                                                    password=request.POST['password'])
                                    user.save()

                                    email_address = EmailAddress(user=user, email=request.POST['email'], verified=1,
                                                                 primary=1)
                                    email_address.save()

                                    application = Application.objects.get(id=5)
                                    ua = UserApplication.objects.create(user=user, application=application)
                                    ua.save()

                                    # 设置为马主
                                    role = Role.objects.get(id=4)
                                    rp = Role_Permission.objects.create(user=user, role=role)
                                    rp.save()

                                    # 2.创建单位信息
                                    cp_model = cpform.save(commit=False)
                                    cp_model.admin = user
                                    cp_model.save()

                                    # 3.创建实名文件
                                    mc_model = mcform.save(commit=False)
                                    mc_model.user = user
                                    mc_model.id_photo_certificated = True
                                    mc_model.mobile_certificated = True
                                    mc_model.user_photo_certificated = True
                                    mc_model.corporation_photo_certificate_status = 4
                                    mc_model.authorization_photo_certificate_status = 4
                                    mc_model.uscc = cp_model
                                    mc_model.save()

                                    # 4.创建队伍信息
                                    member = memberform.save(commit=False)
                                    member.user = mc_model
                                    member.qualified = 1
                                    member.save()

                                    rs['status'] = 1

                            except Error:
                                print("错误1")
                                return JsonResponse(rs)
                            except BaseException:
                                print("错误2")
                                return JsonResponse(rs)
                        else:
                            rs["msg"] = ""
                            print(memberform.errors)
                            return JsonResponse(rs)
                    else:
                        rs["msg"] = ""
                        print(cpform.errors)
                        return JsonResponse(rs)
                else:
                    rs["msg"] = ""
                    print(mcform.errors.as_json())
                    return JsonResponse(rs)
            else:
                rs["msg"] = ""
                print(user_modelform.errors)
                print(user_modelform.errors.as_json())
                return JsonResponse(rs)
        # 更新
        else:
            pass

        return JsonResponse(rs)
"""


# 参赛报名
@login_required
def enrollment_admin_member_game(request, member_id):
    #访问参赛报名页
    if request.method == "GET":

        regulation = Regulation.objects.get(active=1)
        member = Member.objects.get(user__user_id=member_id)
        try:
            enrollment = Enrollment.objects.get(year=regulation, member=member)
        except Enrollment.DoesNotExist:
            enrollment = Enrollment.objects.create(year=regulation, member=member)
        enrolldetail_list = EnrollmentDetail.objects.filter(enrollment=enrollment)

        context = {
            "member": member,
            "enrollment": enrollment,
            "enrolldetail_list": enrolldetail_list
        }

        return render(request, "racing/enrollment_admin/member_game_enrollment.html", context)
    #处理参赛报名请求
    elif request.method == "POST":
        rs = {"status": 0}
        ed_form = EnrollmentDetailModelForm(request.POST)
        member = Member.objects.get(user__user_id=member_id)
        if ed_form.is_valid() and member.qualified == 1:
            ed_form.save()
            rs['status'] = 1
        elif member.qualified == -1:
            rs['status']=-1
            rs['html_form']="您的信息还没有审核通过，不能进行参赛报名！"
        else:
            regulation = Regulation.objects.get(active=1)
            enrollment = Enrollment.objects.get(year=regulation, member=member)
            html_form = render_to_string("racing/enrollment_admin/member_game_create.html", \
                                         {
                                             "ed_modelform": ed_form,
                                             "enrollment": enrollment
                                         }, request)

            rs['html_form'] = html_form
        return JsonResponse(rs)


# 获取参赛表单
@login_required
def enrollment_admin_get_enrollment_html(request, enrollment_id):
    rs = {"status": 0}

    enrollment_list = Enrollment.objects.filter(id=enrollment_id)
    if enrollment_list[0].fare_paid == 1:
        rs['html_form'] = "<h3 style=\"color:red\">已经缴费确认，无法继续报名！</h3>"
        rs['status'] = 1
        return JsonResponse(rs)

    ed_modelform = EnrollmentDetailModelForm(initial={"enrollment": enrollment_list[0]})

    html_form = render_to_string("racing/enrollment_admin/member_game_create.html", \
                                 {
                                     "ed_modelform": ed_modelform,
                                     "enrollment": enrollment_list[0]
                                 }, request)

    rs['html_form'] = html_form
    rs['status'] = 1
    return JsonResponse(rs)


# 搜索马匹
@login_required
def enrollment_admin_search_horse(request):
    rs = {"status": 0, "msg": "未知错误！"}
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        horse_list = Horse.objects.filter((Q(qualified=1)) \
                                          & (Q(name__contains=request.GET["search_text"]) \
                                             | Q(chip_no__contains=request.GET["search_text"]) \
                                             | Q(brand__contains=request.GET["search_text"]) \
                                             | Q(passport_no__contains=request.GET["search_text"])))[:5]
    if horse_list.count() > 0:
        rs["status"] = 1
        horse_list_html = render_to_string("racing/enrollment_admin/horse_list_render.html", \
                                           {"horse_list": horse_list}, request)
        rs["horse_list_html"] = horse_list_html

        horse_list_json = []
        for hitem in horse_list:
            horse_json = {}
            horse_json['horse_name'] = hitem.name
            horse_json['chip_no'] = hitem.chip_no
            horse_json['brand'] = hitem.brand
            horse_json['horseowner_name'] = hitem.user.userName
            horse_json['horseowner_identity'] = hitem.user.identity
            horse_json['id'] = hitem.id
            horse_list_json.append(horse_json)

        rs["horse_list"] = horse_list_json

    else:
        horse_list_html = render_to_string("racing/enrollment_admin/horse_list_render.html", \
                                           {"horse_list": horse_list}, request)
        rs["horse_list_html"] = horse_list_html
        rs["status"] = -1

    return JsonResponse(rs)

# 搜索骑师
@login_required
def enrollment_admin_search_horseman(request):
    rs = {"status": 0, "msg": "未知错误！"}
    if "search_text" in request.GET \
            and request.GET["search_text"] != "":
        horseman_list = Horseman.objects.filter((Q(qualified=1)) \
                                                & (Q(user__userName__contains=request.GET["search_text"]) \
                                                   | Q(user__identity__contains=request.GET["search_text"]) \
                                                   | Q(user__mobile__contains=request.GET["search_text"])))[:5]
    if horseman_list.count() > 0:
        rs["status"] = 1
        horseman_list_html = render_to_string("racing/enrollment_admin/horseman_list_render.html", \
                                              {"horseman_list": horseman_list}, request)
        rs["horseman_list_html"] = horseman_list_html

        horseman_list_json = []

        for hmitem in horseman_list:
            horseman_json = {}
            horseman_json['name'] = hmitem.user.userName
            horseman_json['identity'] = hmitem.user.identity
            horseman_json['mobile'] = hmitem.user.mobile
            horseman_json['id'] = hmitem.user.user.id
            horseman_list_json.append(horseman_json)

        rs["horseman_list"] = horseman_list_json

    else:
        horseman_list_html = render_to_string("racing/enrollment_admin/horseman_list_render.html", \
                                              {"horseman_list": horseman_list}, request)
        rs["horseman_list_html"] = horseman_list_html
        rs["status"] = -1

    return JsonResponse(rs)


# 缴费确认
@require_http_methods(["POST"])
@login_required
@permission_required('racing.admin', raise_exception=True)
def payment_lock(request,e_id):
    rs = {"status": 0, "msg": "请求失败!"}
    enrollment=Enrollment.objects.get(id=e_id)

    for item in enrollment.enrollmentdetail_set.all():
        if EnrollmentDetail.objects.filter(game=item.game,\
                                           horse=item.horse,enrollment__fare_paid=1).count()>0:
            rs['msg'] = "马匹\""+item.horse.name+"\"已经在比赛\""+item.game.name+"\"中被其他队伍使用了！"
            return JsonResponse(rs)
        if EnrollmentDetail.objects.filter(game=item.game,\
                                           horseman=item.horseman,enrollment__fare_paid=1).count()>0:
            rs['msg'] = "骑师\""+item.horseman.user.userName+"\"已经在比赛\""+item.game.name+"\"中被其他队伍使用了！"
            return JsonResponse(rs)

    if enrollment.year.active == 1 and\
                    enrollment.fare > 0 and \
                    enrollment.member.qualified == 1:
        enrollment.enrollment_fixed=1
        enrollment.qualified=1
        enrollment.fare_paid=1
        enrollment.save()
        rs['status'] = 1
    elif enrollment.fare <= 0 or \
         enrollment.discipline_number <= 0:
        rs['msg']="请先进行参赛报名!"

    return JsonResponse(rs)

# 缴费返还
@require_http_methods(["POST"])
@login_required
@permission_required('racing.admin', raise_exception=True)
def payment_return(request,e_id):
    rs = {"status": 0, "msg": "请求失败!"}
    enrollment=Enrollment.objects.get(id=e_id)

    #只要有报名的比赛中有比赛已开赛，就不能退费
    for item in enrollment.enrollmentdetail_set.all():
        if item.game.game_status != 1:
            rs['msg']=str(item.game)+"已开赛，无法退费！"
            return JsonResponse(rs)

    if enrollment.year.active == 1 and\
        enrollment.fare_paid == 1:
        enrollment.enrollment_fixed=0
        enrollment.qualified=0
        enrollment.fare_paid=0
        enrollment.save()
        rs['status'] = 1

    return JsonResponse(rs)

#删除报名信息
@require_http_methods(["POST"])
@login_required
@permission_required('racing.admin', raise_exception=True)
def delete_enrollment_detail(request,ed_id):
    rs={"status":0,"msg":"请求失败！"}

    #获取单个报名细节，
    ed=EnrollmentDetail.objects.get(id=ed_id)

    #当前处于激活赛季，并且没有支付参赛费，才能删除！
    if ed.game.year.active == 1 \
            and ed.enrollment.fare_paid != 1:
        ed.delete()
        rs['status']=1
    elif ed.enrollment.fare_paid == 1:
        rs['msg']="已确认缴费，无法删除！"

    return JsonResponse(rs)
#redirect(reverse('commons.views.invoice_return_index', args=[]))
#return HttpResponseRedirect('/commons/invoice_return/index/?message=error')
#return redirect(reverse('commons.views.invoice_return_index', args=[]))


# 角色权限更改
@login_required
@permission_required('racing.admin', raise_exception=True)
def enrollment_admin_permission_change(request, user_id):
    rs = {"status": 0}
    context = {"user_id": user_id}

    # 获取角色列表,去掉裁判员(id=2)
    role_list = Role.objects.all().exclude(id=1).exclude(id=3).exclude(id=2)
    user = User.objects.get(id=user_id)
    user_list = User.objects.filter(id=user_id)

    # 获取当前用户的所有权限
    user_permission_list = User_Permission.objects.filter(user=user)
    role_id_list = []

    # 获取当前用户的所有权限的id
    # 在用户角色中排除掉当前已选角色
    for item in user_permission_list:
        role_id_list.append(item.role.id)
        role_list = role_list.exclude(id=item.role.id)

    # 当前用户没有选择单位队伍或单位马主,就排除掉单位角色
    if 4 not in role_id_list and \
                    6 not in role_id_list:
        role_list = role_list.exclude(id=4).exclude(id=6)
    else:
        role_list = role_list.exclude(id=5).exclude(id=7)

    # GET请求
    if request.method == "GET":
        userrole_modelform = PermissionModelForm(initial={"user": user}, \
                                                 role_query_set=role_list, \
                                                 user_query_set=user_list)

        context['userrole_modelform'] = userrole_modelform
        context['user_permission_list'] = user_permission_list

        # for item in user_permission_list:
        #    print(item.role.name)

        rs['html_form'] = render_to_string("racing/enrollment_admin/render_userrole_change.html", \
                                           context, request)

        return JsonResponse(rs)

    # POST请求
    elif request.method == "POST":
        userrole_modelform = PermissionModelForm(initial={"user": user}, \
                                                 role_query_set=role_list, \
                                                 user_query_set=user_list, data=request.POST)
        # 用户请求信息验证
        if userrole_modelform.is_valid():
            # 裁判
            if userrole_modelform.cleaned_data['role'].id == 2:
                referee = Referee.objects.create(user=user.certification, referee_qualified=0)
                referee.save()
                userrole_modelform.save()
                rs['status'] = 1
            # 单位队伍
            elif userrole_modelform.cleaned_data['role'].id == 4:
                member = Member.objects.create(user=user.certification,member_name=str(time.time()), member_type=2, qualified=0)
                member.save()
                userrole_modelform.save()
                print("你好，单位会员")
                rs['status'] = 1
            # 个人队伍
            elif userrole_modelform.cleaned_data['role'].id == 5:
                member = Member.objects.create(user=user.certification,member_name=str(time.time()), member_type=1, qualified=0)
                member.save()
                userrole_modelform.save()
                print("你好，个人会员")
                rs['status'] = 1
            # 骑师
            elif userrole_modelform.cleaned_data['role'].id == 8:
                try:
                    Horseman.objects.get(user=user.certification)
                except Horseman.DoesNotExist:
                    horseman = Horseman.objects.create(user=user.certification, qualified=0, \
                                                   health_certificate_photo=None, \
                                                   weight=0, \
                                                   height=0, \
                                                   horseman_photo=None, \
                                                   employer="")
                    horseman.save()
                userrole_modelform.save()
                rs['status'] = 1
            # 其他
            elif userrole_modelform.cleaned_data['role'].id in [6, 7]:
                userrole_modelform.save()
                rs['status'] = 1
        else:
            context['userrole_modelform'] = userrole_modelform
            context['user_permission_list'] = user_permission_list
            rs['html_form'] = render_to_string("racing/enrollment_admin/render_userrole_change.html", \
                                               context, request)
        return JsonResponse(rs)
