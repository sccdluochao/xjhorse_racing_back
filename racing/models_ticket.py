import datetime
import os
import uuid

from django.core.files.storage import FileSystemStorage
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from racing.models_enrollment import GameDay
from certification.models import Certification
from racing.models import Charge


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 重复上传照片时，用新照片覆盖老照片
class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return super(OverwriteStorage, self).get_available_name(name, max_length)


# 骑师蓝底免冠照片
def upload_ticket_image(instance, filename):
    return "racing/ticket/%s" % filename


# 门票类别
TICKET_TYPE = (
    (1, _("Adult Ticket")),  # 成人门票
    (2, _("Children Ticket")),  # 儿童门票
)


# 门票产品
class Product(models.Model):
    name = models.CharField(  # 门票名称
        max_length=200,
        null=False,
        blank=False,
        default='天马赛事门票',
        verbose_name=_("Ticket Name")
    )
    image = models.ImageField(  # 门票图片
        upload_to=upload_ticket_image,
        null=False,
        blank=False,
        verbose_name=_("Ticket Image")
    )
    description = models.TextField(
        blank=True,
        default='当日有效，仅限壹人！',
        verbose_name=_("Ticket Description")
    )
    game_day = models.ForeignKey(  # 比赛日
        GameDay,
        null=False,
        blank=False,
        verbose_name=_("Game Day")
    )
    ticket_type = models.IntegerField(  # 门票类别
        choices=TICKET_TYPE,
        default=1,
        verbose_name=_("Ticket Type")
    )
    price = models.DecimalField(  # 单价
        max_digits=10,
        decimal_places=2,
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Ticket Price")
    )
    stock = models.PositiveIntegerField(  # 库存数量
        null=False,
        blank=False,
        default=10000,
        verbose_name=_("Ticket Stock")
    )
    sold = models.PositiveIntegerField(  # 售出数量
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Ticket Sold")
    )
    available = models.BooleanField(  # 是否允许出售
        default=True,
        verbose_name=_("Allow Sell")
    )

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('ticket_detail', kwargs={'pk': self.id})


# 门票订单
class Order(models.Model):
    user = models.ForeignKey(  # 用户
        Certification,
        null=False,
        blank=False,
        verbose_name=_("UserID")
    )
    created = models.DateField(  # 订单创建时间
        null=False,
        blank=False,
        auto_now_add=True,
        verbose_name=_("Order Created")
    )
    paid = models.BooleanField(  # 是否支付
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Paid")
    )

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')
        ordering = ('-created',)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.orderdetail_set.all())

    def get_unpaid_items(self):
        return sum(item.quantity for item in self.orderdetail_set.filter(order__paid=False))

    def get_paid_items(self):
        return sum(item.quantity for item in self.orderdetail_set.filter(order__paid=True))

    def get_adult_unpaid_items(self):
        return sum(item.quantity for item in self.orderdetail_set.filter(order__paid=False, product__ticket_type=1))

    def get_children_unpaid_items(self):
        return sum(item.quantity for item in self.orderdetail_set.filter(order__paid=False, product__ticket_type=2))

    def __str__(self):
        return _('Ticket Order {}').format(self.id)


# 门票订单明细
class OrderDetail(models.Model):
    order = models.ForeignKey(  # 订单ID
        Order,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("Ticket Order")
    )
    product = models.ForeignKey(
        Product,
        related_name='order_detail',
        null=False,
        blank=False,
        verbose_name=_("Product")
    )
    price = models.DecimalField(
        max_digits=10,
        decimal_places=2
    )
    quantity = models.SmallIntegerField(  # 数量
        null=False,
        blank=False,
        default=1,
        verbose_name=_("Quantity")
    )

    isused = models.BooleanField(
        default=0,
        verbose_name=_("Is Used")
    )

    class Meta:
        verbose_name = _('Order Detail')
        verbose_name_plural = _('Order Details')

    def __str__(self):
        return _('Order Detail{}').format(self.id)

    def get_cost(self):
        return self.price * self.quantity

#暂时电子票
class TemporaryTicket(models.Model):
    user = models.ForeignKey(  # 用户
        Certification,
        null=False,
        blank=False,
        verbose_name=_("UserID")
    )
    ticketcode = models.CharField(
        max_length=30,
        verbose_name=_("Ticket Code"),
        unique=True
    )
    starttime = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Start Time")
    )
    endtime = models.DateTimeField(
        verbose_name=_("End Time")
    )

    def save(self, *args, **kwargs):
        # 自动设置二维码有效时间
        if not self.id:
            self.endtime = timezone.now()+datetime.timedelta(minutes=20)
        return super(TemporaryTicket, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Temporary Ticket')
        verbose_name_plural = _('Temporary Ticket')

# 支付宝交易
class AliPay(models.Model):
    user = models.ForeignKey(
        Certification,
        null=False,
        blank=False,
        verbose_name=_("UserID")
    )
    id = models.UUIDField(  # 支付单唯一标识符
        primary_key=True,
        default=uuid.uuid1,  # 由 MAC 地址（主机物理地址）、当前时间戳、随机数生成
        editable=False,
        verbose_name=_("Payment ID")
    )
    subject = models.CharField(  # 支付单标题
        max_length=256,
        null=False,
        blank=False,
        default='天马赛事门票',
        verbose_name=_("Payment Subject")
    )
    total_item = models.PositiveIntegerField(  # 总票数
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Payment Item")
    )
    total_amount = models.DecimalField(  # 支付单总金额
        max_digits=10,
        decimal_places=2,
        blank=False,
        null=False,
        default=0.00,
        verbose_name=_("Payment Amount")
    )
    created = models.DateTimeField(  # 记录创建时间
        auto_now_add=True, # 自动维护创建时间
        verbose_name=_("Payment Create Time")
    )
    paid = models.BooleanField(  # 是否完成支付
        default=False,
        verbose_name=_("Paid")
    )
    paid_time = models.DateTimeField(
        auto_now=True,   # 自动维护更新时间
        verbose_name=_("Paid Time")
    )

    class Meta:
        verbose_name = _('AliPay')
        verbose_name_plural = _('AliPay')

    def __str__(self):
        return self.subject

    def get_orders(self):
        return  OrderDetail.objects.filter(order__alipayorder__pay=self)


# 支付宝交易明细
class AliPayOrder(models.Model):
    pay = models.ForeignKey(
        AliPay,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("AliPay")
    )
    order = models.ForeignKey(
        Order,
        null=False,
        blank=False,
        verbose_name=_("Ticket Order")
    )
