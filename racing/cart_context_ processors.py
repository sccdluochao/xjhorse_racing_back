from django.shortcuts import get_object_or_404

from racing.cart import Cart
from racing.models_ticket import Order, AliPay, AliPayOrder
from certification.models import Certification
from rules import has_perm


# Context processors, 请求的环境变量处理
def cart(request):
    return {'cart': Cart(request)}


def order(request):
    if request.user.is_authenticated():
        if True:
                #request.user.has_perm('racing.paid_attendance'):
            try:
                certification = Certification.objects.get(user=request.user)
                # 计算为转入结算状态的门票数量
                payment_order_id_list = AliPayOrder.objects.filter(pay__user=certification).values_list(
                    'order_id', flat=True)
                orders = Order.objects.filter(user=certification).exclude(id__in=payment_order_id_list)
                unchecked_items = sum(order.get_unpaid_items() for order in orders)

                # 计算已经转入结算状态当未完成支付的门票数量
                alipays = AliPay.objects.filter(user=certification, paid=False)
                unpaid_items = sum(alipay.total_item for alipay in alipays)

                # 计算已经完成支付的门票数量
                alipays = AliPay.objects.filter(user=certification, paid=True)
                paid_items = sum(alipay.total_item for alipay in alipays)
                return {'unchecked_items': unchecked_items, 'unpaid_items': unpaid_items, 'paid_items': paid_items}
            except Certification.DoesNotExist:
                return {'unchecked_items': 0, 'unpaid_items': 0, 'paid_items': 0}
        else:
            return {'unchecked_items': 0, 'unpaid_items': 0, 'paid_items': 0}
    else:
        return {'unchecked_items': 0, 'unpaid_items': 0, 'paid_items': 0}
