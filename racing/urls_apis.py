from django.conf.urls import url
from racing import view_apis

urlpatterns = [
    url(r'^wx_authentication/$', view_apis.wx_authentication, name='wx_authentication'),
]