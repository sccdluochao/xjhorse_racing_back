# -------------------------------------------------------------------------------
# File:         views_honorary_wall.py
# Description:  名誉墙
# Author:       loseboy
# Email:        loseboycn@qq.com
# Date:         2018/10/16 1:26 PM
# Software:     PyCharm
# -------------------------------------------------------------------------------

import json
import datetime

from django.shortcuts import render
from django.views.generic.base import View
from django.http import HttpResponse
from django.db.models import Q


from .models_enrollment import Horseman, Horse, Certification, EnrollmentDetail
    

class HonoraryWallView(View):
    """
    名誉墙页面
    """
    def get(self, request):
        all_horses = Horse.objects.all()[:5]
        return render(request, 'racing/honorary_wall/honorary_wall.html', {
            'all_horses': all_horses,
        })


class SearchView(View):
    """
    异步AJAX查询数据：赛马、骑师
    """
    def post(self, request):
        all_result = []
        type = request.POST.get('type')
        keywords = request.POST.get('keywords')
        if type == '0':
            all_data = Horse.objects.filter(
                Q(name__icontains=keywords) | Q(user__userName__icontains=keywords) |
                Q(breed__icontains=keywords) | Q(brand__icontains=keywords)
            )
            for data in all_data:
                all_result.append({
                    "id": data.id,
                    "name": data.name,
                    "gender": data.gender,
                    "birthday": str(data.birthday),
                    "birthplace": data.birthplace,
                    "breed": data.breed,
                    "brand": data.brand,
                    "record": data.record,
                    "horse_photo": str(data.horse_photo),
                    "user":str(data.user.userName)
                })
        elif type == '1':
            all_data = Horseman.objects.filter(user__userName__icontains=keywords)
            for data in all_data:
                if datetime.datetime.now().month >= data.user.birthday.month:
                    old = int(datetime.datetime.now().year)-data.user.birthday.year
                else:
                    old = int(datetime.datetime.now().year) - data.user.birthday.year - 1
                all_result.append({
                    "user_id": data.user_id,
                    "user_name": data.user.userName,
                    "gender": data.user.gender,
                    "old": old,
                    "qualified": data.qualified,
                    "horseman_photo": str(data.horseman_photo),
                    "height": data.height,
                    "weight": data.weight,
                    "native_place": data.native_place,
                    "employer": data.employer,
                    "record": data.record,
                    "member_id": data.member_id
                })
        return HttpResponse(json.dumps(all_result), content_type='application/json')


class GetCompetitionDetailView(View):
    def post(self, request):
        id = request.POST.get('id')
        type = request.POST.get('type')
        all_result = []
        if type == '0':  # 赛马
            all_competitions = EnrollmentDetail.objects.filter(horse_id=id)
            for data in all_competitions:
                all_result.append({
                    "game": data.game.name,
                    "ranking": data.get_rank(),
                })
        elif type == '1':  # 骑师
            all_competitions = EnrollmentDetail.objects.filter(horseman_id=id)
            for data in all_competitions:
                all_result.append({
                    "game": data.game.name,
                    "ranking": data.get_rank(),
                })
        return HttpResponse(json.dumps(all_result), content_type='application/json')
