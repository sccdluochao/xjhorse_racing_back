# -------------------------------------------------------------------------------
# File:         urls_honorary_wall.py
# Description:  名誉墙
# Author:       loseboy
# Email:        loseboycn@qq.com
# Date:         2018/10/16 1:25 PM
# Software:     PyCharm
# -------------------------------------------------------------------------------

from django.conf.urls import url
from .views_honorary_wall import HonoraryWallView, SearchView, GetCompetitionDetailView

urlpatterns = [
    url(r'^racing/honorary_wall', HonoraryWallView.as_view(), name='honorary_wall'),
    url(r'^racing/honorary_search', SearchView.as_view(), name='honorary_search'),
    url(r'^racing/get_competition_detail', GetCompetitionDetailView.as_view(), name='get_competition_detail'),
]

