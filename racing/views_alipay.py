from django.contrib.auth.decorators import login_required
from rules.contrib.views import permission_required
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from alipay import AliPay
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from certification.decorator import certificate_required
from racing.models import AliPaySetting
from racing.models_ticket import AliPay as Payment, Order


# 调用支付宝进行支付
@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def pay(request, pk):
    my_payment = get_object_or_404(Payment, id=pk)
    # 获取门票销售支付宝配置参数
    alipay_setting = AliPaySetting.objects.get(id=1)
    # 创建支付宝工具对象
    alipay = AliPay(
        # 沙盒测试appid， 正式上线需要替换为蚂蚁金服开放平台（open.alipay.com）开发者中心创建的应用唯一标识（APPID）
        appid=alipay_setting.app_id,
        # 处理支付宝返回结果的URL
        app_notify_url=alipay_setting.notify_url,
        # 应用私钥（目前使用沙盒测试应用私钥）
        app_private_key_string=alipay_setting.app_private_key,
        # 支付宝应用公钥（目前使用沙盒测试应用公钥）
        alipay_public_key_string=alipay_setting.alipay_public_key,
        # 签名算法类型：RSA 或者 RSA2
        sign_type=alipay_setting.sign_type,
        # 默认为False，这里设为True是为了配合沙箱模式使用
        debug=alipay_setting.debug_mode
    )

    order_string = alipay.api_alipay_trade_page_pay(
        out_trade_no=str(my_payment.id),
        total_amount=str(my_payment.total_amount),
        subject="天马赛事门票网店",
        # 支付完成后跳转的页面
        return_url=alipay_setting.return_url,
        notify_url=alipay_setting.notify_url  # 可选，None表示使用默认的notify url
    )

    # 电脑网站支付，需要跳转到https://openapi.alipay.com/gateway.do? + order_string
    # 沙盒测试，需要跳转到https://openapi.alipaydev.com/gateway.do? + order_string
    alipay_url = alipay_setting.gateway
    url = alipay_url + "?" + order_string

    return HttpResponseRedirect(url)


###################################################################
# 支付宝同步通知处理视图
# 通过安全验证之后跳转到【已购门票】页面
# 只适用沙盒测试调试模式
# 实际生产环境应当使用支付宝异步通知处理视图
###################################################################
@csrf_exempt
@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def alipay_return_url(request):
    # 签名验证
    if return_verify(request):
        # 商户网站支付单号
        pay_id = request.GET.get('out_trade_no')

        # 修改支付单的支付状态
        payment = Payment.objects.get(id=pay_id)
        payment.paid = True
        payment.save()

        #获取当前账单的订单
        orders = Order.objects.filter(alipayorder__pay=payment)
        for order in orders:
            order.paid = 1
            order.save()

    # 跳转到【已购门票】页面
    return HttpResponseRedirect('/racing/ticket/paid')


# 支付宝同步通知签名验证，防止黑客攻击
def return_verify(request):
    # 获取支付宝配置参数
    alipay_setting = AliPaySetting.objects.get(id=1)

    # 创建支付宝工具对象
    alipay = AliPay(
        # 沙盒测试appid， 正式上线需要替换为蚂蚁金服开放平台（open.alipay.com）开发者中心创建的应用唯一标识（APPID）
        appid=alipay_setting.app_id,
        # 处理支付宝返回结果的URL
        app_notify_url=alipay_setting.notify_url,
        # 应用私钥（目前使用沙盒测试应用私钥）
        app_private_key_string=alipay_setting.app_private_key,
        # 支付宝应用公钥（目前使用沙盒测试应用公钥）
        alipay_public_key_string=alipay_setting.alipay_public_key,
        # 签名算法类型：RSA 或者 RSA2
        sign_type=alipay_setting.sign_type,
        # 默认为False，这里设为True是为了配合沙箱模式使用
        debug=alipay_setting.debug_mode
    )

    # 获取支付宝同步通知传来的参数数据，转换为一个字典
    data = {x: request.GET.get(x) for x in request.GET.keys()}
    signature = data.pop("sign")

    # 签名验证
    success = alipay.verify(data, signature)
    if success:
        return True
    else:
        return False


###################################################################
# 支付宝异步通知处理视图
# 实际生产环境应当使用支付宝异步通知处理视图
###################################################################
@csrf_exempt
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def alipay_notify_url(request):
    if notify_verify(request):
        # 商户网站支付单号
        pay_id = request.POST.get('out_trade_no')

        # 修改支付单的支付状态
        payment = Payment.objects.get(id=pay_id)
        payment.paid = True
        payment.save()

        # 获取当前账单的订单
        orders = Order.objects.filter(alipayorder__pay=payment)
        for order in orders:
            order.paid = 1
            order.save()

        # 向支付宝返回交易完成标志
        return HttpResponse("success")
    else:
        # 向支付宝返回交易失败标志
        return HttpResponse("fail")

    # 支付宝异步通知签名验证，防止黑客攻击


def notify_verify(request):
    # 支付宝异步通知使用的是POST方法
    if request.method == 'POST':
        alipay_setting = AliPaySetting.objects.get(id=1)
        # 创建支付宝工具对象
        alipay = AliPay(
            # 沙盒测试appid， 正式上线需要替换为蚂蚁金服开放平台（open.alipay.com）开发者中心创建的应用唯一标识（APPID）
            appid=alipay_setting.app_id,
            # 处理支付宝返回结果的URL
            app_notify_url=alipay_setting.notify_url,
            # 应用私钥（目前使用沙盒测试应用私钥）
            app_private_key_string=alipay_setting.app_private_key,
            # 支付宝应用公钥（目前使用沙盒测试应用公钥）
            alipay_public_key_string=alipay_setting.alipay_public_key,
            # 签名算法类型：RSA 或者 RSA2
            sign_type=alipay_setting.sign_type,
            # 默认为False，这里设为True是为了配合沙箱模式使用
            debug=alipay_setting.debug_mode
        )

        # 获取支付宝异步通知传来的参数数据，转换为一个字典
        data = {x: request.POST.get(x) for x in request.POST.keys()}
        signature = data.pop("sign")

        # 签名验证
        success = alipay.verify(data, signature)
        # 签名验证通过且交易码为【成功】或【完成】
        if success and data["trade_status"] in ("TRADE_SUCCESS", "TRADE_FINISHED"):
            return True
        else:
            return False
    else:
        # 如果不是POST方法，很可能是恶意攻击
        return False
