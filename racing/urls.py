from django.conf.urls import url, include
from racing import views

# 创 建 人：张太红
# 创建日期：2018年03月17日


urlpatterns = [
    url(r'^', include('racing.urls_home')),  # 赛事首页
    url(r'^', include('racing.urls_regulation')),  # 赛事规程
    url(r'^', include('racing.urls_game')),  # 竞赛项目
    url(r'^', include('racing.urls_team')),  # 竞赛项目
    url(r'^', include('racing.urls_enrollment')),  # 参赛报名
    url(r'^', include('racing.urls_schedule')),  # 比赛日程
    url(r'^', include('racing.urls_ticket')),  # 比赛日程
    url(r'^', include('racing.urls_enrollment_admin')),  # 参赛报名
    url(r'^api/', include('racing.urls_apis')),  # 参赛报名
    url(r'^', include('racing.urls_honorary_wall')),  # 名誉墙

    # 购物车
    url(r'^', include('racing.urls_cart', namespace='cart')),

    # ********************************************************
    # 角色权限管理 model：Permission
    # ********************************************************
    url(r'^racing/my_racing_role$', views.my_racing_role, name='my_racing_role'),
    url(r'^racing/my_role_add$', views.my_role_add, name='my_role_add'),
    url(r'^racing/my_role_abandon/(?P<pk>\d+)$', views.my_role_abandon, name='my_role_abandon'),
    url(r'^racing/get_description$', views.get_description, name='get_description'),
    url(r'^racing/role_list$', views.role_list, name='role_list'),
]
