#微信认证装饰器
def wx_authentication(fn):
    def _wrapper(request,*args,**kwargs):
        return fn(request,*args,**kwargs)
    return _wrapper