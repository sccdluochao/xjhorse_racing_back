from django.conf.urls import url
from racing import views_cart

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 门票服务
# ********************************************************
urlpatterns = [
url(r'^racing/ticket/cart$', views_cart.cart_detail, name='cart_detail'),
    url(r'^racing/ticket/cart/add/(?P<product_id>\d+)/$', views_cart.cart_add, name='cart_add'),
    url(r'^racing/ticket/cart/remove/(?P<product_id>\d+)/$', views_cart.cart_remove, name='cart_remove'),
]
