# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-11-05 05:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('racing', '0020_auto_20181105_1322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='member_name',
            field=models.CharField(max_length=100, unique=True, verbose_name='Member Name'),
        ),
    ]
