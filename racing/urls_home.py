from django.conf.urls import url
from racing import views_home as views

# 创 建 人：张太红
# 创建日期：2018年03月17日

urlpatterns = [
    url(r'^racing$', views.racing, name='racing_home'),
    # ********************************************************
    # 赛事公告管理 model：EventsAnnouncement
    # ********************************************************
    url(r'^racing/announcement_manage_list$', views.announcement_manage_list, name='announcement_manage_list'),
    url(r'^racing/announcement_manage/(?P<pk>\d+)$', views.announcement_manage_detail,
        name='announcement_manage_detail'),
    url(r'^racing/announcement_manage/create$', views.announcement_manage_create, name='announcement_manage_create'),
    url(r'^racing/announcement_manage/update/(?P<pk>\d+)$', views.announcement_manage_update,
        name='announcement_manage_update'),
    url(r'^racing/announcement_manage/delete/(?P<pk>\d+)$', views.announcement_manage_delete,
        name='announcement_manage_delete'),
    url(r'^racing/announcement_manage/show/(?P<pk>\d+)$', views.announcement_show,
        name='announcement_show'),
]
