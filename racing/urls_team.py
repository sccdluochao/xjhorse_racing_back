from django.conf.urls import url
from racing import views_team as views

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 参赛队伍
# ********************************************************
urlpatterns = [
    url(r'^racing/team$', views.team, name='team'),
    url(r'^racing/team/horse_detal/(?P<pk>\d+)$', views.horse_detail, name='horse_detail'),
]
