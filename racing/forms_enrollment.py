from django import forms

from racing.models_enrollment import Member, Referee, Horseman, Horse, Enrollment, EnrollmentDetail, Game
from certification.models import Certification


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 申请会员资格
class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ('user',
                  'member_type',
                  'member_name',
                  'contact',
                  'contact_number',
                  'association',
                  'trainer',
                  'trainer_number',
                  'veterinarian',
                  'veterinarian_number')

        widgets = {'user': forms.HiddenInput()}

    # 禁止用户选择会员类型
    def __init__(self, *args, **kwargs):
        super(MemberForm, self).__init__(*args, **kwargs)
        self.fields['member_type'].widget.attrs['disabled'] = 'disabled'
        self.fields['member_type'].required = False

    def clean_member_type(self):
        if self.instance and self.instance.pk:
            return self.instance.member_type
        else:
            return self.cleaned_data['member_type']


# 会员资格审核
class MemberAuditForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ('qualified','qualify_msg')


# 裁判员资格
class RefereeForm(forms.ModelForm):
    class Meta:
        model = Referee
        fields = ('user',
                  'referee_history',
                  'referee_licence')

        widgets = {'user': forms.HiddenInput()}


# 裁判员资格审核
class RefereeAuditForm(forms.ModelForm):
    class Meta:
        model = Referee
        fields = ('referee_qualified','qualify_msg')


# 骑师资格
class HorsemanForm(forms.ModelForm):
    class Meta:
        model = Horseman
        fields = ('user',
                  #'member',
                  'height',
                  'weight',
                  'native_place',
                  'employer',
                  'horseman_photo',
                  'health_certificate_photo',
                  'record'
                  )

        widgets = {'user': forms.HiddenInput()}

    # 只允许选择通过资格审核的会员（参赛队伍）
    def __init__(self, *args, **kwargs):
        super(HorsemanForm, self).__init__(*args, **kwargs)
        members = Member.objects.filter(qualified=True)
        #self.fields['member'].queryset = members


# 骑师资格审核
class HorsemanAuditForm(forms.ModelForm):
    class Meta:
        model = Horseman
        fields = ('qualified','qualify_msg')


# 骑师资格审核
class HorseAuditForm(forms.ModelForm):
    class Meta:
        model = Horse
        fields = ('qualified','qualify_msg')


# 参赛项目报名
class EnrollmentDetailForm(forms.ModelForm):
    class Meta:
        model = EnrollmentDetail
        fields = (
            'enrollment',
            'game',
            'horse',
            'horseman',
        )

        widgets = {
            'enrollment': forms.HiddenInput(),
            'horse': forms.TextInput,
            'horseman': forms.TextInput,
        }

    def __init__(self, *args, **kwargs):
        super(EnrollmentDetailForm, self).__init__(*args, **kwargs)
       # enrollment = self.initial.get("enrollment")
        # 马匹下拉列表限制在本参赛单位之内已经核准的马匹， 并且排除已经报名的马匹
        self.fields['game'].queryset = Game.objects.filter(year__active=1, game_status=1)
        self.fields['horse'].widget.attrs['autocomplete']="off"
        self.fields['horseman'].widget.attrs['autocomplete']="off"

"""
# 参赛项目报名
class EnrollmentDetailForm(forms.ModelForm):
    class Meta:
        model = EnrollmentDetail
        fields = (
            'enrollment',
            'game',
            'horse',
            #'type',
            'horseman',
        )

        widgets = {
            'enrollment': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(EnrollmentDetailForm, self).__init__(*args, **kwargs)
        enrollment = self.initial.get("enrollment")
        # 马匹下拉列表限制在本参赛单位之内已经核准的马匹， 并且排除已经报名的马匹
        print(enrollment.member)
        #certification = Enrollment.objects.get(member=enrollment.member).member

        #horse = Horse.objects.filter(qualified=True, member=certification)


        #used_horse_id_list = Horse.objects.filter(enrollmentdetail__enrollment=enrollment).values_list('id', flat=True)
        #horse = horse.exclude(id__in=used_horse_id_list)
        #self.fields['horse'].queryset = horse
        # 骑师下拉列表限制在本参赛单位之内已经核准的骑师
        #horseman = Horseman.objects.filter(qualified=True, member=certification)
        #self.fields['horseman'].queryset = horseman
"""

# 报名锁定
class EnrollmentFixedForm(forms.ModelForm):
    class Meta:
        model = Enrollment
        fields = ('enrollment_fixed',)

    def clean(self,*args, **kwargs):
        cleaned_data=super(EnrollmentFixedForm, self).clean(*args, **kwargs)

        if self.instance.pk:
            #没有报名项，及当前不处于锁定状态，不能锁定！
            if self.instance.enrollmentdetail_set.all().count()==0 \
                    and self.instance.enrollment_fixed != 1:
                raise forms.ValidationError("没有参赛项目，不能锁定！")

        return cleaned_data


# 报名核准
class EnrollmentAuditForm(forms.ModelForm):
    class Meta:
        model = Enrollment
        fields = ('qualified','qualify_msg')
