from django import forms
from racing.models_enrollment import EnrollmentDetail, Horse, Horseman, Enrollment


#修改
class Game_Group_GateModelForm(forms.ModelForm):
    class Meta:
        model = EnrollmentDetail
        fields = ['horse',
                  'horseman',
                  "enrollment",
                  'group_no',
                  'starting_gate_no',]

    def __init__(self,*args, **kwargs):
        super(Game_Group_GateModelForm, self).__init__(*args, **kwargs)
        self.fields['horse'].widget.attrs['required']="required"
        self.fields['horseman'].widget.attrs['required'] = "required"
        self.fields['enrollment'].widget.attrs['required'] = "required"
        self.fields['group_no'].widget.attrs['required'] = "required"
        self.fields['starting_gate_no'].widget.attrs['required'] = "required"

        if self.instance.pk:
            self.fields['horse'].queryset=Horse.objects.filter(id=self.instance.horse.id)
            self.fields['horseman'].queryset = Horseman.objects.filter(user=self.instance.horseman.user)
            self.fields['enrollment'].queryset = Enrollment.objects.filter(id=self.instance.enrollment.id)