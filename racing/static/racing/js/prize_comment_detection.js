var can_submit=true;
$(function () {
    //奖金验证
    var  prize_comment_detection=function () {
        can_submit=true;
        //识别结果
        var prize_comment_str="";
        //存在的参赛最低数
        var exist_entrys="";
        /*定义验证表达式*/
        var reg=/^\d+=\d{1,2}([\+]{1}[\d]{1,2})+$/;
        var prize_comment=$("#id_prize_comment").val();
        //分解出每段表达式
        var prize_expression_list=prize_comment.split(/\s+/);
        for(var index=0;index<prize_expression_list.length;index++){
            //验证表达式
            if(reg.test(prize_expression_list[index])){

                var entry_num=prize_expression_list[index].split("=")[0];
                var proportion_set=prize_expression_list[index].split("=")[1].split("+");

                prize_comment_str+="<br>"+prize_expression_list[index]+":<br>最低"+entry_num+"个参赛报名，";

                var sum=0;
                var is_order_correct=true;
                var prize_proportion_str="前"+proportion_set.length+"名:";
                var max=parseInt(proportion_set[0]);

                for(var index1=0;index1<proportion_set.length;index1++) {
                    //求解表达式之和，需要等于100
                    sum += parseInt(proportion_set[index1]);
                    prize_proportion_str+=proportion_set[index1]+"% ";
                    //占比应该递减
                    if(max<parseInt(proportion_set[index1])){
                        is_order_correct=false;
                        break;
                    }
                    max=parseInt(proportion_set[index1]);
                }

                //最低赛参数已存在！
                if(exist_entrys.indexOf(entry_num)!=-1){
                    can_submit=false;
                    prize_comment_str+="最低参赛数量已存在！<br>";
                //奖金占比没有递减
                } else if(is_order_correct!=true){
                    can_submit=false;
                    prize_comment_str+="名次奖金所占比有误！<br>";
                //比例之和不等于100
                }else if(sum!=100){
                    can_submit=false;
                    prize_comment_str+="占比之和需要等于100！<br>";
                }else{
                    prize_comment_str+=prize_proportion_str+"<br>";
                }

                exist_entrys+="&"+entry_num;

            }else if(prize_expression_list[index]!=""){
                can_submit=false;
                prize_comment_str+=prize_expression_list[index]+":<br>验证失败！<br>";
            }

        }

        if($("#prize_comment_str")[0]){
            $("#prize_comment_str").html(prize_comment_str);
        }else{
            var obj=$("<h5></h5>");
            obj.attr({"id":"prize_comment_str"});
            obj.html(prize_comment_str);
            $("#id_prize_comment").before(obj);
        }

    }

    $("#id_prize_comment").keyup(function(){
        prize_comment_detection();
    });

    $("#id_prize_comment").blur(function(){
        prize_comment_detection();
    });

    //加载好后
    prize_comment_detection();

    $("form").submit(function () {
        if(can_submit==false){
            alert("请检查‘名次及奖金分配’！");
            return false;
        }
    })

})