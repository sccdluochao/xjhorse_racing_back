var imagetype = ["image/jpg", "image/bmp", "image/png"]
var filesize = 0;
var filename = "";
var idcard_is_upload = false;

//图片改变调用
function id_photo_change() {
    //未选择照片
    if ($("#id_id_photo")[0].files[0] == undefined || $("#id_id_photo")[0].files[0] == null) {
        filename = "";
        filesize = 0;
        alert("请选择身份证图片！");

    } else if (filename != $("#id_id_photo")[0].files[0].name &&
        filesize != $("#id_id_photo")[0].files[0].size) {

        filename = $("#id_id_photo")[0].files[0].name;
        filesize = $("#id_id_photo")[0].files[0].size;

        clear_input_value();
        //上传图片
        upload_idcard();

    }
}


//等待窗口
var wait_Dialog = {
    status: 0,

    show: function () {
        if (this.status == 0) {
            var obj = $("<div>请稍后。。。加载中。。。</div>");
            obj.css({
                "opacity": 0.5,
                "position": "fixed",
                "top": 0 + "px",
                "right": 0 + "px",
                "bottom": 0 + "px",
                "left": 0 + "px",
                "z-index": 1040,
                "background-color": "#000",
                "text-align":"center",
                "color":"white"
            });

            obj.attr("id", "wait_dialog");

            $("body").append(obj);
            this.status = 1;
        }
    },
    hide: function () {
        if (this.status == 1) {
            $("#wait_dialog").remove();
            this.status = 0;
        }
    }
};


//上传图片
function upload_idcard() {

    if (idcard_is_upload == true) {
        alert("正在上传请稍等！");
        return;
    }

    var formdata = new FormData();
    formdata.append("idcardimg", $("#id_id_photo")[0].files[0]);
    formdata.append("csrfmiddlewaretoken", $("input[name='csrfmiddlewaretoken']").val());
    $.ajax({
        url: "/racing/enrollment_admin/idcardrecognize/",
        type: "POST",
        cache: false,
        data: formdata,
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        dataType: "json",
        beforeSend: function () {
            wait_Dialog.show();
            idcard_is_upload = true;
        },
        success: function (data) {
            wait_Dialog.hide();
            idcard_is_upload = false;
            if (data.status == 1) {
                $("#id_userName").val(data.userName);
                $("#id_gender").val(data.gender);
                $("#id_ethnicity").val(data.ethnicity);
                $("#id_birthday").val(data.birthday);
                $("#id_identity").val(data.identity);
                $("#id_address").val(data.address);
            } else {
                alert(data.msg);
                var file = $("#id_id_photo");
                file.after(file.clone().val(""));
                file.remove();
                clear_input_value();
            }
        }
    });
}


function clear_input_value(){
    $("#id_userName").val("");
    $("#id_gender").val("");
    $("#id_ethnicity").val("");
    $("#id_birthday").val("");
    $("#id_identity").val("");
    $("#id_address").val("");
}

$(function () {
    $("#id_photo-clear_id").next().remove();
    $("#id_photo-clear_id").remove();
    //$("#id_id_photo").attr("onchange", "id_photo_change()");

    //当图片文件域改变
    /*$("#id_id_photo").on("change", function () {
        alert("nihao");
        //未选择照片
        if ($(this)[0].files[0] == undefined || $(this)[0].files[0] == null) {
            filename = "";
            filesize = 0;
            alert("请选择身份证图片！");

        } else if (filename != $(this)[0].files[0].name &&
            filesize != $(this)[0].files[0].size) {

            filename = $(this)[0].files[0].name;
            filesize = $(this)[0].files[0].size;

            //上传图片
            upload_idcard();

        }
    });*/

});


