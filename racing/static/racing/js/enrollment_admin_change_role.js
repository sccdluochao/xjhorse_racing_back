$(function () {
    var loadForm = function () {
        var a_href = $(this);
        $.ajax({
            url: a_href.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-dialog").modal("show");
            },
            success: function (data) {
                $("#modal-dialog .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    $("#role-table tbody").html(data.html_role_list);
                    $("#add-role-btn").html(data.html_add_role_btn);
                    $("#modal-dialog").modal("hide");
                    alert("角色创建成功！");
                }
                else {
                    $("#modal-dialog .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    $(".user_role_change").on("click", loadForm);
    $(".modal-dialog").on("submit", ".js-create-role-form",saveForm);

})