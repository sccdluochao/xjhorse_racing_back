$(function () {

    window.onbeforeunload = function (e) {
        var e = window.event || e;
        e.returnValue = ("确定离开当前页面吗？");
    }

    var uploadform = function () {

        if (confirm("确认保存？并审核通过！") == false) {
            return false;
        }

        if (uploading) {
            alert("文件正在上传中，请稍候");
            return false;
        }

        var formdata = new FormData($(this)[0]);
        //$("#form_area form").attr("action")
        $.ajax({
            url: "",
            type: "POST",
            cache: false,
            data: formdata,
            processData: false,
            contentType: false,
            dataType: "json",
            beforeSend: function () {
                uploading = true;
            },
            success: function (data) {
                uploading = false;
                if (data.status == 1) {
                    var r = confirm(data.msg);
                    if (r == true) {
                        location.href = $("#form_area form").attr("data-url");
                    }
                    else {
                        location.href = $("#form_area form").attr("data-url");
                    }
                } else if (data.status == 0) {

                    $("#referee_enrollment_form").remove();
                    $("#form_area").html(data.html_form);
                    alert("创建错误，请检查修改后再次创建！");

                    try {
                        submit_fail_callback();
                    }
                    catch (err) {

                    }

                } else {
                    alert("未知错误！");
                    try {
                        submit_error_callback();
                    }
                    catch (err) {

                    }

                }
            }
        });
        return false;
    }

    $("#form_area").on("submit", ".enrollment_form", uploadform);
    $("#form_area").on("change", "#id_id_photo", id_photo_change);

});