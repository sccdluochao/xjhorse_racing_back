$(function () {
    $('#announcement').scrollbox({
        linear: true,
        step: 1,
        delay: 0,
        speed: 50
    });

    $('#league-table').scrollbox({
        linear: true,
        step: 1,
        delay: 0,
        speed: 50
    });
});
