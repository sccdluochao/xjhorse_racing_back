$(function () {

    $("[time-tag|='select_date']").datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd',
            //mouth视图
            minView:2,
            weekStart: 1,
            todayBtn:  true,
		    autoclose: true,
		    todayHighlight:true,
		    //
		    startView: 2,
		    forceParse: false,
            showMeridian: false
        }).on('changeDay', function(ev){
        	//enddate=ev.date.valueOf();
        });

    $("body .datetimepicker").css("background-color","#fff");

})