from alipay import AliPay
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from rules.contrib.views import permission_required, objectgetter, LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.views.generic import UpdateView, DetailView, ListView, CreateView, DeleteView
from rules.contrib.views import PermissionRequiredMixin
from django.shortcuts import render, get_object_or_404
from certification.models import Certification
from racing.forms_enrollment import MemberForm, MemberAuditForm, RefereeForm, RefereeAuditForm, \
    HorsemanForm, HorsemanAuditForm, HorseAuditForm, EnrollmentDetailForm, EnrollmentFixedForm, \
    EnrollmentAuditForm
from racing.models import Regulation, Role, Permission
from racing.models_enrollment import Member, Referee, Horseman, Horse, Enrollment, EnrollmentDetail
from rules import has_perm

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 参赛报名
# ********************************************************
# 参赛报名首页
from racing.models import AliPaySetting
from racing.views import send_text_msg


def enrolment(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    if request.user.is_authenticated and (request.user.has_perm('racing.corporation_contestant')
                                          or request.user.has_perm('racing.individual_contestant')
                                          or request.user.has_perm('racing.corporation_horse_owner')
                                          or request.user.has_perm('racing.individual_horse_owner')
                                          or request.user.has_perm('racing.horseman')
                                          or request.user.has_perm('racing.referee')):

        # 【管理员】、【购票观众】无需报名
        roles = Role.objects.filter(permission__user=request.user).exclude(id__in=[1, 3])
    else:
        roles = Role.objects.filter(name__in=['单位参赛队伍', '个人参赛队伍', '单位马主', '个人马主', '骑师', '裁判员']).order_by('id')

    context = {
        "regulation": regulation,
        "roles": roles,
    }
    return render(request, "racing/enrollment/enrollment.html", context)


# ********************************************************
# 会员资格申请，单位参赛队伍及个人参赛队伍会员操作
# ********************************************************

# 会员资格申请
class MemberView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'racing/enrollment/enrollment_member_apply.html'
    success_url = '/racing/enrollment'
    form_class = MemberForm
    model = Member
    permission_required = ['permission.application_user', 'racing.contestant', 'racing.not_is_member_qualified']
    raise_exception = False
    login_url = '/racing/enrollment_member_detail'

    # 如果登录的用户存在"会员资料"记录，直接获取之，否则创建一条记录
    def get_object(self, queryset=None):
        try:
            certification = Certification.objects.get(user=self.request.user)
            member = self.model.objects.get(user=certification)
        except Member.DoesNotExist:
            role_id_list = Permission.objects.filter(user=self.request.user).values_list('role_id', flat=True)
            if 4 in role_id_list:
                # 单位参赛队伍
                member = Member(user=certification, member_type=2)
            else:
                # 个人参赛队伍
                member = Member(user=certification, member_type=1)
        return member

    def get_permission_object(self):
        return "天马赛事"


enrollment_member_apply = MemberView.as_view()


# 会员资格明细
class MemberDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Member
    permission_required = ['racing.is_member_creator', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_member_detail.html'

    def get_object(self):
        certification = Certification.objects.get(user=self.request.user)
        return Member.objects.get(user=certification)


enrollment_member_detail = MemberDetailView.as_view()


# ********************************************************
# 会员资格审核，只允许管理员操作
# ********************************************************

# 1、会员（参赛队伍）资格审核

# （1）、会员列表
class MemberAuthorizeListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Member
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/member_authorize_list.html'


member_authorize = MemberAuthorizeListView.as_view()


# （2）、会员详情
class MemberAuthorizeDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Member
    permission_required = ['racing.authenticated', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_member_detail.html'


member_authorize_detail = MemberAuthorizeDetailView.as_view()


# （3）、会员资格审核
@login_required  # 首先必须是登录用户
@permission_required('racing.admin', raise_exception=True)  # 必须是【天马赛事】应用平台的管理员
def member_authorize_update(request, pk):
    data = dict()
    member = get_object_or_404(Member, pk=pk)
    if request.method == 'POST':
        form = MemberAuditForm(request.POST, instance=member)
        if form.is_valid():
            mobile_num = member.user.mobile
            # 审核通过发送短信
            if 'qualified' in request.POST and request.POST['qualified'] == "on":
                send_text_msg(str(mobile_num), "<会员资格审核>通过")
            else:
                send_text_msg(str(mobile_num), "<会员资格审核>,反馈信息:" + request.POST['qualify_msg'])
            form.save()
            data['form_is_valid'] = True
            members = Member.objects.all()
            data['html_list'] = render_to_string('racing/enrollment/member_authorize_list_partial.html', {
                'object_list': members
            })
        else:
            data['form_is_valid'] = False
    else:
        member.qualify_msg=""
        form = MemberAuditForm(instance=member)
    context = {'form': form}
    data['html_form'] = render_to_string(
        'racing/enrollment/member_authorize_update.html',
        context,
        request=request)
    return JsonResponse(data)


# ********************************************************
# 裁判员资格申请，裁判操作
# ********************************************************
# 裁判资格申请
class RefereeView(PermissionRequiredMixin, UpdateView):
    template_name = 'racing/enrollment/enrollment_referee_apply.html'
    success_url = '/racing/enrollment'
    model = Referee
    form_class = RefereeForm
    permission_required = ['permission.application_user', 'racing.referee', 'racing.not_is_referee_qualified']
    raise_exception = False
    login_url = '/racing/enrollment_referee_detail'

    # 如果登录的用户存在"裁判员资料"记录，直接获取之，否则创建一条记录
    def get_object(self, queryset=None):
        try:
            certification = Certification.objects.get(user=self.request.user)
            referee = self.model.objects.get(user=certification)
        except Referee.DoesNotExist:
            referee = Referee(user=certification)
        return referee

    def get_permission_object(self):
        return "天马赛事"


enrollment_referee_apply = RefereeView.as_view()


# 裁判员资格明细
class RefereeDetailView(PermissionRequiredMixin, DetailView):
    model = Referee
    permission_required = ['racing.is_referee_creator', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_referee_detail.html'

    def get_object(self):
        certification = Certification.objects.get(user=self.request.user)
        return Referee.objects.get(user=certification)


enrollment_referee_detail = RefereeDetailView.as_view()


# ********************************************************
# 裁判员资格审核，只允许管理员操作
# ********************************************************

# 1、裁判员资格审核

# （1）、裁判员列表
class RefereeAuthorizeListView(PermissionRequiredMixin, ListView):
    model = Referee
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/referee_authorize_list.html'


referee_authorize = RefereeAuthorizeListView.as_view()


# （2）、裁判员详情
class RefereeAuthorizeDetailView(PermissionRequiredMixin, DetailView):
    model = Referee
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_referee_detail.html'


referee_authorize_detail = RefereeAuthorizeDetailView.as_view()


# （3）、裁判员资格审核
@login_required  # 首先必须是登录用户
@permission_required('racing.admin', raise_exception=True)  # 必须是【天马赛事】应用平台的管理员
def referee_authorize_update(request, pk):
    data = dict()
    referee = get_object_or_404(Referee, pk=pk)
    if request.method == 'POST':
        form = RefereeAuditForm(request.POST, instance=referee)
        if form.is_valid():
            mobile_num = referee.user.mobile
            # 审核通过发送短信
            if 'qualified' in request.POST and request.POST['qualified'] == "on":
                send_text_msg(str(mobile_num), "<裁判资格审核>通过")
            else:
                send_text_msg(str(mobile_num), "<裁判资格审核>,反馈信息:" + request.POST['qualify_msg'])
            form.save()
            data['form_is_valid'] = True
            referees = Referee.objects.all()
            data['html_list'] = render_to_string('racing/enrollment/referee_authorize_list_partial.html', {
                'object_list': referees
            })
        else:
            data['form_is_valid'] = False
    else:
        referee.qualify_msg=""
        form = RefereeAuditForm(instance=referee)
    context = {'form': form}
    data['html_form'] = render_to_string(
        'racing/enrollment/referee_authorize_update.html',
        context,
        request=request)
    return JsonResponse(data)


# ********************************************************
# 骑师资格申请，骑师操作
# ********************************************************
# 骑师资格申请
class HorsemanView(PermissionRequiredMixin, UpdateView):
    template_name = 'racing/enrollment/enrollment_horseman_apply.html'
    success_url = '/racing/enrollment'
    model = Horseman
    form_class = HorsemanForm
    permission_required = ['permission.application_user', 'racing.horseman', 'racing.not_is_horseman_qualified']
    raise_exception = False
    login_url = '/racing/enrollment_horseman_detail'

    # 如果登录的用户存在"骑师资料"记录，直接获取之，否则创建一条记录
    def get_object(self, queryset=None):
        try:
            certification = Certification.objects.get(user=self.request.user)
            horseman = self.model.objects.get(user=certification)
        except Horseman.DoesNotExist:
            horseman = Horseman(user=certification)
        return horseman

    def get_permission_object(self):
        return "天马赛事"


enrollment_horseman_apply = HorsemanView.as_view()


# 骑师资格明细
class HorsemanDetailView(PermissionRequiredMixin, DetailView):
    model = Horseman
    permission_required = ['racing.is_referee_creator', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_horseman_detail.html'

    def get_object(self):
        certification = Certification.objects.get(user=self.request.user)
        return Horseman.objects.get(user=certification)


enrollment_horseman_detail = HorsemanDetailView.as_view()


# ********************************************************
# 骑师资格审核，只允许管理员操作
# ********************************************************

# 1、骑师资格审核

# （1）、骑师列表
class HorsemanAuthorizeListView(PermissionRequiredMixin, ListView):
    model = Horseman
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/horseman_authorize_list.html'


horseman_authorize = HorsemanAuthorizeListView.as_view()


# （2）、骑师详情
class HorsemanAuthorizeDetailView(PermissionRequiredMixin, DetailView):
    model = Horseman
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_horseman_detail.html'


horseman_authorize_detail = HorsemanAuthorizeDetailView.as_view()


# （3）、骑师资格审核
@login_required  # 首先必须是登录用户
@permission_required('racing.admin', raise_exception=True)  # 必须是【天马赛事】应用平台的管理员
def horseman_authorize_update(request, pk):
    data = dict()
    horseman = get_object_or_404(Horseman, pk=pk)
    if request.method == 'POST':
        form = HorsemanAuditForm(request.POST, instance=horseman)
        if form.is_valid():
            # print(horseman.user.mobile)
            mobile_num = horseman.user.mobile
            # 审核通过发送短信
            if 'qualified' in request.POST and request.POST['qualified'] == "on":
                # print(member.contact_number)
                send_text_msg(str(mobile_num), "<骑师资格审核>通过")
            else:
                send_text_msg(str(mobile_num), "<骑师资格审核>,反馈信息:" + request.POST['qualify_msg'])
            form.save()
            data['form_is_valid'] = True
            horsemen = Horseman.objects.all()
            data['html_list'] = render_to_string('racing/enrollment/horseman_authorize_list_partial.html', {
                'object_list': horsemen
            })
        else:
            data['form_is_valid'] = False
    else:
        horseman.qualify_msg=""
        form = HorsemanAuditForm(instance=horseman)
    context = {'form': form}
    data['html_form'] = render_to_string(
        'racing/enrollment/horseman_authorize_update.html',
        context,
        request=request)
    return JsonResponse(data)


# ********************************************************
# 马匹资格申请，马主操作
# ********************************************************

# 马主马匹列表
class HorseOwnerListView(PermissionRequiredMixin, ListView):
    permission_required = ['racing.horse_owner', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_horse_owner_list.html'

    def queryset(self):
        if self.request.user.is_superuser or has_perm('racing.admin', self.request.user):
            return Horse.objects.all()
        try:
            certification = Certification.objects.get(user=self.request.user)
            return Horse.objects.all().filter(user=certification)
        except Certification.DoesNotExist:
            return None


enrollment_horse_owner_list = HorseOwnerListView.as_view()


# 添加马匹
class HorseCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'racing/enrollment/enrollment_horse_owner_create.html'
    model = Horse
    permission_object = None
    permission_required = ['racing.horse_owner']
    raise_exception = True
    fields = ['year',
              'member',
              'horse_type',
              'name',
              'gender',
              'birthday',
              'birthplace',
              'breed',
              'brand',
              'chip_no',
              'passport_no',
              'father_name',
              'mother_name',
              'grandfather_name',
              'vaccinated',
              'vaccinate_date',
              'vaccine',
              'record',
              'horse_photo',
              ]

    def get_permission_object(self):
        return "天马赛事"

    # 为新建对象的user属性指定当前实名认证用户
    def form_valid(self, form, *args, **kwargs):
        certification = Certification.objects.get(user=self.request.user)
        form.instance.user = certification
        return super(HorseCreateView, self).form_valid(form, *args, **kwargs)


enrollment_horse_owner_create = HorseCreateView.as_view()


# 修改马匹
class HorseUpdateView(PermissionRequiredMixin, UpdateView):
    model = Horse
    permission_required = ['racing.is_horse_creator', 'racing.not_is_horse_qualified']
    raise_exception = True

    template_name = 'racing/enrollment/enrollment_horse_owner_update.html'
    fields = ['year',
              'member',
              'horse_type',
              'name',
              'gender',
              'birthday',
              'birthplace',
              'breed',
              'brand',
              'chip_no',
              'passport_no',
              'father_name',
              'mother_name',
              'grandfather_name',
              'vaccinated',
              'vaccinate_date',
              'vaccine',
              'record',
              'horse_photo',
              ]


enrollment_horse_owner_update = HorseUpdateView.as_view()


# 删除马匹
class HorseDeleteView(PermissionRequiredMixin, DeleteView):
    model = Horse
    success_url = reverse_lazy('enrollment_horse_owner_list')
    permission_required = ['racing.is_horse_creator', 'racing.not_is_horse_qualified']
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_horse_owner_delete.html'


enrollment_horse_owner_delete = HorseDeleteView.as_view()


# 马匹详细信息
class HorseDetailView(PermissionRequiredMixin, DetailView):
    model = Horse
    permission_required = ['racing.is_horse_creator', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_horse_detail.html'


enrollment_horse_detail = HorseDetailView.as_view()


# ********************************************************
# 马匹资格审核，只允许管理员操作
# ********************************************************

# （1）、马匹列表
class HorseAuthorizeListView(PermissionRequiredMixin, ListView):
    model = Horse
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/horse_authorize_list.html'


horse_authorize = HorseAuthorizeListView.as_view()


# （2）、马匹详情
class HorseAuthorizeDetailView(PermissionRequiredMixin, DetailView):
    model = Horse
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/horse_authorize_detail.html'


horse_authorize_detail = HorseAuthorizeDetailView.as_view()


# （3）、马匹审核
@login_required  # 首先必须是登录用户
@permission_required('racing.admin', raise_exception=True)  # 必须是【天马赛事】应用平台的管理员
def horse_authorize_update(request, pk):
    data = dict()
    horse = get_object_or_404(Horse, pk=pk)
    if request.method == 'POST':
        form = HorseAuditForm(request.POST, instance=horse)
        if form.is_valid():
            mobile_num = horse.user.mobile
            # print(horse.user.mobile)
            # 审核通过发送短信
            if 'qualified' in request.POST and request.POST['qualified'] == "on":
                # print(member.contact_number)
                send_text_msg(str(mobile_num), "<马匹资格审核>通过")
            else:
                send_text_msg(str(mobile_num), "<马匹资格审核>,反馈信息:" + request.POST['qualify_msg'])
            form.save()
            data['form_is_valid'] = True
            horses = Horse.objects.all()
            data['html_list'] = render_to_string('racing/enrollment/horse_authorize_list_partial.html', {
                'object_list': horses
            })
        else:
            data['form_is_valid'] = False
    else:
        horse.qualify_msg=""
        form = HorseAuditForm(instance=horse)
    context = {'form': form}
    data['html_form'] = render_to_string(
        'racing/enrollment/horse_authorize_update.html',
        context,
        request=request)
    return JsonResponse(data)


# ********************************************************
# 参赛项目报名，参赛队伍操作
# ********************************************************

# 参赛队伍比赛项报名
@login_required  # 首先必须是登录用户
@permission_required('racing.contestant', raise_exception=True)  # 必须是参赛队伍
def team_game_enrollment(request):
    # 获取当前激活的赛事对象，要求Regulation只有一条激活的记录，否则会发生错误
    regulation = Regulation.objects.get(active=True)
    # 获取当前用户的实名认证对象
    certification = Certification.objects.get(user=request.user)
    # 获取当前用户的会员对象
    member = Member.objects.get(user=certification)

    # 如果为首次报名则创建一个Enrollment对象，否则直接取出报名对象
    try:
        enrollment = Enrollment.objects.get(year=regulation, member=member)
    except Enrollment.DoesNotExist:
        enrollment = Enrollment(year=regulation, member=member)
        enrollment.save()

    context = {
        "enrollment": enrollment,
    }
    return render(request, "racing/enrollment/team_game_enrollment.html", context)


# 添加比赛项目
@login_required  # 首先必须是登录用户
@permission_required('racing.contestant', raise_exception=True)  # 必须是参赛队伍
def enrollment_team_game_create(request, enrollment_id):
    data = dict()
    enrollment = Enrollment.objects.get(id=enrollment_id)
    if request.method == 'POST':
        form = EnrollmentDetailForm(request.POST, initial={'enrollment': enrollment})
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            enrollment = Enrollment.objects.get(id=enrollment_id)
            data['html_list'] = render_to_string('racing/enrollment/team_game_enrollment_partial.html', {
                'enrollment': enrollment
            }, request=request)
        else:
            data['form_is_valid'] = False
    else:
        print(enrollment)
        form = EnrollmentDetailForm(initial={'enrollment': enrollment})
    context = {
        'form': form,
        'enrollment': enrollment,
    }
    data['html_form'] = render_to_string(
        'racing/enrollment/team_game_create.html',
        context,
        request=request)
    return JsonResponse(data)


'''
# 添加比赛项目
class TeamGameCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'racing/enrollment/team_game_create.html'
    model = EnrollmentDetail
    form_class = EnrollmentDetailForm
    permission_object = None
    permission_required = ['racing.contestant']
    success_url = '/racing/team_game_enrollment'
    raise_exception = True

    # 新纪录初值处理，尤其是enrollment外键的值
    def get_initial(self):
        initials = super(TeamGameCreateView, self).get_initial()
        enrollment = Enrollment.objects.get(pk=self.kwargs['enrollment_id'])
        initials['enrollment'] = enrollment  # 为新建对象的外键enrollment属性指定当前Enrollment对象
        initials['type'] = 1  # 缺省类型为【参赛马匹】
        return initials

    def post(self, request, *args, **kwargs):
        data = dict()
        form = self.form_class
        if self.form.is_valid():
            form.save()
            data['form_is_valid'] = True
            enrollment = self.initial.get('enrollment')
            data['html_list'] = render_to_string('racing/enrollment/team_game_enrollment_partial.html', {
                'enrollment': enrollment
            }, request=self.request)
        else:
            data['form_is_valid'] = False
        return JsonResponse(data)
    def form_valid(self, form):
        data = dict()
        print("form_valid")
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            enrollment = self.initial.get('enrollment')
            data['html_list'] = render_to_string('racing/enrollment/team_game_enrollment_partial.html', {
                'enrollment': enrollment
            }, request=self.request)
        else:
            data['form_is_valid'] = False
        return JsonResponse(data)


enrollment_team_game_create = TeamGameCreateView.as_view()
'''


# 删除比赛项目
@login_required  # 首先必须是登录用户
@permission_required('racing.contestant', raise_exception=True)  # 必须是参赛队伍
def enrollment_team_game_delete(request, pk):
    enrollment_detail = get_object_or_404(EnrollmentDetail, pk=pk)
    enrollment = enrollment_detail.enrollment
    data = dict()
    if request.method == 'POST':
        enrollment_detail.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        data['html_list'] = render_to_string('racing/enrollment/team_game_enrollment_partial.html',
                                             {'enrollment': enrollment},
                                             request=request)
    else:
        context = {'enrollment_detail': enrollment_detail}
        data['html_form'] = render_to_string('racing/enrollment/team_game_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


'''
# 删除比赛项目
class TeamGameDeleteView(PermissionRequiredMixin, DeleteView):
    model = EnrollmentDetail
    success_url = '/racing/team_game_enrollment'
    permission_required = ['racing.contestant']
    raise_exception = True
    template_name = 'racing/enrollment/team_game_delete.html'


enrollment_team_game_delete = TeamGameDeleteView.as_view()
'''


# 报名锁定

@permission_required('racing.can_change_enrollment_fixed', fn=objectgetter(Enrollment, 'pk'),
                     raise_exception=True)  # 报名尚未被管理员核准
@permission_required('racing.contestant', raise_exception=True)  # 必须是参赛队伍
@login_required  # 首先必须是登录用户
def enrollment_fixed(request, pk):
    data = dict()
    enrollment = get_object_or_404(Enrollment, pk=pk)
    if request.method == 'POST':
        form = EnrollmentFixedForm(request.POST,
                                   instance=enrollment)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            data['html_list'] = render_to_string('racing/enrollment/team_game_enrollment_partial.html',
                                                 {'enrollment': enrollment},
                                                 request=request)
        else:
            data['form_is_valid'] = False
    else:

        form = EnrollmentFixedForm(instance=enrollment)

    context = {'form': form}
    data['html_form'] = render_to_string(
        'racing/enrollment/team_game_enrollment_fixed.html',
        context,
        request=request)
    return JsonResponse(data)


# ********************************************************
# 报名项目审核，只允许管理员操作
# ********************************************************

# （1）、报名项目列表
class EnrollmentAuthorizeListView(PermissionRequiredMixin, ListView):
    model = Enrollment
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_authorize_list.html'
    # 只显示已经锁定的报名
    queryset = Enrollment.objects.filter(enrollment_fixed=True)


enrollment_authorize = EnrollmentAuthorizeListView.as_view()


# （2）、报名项目详情
class EnrollmentAuthorizeDetailView(PermissionRequiredMixin, DetailView):
    model = Enrollment
    permission_required = ['racing.admin', ]
    raise_exception = True
    template_name = 'racing/enrollment/enrollment_authorize_detail.html'


enrollment_authorize_detail = EnrollmentAuthorizeDetailView.as_view()


@login_required  # 首先必须是登录用户
@permission_required('racing.admin', raise_exception=True)  # 必须是【天马赛事】应用平台的管理员
@permission_required('racing.can_qualify_enrollment', fn=objectgetter(Enrollment, 'eid'), raise_exception=True)  # 必须允许核准
def confirmPayment(request,eid):
    rs={"status":0,"msg":"确认失败！"}

    if request.method == "POST":
        enrollment=Enrollment.objects.get(id=eid)
        for item in enrollment.enrollmentdetail_set.all():
            if EnrollmentDetail.objects.filter(game=item.game, \
                                               horse=item.horse, enrollment__fare_paid=1).count() > 0:
                rs['msg'] = "马匹\"" + item.horse.name + "\"已经在比赛\"" + item.game.name + "\"中被其他队伍使用了！"
                return JsonResponse(rs)
            if EnrollmentDetail.objects.filter(game=item.game, \
                                               horseman=item.horseman, enrollment__fare_paid=1).count() > 0:
                rs['msg'] = "骑师\"" + item.horseman.user.userName + "\"已经在比赛\"" + item.game.name + "\"中被其他队伍使用了！"
                return JsonResponse(rs)

        if enrollment.year.active == 1 and \
                        enrollment.fare > 0 and \
                        enrollment.member.qualified == 1:

            #enrollment.enrollment_fixed = 1
            #enrollment.qualified = 1
            enrollment.fare_paid = 1
            enrollment.save()
            rs['status'] = 1
        elif enrollment.fare <= 0 or \
                        enrollment.discipline_number <= 0:
            rs['msg'] = "请先进行参赛报名!"

        return JsonResponse(rs)
    else:
        return JsonResponse(rs)



# （3）、报名项目审核
@login_required  # 首先必须是登录用户
@permission_required('racing.admin', raise_exception=True)  # 必须是【天马赛事】应用平台的管理员
@permission_required('racing.can_qualify_enrollment', fn=objectgetter(Enrollment, 'pk'), raise_exception=True)  # 必须允许核准
def enrollment_authorize_update(request, pk):
    data = dict()
    enrollment = get_object_or_404(Enrollment, pk=pk)
    if request.method == 'POST':
        form = EnrollmentAuditForm(request.POST, instance=enrollment)
        if form.is_valid():
            mobile_num = enrollment.member.user.mobile
            # 审核通过发送短信
            if 'qualified' in request.POST and request.POST['qualified'] == "on":
                # print(member.contact_number)
                send_text_msg(str(mobile_num), "<报名资格核准>通过")
            else:
                send_text_msg(str(mobile_num), "<报名资格核准>,反馈信息:" + request.POST['qualify_msg'])
            form.save()
            data['form_is_valid'] = True
            enrollments = Enrollment.objects.filter(enrollment_fixed=True)
            data['html_list'] = render_to_string(
                'racing/enrollment/enrollment_authorize_list_partial.html',
                {'object_list': enrollments})
        else:
            data['form_is_valid'] = False
    else:
        enrollment.qualify_msg=""
        form = EnrollmentAuditForm(instance=enrollment)
    context = {'form': form}
    data['html_form'] = render_to_string(
        'racing/enrollment/enrollment_authorize_update.html',
        context,
        request=request)
    return JsonResponse(data)


# ********************************************************
# 通过支付宝支付马匹出赛费
# ********************************************************
# 调用支付宝进行支付
@login_required  # 首先必须是登录用户
@permission_required('racing.contestant', raise_exception=True)  # 必须是【天马赛事】应用平台的参赛队伍
def pay(request, payment_uuid):
    enrollment = get_object_or_404(Enrollment, payment_uuid=payment_uuid)
    # 获取马匹出赛费支付宝配置参数
    alipay_setting = AliPaySetting.objects.get(id=2)
    # 创建支付宝工具对象
    alipay = AliPay(
        # 沙盒测试appid， 正式上线需要替换为蚂蚁金服开放平台（open.alipay.com）开发者中心创建的应用唯一标识（APPID）
        appid=alipay_setting.app_id,
        # 处理支付宝返回结果的URL
        app_notify_url=alipay_setting.notify_url,
        # 应用私钥（目前使用沙盒测试应用私钥）
        app_private_key_string=alipay_setting.app_private_key,
        # 支付宝应用公钥（目前使用沙盒测试应用公钥）
        alipay_public_key_string=alipay_setting.alipay_public_key,
        # 签名算法类型：RSA 或者 RSA2
        sign_type=alipay_setting.sign_type,
        # 默认为False，这里设为True是为了配合沙箱模式使用
        debug=alipay_setting.debug_mode
    )

    order_string = alipay.api_alipay_trade_page_pay(
        out_trade_no=str(enrollment.payment_uuid),
        total_amount=str(enrollment.fare),
        subject="天马赛事马匹出赛费",
        # 支付完成后跳转的页面
        return_url=alipay_setting.return_url,
        notify_url=alipay_setting.notify_url  # 可选，None表示使用默认的notify url
    )

    # 电脑网站支付，需要跳转到https://openapi.alipay.com/gateway.do? + order_string
    # 沙盒测试，需要跳转到https://openapi.alipaydev.com/gateway.do? + order_string
    alipay_url = alipay_setting.gateway
    url = alipay_url + "?" + order_string

    return HttpResponseRedirect(url)


###################################################################
# 支付宝同步通知处理视图
# 通过安全验证之后跳转到【已购门票】页面
# 只适用沙盒测试调试模式
# 实际生产环境应当使用支付宝异步通知处理视图
###################################################################
@csrf_exempt
@login_required  # 首先必须是登录用户
@permission_required('racing.contestant', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def alipay_return_url(request):
    # 签名验证
    if return_verify(request):
        # 商户网站支付单号
        pay_id = request.GET.get('out_trade_no')

        # 修改支付状态
        enrollment = Enrollment.objects.get(payment_uuid=pay_id)
        enrollment.fare_paid = True
        enrollment.save()

    # 跳转到【已购门票】页面
    return HttpResponseRedirect('/racing/team_game_enrollment')


# 支付宝同步通知签名验证，防止黑客攻击
def return_verify(request):
    # 获取马匹出赛费支付宝配置参数
    alipay_setting = AliPaySetting.objects.get(id=2)

    # 创建支付宝工具对象
    alipay = AliPay(
        # 沙盒测试appid， 正式上线需要替换为蚂蚁金服开放平台（open.alipay.com）开发者中心创建的应用唯一标识（APPID）
        appid=alipay_setting.app_id,
        # 处理支付宝返回结果的URL
        app_notify_url=alipay_setting.notify_url,
        # 应用私钥（目前使用沙盒测试应用私钥）
        app_private_key_string=alipay_setting.app_private_key,
        # 支付宝应用公钥（目前使用沙盒测试应用公钥）
        alipay_public_key_string=alipay_setting.alipay_public_key,
        # 签名算法类型：RSA 或者 RSA2
        sign_type=alipay_setting.sign_type,
        # 默认为False，这里设为True是为了配合沙箱模式使用
        debug=alipay_setting.debug_mode
    )

    # 获取支付宝同步通知传来的参数数据，转换为一个字典
    data = {x: request.GET.get(x) for x in request.GET.keys()}
    signature = data.pop("sign")

    # 签名验证
    success = alipay.verify(data, signature)
    if success:
        return True
    else:
        return False


###################################################################
# 支付宝异步通知处理视图
# 实际生产环境应当使用支付宝异步通知处理视图
###################################################################
@csrf_exempt
#@login_required  # 首先必须是登录用户
#@permission_required('racing.contestant', raise_exception=True)  # 必须是【天马赛事】应用平台的参赛队伍
def alipay_notify_url(request):
    if notify_verify(request):
        # 商户网站支付单号
        pay_id = request.POST.get('out_trade_no')

        # 修改支付状态racing_alipayorder
        enrollment = Enrollment.objects.get(payment_uuid=pay_id)
        enrollment.fare_paid = True
        enrollment.save()

        # 向支付宝返回交易完成标志
        return HttpResponse("success")
    else:
        # 向支付宝返回交易失败标志
        return HttpResponse("fail")

    # 支付宝异步通知签名验证，防止黑客攻击


def notify_verify(request):
    # 支付宝异步通知使用的是POST方法
    if request.method == 'POST':
        # 获取马匹出赛费支付宝配置参数
        alipay_setting = AliPaySetting.objects.get(id=2)
        # 创建支付宝工具对象
        alipay = AliPay(
            # 沙盒测试appid， 正式上线需要替换为蚂蚁金服开放平台（open.alipay.com）开发者中心创建的应用唯一标识（APPID）
            appid=alipay_setting.app_id,
            # 处理支付宝返回结果的URL
            app_notify_url=alipay_setting.notify_url,
            # 应用私钥（目前使用沙盒测试应用私钥）
            app_private_key_string=alipay_setting.app_private_key,
            # 支付宝应用公钥（目前使用沙盒测试应用公钥）
            alipay_public_key_string=alipay_setting.alipay_public_key,
            # 签名算法类型：RSA 或者 RSA2
            sign_type=alipay_setting.sign_type,
            # 默认为False，这里设为True是为了配合沙箱模式使用
            debug=alipay_setting.debug_mode
        )

        # 获取支付宝异步通知传来的参数数据，转换为一个字典
        data = {x: request.POST.get(x) for x in request.POST.keys()}
        signature = data.pop("sign")

        # 签名验证
        success = alipay.verify(data, signature)
        # 签名验证通过且交易码为【成功】或【完成】
        if success and data["trade_status"] in ("TRADE_SUCCESS", "TRADE_FINISHED"):
            return True
        else:
            return False
    else:
        # 如果不是POST方法，很可能是恶意攻击
        return False
