from django.contrib import admin

from racing.models import Role, Permission, EnrollmentWorkflow, AliPaySetting

from racing.models_home import Carousel, EventsAnnouncement, Video
from racing.models_regulation import Regulation
from racing.models_enrollment import Association, Member, Horseman, Horse, Enrollment, EnrollmentDetail, Game, GameDay, \
    Referee
from racing.models_ticket import Product, Order, OrderDetail


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 1、首页轮播照片
class CarouselAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'slogan', 'ranking', 'photo', 'enabled', 'active')


# 2、赛事年度
class RegulationAdmin(admin.ModelAdmin):
    list_display = ('id', 'year', 'name', 'active')


# 3、比赛日
class GameDayAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'year')


# 4、比赛项目
class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'time', 'active')


# 5、赛事公告
class EventsAnnouncementAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'created', 'enabled')


# 6、比赛视频
class VideoAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'ranking', 'duration', 'enabled')


# 7、赛事平台用户角色
class RoleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')


# 8、赛事平台用户权限
class PermissionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'role')


# 9、协会
class AssociationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


# 10、协会会员单位
class MemberAdmin(admin.ModelAdmin):
    list_display = ('user','member_name', 'member_type', 'contact', 'contact_number', 'qualified')


# 11、骑师
class HorsemanAdmin(admin.ModelAdmin):
    list_display = ('user', 'member', 'qualified', 'height', 'weight')


# 12、马匹
class HorseAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'name', 'gender', 'birthday', 'qualified')


# 13、比赛报名
class EnrollmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'year', 'member', 'discipline_number', 'horse_number', 'horseman_number', 'fare')


# 14、比赛报名明细
class EnrollmentDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'enrollment', 'game', 'horse', 'horseman', 'type')


# 15、门票产品
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'game_day', 'ticket_type', 'price', 'stock', 'sold')


# 16、门票订单
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'created', 'paid')


# 17、门票订单明细
class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'product', 'quantity')


# 18、门票订单明细
class EnrollmentWorkflowAdmin(admin.ModelAdmin):
    list_display = ('id', 'role', 'step', 'step_name')

class RefereeAdmin(admin.ModelAdmin):
    pass

class AliPaySettingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Carousel, CarouselAdmin)  # 1、首页轮播照片
admin.site.register(Regulation, RegulationAdmin)  # 2、赛事规程
admin.site.register(GameDay, GameDayAdmin)  # 3、比赛日
admin.site.register(Game, GameAdmin)  # 4、比赛项目
admin.site.register(EventsAnnouncement, EventsAnnouncementAdmin)  # 5、赛事公告
admin.site.register(Video, VideoAdmin)  # 6、比赛视频
admin.site.register(Role, RoleAdmin)  # 7、赛事平台用户角色
admin.site.register(Permission, PermissionAdmin)  # 8、赛事平台用户权限
admin.site.register(Association, AssociationAdmin)  # 9、协会
admin.site.register(Member, MemberAdmin)  # 10、协会会员单位
admin.site.register(Horseman, HorsemanAdmin)  # 11、骑师
admin.site.register(Horse, HorseAdmin)  # 12、马匹
admin.site.register(Enrollment, EnrollmentAdmin)  # 13、比赛报名
admin.site.register(EnrollmentDetail, EnrollmentDetailAdmin)  # 14、比赛报名
admin.site.register(Product, ProductAdmin)  # 15、门票产品
admin.site.register(Order, OrderAdmin)  # 16、门票订单
admin.site.register(OrderDetail, OrderDetailAdmin)  # 17、门票订单明细
admin.site.register(EnrollmentWorkflow, EnrollmentWorkflowAdmin)  # 18、门票订单明细

admin.site.register(Referee, RefereeAdmin)
admin.site.register(AliPaySetting, AliPaySettingAdmin)  # 18、门票订单明细

