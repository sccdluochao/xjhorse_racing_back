import datetime
import uuid

import qrcode
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from io import BytesIO

from django.views.decorators.csrf import csrf_exempt
from rules.contrib.views import permission_required
from django.shortcuts import render, get_object_or_404, redirect

from certification.decorator import certificate_required
from certification.models import Certification
from racing.models import Regulation
from racing.models_ticket import Product, Order, OrderDetail, AliPay, AliPayOrder, TemporaryTicket
from racing.forms_cart import CartAddProductForm
from racing.cart import Cart
from django.utils.translation import ugettext as _


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 票务服务


def ticket(request):
    try:
        regulation = Regulation.objects.get(active=True)
        products = Product.objects.filter(available=True, game_day__year__active=True)
    except Regulation.DoesNotExist:
        regulation = None
    context = {
        "regulation": regulation,
        'products': products,
    }
    return render(request, "racing/ticket/ticket.html", context)


# 门票明细
def ticket_detail(request, pk):
    cart_product_form = CartAddProductForm()
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    product = get_object_or_404(Product, pk=pk)

    context = {
        'regulation': regulation,
        'product': product,
        'cart_product_form': cart_product_form,
    }
    return render(request, "racing/ticket/ticket_detail.html", context)


# 创建订单
@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def order_create(request):
    cart = Cart(request)

    # 创建订单对象
    certification = get_object_or_404(Certification, user=request.user)
    my_order = Order(user=certification)
    my_order.save()

    # 将购物车的内容添加到订单明细中
    for item in cart:
        OrderDetail.objects.create(
            order=my_order,
            product=item['product'],
            price=item['price'],
            quantity=item['quantity']
        )
    # 清除购物车
    cart.clear()

    # 跳转到订单页面
    return redirect('order')


# 订单
@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def order(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None

    certification = get_object_or_404(Certification, user=request.user)
    # orders = Order.objects.filter(paid=False, user=certification)
    payment_order_id_list = AliPayOrder.objects.filter(pay__user=certification).values_list(
        'order_id', flat=True)
    orders = Order.objects.filter(user=certification).exclude(id__in=payment_order_id_list)
    total_amount = sum(order.get_total_cost() for order in orders)
    total_item = sum(order.get_unpaid_items() for order in orders)
    context = {
        'regulation': regulation,
        'orders': orders,
        'total_amount': total_amount,
        'total_item': total_item,
    }
    return render(request, "racing/ticket/order.html", context)


@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def order_remove(request, order_id):
    my_order = get_object_or_404(Order, id=order_id)
    my_order.delete()
    return redirect('order')

# 创建结账支付单
@login_required  # 首先必须是登录用户
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def payment_create(request):
    # 创建支付宝交易对象
    certification = get_object_or_404(Certification, user=request.user)
    payment_order_id_list = AliPayOrder.objects.filter(pay__user=certification).values_list(
        'order_id', flat=True)
    orders = Order.objects.filter(user=certification).exclude(id__in=payment_order_id_list)
    total_amount = sum(order.get_total_cost() for order in orders)
    total_item = sum(order.get_unpaid_items() for order in orders)

    certification = get_object_or_404(Certification, user=request.user)
    alipay = AliPay(user=certification, total_amount=total_amount, total_item=total_item)
    alipay.save()

    # 将为结账的订单添加到支付宝交易明细中
    for order in orders:
        AliPayOrder.objects.create(
            pay=alipay,
            order=order,
        )
    # 跳转到订单页面
    return redirect('payment')

# 结账支付单
@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def payment(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    # 创建支付宝交易对象
    certification = get_object_or_404(Certification, user=request.user)
    alipays = AliPay.objects.filter(user=certification, paid=False)
    total_amount = sum(alipay.total_amount for alipay in alipays)
    total_item = sum(alipay.total_item for alipay in alipays)
    context = {
        'regulation': regulation,
        'alipays': alipays,
        'total_item': total_item,
        'total_amount': total_amount,
    }
    return render(request, "racing/ticket/payment.html", context)


@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def payment_remove(request, pk):
    my_alipay = get_object_or_404(AliPay, id=pk)
    my_alipay.delete()
    return redirect('payment')


@login_required  # 首先必须是登录用户
@certificate_required('3')
#@permission_required('racing.paid_attendance', raise_exception=True)  # 必须是【天马赛事】应用平台的购票观众
def paid(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    # 创建支付宝交易对象
    certification = get_object_or_404(Certification, user=request.user)
    alipays = AliPay.objects.filter(user=certification, paid=True)
    total_amount = sum(alipay.total_amount for alipay in alipays)
    total_item = sum(alipay.total_item for alipay in alipays)
    context = {
        'regulation': regulation,
        'alipays': alipays,
        'total_item': total_item,
        'total_amount': total_amount,
    }
    return render(request, "racing/ticket/paid.html", context)


#获取电子票验证码
@login_required  # 首先必须是登录用户
@certificate_required('3')
def ticket_qrcode(request):

    #先判断是否有还在有效期的电子票
    ttlist=TemporaryTicket.objects.filter(
        user=request.user.certification,
        endtime__gte=timezone.now()
    )

    #删除掉所有
    if ttlist.count()>0:
        for item in ttlist:
            item.delete()

    #生成电子票识别码
    ticket_code = str(uuid.uuid1())
    #新增电子票
    tt=TemporaryTicket.objects.create(
        user=request.user.certification,
        ticketcode=ticket_code
    )
    tt.save()

    #生成返回链接
    data="tmtk"+ticket_code
    #生成二维码
    img = qrcode.make(data)
    buf = BytesIO()
    img.save(buf)
    image_stream = buf.getvalue()

    return HttpResponse(image_stream, content_type="image/png")

#电子码显示页面
@login_required  # 首先必须是登录用户
@certificate_required('3')
def ticket_qrcode_page(request):

    #判断用户是否有购票
    odlist=OrderDetail.objects.filter(
        order__user=request.user.certification,
        order__paid=1)

    context={"has_ticket":1}

    if odlist.count() == 0:
        context['has_ticket']=0

    return render(request,"racing/ticket/electronic_ticket.html",context)

#获取当前所在日期的票务
def get_today_tickets(request,ticket_code):
    rs={"status":0,"msg":""}

    #获取存在的列表
    ttlist=TemporaryTicket.objects.filter(
        ticketcode=ticket_code,
        endtime__gte=timezone.now()
    )

    #存在表示可用
    if ttlist.count() == 1:
        rs['status']=1
        rs['username']=ttlist[0].user.userName
        rs['phone']=ttlist[0].user.mobile
        #获取已支付，未使用的门票
        odlist=OrderDetail.objects.filter(
            order__user=ttlist[0].user,
            order__paid=1
        ).order_by('order__created')

        useful_od_list=[]
        for item in odlist:
            if item.product.game_day.year.active == True:
                od_dict={}
                od_dict['id']=item.id
                od_dict['type']=item.product.ticket_type
                od_dict['name']=item.product.game_day.name
                od_dict['quantity']=item.quantity
                od_dict['isused'] = item.isused
                od_dict['year'] = item.product.game_day.year.year
                useful_od_list.append(od_dict)
        rs['odlist']=useful_od_list
    else:
        rs['msg']=_("Invalid Code")
    return JsonResponse(rs)

#使用电子票
@csrf_exempt
def use_ticket(request):
    rs = {"status": 0, "msg": ""}
    if request.method == "POST":

        od=OrderDetail.objects.get(id=request.POST['t_id'])
        od.isused=1
        od.save()

        rs['status']=1
        rs['t_position']=request.POST['t_position']
        return JsonResponse(rs)
    else:
        return JsonResponse(rs)

