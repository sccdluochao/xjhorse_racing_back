import datetime
import json
import random
import uuid

import re
from django.core.exceptions import ValidationError
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
import os
from certification.models import Certification, Corporation
from racing.models_regulation import Regulation


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 重复上传照片时，用新照片覆盖老照片
class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return super(OverwriteStorage, self).get_available_name(name, max_length)


# 骑师蓝底免冠照片
def upload_horseman_photo(instance, filename):
    return "racing/horseman/%s" % filename


# 骑师健康证照片
def upload_health_certificate(instance, filename):
    return "racing/horseman/helipaysettingalth_certificate/%s" % filename


# 马匹照片
def upload_horse_photo(instance, filename):
    return "racing/horse/%s" % filename


# 裁判证照片
def upload_referee_licence_photo(instance, filename):
    return "racing/enrollment/referee_licence/%s" % filename


class HorseType(models.Model):
    type_name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("Horse Type")  # 马匹类型
    )

    class Meta:
        verbose_name = _('Horse Type')
        verbose_name_plural = _('Horse Type')

    def __str__(self):
        return self.type_name


# 3、比赛日
class GameDay(models.Model):
    name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Game Day")  # 比赛日
    )
    year = models.ForeignKey(
        Regulation,
        null=False,
        blank=False,
        verbose_name=_('Game Year')
    )

    class Meta:
        verbose_name = _('Game Day')
        verbose_name_plural = _('Game Days')

    def __str__(self):
        return self.name

# 比赛状态类型
Game_Status = (
    (1, "参赛报名中"),  #
    (2, "比赛中"),  #
    (3, "比赛成绩公布"),  #
)
# 4、比赛项目
class Game(models.Model):
    name = models.CharField(  # 比赛项目名称
        max_length=300,
        null=False,
        blank=False,
        verbose_name=_("Game")
    )
    fare_rate = models.DecimalField(  # 出赛费比率
        max_digits=3,
        decimal_places=2,
        null=False,
        blank=-False,
        default=0.01,
        verbose_name=_('Fare Rate')
    )
    game_day = models.ForeignKey(  # 比赛日
        GameDay,
        null=False,
        blank=False,
        verbose_name=_('Game Day')
    )
    horse_type = models.ForeignKey(  # 马匹类型
        HorseType,
        null=False,
        blank=False,
        default=1,
        verbose_name=_('Horse Type')
    )
    racing_horse_limit = models.SmallIntegerField(
        blank=False,
        null=False,
        default=3,
        verbose_name=_('Racing Horse Limit')
    )
    backup_horse_limit = models.SmallIntegerField(
        blank=False,
        null=False,
        default=0,
        verbose_name=_('Backup Horse Limit')
    )
    horse_birthday_begin = models.DateField(
        blank=True,
        null=True,
        verbose_name=_('Horse Birthday Begin')
    )
    horse_birthday_end = models.DateField(
        blank=True,
        null=True,
        verbose_name=_('Horse Birthday End')
    )
    time = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("Time")  # 比赛时间
    )
    prize = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_('Prize')
    )
    prize_comment = models.TextField(
        null=False,
        blank=False,
        verbose_name=_('Prize Comment')
    )

    year = models.ForeignKey(
        Regulation,
        null=False,
        blank=False,
        verbose_name=_('Game Year')
    )

    referees = models.ManyToManyField(
        "Referee",
        limit_choices_to={"referee_qualified": 1},
        blank=True,
        verbose_name=_('Referee')
    )

    active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("Active Game")  # 有效状态
    )

    """
    比赛状态
    """
    game_status=models.IntegerField(
        null=False,
        blank=True,
        choices=Game_Status,
        default=1,
        verbose_name=_("Game Status")  # 有效状态
    )

    class Meta:
        verbose_name = _('Game')
        verbose_name_plural = _('Games')

    def __str__(self):
        return self.name+"-"+\
               "("+str(self.horse_birthday_begin)\
               +"至"+str(self.horse_birthday_end)+")"+"-"+str(self.horse_type)

    #获取表达式
    def get_prize_comment_expression(self):
        prize_comment_str = ""
        try:
            for key, value in eval(self.prize_comment).items():
                prize_comment_str+=key+"="
                counter=0
                for item in value:
                    counter+=1
                    if len(value) == counter:
                        prize_comment_str += str(item)
                    else:
                        prize_comment_str+=str(item)+"+"
                prize_comment_str += "\r\n"
        except Exception:
            return self.prize_comment
        return prize_comment_str

    #获取奖励比例
    def get_prize_comment(self):
        prize_comment_str = ""
        counter=1
        try:
            for key,value in eval(self.prize_comment).items():
                if counter == 1:
                    prize_comment_str += str(counter) + "、当参赛数量超过%d" % int(key) + "个时,"
                else:
                    prize_comment_str+="<br>"+str(counter)+"、当参赛数量超过%d"%int(key)+"个时,"
                prize_comment_str += "前%d" % len(value) + "名:"
                for item in value:
                    prize_comment_str+=str(item)+"% "
                counter+=1
            prize_comment_str += "。"
        except Exception:
            return "有错误！请修改！"
        return prize_comment_str

    #对数据库的字段进行验证
    def clean(self):
        super(Game, self).clean()

        #1.分割
        prize_comment_list=str(self.prize_comment).split("\r\n")
        #2.去空
        for item in prize_comment_list:
            if item == "":
                prize_comment_list.remove(item)
        prize_divide_key=[]
        prize_divide_dict = {}
         #3.验证格式
        for item in prize_comment_list:
            #验证格式是否正确
            if re.match(r'^\d+=\d{1,2}([\+]{1}[\d]{1,2})+$', item):
                if item.split("=")[0] in prize_divide_key:
                    raise ValidationError(item+"")
                prize_divide_key.append(item.split("=")[0])
                proportions=[]
                #第一个比例最大
                max=int(item.split("=")[1].split("+")[0])
                sum=0
                for proportion in item.split("=")[1].split("+"):
                    #求所有占比之和
                    sum+=int(proportion)
                    proportions.append(int(proportion))
                    #前面的比例必须大于后面的比例
                    if max<int(proportion):
                        raise ValidationError(item+"中，占比大小顺序有误！请检查!")
                    max=int(proportion)
                #占比之和必须是100
                if sum != 100:
                    raise ValidationError(item+"中，比例之和不等于100！请检查后再重试！")
                prize_divide_dict[item.split("=")[0]]=proportions
            else:
                raise ValidationError(item+"验证未通过！")
        #将表达式，更换为字典
        self.prize_comment=prize_divide_dict

    # 添加新对象成功后跳转的URL
    def get_absolute_url(self):
        return reverse('game_manage_detail', kwargs={'pk': self.id})

    def get_enrollment_group(self):
        data = dict()
        group_list = EnrollmentDetail.objects.filter(game__active=True, game=self,
                                                     enrollment__fare_paid=True).values_list(
            'group_no', flat=True)
        groups = list(set(group_list))
        for group in groups:
            enrollment_details = EnrollmentDetail.objects.filter(game__active=True, game=self, group_no=group)
            if enrollment_details.count() > 0:
                data[group] = enrollment_details
        return data

    # 获取裁判
    def get_referees(self):
        if self.referees.count() > 0:
            refereenames = ""
            for item in self.referees.all():
                refereenames = refereenames + item.user.userName + " "
            return refereenames
        else:
            return ""


# 协会
class Association(models.Model):
    name = models.CharField(
        max_length=300,
        null=False,
        blank=False,
        verbose_name=_("Association Name")
    )

    class Meta:
        verbose_name = _('Association')
        verbose_name_plural = _('Associations')

    def __str__(self):
        return self.name


# 会员类型
MEMBER_TYPE = (
    (1, _("Individual Member")),  # 个人会员
    (2, _("Corporation Member"))  # 单位会员
)


# 2、协会会员
class Member(models.Model):
    user = models.OneToOneField(  # 实名认证用户
        Certification,
        primary_key=True,
        verbose_name=_("Member")
    )
    member_name=models.CharField(
        null=False,
        blank=False,
        max_length=100,
        unique=True,
        verbose_name=_("Member Name")
    )
    member_type = models.IntegerField(  # 性别
        choices=MEMBER_TYPE,
        default=1,
        verbose_name=_("Member Type")
    )

    contact = models.CharField(  # 联系人
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Contact")
    )
    contact_number = models.CharField(  # 联系电话
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Contact Number")
    )
    association = models.ManyToManyField(
        Association,
        blank=True,
        verbose_name=_("Association")
    )
    trainer = models.CharField(  # 驯马师
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Trainer")
    )
    trainer_number = models.CharField(  # 驯马师电话
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Trainer Number")
    )

    veterinarian = models.CharField(  # 兽医
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Veterinarian")
    )
    veterinarian_number = models.CharField(  # 兽医电话
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Veterinarian Number")
    )
    qualified = models.BooleanField(  # 符合会员资格
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Member Qualified")
    )

    qualify_msg = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_("反馈信息(如果不符合，请填入原因，并按确定键！)")
    )

    class Meta:
        verbose_name = _('Member')
        verbose_name_plural = _('Members')

    def __str__(self):
        if self.member_name == None:
            return "无队伍名"
        return self.member_name

    def get_member_type(self):
        if self.member_type == 1:
            return _("Individual Member")
        else:
            return _("Corporation Member")

    def get_corporation(self):
        try:
            corporation = Corporation.objects.get(admin=self.user.user)
            return corporation
        except Corporation.DoesNotExist:
            return None

    def get_associations(self):
        associations = Association.objects.filter(member=self)
        return "<br>".join(association.name for association in associations)

    def get_member_name(self):
        if self.member_type == 1:
            return self.user.userName
        else:
            return self.get_corporation().name

    def get_enrollment(self):
        enrollment = Enrollment.objects.get(member=self, year__active=True, fare_paid=True)
        return enrollment

    def get_enrollment_detail(self):
        #enrollment = Enrollment.objects.get(member=self, year__active=True, fare_paid=True)
        enrollment = Enrollment.objects.filter(member=self, year__active=True, fare_paid=True)
        if enrollment.count() > 0:
            enrollment_detail = EnrollmentDetail.objects.filter(enrollment=enrollment)
        else:
            enrollment_detail = EnrollmentDetail.objects.filter(id=0)
        print(enrollment_detail.count())
        return enrollment_detail


# 3、骑师
class Horseman(models.Model):
    '''
    姓名、性别、民族、出生年月、身份证号、联系电话在实名认证（Certification）模型中
    '''
    user = models.OneToOneField(  # 用户ID, 主键
        Certification,
        blank=True,
        primary_key=True,
        verbose_name=_("UserID")
    )

    member = models.ForeignKey(
        Member,
        null=True,
        blank=True,
        verbose_name=_("Team")
    )

    qualified = models.BooleanField(  # 符合报名要求
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Horseman Qualified")
    )

    qualify_msg = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_("反馈信息(如果不符合，请填入原因，并按确定键！)")
    )

    horseman_photo = models.ImageField(  # 骑师蓝底免冠照片
        upload_to=upload_horseman_photo,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Horseman Photo")
    )

    height = models.SmallIntegerField(  # 身高
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Height")
    )

    weight = models.SmallIntegerField(  # 体重
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Weight")
    )

    native_place = models.CharField(  # 籍贯
        max_length=300,
        null=True,
        blank=True,
        verbose_name=_('Native Place')
    )

    employer = models.CharField(  # 工作单位
        max_length=300,
        null=False,
        blank=False,
        verbose_name=_('Employer')
    )

    health_certificate_photo = models.ImageField(  # 健康证明照片
        upload_to=upload_health_certificate,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Health Certificate Photo")
    )

    record = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Record")
    )

    class Meta:
        verbose_name = _('Horseman')
        verbose_name_plural = _('Horsemen')

    def __str__(self):
        if self.user.userName == None:
            return "无骑师名"
        return self.user.userName

    # 获取所有奖金
    def get_all_prize(self):
        # 当前马匹参加的比赛，比赛状态必须为结束，必须已支付
        ed_list = EnrollmentDetail.objects.filter(horseman__user__user_id=self.user.user.id,\
                                                  game__game_status=3,\
                                                  enrollment__fare_paid=1)
        sum_prize = 0
        for item in ed_list:
            sum_prize = sum_prize + item.get_prize_num()
        return sum_prize


# 马匹性别
HORSE_GENDER = (
    (1, _("Stallion")),  # 公马
    (2, _("Mare")),  # 母马
    (3, _("Gelding"))  # 阉马
)


# 4、马匹
class Horse(models.Model):
    user = models.ForeignKey(  # 马主
        Certification,
        null=False,
        blank=True,
        verbose_name=_("Horse Owner")
    )
    year = models.ForeignKey(
        Regulation,
        null=True,
        blank=True,
        verbose_name=_('Game Year')
    )
    member = models.ForeignKey(  # 参赛队伍
        Member,
        null=True,
        blank=True,
        verbose_name=_("Team")
    )
    horse_type = models.ForeignKey(  # 马匹类型
        HorseType,
        null=False,
        blank=False,
        default=1,
        verbose_name=_('Horse Type')
    )

    name = models.CharField(  # 马名（区码）
        max_length=200,
        null=False,
        blank=False,
        unique=True,
        verbose_name=_("Horse Name")
    )
    gender = models.IntegerField(  # 性别
        choices=HORSE_GENDER,
        default=1,
        verbose_name=_("Gender")
    )
    birthday = models.DateField(  # 出生日期
        null=False,
        blank=False,
        verbose_name=_("Birthday")
    )
    birthplace = models.CharField(  # 出生地
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Birthplace")
    )
    breed = models.CharField(  # 品种
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Breed")
    )
    brand = models.CharField(  # 品种
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Brand")
    )
    chip_no = models.CharField(  # 芯片号
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Chip No")
    )
    passport_no = models.CharField(  # 护照号
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Passport No")
    )
    father_name = models.CharField(  # 父名
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Father Name")
    )
    mother_name = models.CharField(  # 母名
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Mother Name")
    )
    grandfather_name = models.CharField(  # 外祖父名
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Grandfather Name")
    )
    vaccinated = models.BooleanField(  # 是否接种疫苗
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Vaccinated")
    )
    vaccinate_date = models.DateField(  # 疫苗接种日期
        null=True,
        blank=True,
        verbose_name=_("Vaccinate Date")
    )
    vaccine = models.CharField(  # 疫苗品牌
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("vaccine")
    )
    record = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Record")
    )
    horse_photo = models.ImageField(  # 马匹照片
        upload_to=upload_horse_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Horse Photo")
    )

    qualified = models.BooleanField(  # 符合报名要求
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Horse Qualified")
    )

    qualify_msg = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_("反馈信息(如果不符合，请填入原因，并按确定键！)")
    )

    class Meta:
        verbose_name = _('Horse')
        verbose_name_plural = _('Horse')

    def __str__(self):
        if self.name == None:
            return "无马名"+"-"+str(self.horse_type)+"-"+str(self.birthday)
        return self.name+"-"+str(self.horse_type)+"-"+str(self.birthday)

    #获取所有奖金
    def get_all_prize(self):
        #当前马匹参加的比赛，比赛状态必须为结束，必须已支付
        ed_list=EnrollmentDetail.objects.filter(horse_id=self.id,game__game_status=3,enrollment__fare_paid=1)
        sum_prize=0
        for item in ed_list:
            sum_prize=sum_prize+item.get_prize_num()
        return sum_prize

    # 添加新对象成功后跳转的URL
    def get_absolute_url(self):
        return reverse('enrollment_horse_detail', kwargs={'pk': self.id})


# 5、裁判员
class Referee(models.Model):
    user = models.OneToOneField(  # 用户ID, 主键
        Certification,
        blank=True,
        primary_key=True,
        verbose_name=_("UserID")
    )
    referee_licence = models.ImageField(  # 裁判员证书照片
        upload_to=upload_referee_licence_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Referee Licence")
    )
    referee_history = models.TextField(  # 执裁历史
        blank=True,
        null=True,
        verbose_name=_("Referee History")
    )
    referee_qualified = models.BooleanField(  # 是否符合裁判资格
        blank=False,
        null=False,
        default=False,
        verbose_name=_("Referee Qualified")
    )

    qualify_msg = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_("反馈信息(如果不符合，请填入原因，并按确定键！)")
    )

    class Meta:
        verbose_name = _('Referee')
        verbose_name_plural = _('Referee')

    def __str__(self):
        if self.user.userName:
            return self.user.userName
        return "无名裁判"


# 马匹用途类别
HORSE_TYPE = (
    (1, _("Racing Horse")),  # 参赛马匹
    (2, _("Backup Horse")),  # 备用马匹
)

# 6、比赛报名
class Enrollment(models.Model):
    year = models.ForeignKey(  # 赛事年度
        Regulation,
        null=False,
        blank=False,
        verbose_name=_("Game Year")
    )
    member = models.ForeignKey(  # 参赛队伍
        Member,
        null=False,
        blank=False,
        verbose_name=_("Team")
    )
    discipline_number = models.SmallIntegerField(  # 参赛项目数量
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Discipline Number")
    )
    horse_number = models.SmallIntegerField(  # 参赛马匹数量
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Horse Number")
    )
    horseman_number = models.SmallIntegerField(  # 参赛骑手数量
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Horseman Number")
    )
    fare = models.DecimalField(  # 出赛费
        max_digits=9,
        decimal_places=2,
        default=0.00,
        verbose_name=_("Racing Fare")
    )
    enrollment_fixed = models.BooleanField(  # 是否不再变动
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Enrollment Fixed")
    )
    qualified = models.BooleanField(  # 报名是否被核准
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Enrollment Qualified")
    )
    qualify_msg = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_("反馈信息(如果不符合，请填入原因，并按确定键！)")
    )
    payment_uuid = models.UUIDField(  # 支付单唯一标识符
        default=uuid.uuid1,  # 由 MAC 地址（主机物理地址）、当前时间戳、随机数自动生成
        unique=True,
        editable=False,
        verbose_name=_("Payment ID")
    )
    fare_paid = models.BooleanField(  # 出赛费是否支付
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Racing Fare Paid")
    )
    paid_time = models.DateTimeField(
        auto_now=True,  # 自动维护更新时间
        verbose_name=_("Paid Time")
    )

    def get_all_racing_fare(self):
        ed_set=self.enrollmentdetail_set.all()
        all_fare=0
        for item in ed_set:
            all_fare=all_fare+item.fare
        return all_fare

    def __str__(self):
        member_name=self.member.get_member_name()
        if member_name == None:
            return "不要选"
        return member_name

    class Meta:
        verbose_name = _('Enrollment')
        verbose_name_plural = _('Enrollments')
        unique_together = (("year", "member"),)  # 同一赛事一个会员只能用1条报名记录


# 7、比赛报名明细
class EnrollmentDetail(models.Model):
    enrollment = models.ForeignKey(
        Enrollment,
        null=False,
        blank=False,
        verbose_name=_("Enrollment")
    )
    game = models.ForeignKey(
        Game,
        null=False,
        blank=False,
        verbose_name=_("Game")
    )
    horse = models.ForeignKey(
        Horse,
        null=False,
        blank=False,
        verbose_name=_("Horse")
    )
    horseman = models.ForeignKey(
        Horseman,
        null=False,
        blank=False,
        verbose_name=_("Horseman")
    )
    type = models.IntegerField(  # 用途类别
        choices=HORSE_TYPE,
        default=1,
        verbose_name=_("Type")
    )

    group_no = models.SmallIntegerField(  # 组号，国产马比赛项目，报名马匹超过14匹时需要分组
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Group Number")
    )
    starting_gate_no = models.SmallIntegerField(  # 马闸号（1~14）
        null=False,
        blank=False,
        default=0,
        verbose_name=_("Starting Gate Number")
    )
    fare = models.DecimalField(  # 出赛费
        max_digits=9,
        decimal_places=2,
        default=0.00,
        verbose_name=_("Racing Fare")
    )
    """
    比赛用时，
    成绩是否有效
    无效原因
    """
    time_limit=models.FloatField(
        null=True,
        blank=True,
        default=0,
        verbose_name=_("Time Limit")
    )
    is_valided = models.BooleanField(  # 结果是否公布
        null=False,
        blank=False,
        default=1,
        verbose_name=_("Is Valid")
    )
    not_valid_reason=models.CharField(  # 结果是否公布
        max_length=100,
        null=True,
        blank=True,
        default="",
        verbose_name=_("Not Valid Reason")
    )
    ranking = models.SmallIntegerField(  # 名次
        null=True,
        blank=True,
        default=0,
        verbose_name=_("Ranking")
    )
    published = models.BooleanField(  # 结果是否公布
        null=False,
        blank=False,
        default=True,
        verbose_name=_("Published")
    )

    def __str__(self):
        return "ceshi1"

    #获取排名
    def get_rank(self):
        #比赛必须完赛，成绩必须有效
        if self.game.game_status == 3 and self.is_valided:
            rank=EnrollmentDetail.objects\
                .filter(game=self.game,time_limit__lte=self.time_limit,is_valided=1).exclude(time_limit=0)\
                .count()
            return rank
        else:
            return -1

    #获取奖金数
    def  get_prize_num(self):

        #比赛必须是已结束才行
        if self.game.game_status != 3:
            return 0

        prize_comment = eval(self.game.prize_comment)
        #获取报名数量
        ed_num=EnrollmentDetail.objects.filter(game=self.game,enrollment__fare_paid=1).count()
        keys=[]
        proportion=None

        #获取所有参赛最低数组合
        for key, value in prize_comment.items():
            keys.append(int(key))

        keys.sort(reverse=True)

        #判断满足的最小参赛数
        for key in keys:
            if ed_num>=key:
                proportion=prize_comment[str(key)]
                break
        #不满足最低参赛数
        if proportion==None:
            return 0

        rank=self.get_rank()
        #排名必须小于占比数，并且排名有效
        if rank>len(proportion) or rank == -1:
            return 0

        return float(proportion[rank-1])/100.0*self.game.prize



    class Meta:
        verbose_name = _('Enrollment Detail')
        verbose_name_plural = _('Enrollment Details')
        # 一匹马在同一赛事的一个项目中不允许出现多次
        # 一个骑手在同一赛事的一个项目中不允许出现多次
        #同一个比赛中,组号与闸号的组合不能重复
        unique_together = (("enrollment", 'horse'),\
                           ("enrollment", "game", 'horseman'),\
                           ("game","group_no","starting_gate_no"))

    # 禁用内建的unique_together校验
    def validate_unique(self, exclude=None):
        pass
    """
        设定限定条件：
            1.同一个比赛中，同一匹马只能出现，一次
            2.同一个比赛中，同一个骑师，是能出现一次。
            3.同一个比赛，同一个单位报名，报名数量有没有限制
            
            4.同一个比赛、同一个单位、同一个骑师，同一匹马只能出现一次
            
    """
    def clean(self, *args, **kwargs):
        super(EnrollmentDetail, self).clean(*args, **kwargs)

        if EnrollmentDetail.objects.filter(game=self.game,\
                                           horse=self.horse,enrollment__fare_paid=1).count()>0:
            raise ValidationError("当前比赛中，这匹马已经被使用，请确认后再选择！")
        if EnrollmentDetail.objects.filter(game=self.game,\
                                           horseman=self.horseman,enrollment__fare_paid=1).count()>0:
            raise ValidationError("当前比赛中，这名骑师已经被使用，请确认后再选择！")

        #  一、数据校验
        # 每参赛马匹只能参加一个单项，不得兼项！
        ##
        if EnrollmentDetail.objects.filter(
                enrollment=self.enrollment,
                horse=self.horse).exists():
            raise ValidationError(_('One horse can not duplicate in same game!'))
        # 同一骑师在同一比赛项目中只能出现一次
        elif EnrollmentDetail.objects.filter(
                enrollment=self.enrollment,
                game=self.game,
                horseman=self.horseman).exists():
            raise ValidationError(_('One horseman can not duplicate in same game!'))
        elif self.game.horse_type != self.horse.horse_type:
            # 参赛马匹的类型与项目要求的马匹类型不匹配！
            raise ValidationError(_('Horse type does not match!'))

        elif not (self.game.horse_birthday_begin <= self.horse.birthday <= self.game.horse_birthday_end):
            # 参赛马匹的出生日期不符合所报项目的要求！
            raise ValidationError(_('Horse birthday does not match!'))

        # 纯血马的单场比赛，单场报满14匹即止
        elif self.game.horse_type_id == 1 \
                and self.type == 1 \
                and EnrollmentDetail.objects.filter(
            enrollment=self.enrollment,  # 统一项目
            horse__horse_type=self.horse.horse_type,  # 统一马匹类型
            type=1  # 参赛马匹
        ).count() >= 14:
            raise ValidationError(_('14 racing blood horses already enrolled for this  game!'))

        # 纯血马的单场比赛，各单位每场可报2匹参赛
        elif self.game.horse_type_id == 1 \
                and self.type == 1 \
                and EnrollmentDetail.objects.filter(
            enrollment=self.enrollment,  # 统一项目
            horse__horse_type=self.horse.horse_type,  # 统一马匹类型
            type=1  # 参赛马匹
        ).count() >= self.game.racing_horse_limit:
            raise ValidationError(_('One Team can only enroll 2 racing blood horses for this  game!'))

        # 纯血马的单场比赛，各单位每场可报1匹后备马匹
        elif self.game.horse_type_id == 1 \
                and self.type == 2 \
                and EnrollmentDetail.objects.filter(
            enrollment=self.enrollment,  # 同一项目
            horse__horse_type=self.horse.horse_type,  # 同一马匹类型
            type=2
        ).count() >= self.game.backup_horse_limit:
            raise ValidationError(_('One Team can only enroll 1 backup blood horses for this  game!'))

        # 国产马比赛，每个项目每单位参赛马匹不超过3匹
        elif self.game.horse_type_id != 1 \
                and EnrollmentDetail.objects.filter(
            enrollment=self.enrollment,  # 同一项目
            horse__horse_type=self.horse.horse_type,  # 同一马匹类型
        ).count() >= self.game.racing_horse_limit:
            raise ValidationError(_('One Team can only enroll 3 domestic horses for this  game!'))
        else:
            # 二、自动分组、自动随机分配闸号
            starting_gates = 14  # 马闸数量
            all_starting_gate_no = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]  # 有效的马闸号

            # 1、计算并设置出赛费
            game = Game.objects.get(id=self.game.id)
            fare = game.prize * 10000 * game.fare_rate
            self.fare = fare

            # 2、计算并设置分组号
            horses = EnrollmentDetail.objects.filter(game=self.game).count() + 1
            group_no = horses // starting_gates + 1
            self.group_no = group_no

            # 3、计算并设置马闸号
            # 同组已经使用的马闸号
            used_starting_gate_no = EnrollmentDetail.objects.filter(game=self.game, group_no=group_no).values_list(
                'starting_gate_no', flat=True)
            # 可用的马闸号
            available_starting_gate_no = [item for item in all_starting_gate_no if item not in used_starting_gate_no]
            self.starting_gate_no = random.choice(available_starting_gate_no)
            """
            # 三、更新报名汇总记录

            # 报名马匹增加1
            enrollment = self.enrollment
            enrollment.horse_number += 1

            # 修正报名项目数量
            game_id_list = EnrollmentDetail.objects.filter(enrollment=self.enrollment, game=self.game).values_list(
                'game_id', flat=True)
            if self.game_id not in game_id_list:
                enrollment.discipline_number += 1

            # 修正报名骑师数量
            horseman_id_list = EnrollmentDetail.objects.filter(enrollment=self.enrollment).values_list(
                'horseman_id', flat=True)
            if self.horseman_id not in horseman_id_list:
                enrollment.horseman_number += 1

            # 修正出赛费
            enrollment.fare += self.fare

            # 四、保存报名汇总记录
            enrollment.save()
            """

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        #更新报名汇总记录
        # 报名马匹增加1
        enrollment = self.enrollment
        enrollment.horse_number += 1

        # 修正报名项目数量
        game_id_list = EnrollmentDetail.objects.filter(enrollment=self.enrollment, game=self.game).values_list(
            'game_id', flat=True)
        if self.game_id not in game_id_list:
            enrollment.discipline_number += 1

        # 修正报名骑师数量
        horseman_id_list = EnrollmentDetail.objects.filter(enrollment=self.enrollment).values_list(
            'horseman_id', flat=True)
        if self.horseman_id not in horseman_id_list:
            enrollment.horseman_number += 1

        # 修正出赛费
        enrollment.fare += self.fare

        # 四、保存报名汇总记录
        enrollment.save()

        super(EnrollmentDetail, self).save(force_insert, force_update, using,update_fields)

    def delete(self):
        # 更新报名汇总记录

        # 报名马匹减少1
        enrollment = self.enrollment
        enrollment.horse_number -= 1

        # 修正报名项目数量
        if EnrollmentDetail.objects.filter(enrollment=self.enrollment, game=self.game).count() == 1:
            enrollment.discipline_number -= 1

        # 修正报名骑师数量
        if EnrollmentDetail.objects.filter(enrollment=self.enrollment, horseman=self.horseman).count() == 1:
            enrollment.horseman_number -= 1

        # 修正出赛费
        enrollment.fare -= self.fare

        # 保存报名汇总记录
        enrollment.save()

        super().delete()
