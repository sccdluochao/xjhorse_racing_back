from django import forms

from racing.models_enrollment import GameDay
from racing.models_regulation import Regulation


class GameDayModelForm(forms.ModelForm):
    class Meta:
        model = GameDay
        fields = ["name",
                "year",]

    def __init__(self,*args, **kwargs):
        super(GameDayModelForm, self).__init__(*args, **kwargs)
        if "initial" in kwargs:
            regulation_set=Regulation.objects.filter(id=kwargs['initial']['year'].id)
            self.fields['year'].queryset=regulation_set
        self.fields['name'].widget.attrs['placeholder'] = "请输入比赛日"