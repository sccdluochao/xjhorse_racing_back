from django.conf.urls import url
from racing import views_ticket as views
from racing import views_alipay

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 门票服务，购物车
# ********************************************************
urlpatterns = [

    url(r'^racing/ticket$', views.ticket, name='ticket'),
    url(r'^racing/ticket/(?P<pk>\d+)$', views.ticket_detail, name='ticket_detail'),

    #下单
    url(r'^racing/ticket/order_create$', views.order_create, name='order_create'),
    url(r'^racing/ticket/order_remove/(?P<order_id>\d+)/$', views.order_remove, name='order_remove'),
    url(r'^racing/ticket/order$', views.order, name='order'),

    #结账
    url(r'^racing/ticket/payment_create$', views.payment_create, name='payment_create'),
    url(r'^racing/ticket/payment_remove/(?P<pk>[\w-]+)/$', views.payment_remove, name='payment_remove'),
    url(r'^racing/ticket/payment$', views.payment, name='payment'),
    #付款页面
    url(r'^racing/ticket/paid$', views.paid, name='paid'),


    #展示验票码、查询票务信息
    url(r'^racing/ticket/ticket_qrcode$', views.ticket_qrcode, name='ticket_qrcode'),
    url(r'^racing/ticket/ticket_qrcode_page$', views.ticket_qrcode_page, name='ticket_qrcode_page'),
    url(r'^racing/ticket/get_today_tickets/(?P<ticket_code>\S+)$',views.get_today_tickets,name='get_today_tickets'),
    url(r'^racing/ticket/use_ticket$',views.use_ticket,name='use_ticket'),

    # 支付宝支付入口
    url(r'^racing/ticket/alipay/(?P<pk>[\w-]+)/$', views_alipay.pay, name='alipay'),

    # 支付宝异步通知处理接口
    url(r'^racing/ticket/alipay/notify_url$', views_alipay.alipay_notify_url, name='alipay_notify_url'),

    # 支付宝同步通知处理接口
    url(r'^racing/ticket/alipay/return_url$', views_alipay.alipay_return_url, name='alipay_return_url'),
]
