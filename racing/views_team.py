from django.shortcuts import render
from django.views.generic import DetailView

from racing.models import Regulation
from racing.models_enrollment import Member, Horse


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 参赛队伍（游客）
def team(request):
    members = Member.objects.filter(qualified=True)
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    context = {
        "regulation": regulation,
        "members": members,
    }
    return render(request, "racing/team/team.html", context)


# 马匹详细信息
class HorseDetailView(DetailView):
    model = Horse
    template_name = 'racing/team/horse_detail.html'


horse_detail = HorseDetailView.as_view()
