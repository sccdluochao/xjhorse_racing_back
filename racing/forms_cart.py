from django import forms

# 一次添加1至20张门票
PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 20)]


class CartAddProductForm(forms.Form):
    quantity = forms.TypedChoiceField(
        choices=PRODUCT_QUANTITY_CHOICES,
        coerce=int
    )
    update = forms.BooleanField(
        required=False,
        initial=False,
        widget=forms.HiddenInput)
