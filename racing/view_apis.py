import json

import urllib3
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from xjhorse import settings

#微信认证
@csrf_exempt
def wx_authentication(request):
    rs={"status":0,"msg":""}
    http = urllib3.PoolManager()
    urllib3.disable_warnings()
    try:
        resp = http.request('GET',
                            "https://api.weixin.qq.com/sns/jscode2session?appid=" \
                            + settings.AppID + "&secret=" + settings.AppSecret \
                            + "&js_code=" + request.POST['code'] \
                            + "&grant_type=authorization_code")
    except Exception as e:
        print(e)
        rs['msg'] = "出现了一个问题，请稍后再试，或检查网络是否通畅！"
        return JsonResponse(rs)
    r_msg = json.loads(resp.data.decode('utf-8'))

    # 请求成功
    if resp.status == 200 and "errcode" in r_msg:
        rs['msg'] = "出现了一个问题，请稍后再试，或检查网络是否通畅！"
        return JsonResponse(rs)
    elif resp.status == 200:
        request.session['openid'] = r_msg['openid']
        request.session['session_key'] = r_msg['session_key']

    return JsonResponse(rs)

#微信认证
def wx_login(request):
    rs = {"status": 1, "msg": ""}

    return JsonResponse(rs)
