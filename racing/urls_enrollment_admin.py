from django.conf.urls import url
from racing import views_enrollment_admin as views

# 创 建 人：骆超
# 创建日期：2018年10月04日

# ********************************************************
# 通过管理员来进行赛事报名
# ********************************************************

urlpatterns = [

    #首页
    url(r'^racing/enrollment_admin/index/$',
        views.enrolment_admin_index, name='enrollment_admin_index'),
    #识别身份证
    url(r'^racing/enrollment_admin/idcardrecognize/$',
        views.enrolment_admin_idcardrecognize, name='enrollment_admin_idcardrecognize'),
    #角色管理
    url(r'^racing/enrollment_admin/permission_change/(?P<user_id>\d+)/$',
        views.enrollment_admin_permission_change,name='enrollment_admin_permission_change'),

    #马主操作
    url(r'^racing/enrollment_admin/horseowner_list/$',
        views.enrollment_admin_horseowner_list,name='enrollment_admin_horseowner_list'),
    url(r'^racing/enrollment_admin/horseowner/$',
        views.enrollment_admin_horseowner,name='enrollment_admin_horseowner'),
    url(r'^racing/enrollment_admin/horsememberowner/$',
        views.enrollment_admin_horsememberowner,name='enrollment_admin_horsememberowner'),

    #马匹操作
    url(r'^racing/enrollment_admin/horse_list/$',
        views.enrollment_admin_horse_list,name='enrollment_admin_horse_list'),
    url(r'^racing/enrollment_admin/horse/$',
        views.enrollment_admin_horse, name='enrollment_admin_horse'),
    url(r'^racing/enrollment_admin/search_horseowner/',views.enrollment_admin_search_horseowner,name='enrollment_admin_search_horseowner'),

    #骑师操作
    url(r'^racing/enrollment_admin/horseman_list/$',
        views.enrollment_admin_horseman_list,name='enrollment_admin_horseman_list'),
    url(r'^racing/enrollment_admin/horseman/$',
        views.enrollment_admin_horseman, name='enrollment_admin_horseman'),

    #裁判操作
    url(r'^racing/enrollment_admin/referee_list/$',
        views.enrollment_admin_referee_list,name='enrollment_admin_referee_list'),
    url(r'^racing/enrollment_admin/referee/$',
        views.enrollment_admin_referee, name='enrollment_admin_referee'),

    #团队操作
    url(r'^racing/enrollment_admin/member_list/$',
        views.enrollment_admin_member_list,name='enrollment_admin_member_list'),
    url(r'^racing/enrollment_admin/indivdualmember/$',
        views.enrollment_admin_indivdualmember, name='enrollment_admin_indivdualmember'),
    url(r'^racing/enrollment_admin/organizationmember/$',
        views.enrollment_admin_organizationmember, name='enrollment_admin_organizationmember'),

    #参赛报名页
    url(r'^racing/enrollment_admin/member_game/(?P<member_id>\d+)/$',
        views.enrollment_admin_member_game,name='enrollment_admin_member_game'),
    #获取报名页面
    url(r'^racing/enrollment_admin/get_enrollment_html/(?P<enrollment_id>\d+)/$',
        views.enrollment_admin_get_enrollment_html,name='enrollment_admin_get_enrollment_html'),
    #搜索马匹与骑师
    url(r'^racing/enrollment_admin/search_horse/',
        views.enrollment_admin_search_horse,name='enrollment_admin_search_horse'),
    url(r'^racing/enrollment_admin/search_horseman/',
        views.enrollment_admin_search_horseman,name='enrollment_admin_search_horseman'),
    #删除报名细节
    url(r'^racing/enrollment_admin/delete_enrollment_detail/(?P<ed_id>\d+)/$',
        views.delete_enrollment_detail,name='enrollment_admin_delete_enrollment_detail'),
    #缴费锁定
    url(r'^racing/enrollment_admin/payment_lock/(?P<e_id>\d+)/$',
        views.payment_lock,name='enrollment_admin_payment_lock'),
    #缴费返还
    url(r'^racing/enrollment_admin/payment_return/(?P<e_id>\d+)/$',
        views.payment_return,name='enrollment_admin_payment_return'),

]
