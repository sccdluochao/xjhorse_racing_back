import time
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.forms import modelformset_factory
from django.http import JsonResponse
from django.views.generic import ListView, DeleteView, UpdateView, CreateView, DetailView
from django.urls import reverse_lazy, reverse
from rules.contrib.views import PermissionRequiredMixin, permission_required
from django.shortcuts import render, redirect
from django.db.utils import OperationalError, ProgrammingError, Error

from racing.forms_games import Game_Group_GateModelForm
from racing.models_regulation import Regulation
from racing.models_enrollment import Game, EnrollmentDetail, GameDay
from django.utils.translation import ugettext as _
from django import forms


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 比赛项目(游客)
def game_list(request):
    try:
        regulation = Regulation.objects.get(active=True)
    except Regulation.DoesNotExist:
        regulation = None
    games = Game.objects.all().filter(active=True, year=regulation).order_by('id')
    context = {
        "regulation": regulation,
        "games": games,
    }
    return render(request, "racing/game/game_list.html", context)


# ********************************************************
# 竞赛项目管理 model：Game
# ********************************************************

class GameManageListView(PermissionRequiredMixin, ListView):
    try:
        regulation = Regulation.objects.get(active=True)
    except OperationalError:  # 否则，makemigrations 访问数据库会报no such table错误
        regulation = None
    except ProgrammingError:
        regulation = None
    except Regulation.DoesNotExist:
        regulation = None
    queryset = Game.objects.filter(active=True)

    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/game/game_manage_list.html'

    def get_permission_object(self):
        return "天马赛事"

    #获取上下文参数
    def get_context_data(self, **kwargs):
        context=super(GameManageListView, self).get_context_data(**kwargs)
        regulation = Regulation.objects.get(active=True)
        context['regulation']=regulation
        return context

game_manage_list = GameManageListView.as_view()


class GameManageDetailView(PermissionRequiredMixin, DetailView):
    model = Game
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/game/game_manage_detail.html'

    def get_permission_object(self):
        return "天马赛事"


game_manage_detail = GameManageDetailView.as_view()


class GameManageCreateView(PermissionRequiredMixin, CreateView):
    model = Game
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/game/game_manage_create.html'
    fields = ['name',
              'fare_rate',
              'game_day',
              'horse_type',
              'referees',
              'racing_horse_limit',
              'backup_horse_limit',
              'horse_birthday_begin',
              'horse_birthday_end',
              'time',
              'prize',
              'prize_comment',
              'year',
              'active',
              ]


    def get_form(self, form_class=None):
        form=super(GameManageCreateView, self).get_form(form_class)
        #只能选激活的
        form.fields['year'].queryset = Regulation.objects.filter(active=1)
        form.fields['game_day'].queryset = GameDay.objects.filter(year__active=1)

        form.fields['horse_birthday_begin'].widget.attrs['time-tag'] = "select_date"
        form.fields['horse_birthday_begin'].widget.attrs['autocomplete'] = "off"

        form.fields['horse_birthday_end'].widget.attrs['time-tag'] = "select_date"
        form.fields['horse_birthday_end'].widget.attrs['autocomplete'] = "off"

        #form.fields['time'].widget.attrs['time-tag'] = "select_date"
        #form.fields['time'].widget.attrs['autocomplete'] = "off"

        form.fields['prize_comment'].widget.attrs['placeholder'] = \
            "形式:\n\"最低参赛数=第一名占比+第二名占比+第三名占比\"\n要求:\n(1)多条必须换行;\n(2)占比总和必须等于100。\n(3)占比必须是从大往小。\n举例:\n6=40+30+20+10\r15=30+20+15+10+10+10+5"

        return form

    def get_permission_object(self):
        return "天马赛事"


game_manage_create = GameManageCreateView.as_view()


class GameManageUpdateView(PermissionRequiredMixin, UpdateView):
    model = Game
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True

    template_name = 'racing/game/game_manage_update.html'
    fields = ['name',
              'fare_rate',
              'game_day',
              'horse_type',
              'referees',
              'racing_horse_limit',
              'backup_horse_limit',
              'horse_birthday_begin',
              'horse_birthday_end',
              'time',
              'prize',
              'prize_comment',
              'year',
              'active',
              ]

    # 验证通过调用的方法
    def form_valid(self, form, *args, **kwargs):
        form = super(GameManageUpdateView, self).form_valid(form, *args, **kwargs)
        return form

    #获取对象
    def get_object(self, queryset=None):
        object=super(GameManageUpdateView, self).get_object(queryset)
        #修改对象的某个字段，但不保存
        object.prize_comment=object.get_prize_comment_expression()
        return object

    def get_form(self, form_class=None):
        form=super(GameManageUpdateView, self).get_form(form_class)

        #只能选激活的
        form.fields['year'].queryset = Regulation.objects.filter(active=1)
        form.fields['game_day'].queryset = GameDay.objects.filter(year__active=1)

        form.fields['horse_birthday_begin'].widget.attrs['time-tag'] = "select_date"
        form.fields['horse_birthday_begin'].widget.attrs['autocomplete'] = "off"

        form.fields['horse_birthday_end'].widget.attrs['time-tag'] = "select_date"
        form.fields['horse_birthday_end'].widget.attrs['autocomplete'] = "off"

        #form.fields['time'].widget.attrs['time-tag'] = "select_date"
        #form.fields['time'].widget.attrs['autocomplete'] = "off"

        form.fields['prize_comment'].widget.attrs['placeholder'] = \
            "形式:\n\"最低参赛数=第一名占比+第二名占比+第三名占比\"\n要求:\n(1)多条必须换行;\n(2)占比总和必须等于100;\n(3)占比必须是从大往小。\n举例:\n6=40+30+20+10\r15=30+20+15+10+10+10+5"

        return form

    def get_permission_object(self):
        return "天马赛事"


game_manage_update = GameManageUpdateView.as_view()


class GameManageDeleteView(PermissionRequiredMixin, DeleteView):
    model = Game
    success_url = reverse_lazy('game_manage_list')
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/game/game_manage_delete.html'

    def get_permission_object(self):
        return "天马赛事"


game_manage_delete = GameManageDeleteView.as_view()


#********************************************************************************
#裁判的操作
#********************************************************************************
class JudgeGameListView(PermissionRequiredMixin,ListView):

    model = Game
    permission_required = ['racing.is_referee_qualified', ]
    raise_exception = True
    template_name = 'racing/game/judge_game_list.html'

    def get_permission_object(self):
        return "天马赛事"

    def get_context_data(self, **kwargs):
        context = super(JudgeGameListView, self).get_context_data(**kwargs)
        try:
            regulation = Regulation.objects.get(active=True)
        except OperationalError:  # 否则，makemigrations 访问数据库会报no such table错误
            regulation = None
        except ProgrammingError:
            regulation = None
        except Regulation.DoesNotExist:
            regulation = None
        context['object_list'] = Game.objects.filter(referees=self.request.user.certification.referee,active=True,year=regulation)
        context['regulation']=regulation
        return context

judge_game_list=JudgeGameListView.as_view()

#执裁比赛
@login_required
def judge_game(request,gameid):
    if request.method == "POST":
        rs={"status":0,"msg":_("error!")}
        for k,v in request.POST.items():
            #排除密钥以及以p开头的字符（p）
            if str(k) != "csrfmiddlewaretoken" \
                    and \
                    not str(k).startswith("p"):
                ed = EnrollmentDetail.objects.get(id=k)
                if request.POST[k] != "":
                    ed.ranking=request.POST[k]
                else:
                    ed.ranking = 0
                #如果包含以p
                if "p"+k in request.POST:
                    ed.published=1
                else:
                    ed.published = 0
                ed.save()
                rs['status']=1
                rs['msg'] = _("success!")

        return JsonResponse(rs)
    else:
        edlist=EnrollmentDetail.objects.filter(game__id=gameid).order_by('starting_gate_no','group_no')
        game=Game.objects.get(id=gameid)

        try:
            regulation = Regulation.objects.get(active=True)
        except OperationalError:  # 否则，makemigrations 访问数据库会报no such table错误
            regulation = None
        except ProgrammingError:
            regulation = None
        except Regulation.DoesNotExist:
            regulation = None

        context={
            "object_list":edlist,
            "game":game
        }
        return render(request,"racing/game/judge_game.html",context)

#比赛信息管理
@login_required
@permission_required('racing.admin', raise_exception=True)
@transaction.atomic
def group_gate_change(request,game_id):

    game = Game.objects.get(id=game_id)
    enrollment_detail_list = EnrollmentDetail.objects.filter(game=game).order_by("-enrollment__fare_paid")
    print(enrollment_detail_list.count())
    #通过奖金排序
    enrollment_detail_list_instance=[]
    for item in enrollment_detail_list:
        enrollment_detail_list_instance.append(item)
        enrollment_detail_list_instance.sort(key=lambda ed:ed.get_prize_num(),reverse=True)

    if request.method == "GET":
        #求最大组号
        horses = EnrollmentDetail.objects.filter(game=game,
                                                 enrollment__fare_paid=1).count() + 1
        max_group_no = horses // 14 + 1

        context={
            "enrollment_detail_list":enrollment_detail_list_instance,
            "game":game,
            "max_group_no":max_group_no
        }

        return render(request,
                      "racing/game/game_group_gate_manage.html",context)

    elif request.method == "POST":
        rs={"status":0,"msg":""}

        if "game_status" not in request.POST or \
                request.POST['game_status'] == "":
            rs['msg']="遇到错误"
            return JsonResponse(rs)

        game_status=request.POST['game_status']
        try:
            with transaction.atomic():
                for k,v in request.POST.items():
                    #一组数据
                    if (str(k).startswith("group_") or str(k).startswith("time_")):
                        #获取id
                        if str(k).startswith("group_"):
                            edid = str(k)[6:]
                        else:
                            edid = str(k)[5:]
                        ed = EnrollmentDetail.objects.get(id=edid)
                        game = ed.game
                        if game_status == "1":
                            ed.group_no=request.POST["group_"+edid]
                            ed.starting_gate_no = request.POST["gate_" + edid]
                            game.game_status = 2
                        else:
                            ed.time_limit = request.POST["time_" + edid]
                            ed.not_valid_reason = request.POST["reason_" + edid]
                            if "valid_"+edid in request.POST:
                                ed.is_valided = 1
                            else:
                                ed.is_valided = 0
                            game.game_status = 3
                        #是否允许发布
                        if "pub_" + edid in request.POST:
                            ed.published = 1
                        else:
                            ed.published = 0

                        ed.save()
                        game.save()

                        rs['status']=1
        except BaseException:
            rs['msg']="未知错误！"
        except Error:
            rs['msg'] = "未知错误！"
        return JsonResponse(rs)

@login_required
@permission_required('racing.admin', raise_exception=True)
def game_back_to_not_start(request,game_id):
    game = Game.objects.get(id=game_id)
    game.game_status=1
    game.save()
    return redirect(reverse("group_gate_change",args=(game_id,)))

@login_required
@permission_required('racing.admin', raise_exception=True)
def game_back_to_start(request,game_id):
    game = Game.objects.get(id=game_id)
    game.game_status = 2
    game.save()
    return redirect(reverse("group_gate_change",args=(game_id,)))