from django.db import models
from django.core.files.storage import FileSystemStorage
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.urls import reverse
import os


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 重复上传照片时，用新照片覆盖老照片
class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return super(OverwriteStorage, self).get_available_name(name, max_length)


# 赛事轮播照片上传位置和文件名称
def upload_photo(instance, filename):
    return "racing/%s" % filename


# 赛事公告照片上传位置和文件名称
def upload_announcement_photo(instance, filename):
    return "racing/announcement_%s" % filename


# 赛事视屏海报照片上传位置和文件名称
def upload_racing_video_poster(instance, filename):
    return "racing/racing_%s" % filename


# 1、首页轮播照片
class Carousel(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Title'))
    slogan = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Slogan'))
    ranking = models.SmallIntegerField(default=0, verbose_name=_('Ranking'))
    photo = models.ImageField(
        upload_to=upload_photo,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Photo")
    )
    url = models.URLField(null=True, blank=True)
    enabled = models.BooleanField(default=True, verbose_name=_("Enabled"))
    active = models.BooleanField(default=False, verbose_name=_("Active"))

    class Meta:
        verbose_name = _('Photo Carousel')
        verbose_name_plural = _('Photo Carousels')


# 2、赛事公告
class EventsAnnouncement(models.Model):
    title = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Announcement Title")  # 赛事公告标题
    )
    photo = models.ImageField(
        upload_to=upload_announcement_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Photo")  # 赛事公告照片
    )
    content = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Announcement Content")  # 赛事公告内容
    )
    created = models.DateTimeField(
        null=False,
        blank=True,
        verbose_name=_("Create Time")  # 赛事公告创建时间，用于排序
    )
    enabled = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Announcement Enabled")  # 赛事公告是否启用
    )

    def save(self, *args, **kwargs):
        # 自动设置对象创建时间
        if not self.id:
            self.created = timezone.now()

        return super(EventsAnnouncement, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Announcement')
        verbose_name_plural = _('Announcements')

    def __str__(self):
        return self.title

    # 添加新对象成功后跳转的URL
    def get_absolute_url(self):
        return reverse('announcement_manage_detail', kwargs={'pk': self.id})


# 3、比赛视频
class Video(models.Model):
    title = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_('Video Title')
    )
    ranking = models.SmallIntegerField(
        default=0,
        verbose_name=_('Ranking')
    )
    duration = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_("Duration")
    )
    url = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("Video url")
    )
    enabled = models.BooleanField(
        default=False,
        verbose_name=_("Enabled")
    )
    poster = models.ImageField(
        upload_to=upload_racing_video_poster,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Poster"))

    class Meta:
        verbose_name = _('Racing Video')
        verbose_name_plural = _('Racing Videos')

    def __str__(self):
        return self.title
