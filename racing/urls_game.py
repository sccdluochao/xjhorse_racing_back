from django.conf.urls import url
from racing import views_game as views

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 竞赛项目
# ********************************************************
urlpatterns = [
    url(r'^racing/game_list$', views.game_list, name='game_list'),
    url(r'^racing/game_manage_list$', views.game_manage_list, name='game_manage_list'),
    url(r'^racing/game_manage/(?P<pk>\d+)$', views.game_manage_detail, name='game_manage_detail'),
    url(r'^racing/game_manage/create$', views.game_manage_create, name='game_manage_create'),
    url(r'^racing/game_manage/update/(?P<pk>\d+)$', views.game_manage_update, name='game_manage_update'),
    url(r'^racing/game_manage/delete/(?P<pk>\d+)$', views.game_manage_delete, name='game_manage_delete'),

    #**************************************************
    #裁判的功能
    #**************************************************
    url(r'^racing/judge_game_list$', views.judge_game_list, name='judge_game_list'),
    url(r'^racing/judge_game/(?P<gameid>\d+)$', views.judge_game, name='judge_game'),

    #组号 闸号 管理
    url(r'^racing/group_gate_change/(?P<game_id>\d+)$', views.group_gate_change, name='group_gate_change'),
    #修改为 赛事未开赛状态
    url(r'^racing/game_back_to_not_start/(?P<game_id>\d+)$', views.game_back_to_not_start, name='game_back_to_not_start'),
    #修改为 赛事开赛状态
    url(r'^racing/game_back_to_start/(?P<game_id>\d+)$', views.game_back_to_start, name='game_back_to_start'),
]
