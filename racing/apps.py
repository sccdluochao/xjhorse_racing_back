from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


# 创 建 人：张太红
# 创建日期：2018年03月17日

class RacingConfig(AppConfig):
    name = 'racing'
    verbose_name = _('Racing')
