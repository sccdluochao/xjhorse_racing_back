import rules
from rules import is_authenticated

from racing.models import Role, Permission
from racing.models_enrollment import Member, Referee, Horseman, Horse, Enrollment
from certification.models import Certification


# 创 建 人：张太红
# 创建日期：2018年03月17日

# 使用修饰符@rules.predicate自定义predicates（谓词或判断式），返回True表示有权限，False表示无权限


def get_permission(user):
    """
    :param user:用户
    :return:
    None
    管理员
    裁判员
    单位参赛队伍
    个人参赛队伍
    单位马主
    个人骑师
    购票观众
    """
    # 返回角色名称列表
    if is_authenticated(user):
        return Role.objects.filter(permission__user=user).values_list('name', flat=True)
    else:
        return {}


# 是否为系统管理员
@rules.predicate
def is_admin(user):
    if '管理员' in get_permission(user):
        return True
    else:
        return False


# 是否为裁判员
@rules.predicate
def is_referee(user):
    if '裁判员' in get_permission(user):
        return True
    else:
        return False


# 是否为单位参赛队伍
@rules.predicate
def is_corporation_contestant(user):
    if '单位参赛队伍' in get_permission(user):
        return True
    else:
        return False


# 是否为个人参赛队伍
@rules.predicate
def is_individual_contestant(user):
    if '个人参赛队伍' in get_permission(user):
        return True
    else:
        return False


# 是否为单位马主
@rules.predicate
def is_corporation_horse_owner(user):
    if '单位马主' in get_permission(user):
        return True
    else:
        return False


# 是否为个人马主
@rules.predicate
def is_individual_horse_owner(user):
    if '个人马主' in get_permission(user):
        return True
    else:
        return False


# 是否为骑师
@rules.predicate
def is_horseman(user):
    if '骑师' in get_permission(user):
        return True
    else:
        return False


# 是否为购票观众
@rules.predicate
def is_paid_attendance(user):
    if '购票观众' in get_permission(user):
        return True
    else:
        return False


# 角色已经选定
@rules.predicate
def has_role(user):
    if get_permission(user):
        return True
    else:
        return False


# 角色已经选定
@rules.predicate
def is_role(user, role):
    if not role:
        return False
    try:
        Permission.objects.get(user=user, role=role)
        return True
    except Permission.DoesNotExist:
        return False


# 角色已经选定
@rules.predicate
def is_certificated(user, role):
    if not role:
        return False
    try:
        cert = Certification.objects.get(user=user)
        return cert.get_cert() >= role.min_certification
    except Certification.DoesNotExist:
        return False


# 会员资格是否通过审核
@rules.predicate
def is_member_qualified(user):
    try:
        member = Member.objects.get(user__user=user)
        return member.qualified
    except Member.DoesNotExist:
        return False


# 裁判员资格是否通过审核
@rules.predicate
def is_referee_qualified(user):
    try:
        referee = Referee.objects.get(user_id=user.id)
        return referee.referee_qualified
    except Referee.DoesNotExist:
        return False


# 骑师资格是否通过审核
@rules.predicate
def is_horseman_qualified(user):
    try:
        horseman = Horseman.objects.get(user_id=user.id)
        return horseman.qualified
    except Horseman.DoesNotExist:
        return False


# 马匹资格是否通过审核
@rules.predicate
def is_horse_qualified(user, horse):
    if not horse:
        return False
    return horse.qualified


# 会员对象创建者
@rules.predicate
def is_member_creator(user, member):
    if not member:
        return False
    return member.user.user == user


# 裁判员对象创建者
@rules.predicate
def is_referee_creator(user, referee):
    if not referee:
        return False
    return referee.user.user == user


# 骑师对象创建者
@rules.predicate
def is_horseman_creator(user, horseman):
    if not horseman:
        return False
    return horseman.user.user == user


# 马匹对象创建者
@rules.predicate
def is_horse_creator(user, horse):
    if not horse:
        return False
    return horse.user.user == user


# 没有录入马匹数据
@rules.predicate
def has_no_horse(user):
    if not is_authenticated(user):
        return True
    try:
        cert = Certification.objects.get(user=user)
        return Horse.objects.filter(user=cert).count() == 0
    except Certification.DoesNotExist:
        return True



# 已经有核准的马匹
@rules.predicate
def has_qualified_horse(user):
    cert = Certification.objects.get(user=user)
    horses = Horse.objects.filter(user=cert, qualified=True)
    return horses.count() > 0


# 报名是否锁定
@rules.predicate
def is_enrollment_fixed(user, enrollment):
    if not enrollment:
        return False
    return enrollment.enrollment_fixed


# 报名是否核准
@rules.predicate
def is_enrollment_qualified(user, enrollment):
    if not enrollment:
        return False
    return enrollment.qualified


# 报名马匹出赛费是否已经支付
@rules.predicate
def is_enrollment_fare_paid(user, enrollment):
    if not enrollment:
        return False
    return enrollment.fare_paid


# 已经存在角色对应的相关记录
@rules.predicate
def can_delete_role(user, role):
    if role.name == '管理员':
        return False
    elif role.name == '裁判员':
        return Referee.objects.filter(user__user=user).count() == 0
    elif role.name == '单位参赛队伍':
        return Enrollment.objects.filter(member__user__user=user).count() == 0
    elif role.name == '个人参赛队伍':
        return Enrollment.objects.filter(member__user__user=user).count() == 0
    elif role.name == '单位马主':
        return Horse.objects.filter(user__user=user).count() == 0
    elif role.name == '个人马主':
        return Horse.objects.filter(user__user=user).count() == 0
    elif role.name == '骑师':
        return Horseman.objects.filter(user__user=user).count() == 0
    elif role.name == '购票观众':
        return False
    else:
        return False


# 设置Permissions
rules.add_perm('racing', rules.always_allow)

# 管理员权限
rules.add_perm('racing.admin', is_admin | rules.is_superuser)

# 裁判员权限
rules.add_perm('racing.referee', is_referee)

# 单位参赛队伍权限
rules.add_perm('racing.corporation_contestant', is_corporation_contestant)

# 个参赛队伍权限
rules.add_perm('racing.individual_contestant', is_individual_contestant)

# 参赛队伍权限
rules.add_perm('racing.contestant', is_corporation_contestant | is_individual_contestant)

# 单位马主权限
rules.add_perm('racing.corporation_horse_owner', is_corporation_horse_owner)

# 个人马主权限
rules.add_perm('racing.individual_horse_owner', is_individual_horse_owner)

# 马主权限
rules.add_perm('racing.horse_owner', is_corporation_horse_owner | is_individual_horse_owner)

# 骑师权限
rules.add_perm('racing.horseman', is_horseman)

# 购票观众权限
rules.add_perm('racing.paid_attendance', is_paid_attendance)

# 角色已经选定
rules.add_perm('racing.has_role', has_role)

# 角色尚未选定
rules.add_perm('racing.has_no_role', ~has_role)

# 拥有指定角色权限
rules.add_perm('racing.is_role', is_role)

# 拥有个人角色权限
rules.add_perm('racing.is_individual_role', is_individual_contestant | is_individual_horse_owner | is_horseman)

# 用户实名认证满足角色要求
rules.add_perm('racing.is_certificated', is_certificated)

# 审核通过的协会会员
rules.add_perm('racing.is_member_qualified', is_member_qualified)

# 审核未通过的协会会员
rules.add_perm('racing.not_is_member_qualified', ~is_member_qualified)

# 审核通过的裁判员
rules.add_perm('racing.is_referee_qualified', is_referee_qualified)

# 审核通未过的裁判员
rules.add_perm('racing.not_is_referee_qualified', ~is_referee_qualified)

# 审核通过的骑师
rules.add_perm('racing.is_horseman_qualified', is_horseman_qualified)

# 审核未通过的骑师
rules.add_perm('racing.not_is_horseman_qualified', ~is_horseman_qualified)

# 审核通过的马匹
rules.add_perm('racing.is_horse_qualified', is_horse_qualified)

# 审核未通过的马匹
rules.add_perm('racing.not_is_horse_qualified', ~is_horse_qualified)

# 会员对象创建者
rules.add_perm('racing.is_member_creator', is_member_creator)

# 裁判员对象创建者
rules.add_perm('racing.is_referee_creator', is_referee_creator)

# 骑师对象创建者
rules.add_perm('racing.is_horseman_creator', is_horseman_creator)

# 骑师对象创建者
rules.add_perm('racing.is_horse_creator', is_horse_creator)

# 没有录入马匹数据
rules.add_perm('racing.has_no_horse', has_no_horse)

# 已经有核准的马匹
rules.add_perm('racing.has_qualified_horse', has_qualified_horse)

# 能否删除角色
rules.add_perm('racing.can_delete_role', can_delete_role)

# 报名锁定
rules.add_perm('racing.is_enrollment_fixed', is_enrollment_fixed)

# 出赛费是否可以支付
rules.add_perm('racing.can_pay_enrollment_fare', is_enrollment_fixed & is_enrollment_qualified & (~is_enrollment_fare_paid))

# 能否变更报名锁定
rules.add_perm('racing.can_change_enrollment_fixed', ~is_enrollment_qualified)


# 登录用户
rules.add_perm('racing.authenticated', is_authenticated)

# 报名是否可以核准
rules.add_perm('racing.can_qualify_enrollment', is_enrollment_fixed & (~is_enrollment_fare_paid))
