from django.conf import settings
from decimal import Decimal
from racing.models_ticket import Product

# 购物车
class Cart(object):
    def __init__(self, request):
        """
        初始化购物车
        如果用户session中存在购物车，直接使用，否则新建一个购物车
        """
        self.session = request.session
        cart = self.session.get(settings.TICKET_CART_SESSION_ID)
        if not cart:
            # 在session创建一个空的购物车
            cart = self.session[settings.TICKET_CART_SESSION_ID] = {}
        self.cart = cart

    def add(self, product, quantity=1, update_quantity=False):
        """
        向购物车添加门票Add a product to the cart or update its quantity.
        """
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {'quantity': 0, 'price': str(product.price)}
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        self.save()

    def save(self):
        # 更新session购物车
        self.session[settings.TICKET_CART_SESSION_ID] = self.cart
        # 将标记为"modified"一确保其被保存
        self.session.modified = True

    def remove(self, product):
        """
        从购物车删除门票
        """
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def __iter__(self):
        """
        遍历购物车内所有门票项目并从数据库获取对应的门票记录对象
        """
        product_ids = self.cart.keys()
        # 获取门票对象，将它们添加到购物车
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            self.cart[str(product.id)]['product'] = product
        # 计算并保存金额小计
        for item in self.cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        """
        计算购物车中门票的总数量
        """
        return sum(item['quantity'] for item in self.cart.values())

    # 计算购物车总金额
    def get_total_price(self):
        return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())

    # 清除购物车
    def clear(self):
        # 从session删除购物车
        del self.session[settings.TICKET_CART_SESSION_ID]
        self.session.modified = True
