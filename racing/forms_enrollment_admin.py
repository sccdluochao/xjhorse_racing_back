from django.contrib.auth.models import User
from django import forms
from certification.models import Certification, Corporation
from .models import Permission as User_Permission

#用户表
from permission.models import UserApplication
from racing.models_enrollment import Horse, Member, EnrollmentDetail, Game


#角色
class PermissionModelForm(forms.ModelForm):
    class Meta:
        model = User_Permission
        fields = (
            "user",
            "role",
        )
    def __init__(self, user_query_set,role_query_set,*args, **kwargs):
        super(PermissionModelForm, self).__init__(*args, **kwargs)
        self.fields['role'].queryset=role_query_set
        self.fields['user'].queryset = user_query_set

#参赛细节
class EnrollmentDetailModelForm(forms.ModelForm):
    class Meta:
        model = EnrollmentDetail
        fields = ('enrollment',
                  'game',
                  'horse',
                  'horseman',)
        widgets = {
            'horse': forms.TextInput,
            'horseman': forms.TextInput
        }

    def clean(self, *args, **kwargs):
        cleaned_data=super(EnrollmentDetailModelForm, self).clean(*args, **kwargs)
        return cleaned_data

    """
            if EnrollmentDetail.objects.filter(game=cleaned_data['game'],horse=cleaned_data['horse']).count()>0:
                raise forms.ValidationError("当前比赛中，这匹马已经被使用，请确认后再选择！")
            if EnrollmentDetail.objects.filter(game=cleaned_data['game'], horseman=cleaned_data['horseman']).count()>0:
                raise forms.ValidationError("当前比赛中，这名骑师已经被使用，请确认后再选择！")

            """

    def __init__(self, *args, **kwargs):
        super(EnrollmentDetailModelForm, self).__init__(*args, **kwargs)
        #self.fields['enrollment'].widget.attrs['disabled'] = 'disabled'
        self.fields['game'].queryset=Game.objects.filter(year__active=1,game_status=1)
        self.fields['horse'].widget.attrs['value'] = ""
        self.fields['horse'].widget.attrs['placeholder'] = "请输入马匹的名称、芯片号、烙号或护照号"
        self.fields['horse'].widget.attrs['autocomplete'] = "off"
        self.fields['horseman'].widget.attrs['value'] = ""
        self.fields['horseman'].widget.attrs['placeholder'] = "请输入骑师的身份证号码、真实姓名或手机号"
        self.fields['horseman'].widget.attrs['autocomplete'] = "off"

#队伍会员
class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ('member_type',
                  'member_name',
                  'contact',
                  'contact_number',
                  'association',
                  'trainer',
                  'trainer_number',
                  'veterinarian',
                  'veterinarian_number')

    # 禁止用户选择会员类型
    def __init__(self, *args, **kwargs):
        super(MemberForm, self).__init__(*args, **kwargs)
        self.fields['member_type'].widget.attrs['disabled'] = 'disabled'
        self.fields['member_type'].required = False

    def clean_member_type(self):
        if self.instance and self.instance.pk:
            return self.instance.member_type
        else:
            return self.cleaned_data['member_type']

#用户模型
class UserModelForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("username",
                  "email",
                  "password",)

    def clean_username(self):
        cleaned_data=super(UserModelForm, self).clean()
        user_list=User.objects.filter(username=cleaned_data['username'])
        if user_list.count() > 0:
            raise forms.ValidationError("用户名已经存在！请更换后再重试！")

    def clean_email(self):
        cleaned_data = super(UserModelForm, self).clean()
        user_list = User.objects.filter(email=cleaned_data['email'])
        if user_list.count() > 0:
            raise forms.ValidationError("邮箱号已经存在！请更换后再重试！")

    #验证错误
    def is_valid_custom(self,request):
        errors=self.errors
        user_list = User.objects.filter(username=request.POST['username'])
        #不能为空或被使用
        if request.POST["username"] == "" \
                or request.POST["username"] == None \
                or user_list.count() > 0:
            return False
        print()
        #判断是否有关于邮箱和密码的错误信息
        if "email" in errors \
                or "password" in errors:
            return False
        return True

    def __init__(self,*args, **kwargs):
        super(UserModelForm, self).__init__(*args, **kwargs)

        #当存在实例的时候
        if self.instance and self.instance.pk:
            self.fields['password'].widget.attrs['placeholder'] = "如果不修改密码,可以不用填写！"
            self.use_required_attribute=False
            self.fields['username'].widget.attrs['required'] = ""

        self.fields['password'].widget.attrs['value'] = ""
        self.fields['password'].widget.attrs['minlength'] = "8"
        self.fields['username'].widget.attrs['autocomplete'] = "off"
        self.fields['username'].widget.attrs['placeholder'] = "可以直接使用手机号码！"
        self.fields['email'].widget.attrs['autocomplete'] = "off"
        self.fields['email'].widget.attrs['required'] = ""
        self.fields['email'].widget.attrs['placeholder'] = "可以用qq邮箱，或者注册一个网易邮箱"

#个人实名认证
class IndividualCertificationModelForm(forms.ModelForm):
    class Meta:
        model = Certification
        fields = ["id_photo",
                "userName",
                "gender",
                "ethnicity",
                "birthday",
                "identity",
                "address",
                "mobile",]

    def __init__(self,*args, **kwargs):
        super(IndividualCertificationModelForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.use_required_attribute=False
            #没有图片，设为必须上传
            if self.instance.id_photo == "":
                self.fields['id_photo'].widget.attrs['required'] = "required"
        else:
            self.fields['id_photo'].widget.attrs['required'] = "required"
        self.fields['userName'].widget.attrs['readonly'] = "readonly"
        self.fields['userName'].widget.attrs['required'] = "required"
        self.fields['gender'].widget.attrs['readonly'] = "readonly"
        self.fields['ethnicity'].widget.attrs['readonly'] = "readonly"
        self.fields['birthday'].widget.attrs['readonly'] = "readonly"
        self.fields['identity'].widget.attrs['readonly'] = "readonly"
        self.fields['address'].widget.attrs['readonly'] = "readonly"
        self.fields['mobile'].widget.attrs['autocomplete'] = "off"
        self.fields['mobile'].widget.attrs['required'] = "required"


#单位实名认证
class MemberCertificationModelForm(forms.ModelForm):
    class Meta:
        model = Certification
        fields = ["id_photo",
                "userName",
                "gender",
                "ethnicity",
                "birthday",
                "identity",
                "address",
                "mobile",
                "authorization_photo",]

    def __init__(self,*args, **kwargs):
        super(MemberCertificationModelForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.use_required_attribute=False
            #没有图片，设为必须上传
            if self.instance.id_photo == "":
                self.fields['id_photo'].widget.attrs['required'] = "required"
        else:
            self.fields['id_photo'].widget.attrs['required'] = "required"
        self.fields['userName'].widget.attrs['readonly'] = "readonly"
        self.fields['gender'].widget.attrs['readonly'] = "readonly"
        self.fields['ethnicity'].widget.attrs['readonly'] = "readonly"
        self.fields['birthday'].widget.attrs['readonly'] = "readonly"
        self.fields['identity'].widget.attrs['readonly'] = "readonly"
        self.fields['address'].widget.attrs['readonly'] = "readonly"
        self.fields['mobile'].widget.attrs['autocomplete'] = "off"
        self.fields['mobile'].widget.attrs['required'] = "required"

#单位信息表单
class CorporationModelForm(forms.ModelForm):
    class Meta:
        model = Corporation
        fields = ["uscc",
                "name",
                "corporation_type",
                "domicile",
                "legal_representative",
                "registered_capital",
                "operating_period",
                "business_scope",
                "registration_authority",
                "corporation_photo",]

    def __init__(self,*args, **kwargs):
        super(CorporationModelForm, self).__init__(*args, **kwargs)

#马匹表单
class HorseModelForm(forms.ModelForm):
    class Meta:
        model = Horse
        fields = [
              'user',
              'horse_type',
              'name',
              'gender',
              'birthday',
              'birthplace',
              'breed',
              'brand',
              'chip_no',
              'passport_no',
              'father_name',
              'mother_name',
              'grandfather_name',
              'vaccinated',
              'vaccinate_date',
              'vaccine',
              'record',
              'horse_photo',]

        widgets={
            'user': forms.TextInput
        }

    def __init__(self,*args, **kwargs):
        super(HorseModelForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.fields['user'].widget.attrs['readonly'] = "readonly"
        self.fields['user'].widget.attrs['value'] = ""
        self.fields['user'].widget.attrs['placeholder'] = "请输入马主的身份证号码或者真实姓名"
        self.fields['user'].widget.attrs['autocomplete'] = "off"
        self.fields['vaccinate_date'].widget.attrs['time-tag'] = "select_date"
        self.fields['vaccinate_date'].widget.attrs['autocomplete'] = "off"
        self.fields['birthday'].widget.attrs['time-tag'] = "select_date"
        self.fields['birthday'].widget.attrs['autocomplete'] = "off"

# 骑师资格
"""
class HorsemanForm(forms.ModelForm):
    class Meta:
        #model = Horseman
        fields = ('user',
                  #'member',
                  'height',
                  'weight',
                  'native_place',
                  'employer',
                  'horseman_photo',
                  'health_certificate_photo',
                  'record'
                  )

        widgets = {'user': forms.HiddenInput()}

    # 只允许选择通过资格审核的会员（参赛队伍）
    def __init__(self, *args, **kwargs):
        super(HorsemanForm, self).__init__(*args, **kwargs)
        #members = Member.objects.filter(qualified=True)
        #self.fields['member'].queryset = members
"""