from django.views.generic import ListView, DeleteView, UpdateView, CreateView, DetailView
from django.urls import reverse_lazy
from rules.contrib.views import PermissionRequiredMixin
from django.shortcuts import render

from racing.models_enrollment import Game
from racing.models_home import Carousel, EventsAnnouncement, Video


# 创 建 人：张太红
# 创建日期：2018年03月17日


# 首页
def racing(request):
    carousels = Carousel.objects.filter(enabled=True).order_by('ranking')
    videos = Video.objects.all().order_by('ranking')
    events_announcements = EventsAnnouncement.objects.filter(enabled=True).order_by('-created')
    games = Game.objects.filter(active=1,year__active=1)

    for item in games:
        for ed in item.enrollmentdetail_set.all():
            print(ed.horse)
            print(ed.horseman)
            print(ed.enrollment.member)

    context = {
        "videos": videos,
        "carousels": carousels,
        "events_announcements": events_announcements,
        "games": games,
    }
    return render(request, "racing/home/index.html", context)


# ********************************************************
# 赛事公告 model：EventsAnnouncement
# ********************************************************
class AnnouncementManageListView(PermissionRequiredMixin, ListView):
    queryset = EventsAnnouncement.objects.all().order_by('-created')

    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/home/announcement_manage_list.html'

    def get_permission_object(self):
        return "天马赛事"


announcement_manage_list = AnnouncementManageListView.as_view()


class AnnouncementManageDetailView(PermissionRequiredMixin, DetailView):
    model = EventsAnnouncement
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/home/announcement_manage_detail.html'

    def get_permission_object(self):
        return "天马赛事"


announcement_manage_detail = AnnouncementManageDetailView.as_view()


class AnnouncementManageCreateView(PermissionRequiredMixin, CreateView):
    model = EventsAnnouncement
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/home/announcement_manage_create.html'
    fields = ['title',
              'content',
              'photo',
              'enabled',
              ]

    def get_permission_object(self):
        return "天马赛事"


announcement_manage_create = AnnouncementManageCreateView.as_view()


class AnnouncementManageUpdateView(PermissionRequiredMixin, UpdateView):
    model = EventsAnnouncement
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True

    template_name = 'racing/home/announcement_manage_update.html'
    fields = ['title',
              'content',
              'photo',
              'enabled',
              ]

    def get_permission_object(self):
        return "天马赛事"


announcement_manage_update = AnnouncementManageUpdateView.as_view()


class AnnouncementManageDeleteView(PermissionRequiredMixin, DeleteView):
    model = EventsAnnouncement
    success_url = reverse_lazy('announcement_manage_list')
    permission_required = ['permission.application_user', 'racing.admin', ]
    raise_exception = True
    template_name = 'racing/home/announcement_manage_delete.html'

    def get_permission_object(self):
        return "天马赛事"


announcement_manage_delete = AnnouncementManageDeleteView.as_view()

#赛事公告展示
def announcement_show(request,pk):
    event=EventsAnnouncement.objects.get(id=pk,enabled=1)
    return render(request,"racing/home/announcement_show.html",{"object":event})