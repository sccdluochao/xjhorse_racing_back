from django.conf.urls import url
from racing import views_regulation as views

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 赛事规程
# ********************************************************
urlpatterns = [
    url(r'^racing/regulation$', views.regulation, name='regulation'),
    url(r'^racing/regulation_manage_list$', views.regulation_manage_list, name='regulation_manage_list'),
    url(r'^racing/regulation_manage/(?P<pk>\d+)$', views.regulation_manage_detail, name='regulation_manage_detail'),
    url(r'^racing/regulation_manage/create$', views.regulation_manage_create, name='regulation_manage_create'),
    url(r'^racing/regulation_manage/update/(?P<pk>\d+)$', views.regulation_manage_update,
        name='regulation_manage_update'),
    url(r'^racing/regulation_manage/delete/(?P<pk>\d+)$', views.regulation_manage_delete,
        name='regulation_manage_delete'),
    #编辑比赛日
    url(r'^racing/regulation_manage/edit_gameday/(?P<r_id>\d+)/$', views.edit_gameday,
        name='regulation_manage_edit_gameday'),
]
