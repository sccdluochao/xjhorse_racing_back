from django.conf.urls import url
from racing import views_schedule as views

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 比赛日程
# ********************************************************

urlpatterns = [
    url(r'^racing/schedule_list$', views.schedule_list, name='schedule_list'),
    url(r'^racing/get_schedule_pdf/(?P<pk>\d+)$', views.get_schedule_pdf, name='get_schedule_pdf'),
]
