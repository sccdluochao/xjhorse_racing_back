from django.conf.urls import url
from racing import views_enrollment as views

# 创 建 人：张太红
# 创建日期：2018年03月17日

# ********************************************************
# 赛事报名
# ********************************************************

urlpatterns = [
    url(r'^racing/enrollment$', views.enrolment, name='enrollment'),

    # 会员资格申请
    url(r'^racing/enrollment_member_apply$', views.enrollment_member_apply, name='enrollment_member_apply'),
    url(r'^racing/enrollment_member_detail$', views.enrollment_member_detail, name='enrollment_member_detail'),

    # 裁判员资格申请
    url(r'^racing/enrollment_referee_apply$', views.enrollment_referee_apply, name='enrollment_referee_apply'),
    url(r'^racing/enrollment_referee_detail$', views.enrollment_referee_detail, name='enrollment_referee_detail'),

    # 骑师资格申请
    url(r'^racing/enrollment_horseman_apply$', views.enrollment_horseman_apply, name='enrollment_horseman_apply'),
    url(r'^racing/enrollment_horseman_detail$', views.enrollment_horseman_detail, name='enrollment_horseman_detail'),

    # 马匹资格申请
    url(r'^racing/enrollment_horse_owner_list$', views.enrollment_horse_owner_list, name='enrollment_horse_owner_list'),
    url(r'^racing/enrollment_horse_owner_create$', views.enrollment_horse_owner_create, name='enrollment_horse_owner_create'),
    url(r'^racing/enrollment_horse_detail/(?P<pk>\d+)$', views.enrollment_horse_detail, name='enrollment_horse_detail'),
    url(r'^racing/enrollment_horse_owner_update/(?P<pk>\d+)$', views.enrollment_horse_owner_update, name='enrollment_horse_owner_update'),
    url(r'^racing/enrollment_horse_owner_delete/(?P<pk>\d+)$', views.enrollment_horse_owner_delete, name='enrollment_horse_owner_delete'),

    # 参赛项目报名
    url(r'^racing/team_game_enrollment', views.team_game_enrollment, name='team_game_enrollment'),
    url(r'^racing/enrollment_team_game_create/(?P<enrollment_id>\d+)$', views.enrollment_team_game_create, name='enrollment_team_game_create'),
    url(r'^racing/enrollment_team_game_delete/(?P<pk>\d+)$', views.enrollment_team_game_delete, name='enrollment_team_game_delete'),
    url(r'^racing/enrollment_fixed/(?P<pk>\d+)$', views.enrollment_fixed, name='enrollment_fixed'),

    # ********************************************************
    # 马匹出赛费支付
    # ********************************************************
    # 支付宝支付入口
    url(r'^racing/enrollment/alipay/(?P<payment_uuid>[\w-]+)/$', views.pay, name='racing_fare_alipay'),
    # 支付宝异步通知处理接口
    url(r'^racing/enrollment/notify_url$', views.alipay_notify_url, name='racing_fare_alipay_notify_url'),
    # 支付宝同步通知处理接口
    url(r'^racing/enrollment/return_url$', views.alipay_return_url, name='racing_fare_alipay_return_url'),


    # ********************************************************
    # 以下是管理员的操作功能
    # ********************************************************

    # 会员资格审核
    url(r'^racing/member_authorize$', views.member_authorize, name='member_authorize'),
    url(r'^racing/member_authorize/(?P<pk>\d+)$', views.member_authorize_detail, name='member_authorize_detail'),
    url(r'^racing/member_authorize_update/(?P<pk>\d+)$', views.member_authorize_update, name='member_authorize_update'),

    # 裁判员资格审核
    url(r'^racing/referee_authorize$', views.referee_authorize, name='referee_authorize'),
    url(r'^racing/referee_authorize/(?P<pk>\d+)$', views.referee_authorize_detail, name='referee_authorize_detail'),
    url(r'^racing/referee_authorize_update/(?P<pk>\d+)$', views.referee_authorize_update,
        name='referee_authorize_update'),

    # 骑师资格审核
    url(r'^racing/horseman_authorize$', views.horseman_authorize, name='horseman_authorize'),
    url(r'^racing/horseman_authorize/(?P<pk>\d+)$', views.horseman_authorize_detail, name='horseman_authorize_detail'),
    url(r'^racing/horseman_authorize_update/(?P<pk>\d+)$', views.horseman_authorize_update,
        name='horseman_authorize_update'),

    # 马匹资格审核
    url(r'^racing/horse_authorize$', views.horse_authorize, name='horse_authorize'),
    url(r'^racing/horse_authorize/(?P<pk>\d+)$', views.horse_authorize_detail, name='horse_authorize_detail'),
    url(r'^racing/horse_authorize_update/(?P<pk>\d+)$', views.horse_authorize_update,name='horse_authorize_update'),

    # 报名审核
    url(r'^racing/enrollment_authorize$', views.enrollment_authorize, name='enrollment_authorize'),
    url(r'^racing/enrollment_authorize/(?P<pk>\d+)$', views.enrollment_authorize_detail, name='enrollment_authorize_detail'),
    url(r'^racing/enrollment_authorize_update/(?P<pk>\d+)$', views.enrollment_authorize_update, name='enrollment_authorize_update'),
    url(r'^racing/enrollment_confirmpayment/(?P<eid>\d+)$', views.confirmPayment, name='enrollment_confirmpayment'),
]
