# xjhorse/homePage/views.py

from django.shortcuts import render
from homepage.models import Carousels


def home(request):
    carousels = Carousels.objects.filter(enabled=True).order_by('ranking')
    context = {
        "carousels": carousels,
    }
    return render(request, "homepage/index.html", context)
