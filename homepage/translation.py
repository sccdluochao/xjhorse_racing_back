'''
   创 建 人：张太红
   创建日期：2018年01月22日
   注册需要翻译（多语言支持）的模型（Models）
'''

from modeltranslation.translator import translator, TranslationOptions
from homepage.models import Carousels

# 指定品种轮播表中需要翻译的字段
class CarouselsTranslationOptions(TranslationOptions):
    fields = ('title', 'slogan',)

# 注册需要翻译的表
translator.register(Carousels, CarouselsTranslationOptions)
