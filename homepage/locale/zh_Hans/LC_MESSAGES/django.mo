��    (      \  5   �      p     q     �     �     �  <   �     �  2   �  	        '     :  J   B     �     �     �  
   �  9   �     �     	       
        *     0     ?     O  G   ]     �     �     �     �     �  =   �            1   #  #   U  7   y  /   �  +   �  &     @  4     u     �     �     �  !   �     �  D   �     	     *	     7	  6   >	     u	     �	     �	     �	  !   �	     �	     �	     �	     �	     
     
     
     &
  '   9
     a
     n
     u
     �
     �
  )   �
     �
     �
  *   �
     �
  '     6   7  *   n     �                                      %                        $   
             (       &          	               !      "             '   #                                            Account binding Active Auction Bole Classroom Boost Region’s Horse Industry Transformation and Upgrading Certification Copyright © 2017 Xinjiang Agricultural University Developer Disease Prevention Enabled Exploring the New Form of "Internet + Horse Industry" Economic Development Fund Program HomePage Horse Breeds Horse Farm Horse Industry Upgrading Technological Innovation Program Language Preference Logout My Application Objectives Photo Photo Carousel Photo Carousels Program Teams Promoting The integration of Internet with Traditional Horse Industries Racing Ranking Registration Sign In Slogan Task No.5 Horse Industry S&T Innovative Platform Construction Title Xinjiang Horse Xinjiang Uygur Autonomous Region Key S&T Projects ①Xinjiang Agricultural University ②Zhaosu Horse Farm of Ili Kazak Autonomous Prefecture ③Ili Stud of Xinjiang Uygur Autonomous Region ④Zhaosu Western Region Horse Industry LLC ⑤Xinjiang Horse Industry Association Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-08 23:52+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 账户绑定 轮播起点 马匹竞拍 伯乐学堂 助推我区马产业转型升级 实名认证 版权所有© 2017 新疆农业大学计算机与信息工程学院 开发人员 马病预防 启用 探索“互联网＋马产业”经济发展新形态 基金项目 新疆马业首页 品种展示 马场管理 马产业升级技术创新工程 语言偏好 注销 我的应用平台 课题目标 照片 照片轮播 照片轮播 课题承担单位 推动互联网与传统马产业融合 天马赛事 排名 马匹登记 登录 标语 课题5:马产业科技创新平台建设 标题 新疆马业 新疆维吾尔自治区重大科技专项 ①新疆农业大学 ②伊犁哈萨克自治州昭苏马场 ③新疆维吾尔自治区地方国营伊犁种马场 ④昭苏县西域马业有限责任公司 ⑤新疆马业协会 