from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from homepage.models import Carousels


class CarouselsAdmin(TranslationAdmin):
    list_display = ('id', 'title', 'slogan', 'ranking', 'photo', 'enabled', 'active')  # 列表


admin.site.register(Carousels, CarouselsAdmin)
