from django.db import models
from django.core.files.storage import FileSystemStorage
from django.utils.translation import ugettext_lazy as _
import os

# 重复上传照片时，用新照片覆盖老照片
class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return super(OverwriteStorage, self).get_available_name(name, max_length)

# 照片上传位置和文件名称
def upload_photo(instance, filename):
    return "homePage/%s" % (filename)

# 首页轮播照片
class Carousels(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Title'))
    slogan = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Slogan'))
    ranking = models.SmallIntegerField(default=0, verbose_name=_('Ranking'))
    photo = models.ImageField(
        upload_to=upload_photo,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Photo"))
    url = models.URLField(null=True, blank=True)
    enabled = models.BooleanField(default=True, verbose_name=_("Enabled"))
    active = models.BooleanField(default=False, verbose_name=_("Active"))
    class Meta:
        verbose_name = _('Photo Carousel')
        verbose_name_plural = _('Photo Carousels')
