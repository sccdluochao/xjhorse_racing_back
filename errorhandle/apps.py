from django.apps import AppConfig


class ErrorhandleConfig(AppConfig):
    name = 'errorhandle'
