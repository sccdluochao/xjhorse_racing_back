from django.http import HttpResponseForbidden
from django.shortcuts import render


# Error Pages
def server_error(request):
    return render(request, 'errorhandle/500.html')


def not_found(request):
    return render(request, 'errorhandle/404.html')


def permission_denied(request):
    return render(request, 'errorhandle/403.html')


def bad_request(request):
    return render(request, 'errorhandle/400.html')


def csrf_failure(request, reason=""):
    context = {
        "reason": reason,
    }
    response = render(request, 'errorhandle/403_csrf.html', context)
    response.status_code = 403

    return HttpResponseForbidden(response, content_type='text/html')
