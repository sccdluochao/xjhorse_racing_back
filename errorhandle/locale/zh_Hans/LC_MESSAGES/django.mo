��          �      |      �     �            
   2     =     K     Y  5   f     �     �     �  6   �             &   /  #   V  6   z  5   �  B   �     *  <   <  d  y     �     �  %        *     7     D     Q  ?   ^     �     �     �  +   �     �  !   �  $        B  -   a     �  ?   �     �  "   �        
                      	                                                                   Back to Xjhorse Home Bad request Correct your url and try again. Error Code Error Meaning Error Message Error Reason Forbidden, CSRF verification failed. Request aborted! Internal server error! Message  Message Item Most commonly, this error means you typed a wrong url. Permission denied Please contact project team. Please enable cookies on your browser. Request host is not a allowed host. Server get into trouble when running some python code. The page that you are going to access doesn't exists! This request requires cookies, which are disabled on your browser. Treatment Measure You do not have permission to access the requested resource. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-08 23:52+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 返回新疆马业首页 错误请求 输入正确的网址，再试一次! 错误代码 错误含义 出错信息 出错原因 禁止访问，跨站请求伪造验证失败，请求被终止 服务器内部错误 内容 项目 通常是由于你输入了错误的网址! 没有权限 请与项目组开发人员联系 请在你的浏览器上启用Cookie 请求的主机不禁止访问 服务器在运行某些代码时发生错误 你要访问的页面不存在! 该请求需要使用Cookie，而你的浏览器禁用了Cookie 处理措施 你没有访问该资源的权限! 