'''
   创 建 人：张太红
   创建日期：2018年02月01日
   业务逻辑错误处理中间件
'''
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.utils.http import is_safe_url, urlunquote

from errorhandle.exceptions import BusinessLogicException


def get_previous_url(request):
    previous = request.META.get('HTTP_REFERER')
    if previous:
        previous = urlunquote(previous)  # HTTP_REFERER may be encoded.
    if not is_safe_url(url=previous, host=request.get_host()):
        previous = '/'
    return previous


# 发生业务逻辑错误错误时，重定向用户到先前请求的页面并显示错误信息
class RedirectToRefererResponse(HttpResponseRedirect):
    def __init__(self, request, *args, **kwargs):
        redirect_to = get_previous_url(request)
        super(RedirectToRefererResponse, self).__init__(
            redirect_to, *args, **kwargs)


class HandleBusinessExceptionMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_exception(self, request, exception):
        if isinstance(exception, BusinessLogicException):
            message = _("Invalid operation:%(err_message)s" % {"err_message": str(exception)})
            messages.error(request, message)
            return RedirectToRefererResponse(request)
