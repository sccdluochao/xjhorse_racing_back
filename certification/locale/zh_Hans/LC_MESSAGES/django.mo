��    M      �  g   �      �  "   �  U   �          
          !     :  &   N  !   u  (   �     �     �     �     �     �          #     2     8     D     U  $   g     �  &   �     �     �     �     	     	     +	     >	     J	     S	  	   c	     m	     q	     x	     �	     �	     �	     �	     �	     �	  !   
     &
     ;
     B
  #   V
     z
     �
     �
     �
  	   �
  3   �
  *     ,   9  (   f     �     �     �     �     �     �     �          (  "   G  	   j  
   t          �     �     �     �             d    '   |  T   �     �                     -     C     b     {     �     �     �     �     �     �                    !     .     A     Z     m     �     �     �     �     �     �     �     �     �                    "     8     T     d     z     �     �  6   �     �  	   �     �          3     F  	   _     i     �     �     �     �     �          $     1     >     K     d     �     �     �  $   �     �     �     �  3        H     [     t     �     �               6      
                   '      0           A       #                        C   :          B   8   )   &       3      %                *   G              ,   L              F   >   H   I   9      ;      J   .      "       D   -      2   E   +       K   /   (   =      ?       @   <      1                       !   $       M      4   5       7                    	    1. Why do you need to certificate? 2. If i already have finished certificating, could i certificate one more time again? Address Administrator Approved Authorization Doc Sample Authorization Photo Authorization Photo Certificate Status Authorization Photo Certification Authorization Photo Certification Detail Birthday Business Scope Certification Certification Detail Certification Status Certification Type Certifications Close Corporation Corporation Name Corporation Photo Corporation Photo Certificate Status Corporation Photo Certification Corporation Photo Certification Detail Corporation Photo Sample Corporation Type CorporationType CorporationTypes Corporations Disapprove Comment Disapproved Domicile Download Sample Ethnicity FAQ Gender Get SMS Certificate Code ID Photo Certificated ID Photo Certification ID Photo Certification Detail ID Photo Sample Identity Identity Photo Identity Photo Certificated by AI Legal Representative Mobile Mobile Certificated Mobile SMS Certificate successfully Mobile SMS Certification Mobile SMS Certification Detail Not applied Operating Period Operation Please finish Corporation photo Certification first Please finish ID photo Certification first Please finish User photo Certification first Please finish mobile Certification first Registered Capital Registration Authority Result SMS Certification Select user photo Select your ID photo Start Certification Unified social credit code Upload & cerificate your photo Upload & certificate your ID photo User Name User Photo User Photo Certificated User Photo Certificated by AI User Photo Certification User Photo Certification Detail User Photo Sample UserID Wait for review Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-08 23:52+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 1. 为什么需要进行实名认证？ 2. 如果我已经完成了某项实名认证，能否撤销以便重新再认证？ 住址 系统管理员 通过 单位授权委托书 单位授权委托书 单位授权委托认证状态 单位授权委托认证 单位授权委托认证详情 出生日期 经营范围（业务范围） 实名认证 认证详情 实名认证状态 实名认证类型 实名认证 关闭 单位 单位名称 法人证书照片 法人证书认证状态 法人证书认证 法人证书认证详情 法人证书样本 单位类型 单位类型 单位类型 单位 审核未通过原因 审核未通过 单位住所 下载样本 民族 常见问题 性别 获取短信验证码 身份证照片认证成功 身份证认证 身份证认证详情 身份证照片样本 身份证号 身份证照片 您上传的身份证照片由人工智能认证成功 法定代表人 手机号 手机短信认证成功 手机短信认证成功！ 手机短信认证 手机短信认证详情 未申请 经营期限（有效期） 操作 请先完成法人证书认证 请先完成身份证认证 请先完成用户照片认证 请先完成手机短信认证 注册资本（开办资金） 登记机关 认证结果 短信认证 选择您的照片文件 选择您的身份证照片 开始认证 统一社会信用代码 上传并认证您的照片 上传并认证您的身份证照片 姓名 用户照片 用户照片认证成功 您上传的用户照片由人工智能认证成功 用户照片认证 用户照片认证详情 用户照片样本 用户ID 等待审核 