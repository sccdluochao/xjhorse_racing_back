from django.conf.urls import url
from certification import views

urlpatterns = [
    url(r'^certification$', views.certification, name='certification'),
    url(r'^certification/(?P<certification_type>\d+)$', views.certificate, name='certificate'),
    url(r'^certification/detail/(?P<certification_type>\d+)$', views.certificate_detail, name='certificate_detail'),
    url(r'^certification/authorization_pdf$', views.get_authorization_PDF, name='authorization_pdf'),
    url(r'^certification_minority$', views.certificate_minority, name='certificate_minority'),
]
