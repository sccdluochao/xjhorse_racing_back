import datetime
import hashlib
import json

import os
import urllib3
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import JsonResponse
from django.template.loader import render_to_string

# 用于生成PDF
from django.http import HttpResponse
from io import BytesIO
from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.units import inch
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.lib.enums import TA_CENTER
import reportlab.rl_config
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib import fonts
import copy

from xjhorse import settings
from xjhorse.settings import BASE_DIR

'''
    作者：张太红
    时间：2017-12-04
'''

from certification.models import Certification
from certification.models import Corporation
from certification.forms import CertificationIDForm
from certification.forms import CertificationUserForm
from certification.forms import CertificationMobileForm
from certification.forms import CertificationCorporationForm
from certification.forms import CorporationForm
from certification.forms import CertificationAuthorizationForm


# 实名认证主界面
@login_required
def certification(request):
    try:
        # 先尝试获取用户实名认证记录
        certificate = Certification.objects.get(user=request.user)
    except Certification.DoesNotExist:
        # 如果用户实名认证记录不存在，创建一条新纪录
        certificate = Certification(user=request.user)
        certificate.save()
    context = {
        "certificate": certificate,
    }
    return render(request, "certification/certification.html", context)


# 实名认证表格部分
@login_required
def certification_partial(request):
    certificate = Certification.objects.get(user=request.user)
    context = {
        "certificate": certificate,
    }
    render_to_string('certification/certification_partial.html',
                     context,
                     request=request
                     )


# 少数民族认证
@login_required
def certificate_minority(request):
    data = dict()
    data['status']=0
    try:
        # 先尝试获取用户实名认证记录
        certificate = Certification.objects.get(user=request.user)
    except Certification.DoesNotExist:
        # 如果用户实名认证记录不存在，创建一条新纪录
        certificate = Certification(user=request.user)
        certificate.save()

    if request.method=="POST":
        c_form=CertificationIDForm(request.POST, request.FILES, instance=certificate)
        if c_form.is_valid():
            c_form.save()
            new_certificate = Certification.objects.get(user=request.user)
            with open(BASE_DIR+"/"+new_certificate.id_photo.url, "rb") as f:
                body = f.read()  # body内存放所有的文件流
                current_datetime = str(datetime.datetime.now())
                dev_key = "55a7042d338dff78107d7b7599e26ba6"
                headers = {
                    "x-app-key": "c45d5465",
                    "x-request-date": current_datetime,
                    "x-session-key": hashlib.md5((current_datetime + dev_key).encode('utf-8')).hexdigest(),
                    # 身份证正面
                    "x-task-config": "capkey=ocr.cloud.template,property=idcard,templateIndex=0,templatePageIndex=0",
                    # 身份证反面
                    #"x-task-config": "capkey=ocr.cloud.template,property=idcard,templateIndex=0,templatePageIndex=1",
                    "x-sdk-version": "5.2"
                }
                http = urllib3.PoolManager()
                urllib3.disable_warnings()
                r = http.request('POST',
                                 "http://ocr.aicloud.com:8541/ocr ",
                                 body=body,
                                 headers=headers)
                r_msg=json.loads(r.data.decode('utf-8'))

                #请求成功
                if r.status == 200 and r_msg['error'] == 0:
                    r_id_msg = r_msg['data']['forms']['form']['page']['cell']
                    new_certificate.identity=r_id_msg[0]['result']['result']
                    new_certificate.userName=r_id_msg[2]['result']['result']
                    new_certificate.ethnicity = r_id_msg[1]['result']['result']
                    new_certificate.address = r_id_msg[5]['result']['result']
                    new_certificate.gender = r_id_msg[3]['result']['result']
                    new_certificate.birthday = r_id_msg[4][ 'cell'][0]['result']['result']+"-"+r_id_msg[4][ 'cell'][1]['result']['result']+"-"+r_id_msg[4][ 'cell'][2]['result']['result']
                    new_certificate.id_photo_certificated=True
                    new_certificate.save()
                    data['status']=1
                elif r_msg['error'] != 0:
                    new_certificate.id_photo=None
                    new_certificate.save()
        else:
            print(c_form.errors)
        return JsonResponse(data)

    if certificate.id_photo_certificated == True:
        data['html_form'] = ""
        return JsonResponse(data)

    context = {
        'certificate': certificate,
    }
    data['html_form'] = render_to_string("certification/minority_id_photo_certificate.html",
                                         context,
                                         request=request
                                         )
    return JsonResponse(data)


# 身份证认证情况
@login_required
def certificate(request, certification_type):
    data = dict()
    result_template, form_template, detail_template = get_certification_templete(certification_type)
    try:
        # 先尝试获取用户实名认证记录
        certificate = Certification.objects.get(user=request.user)

    except Certification.DoesNotExist:
        # 如果用户实名认证记录不存在，创建一条新纪录
        certificate = Certification(user=request.user)
        certificate.save()

    if request.method == 'POST':
        # request.FILES用来接收上传的照片文件
        if certification_type == '4':
            form_corperation = CertificationCorporationForm(request.POST, instance=certificate)
            if form_corperation.is_valid():
                form_corperation.save()
                new_certificate = Certification.objects.get(user=request.user)
                data['html_certification_result'] = render_to_string(result_template, {
                    'certificate': new_certificate
                })
                data['html_certification'] = render_to_string('certification/certification_partial.html', {
                    'certificate': new_certificate
                })
        form = get_post_form(request, certificate, certification_type)
        if form.is_valid():
            # 表单提交并更新成功
            form.save()
            data['form_is_valid'] = True
            new_certificate = Certification.objects.get(user=request.user)
            data['html_certification'] = render_to_string('certification/certification_partial.html', {
                'certificate': new_certificate
            })
            data['html_certification_result'] = render_to_string(result_template, {
                'certificate': new_certificate
            })
    else:
        form = get_form(request, certificate, certification_type)

    context = {
        'form': form,
        'certificate': certificate,
    }
    data['html_form'] = render_to_string(form_template,
                                         context,
                                         request=request
                                         )
    return JsonResponse(data)

# 身份证认证情况
@login_required
def certificate_detail(request, certification_type):
    result_template, form_template, detail_template = get_certification_templete(certification_type)
    data = dict()
    try:
        # 先尝试获取用户实名认证记录
        certificate = Certification.objects.get(user=request.user)
    except Certification.DoesNotExist:
        # 如果用户实名认证记录不存在，创建一条新纪录
        certificate = Certification(user=request.user)
        certificate.save()
    try:
        # 获取单位记录
        corporation = Corporation.objects.get(admin=request.user)
    except Corporation.DoesNotExist:
        # 如果单位记录不存在
        corporation = None
    context = {
        'certificate': certificate,
        'corporation': corporation,
    }
    data['html'] = render_to_string(detail_template, context, request)
    return JsonResponse(data)


# 根据认证类型生成对应的模板文件名称
def get_certification_templete(certification_name):
    result_template = ""
    form_template = ""
    detail_template = ""
    if certification_name == '1':
        result_template = 'certification/id_photo_certificate_result.html'
        form_template = 'certification/id_photo_certificate.html'
        detail_template = 'certification/id_photo_certificate_detail.html'
    elif certification_name == '2':
        result_template = 'certification/user_photo_certificate_result.html'
        form_template = 'certification/user_photo_certificate.html'
        detail_template = 'certification/user_photo_certificate_detail.html'
    elif certification_name == '3':
        result_template = 'certification/mobile_certificate_result.html'
        form_template = 'certification/mobile_certificate.html'
        detail_template = 'certification/mobile_certificate_detail.html'
    elif certification_name == '4':
        result_template = 'certification/corporation_photo_certificate_result.html'
        form_template = 'certification/corporation_photo_certificate.html'
        detail_template = 'certification/corporation_photo_certificate_detail.html'
    elif certification_name == '5':
        result_template = 'certification/authorization_photo_certificate_result.html'
        form_template = 'certification/authorization_photo_certificate.html'
        detail_template = 'certification/authorization_photo_certificate_detail.html'

    return (result_template, form_template, detail_template)


# 根据认证类型生成对应的form
def get_post_form(request, certificate, certification_name):
    if certification_name == '1':
        form = CertificationIDForm(request.POST, request.FILES, instance=certificate)
    elif certification_name == '2':
        form = CertificationUserForm(request.POST, request.FILES, instance=certificate)
    elif certification_name == '3':
        form = CertificationMobileForm(request.POST, instance=certificate)
    elif certification_name == '4':
        try:
            corporation = Corporation.objects.get(admin=request.user)
            form = CorporationForm(request.POST, request.FILES, instance=corporation)
        except Corporation.DoesNotExist:
            form = CorporationForm(request.POST, request.FILES, initial={'admin': request.user})
    elif certification_name == '5':
        form = CertificationAuthorizationForm(request.POST, request.FILES, instance=certificate)
    return form


def get_form(request, certificate, certification_name):
    if certification_name == '1':
        form = CertificationIDForm(instance=certificate)
    elif certification_name == '2':
        form = CertificationUserForm(instance=certificate)
    elif certification_name == '3':
        form = CertificationMobileForm(instance=certificate)
    elif certification_name == '4':
        try:
            corporation = Corporation.objects.get(admin=request.user)
            form = CorporationForm(instance=corporation)
        except Corporation.DoesNotExist:
            form = CorporationForm(initial={'admin': request.user})
    elif certification_name == '5':
        form = CertificationAuthorizationForm(instance=certificate)
    return form


def get_authorization_PDF(request):

    # 获取用户实名认证记录
    certificate = Certification.objects.get(user=request.user)

    # 获取法人证书实名认证记录
    corporation = Corporation.objects.get(admin=request.user)

    # 关闭字体问题警告
    reportlab.rl_config.warnOnMissingFontGlyphs = 0

    # 注册字体
    registerFont(TTFont('hei', open(os.path.join(settings.BASE_DIR, "media/fonts/simhei.ttf"), 'rb')))
    registerFont(TTFont('song', open(os.path.join(settings.BASE_DIR, "media/fonts/simsun.ttf"), 'rb')))
    fonts.addMapping('song', 0, 0, 'song')
    fonts.addMapping('song', 0, 1, 'song')
    fonts.addMapping('hei', 1, 0, 'hei')
    fonts.addMapping('hei', 1, 1, 'hei')


    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="authorization.pdf"'

    # 内存中生成PDF，不写入磁盘文件，
    buffer = BytesIO()
    pdf_doc = SimpleDocTemplate(buffer, pagesize = A4)
    styles = getSampleStyleSheet()
    story = [Spacer(1, 0)]

    # 首行不缩进段落样式
    style = copy.deepcopy(styles['Normal'])
    style.fontName ='song'
    style.fontSize = 12
    style.spaceBefore = 0
    style.spaceAfter = 10

    # 首行缩进段落样式
    styleI = copy.deepcopy(styles['Normal'])
    styleI.fontName ='song'
    styleI.fontSize = 11
    styleI.firstLineIndent = 22
    styleI.leading = 16
    styleI.spaceBefore = 0
    styleI.spaceAfter = 10
    styleI.spaceShrinkage = 0.05

    # 居中标题段落样式
    styleT = copy.deepcopy(styles['Normal'])
    styleT.fontName = 'hei'
    styleT.fontSize = 24
    styleT.alignment = TA_CENTER


    # 标题
    title = "单位授权委托认证申请信息表"
    p = Paragraph(title, styleT)
    story.append(p)
    story.append(Spacer(1, 0.4 * inch))

    # 认证申请信息表
    data = [['单位信息', '', '', ''],
            ['单位全称', corporation.name, '', ''],
            ['统一社会信用代码', corporation.uscc, '22', '23'],
            ['法定代表人', corporation.legal_representative, '', ''],
            ['住址', corporation.domicile, '', ''],
            ['单位类型', corporation.corporation_type, '', ''],
            ['被委托人信息', '', '', ''],
            ['被委托人姓名', certificate.userName, '身份证号码', certificate.identity],
            ['手机号码', certificate.mobile, '电子邮箱', certificate.user.email],
            ]
    t = Table(data, style=[
        ('GRID', (0, 0), (-1, -1), 0.5, colors.darkgray),        # 表格内部的网格线
        ('BOX', (0, 0), (-1, -1), 1, colors.black),              # 表格四周边框线
        ('FONT', (0, 0), (-1, -1), 'song'),                      # 表格所有单元格均使用宋体
        ('ALIGN', (0, 1), (0, 5), 'RIGHT'),                      # 第一列2-6行右对齐
        ('ALIGN', (0, 7), (0, 8), 'RIGHT'),                      # 第一列8-9行右对齐
        ('SPAN', (0, 0), (-1, 0)),                               # 第一行合并
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),       # 第一行浅灰色背景
        ('ALIGN', (0, 0), (-1, 0), 'CENTER'),                    # 第一行居中对齐
        ('SPAN', (1, 1), (-1, 1)),                               # 第二行后三列合并
        ('SPAN', (1, 2), (-1, 2)),                               # 第三行后三列合并
        ('SPAN', (1, 3), (-1, 3)),                               # 第四行后三列合并
        ('SPAN', (1, 4), (-1, 4)),                               # 第五行后三列合并
        ('SPAN', (1, 5), (-1, 5)),                               # 第六行后三列合并
        ('SPAN', (0, 6), (-1, 6)),                               # 第七行合并
        ('BACKGROUND', (0, 6), (-1, 6), colors.lightgrey),       # 第七行浅灰色背景
        ('ALIGN', (0, 6), (-1, 6), 'CENTER'),                    # 第七行居中对齐
        ('ALIGN', (2, 7), (2, 8), 'RIGHT'),                      # 第三列8-9行右对齐
    ])
    t._argW[0] = 1.25 * inch                                     # 第一列宽度
    t._argW[1] = 1.72 * inch                                     # 第二列宽度
    t._argW[2] = 1.00 * inch                                     # 第三列宽度
    t._argW[3] = 2.00 * inch                                     # 第四列宽度
    story.append(t)

    # 授权委托公函
    story.append(Spacer(1, 0.4 * inch))
    title = "授权委托公函"
    p = Paragraph(title, styleT)
    story.append(p)
    story.append(Spacer(1, 0.4 * inch))

    p = Paragraph('单位全称：' + corporation.name, style)
    story.append(p)

    t = '本单位同意授权委托指定员工<u>' + certificate.userName\
        + '（身份证号码：' + certificate.identity\
        + '）</u>作为系统管理员，以单位名义申请【新疆马产业科技创新服务平台】帐号，'\
        + '并授权其负责使用该帐号进行【新疆马产业科技创新服务平台】允许的所有操作。'
    p = Paragraph(t, styleI)
    story.append(p)

    t = '本单位承诺：'
    p = Paragraph(t, style)
    story.append(p)

    t = '1. 本单位已经阅读、理解并同意《新疆马产业科技创新服务平台服务协议》，提交给【新疆马产业科技创新服务平台】的认证资料真实无误，' \
            + '并授权【新疆马产业科技创新服务平台】及其委托的第三方审核机构对提交的资料进行甄别核实。'
    p = Paragraph(t, styleI)
    story.append(p)

    t = '2. 同时，本单位在【新疆马产业科技创新服务平台】的内容维护及运营管理遵守国家法律法规、' \
            + '政策及《新疆马产业科技创新服务平台服务协议》的相关规定。如违反上述承诺，责任自行承担。'
    p = Paragraph(t, styleI)
    story.append(p)

    t = '3. 本单位/人对以上认证申请信息表所填信息及授权委托公函内容确认无异议。'
    p = Paragraph(t, styleI)
    story.append(p)

    story.append(Spacer(1, 0.8 * inch))

    # 设置日期格式
    from datetime import datetime
    from django.utils import formats
    date_joined = datetime.now()
    formatted_datetime = formats.date_format(date_joined, "SHORT_DATE_FORMAT")

    # 使用表格更容易控制字符串的位置
    data = [['被委托人签字：______________', '申请单位：' + corporation.name, ],
            ['', '盖章'],
            ['', formatted_datetime],
            ]
    t = Table(data, style=[
        ('FONT', (0, 0), (-1, -1), 'song'),
        ('FONTSIZE', (0, 0), (-1, -1), 11),
        ('ALIGN', (0, 0), (0, -1), 'LEFT'),
        ('ALIGN', (-1, 0), (-1, -1), 'RIGHT'),
    ])
    t._argW[0] = 1.6 * inch
    t._argW[1] = 4.1 * inch
    story.append(t)


    pdf_doc.build(story)

    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response
