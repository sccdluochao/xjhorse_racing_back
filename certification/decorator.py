from django.contrib.auth.decorators import login_required
'''
    模块：实名认证装饰器
    作者：张太红
    时间：2017-12-04
    使用方法：
    from certification.decorator import certificate_required
    @certificate_required('3')
'''

from certification.models import Certification
from certification.views import certification

'''
实名认证函数视图装饰器

使用方法：
from django.contrib.auth.decorators import login_required
from certification.decorator import certificate_required

@login_required                      # 首先必须是登录用户
@certificate_required                # (1)
def your_view(request):
    pass

或
@login_required
@certificate_required()              # (2)
def your_view(request):
    pass

或
@login_required
@certificate_required('4')           # (3)
def your_view(request):
    pass

    
其中形式(1)、(2)等价于@certificate_required('5')

形式(3)参数可以为：
'1' 身份证认证
'2' 用户照片认证
'3' 手机短信认证
'4' 法人证书认证
'5' 单位授权委托认证
如果给定的参数不是以上任何一个，则使用@certificate_required('5')
'''
def certificate_required(*args):
    certificate_type = '5'
    def _certificate_required(view_func):
        def wrapper(request, *args, **kwargs):
            try:
                user_certificate = Certification.objects.get(user=request.user)
                if user_certificate.has_certification(certificate_type):
                    response = view_func(request, *args, **kwargs)
                else:
                    response = certification(request)
            except Certification.DoesNotExist:
                response = certification(request)
            return response

        return wrapper

    if len(args) == 1 and callable(args[0]):
        # 调用方式为@certificate_required
        # 即：没有(),也没有参数
        # 实际的参数为被修饰的视图
        certificate_type = '5'   # 强制使用最高级别的实名认证
        return _certificate_required(args[0])
    elif len(args) == 0:
        # 调用方式为@certificate_required()
        # 即：有(),但没有参数
        certificate_type = '5'   # 强制使用最高级别的实名认证
        return _certificate_required
    else:
        # 调用方式为@certificate_required('1')
        certificate_type  = args[0]
        if certificate_type not in ['1', '2', '3', '4', '5']:  # 允许的实名认证类型
            # 如果参数超出允许的实名认证类型，强制使用最高级别的实名认证
            certificate_type = '5'
        return _certificate_required


'''
实名认证类视图Mixin装饰器

使用方法：
from certification.decorator import CertificateMixin
from django.contrib.auth.mixins import LoginRequiredMixin

# 首先必须是登录用户
class MyClassView(LoginRequiredMixin, CertificateMixin, UpdateView):
    certificate_type = '4'
    pass

certificate_type的有效值为：
'1' 身份证认证
'2' 用户照片认证
'3' 手机短信认证
'4' 法人证书认证
'5' 单位授权委托认证
如果certificate_type不是以上任何一个，则使用certificate_type = '5'
'''
class CertificateMixin(object):
    certificate_type = '5'

    def get_certificate_type(self):
        if self.certificate_type in ['1', '2', '3', '4', '5']:
            return self.certificate_type
        else:
            return '5'

    def dispatch(self, request, *args, **kwargs):

        try:
            user_certificate = Certification.objects.get(user=request.user)
            if user_certificate.has_certification(self.get_certificate_type()):
                response = super(CertificateMixin, self).dispatch(request, *args, **kwargs)
            else:
                response = certification(request)
        except Certification.DoesNotExist:
            response = certification(request)
        return response
