from django.contrib import admin

# Register your models here.
from django.contrib import admin
from certification.models import CorporationType, Corporation, Certification


class CorporationTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'type_name')  # 列表

class CorporationAdmin(admin.ModelAdmin):
    list_display = ('uscc', 'name', 'corporation_type', 'legal_representative', 'admin')  # 列表

class CertificationAdmin(admin.ModelAdmin):
    list_display = ('user', 'userName', 'identity', 'uscc')  # 列表

admin.site.register(CorporationType, CorporationTypeAdmin)
admin.site.register(Corporation, CorporationAdmin)
admin.site.register(Certification, CertificationAdmin)
