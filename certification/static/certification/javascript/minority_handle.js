$(function () {
    $.ajax({
            url: '/certification_minority',
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (data) {
                if(data.html_form!=""){
                    $('#modal-dialog').modal({
                        backdrop:false
                    });
                    $("#modal-dialog").modal("show");
                    $("#modal-dialog .modal-content").html(data.html_form);
                }
            }
        });
})