/**
 * Created by zhangtaihong on 17/12/2.
 */
$(function () {
    $(document).on("click", ".js-certification", function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-dialog").modal("show");
            },
            success: function (data) {
                $("#modal-dialog .modal-content").html(data.html_form);
            }
        });
    });

    $(document).on("click", ".js-certification-detail", function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-dialog").modal("show");
            },
            success: function (data) {
                $("#modal-dialog .modal-content").html(data.html);
            }
        });
    });
});
