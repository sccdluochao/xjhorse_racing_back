from django import forms
from django.forms import Textarea, TextInput
from django.utils.translation import ugettext_lazy as _
from certification.models import Certification
from certification.models import Corporation

'''
    模块：实名认证forms
    作者：张太红
    时间：2017-12-04
'''


class CertificationIDForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CertificationIDForm, self).__init__(*args, **kwargs)
        self.fields['userName'].widget.attrs['size'] = 75
        self.fields['address'].widget.attrs['size'] = 75

    class Meta:
        model = Certification
        fields = ('userName',
                  'gender',
                  'ethnicity',
                  'birthday',
                  'address',
                  'identity',
                  'id_photo',
                  'id_photo_certificated'
                  )
        widgets = {
            'user': forms.HiddenInput(),
            'address': Textarea(attrs={'cols': 75, 'rows': 1}),
        }


# 用户照片
class CertificationUserForm(forms.ModelForm):
    class Meta:
        model = Certification
        fields = ('user',
                  'user_photo',
                  'user_photo_certificated',
                  )


# 手机短信
class CertificationMobileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CertificationMobileForm, self).__init__(*args, **kwargs)
        self.fields['mobile'].widget.attrs['maxlength'] = 11

    class Meta:
        model = Certification
        fields = ('user',
                  'mobile',
                  'mobile_certificated',
                  )
        widgets = {
            'user': forms.HiddenInput(),
            'mobile_certificated': forms.HiddenInput(),
        }


# 单位
class CorporationForm(forms.ModelForm):
    class Meta:
        model = Corporation
        fields = ('uscc',
                  'name',
                  'corporation_type',
                  'domicile',
                  'legal_representative',
                  #'registered_capital',
                  #'operating_period',
                  #'business_scope',
                  #'registration_authority',
                  'admin',
                  'corporation_photo',
                  #'disapprove_comment'
                  )
        widgets = {
            'admin': forms.HiddenInput(),
            #'disapprove_comment': forms.HiddenInput(),
            'name': Textarea(attrs={'cols': 75, 'rows': 1}),
            'domicile': Textarea(attrs={'cols': 75, 'rows': 2}),
            #'business_scope': Textarea(attrs={'cols': 75, 'rows': 1}),
        }



# 法人证书
class CertificationCorporationForm(forms.ModelForm):
    class Meta:
        model = Certification
        fields = (
            'user',
            'uscc',
            'corporation_photo_certificate_status',
        )
        widgets = {
            #'user': forms.HiddenInput(),
        }


# 委托授权
class CertificationAuthorizationForm(forms.ModelForm):
    class Meta:
        model = Certification
        fields = (
            'user',
            'authorization_photo',
            'authorization_photo_certificate_status',
        )
        widgets = {
            'user': forms.HiddenInput(),
            'authorization_photo_certificate_status': forms.HiddenInput(),
        }
