from django.db import models
from django.contrib.auth.models import User
from os.path import splitext
from django.utils.translation import ugettext_lazy as _
from django.core.files.storage import FileSystemStorage
import os


# 单位类型表
class CorporationType(models.Model):
    type_name = models.CharField(max_length=30, null=False, blank=False, unique=True,
                                 verbose_name=_("Corporation Type"))  # 单位类型

    class Meta:
        verbose_name = _('CorporationType')
        verbose_name_plural = _('CorporationTypes')

    def __str__(self):
        return self.type_name


# 身份证照片上传位置和文件名称
def upload_location_id_photo(instance, filename):
    file_name, extension = splitext(filename)
    return "certification/%s_id_photo%s" % (instance.user.id, extension)


# 用户照片上传位置和文件名称
def upload_location_user_photo(instance, filename):
    file_name, extension = splitext(filename)
    return "certification/%s_user_photo%s" % (instance.user.id, extension)


# 法人证书照片上传位置和文件名称
def upload_location_corporation_photo(instance, filename):
    file_name, extension = splitext(filename)
    return "certification/%s%s" % (instance.uscc, extension)


# 委托授权书照片上传位置和文件名称
def upload_location_authorization_photo(instance, filename):
    file_name, extension = splitext(filename)
    return "certification/%s_authorization_photo%s" % (instance.user.id, extension)


# 重复上传照片时，用新照片覆盖老照片
class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return super(OverwriteStorage, self).get_available_name(name, max_length)


# 单位表
class Corporation(models.Model):
    uscc = models.CharField(
        primary_key=True,
        max_length=18,
        verbose_name=_("Unified social credit code"))  # 统一社会信用代码，主键

    name = models.CharField(
        max_length=60,
        null=False,
        blank=False,
        verbose_name=_("Corporation Name"))  # 单位名称
    corporation_type = models.ForeignKey(CorporationType, null=False, blank=False,
                                         verbose_name=_("Corporation Type"))  # 单位类型
    domicile = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        verbose_name=_("Domicile"))  # 住所

    legal_representative = models.CharField(
        max_length=30,
        null=False,
        blank=False,
        verbose_name=_("Legal Representative"))  # 法定代表人

    registered_capital = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        verbose_name=_("Registered Capital"))  # 注册资本（开办资金）

    operating_period = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_("Operating Period"))  # 经营期限（有效期）

    business_scope = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name=_("Business Scope"))  # 经营范围（业务范围）

    registration_authority = models.CharField(
        max_length=60,
        null=True,
        blank=True,
        verbose_name=_("Registration Authority"))  # 登记机关

    admin = models.OneToOneField(
        User,
        null=False,
        blank=False,
        verbose_name=_("Administrator"))  # 系统管理员

    corporation_photo = models.ImageField(
        upload_to=upload_location_corporation_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Corporation Photo"))  # 法人证书照片

    disapprove_comment = models.CharField(max_length=50, null=True, blank=True,
                                          verbose_name=_("Disapprove Comment"))  # 法人证书审核未通过原因

    # 上级单位
    superior_uscc = models.ForeignKey("Corporation", null=True, blank=True, verbose_name=_("Superior Corporation"))

    class Meta:
        verbose_name = _('Corporation')
        verbose_name_plural = _('Corporations')

    def __str__(self):
        if self.name == None:
            return "无公司名称"
        return self.name

CERTIFICATE_STATUS_CHOICES = (
    (1, _("Not applied")),  # 未申请
    (2, _("Wait for review")),  # 等待审核
    (3, _("Disapproved")),  # 审核未通过
    (4, _("Approved"))  # 通过
)


# 实名认证表
class Certification(models.Model):
    user = models.OneToOneField(
        User,
        primary_key=True,
        verbose_name=_("UserID"))  # 用户ID, 主键
    userName = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        verbose_name=_("User Name"))  # 真实姓名
    gender = models.CharField(
        max_length=4,
        null=True,
        blank=True,
        verbose_name=_("Gender"))  # 性别
    ethnicity = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        verbose_name=_("Ethnicity"))  # 民族
    birthday = models.DateField(
        blank=True,
        null=True,
        verbose_name=_("Birthday"))  # 出生日期
    identity = models.CharField(
        max_length=18,
        null=True,
        blank=True,
        verbose_name=_("Identity"))  # 身份证号码
    address = models.TextField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Address"))  # 住址
    mobile = models.CharField(
        max_length=15,
        null=True,
        blank=True,
        verbose_name=_("Mobile"))  # 手机号码
    mobile_certificated = models.BooleanField(
        default=False,
        blank=False,
        verbose_name=_("Mobile Certificated"))  # 手机号码是否通过短信验证
    id_photo = models.ImageField(
        upload_to=upload_location_id_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Identity Photo"))  # 身份证正面照片
    id_photo_certificated = models.BooleanField(
        default=False,
        blank=False,
        verbose_name=_("ID Photo Certificated"))  # 身份证照片是否通过认证

    user_photo = models.ImageField(
        upload_to=upload_location_user_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("User Photo"))  # 用户照片

    user_photo_certificated = models.BooleanField(
        default=False,
        blank=False,
        verbose_name=_("User Photo Certificated"))  # 用户照片是否通过认证

    authorization_photo = models.ImageField(
        upload_to=upload_location_authorization_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Authorization Photo"))  # 单位授权委托书照片

    # 法人证书认证状态
    corporation_photo_certificate_status = models.IntegerField(
        choices=CERTIFICATE_STATUS_CHOICES,
        default=1,
        verbose_name=_("Corporation Photo Certificate Status")
    )

    # 单位授权委托书认证状态
    authorization_photo_certificate_status = models.IntegerField(
        choices=CERTIFICATE_STATUS_CHOICES,
        default=1,
        verbose_name=_("Authorization Photo Certificate Status")
    )

    disapprove_comment = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_("Disapprove Comment"))  # 单位授权委托书审核未通过原因

    uscc = models.ForeignKey(
        Corporation,
        null=True,
        blank=True,
        verbose_name=_("Corporation Name"))  # 单位名称

    class Meta:
        verbose_name = _('Certification')
        verbose_name_plural = _('Certifications')

    def has_certification(self, certification_name):
        if certification_name == "1":  # 身份证认证
            return self.id_photo_certificated
        elif certification_name == "2":  # 用户照片认证
            return self.user_photo_certificated
        elif certification_name == "3":  # 手机短信认证
            return self.mobile_certificated
        elif certification_name == "4":  # 法人证书认证
            return self.corporation_photo_certificate_status == 4
        elif certification_name == "5":  # 单位授权委托认证
            return self.authorization_photo_certificate_status == 4
        else:
            return False

    def get_cert(self):
        cert = '0'
        if self.authorization_photo_certificate_status == 4:
            cert = '5'
        elif self.corporation_photo_certificate_status == 4:
            cert = '4'
        elif self.mobile_certificated:
            cert = '3'
        elif self.user_photo_certificated:
            cert = '2'
        elif self.id_photo_certificated:
            cert = '1'
        return cert

    def __str__(self):
        if self.userName == None \
                or self.user.username == None:
            return "无姓名"
        return str(self.userName) + "(" + self.user.username + ")"
