from django.apps import AppConfig


class HorsefarmConfig(AppConfig):
    name = 'horsefarm'
