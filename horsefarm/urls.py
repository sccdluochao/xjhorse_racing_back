from django.conf.urls import url
from horsefarm import views

urlpatterns = [
    url(r'^horsefarm$', views.horsefarm, name='horsefarm_home'),
]
