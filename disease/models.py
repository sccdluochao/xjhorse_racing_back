from django.db import models
from DjangoUeditor.models import UEditorField

#部位
class Position(models.Model):
    code = models.CharField(u'部位编码',max_length=10,unique=True)
    position_name = models.CharField(u'发病部位', max_length=50)
    #汉化admin里面的表单名
    class Meta():
        verbose_name_plural="部位"
    def __str__(self):
        return self.position_name

#病因
class Pathogeny(models.Model):
    code = models.CharField(u'病因编码',max_length=10,unique=True)
    pathogeny_name = models.CharField(u'病因名称',max_length=50)
    class Meta():
        verbose_name_plural="病因"
    def __str__(self):
        return  self.pathogeny_name

#病证
class Disease(models.Model):
    p_code = models.ForeignKey(Pathogeny, to_field='code')
    code = models.CharField(u'病证编码',max_length=10,unique=True)
    disease_name = models.CharField(u'病证名称', max_length=50)
    class Meta():
        verbose_name_plural="病证"
    def __str__(self):
        return  self.disease_name

#马病
class Horsedisease(models.Model):
    code = models.CharField(u'马病编码', max_length=10, unique=True)
    d_code = models.ForeignKey(Disease, to_field='code',null=True,verbose_name='病证名称')
    p_code = models.ManyToManyField(Position,verbose_name='发病部位')
    horse_name = models.CharField(u'马病名称',max_length=50)
    another_name = models.CharField(u'马病别名',max_length=50,null=True)
    introduction = models.TextField(u'马病简介')
    DELETE_CHOICES = (
        ('hasdelete', '已删除'),
        ('notdelete', '未删除'),
    )
    delete_hd = models.CharField(u'是否删除马病',max_length=20,choices=DELETE_CHOICES,default='notdelete')
    class Meta():
        verbose_name_plural="马病"
    def __str__(self):
        return  self.horse_name

#辩证要点
class Dialectical(models.Model):
    code = models.ForeignKey(Horsedisease,to_field='code', verbose_name='马病名称')
    points = UEditorField('辩证要点', height=300, width=1000, default=u'', blank=True, imagePath="disease/bimages/",toolbars='besttome', filePath='disease/files/')
    class Meta():
        verbose_name_plural="辨证要点"
    def __str__(self):
        return str(self.code)

#治疗方案
class Treatment(models.Model):
    code = models.ForeignKey(Horsedisease,to_field='code', verbose_name='马病名称')
    t_method = UEditorField('治疗方案', height=300, width=1000, default=u'', blank=True, imagePath="disease/bimages/",toolbars='besttome', filePath='disease/files/')
    class Meta():
        verbose_name_plural = "治疗方案"
    def __str__(self):
        return str(self.code)

#预防方案
class Prevention(models.Model):
    code = models.ForeignKey(Horsedisease,to_field='code', verbose_name='马病名称')
    p_method = UEditorField('预防方案', height=300, width=1000, default=u'', blank=True, imagePath="disease/bimages/",toolbars='besttome', filePath='disease/files/')
    class Meta():
        verbose_name_plural = "预防方案"
    def __str__(self):
        return str(self.code)

#马病图片
class Imgs(models.Model):
    code = models.ForeignKey(Horsedisease, to_field='code',null=True, verbose_name='马病名称')
    img = models.FileField(u'病状图片',upload_to='./disease/dimages/')
    imgdescribe = models.CharField(u'病状图片描述',max_length=60,blank=True)
    class Meta():
        verbose_name_plural="马病图片"
    def __str__(self):
        return str(self.code)

#最新马讯
class Article(models.Model):
    title = models.CharField(u'文章标题', max_length=256)
    content = UEditorField('内容', height=300, width=1000,default=u'', blank=True, imagePath="disease/aimages/",toolbars='besttome', filePath='disease/files/')
    pub_date = models.DateTimeField('发表时间', auto_now_add=True, editable=True)
    update_time = models.DateTimeField('更新时间', auto_now=True, null=True)
    class Meta():
        verbose_name_plural="最新马讯"
    def __str__(self):
        return self.title