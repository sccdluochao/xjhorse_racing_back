from django.template.loader_tags import register

from disease.services import getallPathogeny


@register.inclusion_tag('disease/disease_nav.html',takes_context=True)
def jump_link(context):
    plist = getallPathogeny()
    return {"plist":plist,"request":context['request'],"user":context['user']}
