INSERT INTO disease_disease (id,code,disease_name,p_code_id,disease_name_en_us,disease_name_kk_ar,disease_name_ug_ar,disease_name_zh_hans) VALUES
 (1, '10101', '消化系统病证', '101', 'Gastroenterology', 'تىنىس جۇيەسى اۋرۋ بەلگىلەرىن', 'ھەزىم قىلىش سىستېمىسى كېسەللىكلىرى', '消化系统病证'),
 (2, '10102', '呼吸系统病证', '101', 'Respiratory Medicine', 'تىنىس جۇيەسى اۋرۋ بەلگىلەرىن', 'نەپەس يولى كېسەللىكلىرى', '呼吸系统病证'),
 (3, '10103', '循环系统病证', '101', 'Circulatory System Disease', 'قان اينالىس جۇيەسى اۋرۋ بەلگىلەرىن', 'ئايلىنىش سىستېمىسى كېسەللىكلىرى  ', '循环系统病证'),
 (4, '10104', '泌尿系统病证', '101', 'Urology', 'نەسەپ شىعارۋ جۇيەسىنىڭ اۋرۋ بەلگىلەرىن', 'سۈيدۈك ئاجرىتىش سىستېمىسى كېسەللىكلىرى  ', '泌尿系统病证'),
 (5, '10105', '神经系统病证', '101', 'Neurology', 'نەرۆ جۇيەسى اۋرۋ بەلگىلەرىن', 'نېرۋا سىستېمىسى كېسەللىكلىرى  ', '神经系统病证'),
 (6, '10106', '内分泌腺病证', '101', 'Anomalous Glands', 'ىشكى سەكرەتسيا بەزى اۋرۋى كۋالىگىن', 'ئىچكى ئاجراتما بەزلىرى كېسەللىكىرى ', '内分泌腺病证'),
 (7, '10107', '营养代谢病证', '101', 'Nutritional Metabolic Disease', 'قورەكتىك مەتابوليزمدىك ىندەتىن كۋالىگىن', 'ئوزۇقلۇق مېتابولىزما كېسەللىكلىرى ', '营养代谢病证'),
 (8, '10201', '疮黄', '102', '', 'جارا سارسۋى', 'ياللۇغلۇق جاراھەت ', '疮黄'),
 (9, '10202', '外伤', '102', 'Trauma', 'سىرتقى جاراقات', 'تاشقى يارا', '外伤'),
 (10, '10203', '肢蹄病证', '102', '', 'قول- اياق', 'قۇل-پۇت كېسەللىكلىرى', '肢蹄病证'),
 (11, '10204', '外周神经麻痹', '102', 'Peripheral Nerve Paralysis', 'نەرۆىنىڭ سالدانۋى', 'تاشقا نېرۋا پالەچ كېسەللىكلىرى', '外周神经麻痹'),
 (12, '10205', '皮肤病证', '102', 'Dermatology', 'تەرى اۋرۋ بەلگىلەرىن', 'تىرە كېسەللىكلىرى', '皮肤病证'),
 (13, '10206', '眼睛病证', '102', 'Ophthalmology', 'كوزىلدىرىك اۋرۋ بەلگىلەرىن', 'كۆز كېسەللىكلىرى', '眼睛病证'),
 (14, '10301', '母马病证', '103', 'Mare Disease', 'بيە اۋرۋ بەلگىلەرىن', 'بايتال كېسەللىكلىرى', '母马病证'),
 (15, '10302', '公马病证', '103', 'Stallion Disease', 'ايعىر اۋرۋ بەلگىلەرىن', 'ئايغىر كېسەللىكلىرى', '公马病证'),
 (16, '10401', '所有幼驹病证', '104', 'All the foal disease', 'قۇلىننىڭ اۋرۋ بەلگىلەرىن', 'بارلىق قۇلۇن كېسەللىكلىرى', '所有幼驹病证'),
 (17, '10501', '所有传染病', '105', 'All the infectious diseases', 'جۇقپالى اۋرۋ', 'بارلىق پارازىت كېسەللىكلىرى', '所有传染病'),
 (18, '10601', '所有寄生虫病', '106', 'All the parasitic diseases', 'بارلىق پارازيت اۋرۋلارى', '', '所有寄生虫病');