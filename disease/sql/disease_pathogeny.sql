INSERT INTO disease_pathogeny (id,code,pathogeny_name,pathogeny_name_en_us,pathogeny_name_kk_ar,pathogeny_name_ug_ar,pathogeny_name_zh_hans) VALUES
(1, '101', '内科病证', 'Internal Medicine', 'ىشكى اۋرۋلار كۋالىگىن', 'ئىچكى كېسەللىكلىرى ', '内科病证'),
(2, '102', '外科病证', 'Surgical Deparment', 'سىرتقى اۋرۋلارى كۋالىگىن', 'تاشقى كېسەللىكلىرى', '外科病证'),
(3, '103', '产科病证', 'Gynecology & Obstetrics', 'تۋۋعا قاتىستى اۋرۋلارى كۋالىگىن', 'تۇغۇت كېسەللىكلىرى', '产科病证'),
(4, '104', '幼驹病证', 'Foal Syndrome', 'قۇلىننىڭ اۋرۋ بەلگىلەرىن', 'قۇلۇن كېسەللىكلىرى', '幼驹病证'),
(5, '105', '传染病', 'Infectious Disease', 'جۇقپالى اۋرۋ', 'يۇقۇملۇق كېسەللىكلىرى', '传染病'),
(6, '106', '寄生虫病', 'Parasite Disease', 'پارازيت اۋرۋلارى', 'پارازىت كېسەللىكلىرى ', '寄生虫病');