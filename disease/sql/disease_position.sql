INSERT INTO disease_position (id,code,position_name,position_name_en_us,position_name_kk_ar,position_name_ug_ar,position_name_zh_hans) VALUES
(1, '201', '头部', 'Head', 'باس ءبولىم', 'باش قىسمى', '头部'),
(2, '202', '颈部', 'Neck', 'مويىن ءبولىم', 'بويۇن قىسمى ', '颈部'),
(3, '203', '腹部', 'Abdomen', 'قۇرساقءبۇلىم', 'قورساق قىسمى', '腹部'),
(4, '204', '四肢', 'Limbs', 'قول- اياق', 'تۆت پۇتى', '四肢');