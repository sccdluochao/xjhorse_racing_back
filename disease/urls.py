from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^horsedisease$',index,name="disease_home"),
    url(r'^horsedisease/disease$',getDisease,name="disease"),
    url(r'^horsedisease/articleindex/$',getarticleindex,name="articleindex"),
    url(r'^horsedisease/articleindex/articlesingle/(?P<id>[0-9]+)/$',articlesingle,name="id"),
    url(r'^horsedisease/hdindex/$',gethorsedisease,name="hdcode"),
    url(r'^horsedisease/hdsingle/(?P<code>[0-9]+)$',hdsingle,name="hdsingle"),
    url(r'^horsedisease/searchlist/$',search, name="search"),
    url(r'^horsedisease/hdsearch/$',hdsearch, name="hdsearch"),
    url(r'^horsedisease/positionsearch/$',positionsearch, name="positionsearch"),
    url(r'^horsedisease/maintenance/$',hdmaintenance,name="maintenance"),
    url(r'^horsedisease/deletemore/$',deletemore, name="deletemore"),
    url(r'^horsedisease/modifydetails/(?P<code>[0-9]+)/$',hddetails,name="modifydetails"),
    url(r'^horsedisease/hdmodify/(?P<code>[0-9]+)/$',hdmodify,name="hdmodify"),
    url(r'^horsedisease/addhd/$',addhd,name="addhd"),
    url(r'^horsedisease/getaddhd/$',getaddhd,name="getaddhd"),
    url(r'^horsedisease/hdimg/(?P<code>[0-9]+)/$',hdimg,name="hdimg"),
]
