from django.db.models import QuerySet, Q
from django.test import TestCase

# Create your tests here.
from disease.models import *

#获取病因
def getallPathogeny():
    #select * from Pathogeny;(list)
    list = Pathogeny.objects.all()
    return list

#获取病证
def getdiseasebycode(code):
    #select * from disease where p_code = code
    list = Disease.objects.filter(p_code=code)
    return list

#index获取文章
def getArticleindex():
    list = Article.objects.order_by('-update_time')[0:6]#获取前六篇文章
    return list

#文章列表
def getallArticle():
    list = Article.objects.all()
    return list

#文章内容
def getcontent(id):
    list = Article.objects.get(id=id)
    allnum = Article.objects.order_by("id").count()#全部文章个数
    ltnum = Article.objects.filter(id__lt=id).count()#小于当前文章id计数
    s = None
    x = None
    if ltnum != 0:
        s=Article.objects.order_by('id')[ltnum-1]
    if ltnum + 1 < allnum:
        x = Article.objects.order_by('id')[ltnum+1]
    return list,s,x

#根据病证查询马病
def getalldisease(code):
    #查询病证
    list = Disease.objects.get(code=code)
    dnamelist = list

    #查询相关病证马病
    hdlist = list.horsedisease_set.filter(delete_hd='notdelete')

    #根据字符串长度排序
    sortlist=[]
    for hd in hdlist:
        sortlist.append(hd)

    sortlist.sort(key=lambda x:len(x.horse_name))#根据horse_name的字符长度排序
    return sortlist,dnamelist

#马病详情
def gethdcontent(code):
    list = Horsedisease.objects.get(code=code)
    #辩证要点
    bzlist = list.dialectical_set.all()
    if len(bzlist)>0 and bzlist[0].points != "":
        bzstr = bzlist[0].points
    else:
        bzstr = "无"

    #治疗方案
    zllist = list.treatment_set.all()
    if len(zllist)>0 and zllist[0].t_method != "":
        zlstr=zllist[0].t_method
    else:
        zlstr = "无"

    #预防方案
    yflist = list.prevention_set.all()
    if len(yflist) > 0 and yflist[0].p_method != "":
        yfstr = yflist[0].p_method
    else:
        yfstr = "无"

    #马病图片
    imglist = list.imgs_set.all()

    # 马病(上翻下翻)
    allnum = Horsedisease.objects.order_by("id").count()  # 全部马病个数
    ltnum = Horsedisease.objects.filter(id__lt=list.id).count()  # 小于当前马病id计数
    s = None
    x = None
    if ltnum != 0:
        s = Horsedisease.objects.order_by('id')[ltnum - 1]
    if ltnum + 1 < allnum:
        x = Horsedisease.objects.order_by('id')[ltnum + 1]

    return list,bzstr,zlstr,yfstr,imglist,s,x

#查询
def getsearch(name):
    list = Horsedisease.objects.filter(Q(horse_name__contains=name)|Q(another_name__contains=name),delete_hd='notdelete')#名称中包含name的人
    print(list)
    # 根据字符串长度排序
    sortlist = []
    for hd in list:
        sortlist.append(hd)

    sortlist.sort(key=lambda x: len(x.horse_name))  # 根据horse_name的字符长度排序
    return sortlist

#部位查询
def getposition(code):
    list = Position.objects.get(code=code)
    pnamelist = list
    hdlist = list.horsedisease_set.filter(delete_hd='notdelete')

    # 根据字符串长度排序
    sortlist = []
    for hd in hdlist:
        sortlist.append(hd)

    sortlist.sort(key=lambda x: len(x.horse_name))  # 根据horse_name的字符长度排序
    return sortlist,pnamelist

#马病维护(全部显示)
def getallhdmaintenance():
    hdlist = Horsedisease.objects.filter(delete_hd='notdelete')
    palist = Pathogeny.objects.all()
    dlist = Disease.objects.all()
    polist = Position.objects.all()
    return hdlist,palist,dlist,polist

#马病维护(检索显示)
def gethdmaintenance(pacode,dcode,pocode,hdinput):
    if pacode != None and pacode != "":
        hdlist=Horsedisease.objects.filter(d_code__p_code=pacode,delete_hd='notdelete')
    if dcode != None and dcode != "":
        hdlist = Horsedisease.objects.filter(d_code=dcode,delete_hd='notdelete')
    if pocode != None and pocode != "":
        list = Position.objects.get(code=pocode)
        hdlist = list.horsedisease_set.filter(delete_hd='notdelete')
    if hdinput != None and hdinput != "":
        hdlist = Horsedisease.objects.filter(horse_name__contains=hdinput,delete_hd='notdelete')
    return hdlist

#马病维护(批量删除)
def delethorse(deletcode):
    delete = deletcode.split(',')
    for hdcode in delete:
        delethd = Horsedisease.objects.filter(code=hdcode).update(delete_hd='hasdelete')
    return delethd

#修改马病
def updatehd(code,name,athname,introduction,dialectical,treatment,prevention):
    hdlist = Horsedisease.objects.filter(code=code).update(horse_name=name,another_name=athname,introduction=introduction)
    dlist = Dialectical.objects.filter(code=code).update(points=dialectical)
    tlist = Treatment.objects.filter(code=code).update(t_method=treatment)
    plist = Prevention.objects.filter(code=code).update(p_method=prevention)
    return hdlist,dlist,tlist,plist

#新增马病(查找病因、部位)
def getcode():
    palist = Pathogeny.objects.all()
    polist = Position.objects.all()
    return palist,polist

#新增马病(查找病证)
def getdlist(pacode):
    bzlist = Disease.objects.filter(p_code=pacode)
    return bzlist

#新增马病(自动生产编号)
def createhd(dcode):
    list = Horsedisease.objects.filter(d_code=dcode).order_by("-code")
    hdcode = int(list[0].code) + 1
    return hdcode

#新增马病
def savehd(hdname,athname,dcode,hdcode,pocode,introduction,dialectical,treatment,prevention):
    dcode= Disease.objects.get(code=dcode)
    hd=Horsedisease.objects.create(code=hdcode, d_code=dcode, horse_name=hdname, another_name=athname,introduction=introduction)
    for i in pocode:
        pocode=Position.objects.get(code=i)
        hd.p_code.add(pocode)
    hdcode = Horsedisease.objects.get(code=hdcode)
    Dialectical.objects.create(code=hdcode,points=dialectical)
    Treatment.objects.create(code=hdcode,t_method=treatment)
    Prevention.objects.create(code=hdcode,p_method=prevention)
    #return hdlist,dlist,tlist,plist

#马病图片页
def gethdimg(code):
    list = Horsedisease.objects.get(code=code)
    imglist = list.imgs_set.all()
    return list,imglist