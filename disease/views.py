from django.shortcuts import render, render_to_response
from django.http import JsonResponse
from django.template import Template, Context
from django.views.generic import TemplateView
from rules.contrib.views import permission_required

from disease.services import *

#病因
def index(request):
    plist = getallPathogeny()
    alist = getArticleindex()
    return render(request,"disease/index.html",{"plist":plist,"alist": alist})

#病证
def getDisease(request):
    #词典
    rs={"status":0}
    #列表
    #['d','d']

    if request.method!="POST":
        return JsonResponse(rs)

    pcode=request.POST['pcode']

    dlist=getdiseasebycode(pcode)

    dlistrs=[]
    for d in dlist:
        drs={}
        drs['name']=d.disease_name
        drs['code']=d.code
        dlistrs.append(drs)

    rs['dlist']=dlistrs
    rs['status']=1

    return JsonResponse(rs)

#文章列表
def getarticleindex(request):
    ailist = getallArticle()
    return render(request,"disease/articleindex.html",{"ailist":ailist})

#文章内容
def articlesingle(request,id):
    slist,altnum,agtnum = getcontent(id)
    return render(request, "disease/articlesingle.html", {"slist": slist, "altnum": altnum, "agtnum":agtnum})

#马病查询结果
def gethorsedisease(request):
    code = request.GET['code']
    hdlist,dnamelist = getalldisease(code)
    return render(request, "disease/hdindex.html", {"hdlist": hdlist,"dnamelist":dnamelist})

#马病详情
def hdsingle(request,code):
    hdxlist,hdbzlist,hdzllist,hdyflist,hdimglist,hltnum,hgtnum = gethdcontent(code)
    return render(request, "disease/hdsingle.html", {"hdxlist": hdxlist,"hdbzlist": hdbzlist,"hdzllist": hdzllist,"hdyflist": hdyflist,"hdimglist": hdimglist,"hltnum":hltnum,"hgtnum":hgtnum})

#马病查询
def search(request):
    name=request.GET['searchname']
    hdlist=getsearch(name)
    return render(request, "disease/hdindex.html", {"hdlist": hdlist})

#查询马病(显示和关键字有关马病)
def hdsearch(request):
    rs = {}
    name=request.GET['searchname']
    list = getsearch(name)
    for d in list:
        rs[d.horse_name] = d.horse_name
    return JsonResponse(rs)

#部位查询
def positionsearch(request):
    code = request.GET['p_code']
    hdlist,pnamelist = getposition(code)
    return render(request, "disease/hdindex.html", {"hdlist": hdlist,"pnamelist":pnamelist})

#马病维护
@permission_required('disease.add_disease', raise_exception=True)
def hdmaintenance(request):
    hdlist,palist, dlist, polist = getallhdmaintenance()
    if request.method == 'POST':
        pacode = request.POST['pacode']
        dcode = request.POST['dcode']
        pocode = request.POST['pocode']
        hdinput = request.POST['searchname']
        hdlist = gethdmaintenance(pacode,dcode,pocode,hdinput)
    return render(request,"disease/maintenance.html",{"hdlist":hdlist,"palist":palist,"dlist":dlist,"polist":polist})

#马病维护(批量删除)
@permission_required('disease.add_disease', raise_exception=True)
def deletemore(request):
    if request.method == 'POST':
        deletcode = request.POST['delitems']
        hddelet = delethorse(deletcode)
    return render(request,"disease/maintenance.html",{"hddelet":hddelet})

#修改马病(显示详情)
@permission_required('disease.add_disease', raise_exception=True)
def hddetails(request,code):
    hdxlist, hdbzlist, hdzllist, hdyflist, hdimglist, hltnum, hgtnum = gethdcontent(code)
    return render(request,"disease/modify.html",{"hdxlist": hdxlist,"hdbzlist": hdbzlist,"hdzllist": hdzllist,"hdyflist": hdyflist,"hdimglist": hdimglist,"hltnum":hltnum,"hgtnum":hgtnum,"type":"update"})

#修改马病(存储修改内容)
@permission_required('disease.add_disease', raise_exception=True)
def hdmodify(request,code):
    if request.method == 'POST':
        name = request.POST['hdname']
        athname = request.POST['athname']
        introduction = request.POST['hdintroduction']
        dialectical = request.POST['hdbz']
        treatment = request.POST['hdzl']
        prevention = request.POST['hdyf']
        hdxlist, hdbzlist, hdzllist, hdyflist = updatehd(code,name,athname,introduction,dialectical,treatment,prevention)
    return render(request, "disease/modify.html", {"hdxlist": hdxlist,"hdbzlist": hdbzlist,"hdzllist": hdzllist,"hdyflist": hdyflist})

#添加马病页面数据
@permission_required('disease.add_disease', raise_exception=True)
def addhd(request):
    palist,polist = getcode()#获取病因和部位

    # 获取病证
    dlist = ""
    if 'pacode' in request.GET:
        rs = {"msg":""}
        pacode = request.GET['pacode']
        dlist = getdlist(pacode)
        dl = []
        for d in dlist:
            dd = {}
            dd['code'] = d.code
            dd['disease_name'] = d.disease_name
            dl.append(dd)
        rs['dl'] = dl
        return JsonResponse(rs)

    # 生成马病编号
    hdcode = ""
    if request.method == 'POST':
        rs = {"msg": "",'code':''}
        dcode = request.POST['dcodedata']
        hdcode = createhd(dcode)
        rs['code'] = hdcode
        return JsonResponse(rs)

    return render(request,"disease/addhorsedisease.html",{"palist":palist,"polist":polist})

#新增马病写入数据库
@permission_required('disease.add_disease', raise_exception=True)
def getaddhd(request):
    rs = {"status": 0, "msg": ""}
    if request.method == 'POST':
        hdname = request.POST['hdname']
        name = Horsedisease.objects.filter(Q(horse_name__contains=hdname)|Q(another_name__contains=hdname))
        if name.count() > 0 and name[0].horse_name != '':
            rs['status']=-1
            rs['msg'] = "该马病已存在。"
        else:
            athname = request.POST['athname']
            dcode = request.POST['dcode']
            hdcode = request.POST['hdcode']
            pocode = request.POST.getlist('pocode')
            introduction = request.POST['hdintroduction']
            dialectical = request.POST['hdbz']
            treatment = request.POST['hdzl']
            prevention = request.POST['hdyf']
            savehd(hdname,athname,dcode,hdcode,pocode,introduction,dialectical,treatment,prevention)
            rs['status'] = 1
            rs['msg'] = "新增马病成功！"
        return JsonResponse(rs)

#马病图片页
def hdimg(request,code):
    hdlist,hdimgs = gethdimg(code)
    return render(request,"disease/hdimg.html",{"hdlist":hdlist,"hdimgs":hdimgs})