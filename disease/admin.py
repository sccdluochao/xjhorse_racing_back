from django.contrib import admin
from disease.models import *
from modeltranslation.admin import TranslationAdmin

#添加admin文章表中显示字段且指定翻译字段
class ArticleAdmin(TranslationAdmin):
    list_display = ('title', 'pub_date', 'update_time',)      #显示文章标题、发布时间、跟新时间
    search_fields = ('title',)                                   #检索文章标题

#添加admin马病表中显示字段
class HorsediseaseAdmin(admin.ModelAdmin):
    list_display = ('horse_name', 'another_name', 'delete_hd',)      #显示马病名称、马病别名、马病是否删除
    search_fields = ('horse_name','another_name',)                    #检索马病名称、别名

admin.site.register(Article, ArticleAdmin)
admin.site.register(Horsedisease,HorsediseaseAdmin)
admin.site.register(Dialectical)
admin.site.register(Treatment)
admin.site.register(Prevention)
#admin.site.register(Pathogeny)
#admin.site.register(Position)
#admin.site.register(Disease)
admin.site.register(Imgs)

#指定部位表中要翻译的字段
class PositionAdmin(TranslationAdmin):
    list_display = ('position_name',)

#指定病因表中要翻译的字段
class PathogenyAdmin(TranslationAdmin):
    list_display = ('pathogeny_name',)

#指定病证表中要翻译的字段
class DiseaseAdmin(TranslationAdmin):
    list_display = ('disease_name',)

admin.site.register(Position,PositionAdmin)
admin.site.register(Pathogeny,PathogenyAdmin)
admin.site.register(Disease,DiseaseAdmin)