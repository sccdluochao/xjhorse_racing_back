from modeltranslation.translator import TranslationOptions, translator
from disease.models import *

#指定部位表中要翻译的字段
class PositionTranslationOptions(TranslationOptions):
    fields = ('position_name',)

#指定病因表中要翻译的字段
class PathogenyTranslationOptions(TranslationOptions):
    fields = ('pathogeny_name',)

#指定病证表中要翻译的字段
class DiseaseTranslationOptions(TranslationOptions):
    fields = ('disease_name',)

#指定病证表中要翻译的字段
class ArticleTranslationOptions(TranslationOptions):
    fields = ('title',)

#注册需要翻译的表
translator.register(Position,PositionTranslationOptions)
translator.register(Pathogeny,PathogenyTranslationOptions)
translator.register(Disease,DiseaseTranslationOptions)
translator.register(Article,ArticleTranslationOptions)