from django.shortcuts import render
from django.utils.translation import ugettext as _

def auction(request):
    context = {
        #"carousels": carousels,
        #"title": title,
    }
    return render(request, "auction/index.html", context)
