from django.conf.urls import url
from auction import views

urlpatterns = [
    url(r'^auction$', views.auction, name='auction_home'),
]
