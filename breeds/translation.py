'''
   创 建 人：张太红
   创建日期：2018年01月22日
   注册需要翻译（多语言支持）的模型（Models）
'''

from modeltranslation.translator import translator, TranslationOptions
from breeds.models import Continents, Carousels, Videos, CountriesRegions, Breeds


# 指定洲表中需要翻译的字段
class ContinentsTranslationOptions(TranslationOptions):
    fields = ('name',)


# 指定品种轮播表中需要翻译的字段
class CarouselsTranslationOptions(TranslationOptions):
    fields = ('breed_name',)


# 指定品种视频表中需要翻译的字段
class VideosTranslationOptions(TranslationOptions):
    fields = ('title',)


# 指定国家地区表中需要翻译的字段
class CountriesRegionsTranslationOptions(TranslationOptions):
    fields = ('name', 'full_name',)


# 指定品种表中需要翻译的字段
class BreedsTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'breeding_history')

# 注册需要翻译的表
translator.register(Continents, ContinentsTranslationOptions)
translator.register(Carousels, CarouselsTranslationOptions)
translator.register(Videos, VideosTranslationOptions)
translator.register(CountriesRegions, CountriesRegionsTranslationOptions)
translator.register(Breeds, BreedsTranslationOptions)
