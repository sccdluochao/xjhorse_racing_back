# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-01-23 15:02
from __future__ import unicode_literals

from django.db import migrations


def load_data_from_sql():
    from xjhorse.settings import BASE_DIR
    import os
    sql_statements = open(os.path.join(BASE_DIR,'breeds/sql/init.sql'), 'rb').read()
    return sql_statements

def delete_data_with_sql():
    return 'DELETE from breeds_continents; DELETE from breeds_countriesregions;  DELETE from breeds_videos;  DELETE from breeds_carousels;'

class Migration(migrations.Migration):

    dependencies = [
        ('breeds', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(load_data_from_sql(), delete_data_with_sql()),
    ]
