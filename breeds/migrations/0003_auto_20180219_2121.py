# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-02-19 13:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('breeds', '0002_auto_20180129_1348'),
    ]

    operations = [
        migrations.AddField(
            model_name='breeds',
            name='comment',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Comment'),
        ),
        migrations.AddField(
            model_name='breeds',
            name='qualified',
            field=models.BooleanField(default=False, verbose_name='Qualified'),
        ),
        migrations.AddField(
            model_name='breeds',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Creator'),
        ),
    ]
