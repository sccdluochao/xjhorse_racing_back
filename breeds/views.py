from django.urls import reverse_lazy
from django.views.generic import TemplateView, DetailView, ListView, CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.utils.translation import ugettext as _
from rules.contrib.views import PermissionRequiredMixin
from rules import is_superuser, has_perm
from sortedcontainers import SortedDict
from pypinyin import pinyin, Style

from breeds.models import Carousels, Continents, Videos, Breeds, CountriesRegions


# from errorhandle.exceptions import BusinessLogicException


# class BreedsHomePageView(TemplateView):
#    template_name = "breeds/index.html"

def breeds(request):
    # carousels = Carousel.objects.all()
    # carousels = Carousel.objects.filter(enabled=True).order_by('-ranking')

    # carousels = Carousel.objects.filter(enabled=True).order_by('-ranking')
    carousels = Carousels.objects.raw(
        'SELECT *,(SELECT count(*) FROM breeds_carousels b WHERE a.ranking >= b.ranking AND b.enabled) AS new_ranking FROM breeds_carousels a WHERE a.enabled ORDER BY new_ranking DESC;')
    count = sum(1 for carousel in carousels)
    title = _('Top %(number)s Beautiful Horse Breeds') % {'number': count}
    # title = _('Top %(number)s Beautiful Horse Breeds') % {'number': carousels.count()}
    context = {
        "carousels": carousels,
        "title": title,
    }
    return render(request, "breeds/index.html", context)


'''
class BreedsManageListView(PermissionListMixin, ListView):
    model = Breeds
    PermissionListMixin.permission_required = ['view_breeds', ]
    template_name = 'breeds/breeds_manage_list.html'
    return_403 = True


breeds_manage_list = BreedsManageListView.as_view()
'''

'''
@permission_required('breeds.add_breeds', raise_exception=True)

def breeds_manage_list(request):
    my_breeds = Breeds.objects.all()
    context = {
        "object_list": my_breeds,
    }
    return render(request, "breeds/breeds_manage_list.html", context)
'''


class BreedsManageListView(PermissionRequiredMixin, ListView):
    # model = Breeds
    permission_required = ['breeds.add_breeds', ]
    raise_exception = True
    template_name = 'breeds/breeds_manage_list.html'

    def queryset(self):
        if self.request.user.is_superuser or has_perm('breeds.translator', self.request.user):
            return Breeds.objects.all().order_by('name_en_us')
        try:
            return Breeds.objects.all().filter(user=self.request.user).order_by('name_en_us')
        except AttributeError:
            return None


breeds_manage_list = BreedsManageListView.as_view()


class BreedsManageDetailView(PermissionRequiredMixin, DetailView):
    model = Breeds
    permission_required = ['breeds.view_breed', ]
    raise_exception = True
    template_name = 'breeds/breeds_manage_detail.html'


breeds_manage_detail = BreedsManageDetailView.as_view()


class BreedsManageCreateView(PermissionRequiredMixin, CreateView):
    model = Breeds
    fields = ['name_en_us',
              'name_zh_hans',
              'name_ug_ar',
              'name_kk_ar',
              'description_en_us',
              'description_zh_hans',
              'description_ug_ar',
              'description_kk_ar',
              'breeding_history_en_us',
              'breeding_history_zh_hans',
              'breeding_history_ug_ar',
              'breeding_history_kk_ar',
              'stallion_photo',
              'mare_photo'
              ]

    permission_object = None
    permission_required = ['breeds.add_breeds']
    raise_exception = True
    template_name = 'breeds/breeds_manage_create.html'

    def form_valid(self, form, *args, **kwargs):
        form.instance.user = self.request.user
        return super(BreedsManageCreateView, self).form_valid(form, *args, **kwargs)


breeds_manage_create = BreedsManageCreateView.as_view()


class BreedsManageUpdateView(PermissionRequiredMixin, UpdateView):
    model = Breeds
    permission_required = ['breeds.change_breed', ]
    raise_exception = True

    template_name = 'breeds/breeds_manage_update.html'

    def dispatch(self, request, *args, **kwargs):
        if is_superuser(request.user):
            self.fields = ['name_en_us',
                           'name_zh_hans',
                           'name_ug_ar',
                           'name_kk_ar',
                           'description_en_us',
                           'description_zh_hans',
                           'description_ug_ar',
                           'description_kk_ar',
                           'breeding_history_en_us',
                           'breeding_history_zh_hans',
                           'breeding_history_ug_ar',
                           'breeding_history_kk_ar',
                           'stallion_photo',
                           'mare_photo',
                           'qualified',  # 超级用户才出现的字段
                           'comment',  # 超级用户才出现的字段
                           ]
        else:
            self.fields = ['name_en_us',
                           'name_zh_hans',
                           'name_ug_ar',
                           'name_kk_ar',
                           'description_en_us',
                           'description_zh_hans',
                           'description_ug_ar',
                           'description_kk_ar',
                           'breeding_history_en_us',
                           'breeding_history_zh_hans',
                           'breeding_history_ug_ar',
                           'breeding_history_kk_ar',
                           'stallion_photo',
                           'mare_photo'
                           ]
        return super().dispatch(request, *args, **kwargs)


breeds_manage_update = BreedsManageUpdateView.as_view()


class BreedsManageDeleteView(PermissionRequiredMixin, DeleteView):
    model = Breeds
    success_url = reverse_lazy('breeds_manage_list')
    permission_required = ['breeds.delete_breed']
    raise_exception = True
    template_name = 'breeds/breeds_manage_delete.html'


breeds_manage_delete = BreedsManageDeleteView.as_view()


def list(request):
    """
    如果用户偏好语言为中文
        使用breed.name_zh_hans字段，按汉语拼音首字母归类，并且按汉语拼音排序。
    否则
       使用breed.name_en_us字段，按英文名称首字母归类，并且按英文字典排序

    使用SortedDict是为了保证字典按key排序
    breeds_group_by_initial是一个按key排序的字典，其中key为首字母，value为一个SortedDict，对应一个字母的所有品种
    value的key为品种名称的英文或拼音全称，其value为对应的品种对象
    如：
    {'H': {'hanxuema' :breed, ...}, ...}
    """
    breeds_group_by_initial = SortedDict()
    if request.LANGUAGE_CODE == 'zh-hans':
        breeds = Breeds.objects.all().filter(qualified=True)
        for breed in breeds:
            initial = pinyin(breed.name_zh_hans, style=Style.FIRST_LETTER)[0][0].upper()
            pinyin_list = pinyin(breed.name_zh_hans, style=Style.NORMAL, errors='ignore')
            pinyin_str = ''.join(str(r) for v in pinyin_list for r in v)

            if initial in breeds_group_by_initial:
                breeds_group_by_initial[initial][pinyin_str] = breed
            else:
                breeds_group_by_initial[initial] = SortedDict({pinyin_str: breed})
    else:

        breeds = Breeds.objects.all().filter(qualified=True).order_by('name_en_us')

        for breed in breeds:
            initial = breed.name_en_us[0]
            if initial in breeds_group_by_initial:
                breeds_group_by_initial[initial][breed.name_en_us] = breed
            else:
                breeds_group_by_initial[initial] = SortedDict({breed.name_en_us: breed})

    context = {
        "total": breeds.count(),
        "breeds": breeds_group_by_initial,
    }
    return render(request, "breeds/list.html", context)


# class BreedsDistributionView(TemplateView):
#    template_name = "breeds/distribution.html"

def distribution(request):
    continents = Continents.objects.all()
    context = {
        "continents": continents,
    }
    return render(request, "breeds/distribution.html", context)


# class BreedsVideoView(TemplateView):
#    template_name = "breeds/video.html"

def video(request):
    videos = Videos.objects.all().order_by('ranking')
    context = {
        "videos": videos,
    }
    return render(request, "breeds/video.html", context)


class BreedsAlbumView(TemplateView):
    template_name = "breeds/album.html"


def breed(request, pk):
    this_breed = Breeds.objects.get(id=pk)
    context = {
        "breed": this_breed,
    }
    return render(request, "breeds/breed.html", context)


def country_region(request, pk):
    this_country_region = CountriesRegions.objects.get(code=pk)
    context = {
        "country_region": this_country_region,
    }
    return render(request, "breeds/country_region.html", context)
