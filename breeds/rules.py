import rules


# 使用修饰符@rules.predicate自定义predicates（谓词或判断式），返回True表示有权限，False表示无权限


# 品种对象创建者
@rules.predicate
def is_breed_creator(user, breed):
    if not breed:
        return False
    return breed.user == user


# 翻译人员
@rules.predicate
def is_translator(user):
    return user.username == 'mik' or user.username == '艾柯代'


# 品种对象审核合格
@rules.predicate
def is_qualified(user, breed):
    if not breed:
        return False
    return breed.qualified


# 设置Rules
# rules.add_rule('can_view_my_own_breed', is_breed_creator)
# rules.add_rule('can_delete_my_own_breed', is_breed_creator)
# rules.add_perm('can_change_my_own_breed', is_breed_creator)

# 设置Permissions
rules.add_perm('breeds', rules.always_allow)

# 品种对象创建者和翻译人员拥有查看权限
rules.add_perm('breeds.view_breed', is_breed_creator | is_translator)

# 只有品种对象创建者拥有删除权限
rules.add_perm('breeds.delete_breed', is_breed_creator & ~is_qualified)

# 品种对象创建者和翻译人员拥有修改权限
rules.add_perm('breeds.change_breed', (is_breed_creator | is_translator) & ~is_qualified)

rules.add_perm('breeds.translator', is_translator)      # 翻译人员权限
