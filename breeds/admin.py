from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from rules.contrib.admin import ObjectPermissionsModelAdmin
from breeds.models import Carousels, Continents, CountriesRegions, Videos, Breeds


class CarouselsAdmin(TranslationAdmin):
    list_display = ('id', 'breed_name', 'ranking', 'photo', 'enabled', 'active')  # 列表


class ContinentsAdmin(TranslationAdmin):
    list_display = ('code', 'name')  # 列表


class CountriesRegionsAdmin(TranslationAdmin):
    list_display = ('code', 'name', 'full_name', 'iso3', 'number_code', 'continent_code', 'flag')  # 列表


class VideosAdmin(TranslationAdmin):
    list_display = ('id', 'title', 'ranking', 'duration', 'url', 'enabled')  # 列表


class BreedsAdmin(ObjectPermissionsModelAdmin, TranslationAdmin):
    list_display = ('id', 'name_en_us', 'name_zh_hans', 'name_ug_ar', 'name_kk_ar')  # 列表
    search_fields = ('name_en_us', 'name_zh_hans', 'name_ug_ar', 'name_kk_ar')
    ordering = ('name_en_us',)


admin.site.register(Carousels, CarouselsAdmin)
admin.site.register(Continents, ContinentsAdmin)
admin.site.register(CountriesRegions, CountriesRegionsAdmin)
admin.site.register(Videos, VideosAdmin)
admin.site.register(Breeds, BreedsAdmin)
