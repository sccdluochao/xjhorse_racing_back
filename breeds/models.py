from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from os.path import splitext
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
import os


# 重复上传照片时，用新照片覆盖老照片
class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return super(OverwriteStorage, self).get_available_name(name, max_length)


# 轮播照片上传位置和文件名称
def upload_carousel_photo(instance, filename):
    return "breeds/carousel/%s" % filename


# 国旗地区旗帜照片上传位置和文件名称
def upload_country_region_flag(instance, filename):
    return "breeds/country_region_flag/%s" % filename


# 品种视频海报照片上传位置和文件名称
def upload_video_poster(instance, filename):
    return "breeds/video_poster/%s" % filename


# 品种公马照片上传位置和文件名称
def upload_stallion_photo(instance, filename):
    file_name, extension = splitext(filename)
    return "breeds/breeds/%s_stallion%s" % (file_name, extension)


# 品种母马照片上传位置和文件名称
def upload_mare_photo(instance, filename):
    file_name, extension = splitext(filename)
    return "breeds/breeds%s_mare%s" % (file_name, extension)


# 马品种模型，一个品种对应一个对象
# 在数据库表中，一个品种对应一条记录
class Breeds(models.Model):
    name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Breed Name")
    )
    description = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Breeds Description")
    )
    breeding_history = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Breeding History")
    )
    stallion_photo = models.ImageField(
        upload_to=upload_stallion_photo,
        storage=OverwriteStorage(),
        null=False,
        blank=False,
        verbose_name=_("Stallion Photo")
    )
    mare_photo = models.ImageField(
        upload_to=upload_mare_photo,
        storage=OverwriteStorage(),
        null=True,
        blank=True,
        verbose_name=_("Mare Photo")
    )
    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
        default=1,
        verbose_name=_("Creator"))
    qualified = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("Qualified"))
    comment = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_("Comment"))

    class Meta:
        verbose_name = _('Breed')
        verbose_name_plural = _('Breeds')

    def __str__(self):
        return self.name

    def get_country_region(self):
        countries_regions = CountriesRegions.objects.filter(breeds=self.id)
        return ",".join(countries_region.name for countries_region in countries_regions)

    # 添加新对象成功后跳转的URL
    def get_absolute_url(self):
        return reverse('breeds_manage_detail', kwargs={'pk': self.id})


# 品种图片轮播
class Carousels(models.Model):
    breed_name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_('Breed Name'))
    ranking = models.SmallIntegerField(
        default=0,
        verbose_name=_('Ranking'))
    photo = models.ImageField(
        upload_to=upload_carousel_photo,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        verbose_name=_("Breed Photo"))
    enabled = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"))
    active = models.BooleanField(
        default=False,
        verbose_name=_("Active"))

    class Meta:
        verbose_name = _('Breeds Carousel')
        verbose_name_plural = _('Breeds Carousels')


# 七大洲
class Continents(models.Model):
    code = models.CharField(primary_key=True,
                            max_length=2,
                            null=False,
                            blank=False,
                            verbose_name=_('Continent Code'))
    name = models.CharField(max_length=200,
                            null=False,
                            blank=False,
                            verbose_name=_('Continent Name'))

    class Meta:
        verbose_name = _('Continent')
        verbose_name_plural = _('Continents')

    def __str__(self):
        return self.name

    def get_countries(self):
        countriesRegions = CountriesRegions.objects.filter(continent_code=self.code)
        return countriesRegions


# 国家地区，ISO 3166-1:2013版
class CountriesRegions(models.Model):
    # 2字母国家地区代码（ISO 3166-1 alpha-2）
    code = models.CharField(primary_key=True,
                            max_length=2,
                            null=False,
                            blank=False,
                            verbose_name=_('Country Regions Code'))
    # 国家地区英文简称
    name = models.CharField(max_length=200,
                            null=False,
                            blank=False,
                            verbose_name=_('Country Regions Name'))
    # 国家地区英文全称
    full_name = models.CharField(max_length=200,
                                 null=False,
                                 blank=False,
                                 verbose_name=_('Country Regions Full Name'))
    # 3字母国家地区代码（ISO 3166-1 alpha-3）
    iso3 = models.CharField(max_length=3,
                            null=False,
                            blank=False,
                            verbose_name=_('Three-letter Country Regions Code'))
    # 3为数字国家地区代码（ISO 3166-1 numeric）
    number_code = models.CharField(max_length=3,
                                   null=False,
                                   blank=False,
                                   verbose_name=_('Three-digit Country Regions Code'))
    # 洲代码，外键（Continents.code）
    continent_code = models.ForeignKey(Continents,
                                       on_delete=models.CASCADE,
                                       null=False,
                                       blank=False,
                                       verbose_name=_('Continent Code'))
    flag = models.ImageField(
        upload_to=upload_country_region_flag,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Flag"))

    breeds = models.ManyToManyField(
        Breeds,
        verbose_name=_("Breeds")
    )

    class Meta:
        verbose_name = _('CountryRegion')
        verbose_name_plural = _('CountriesRegions')

    def __str__(self):
        return self.name


# 品种视频
class Videos(models.Model):
    title = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_('Video Title'))
    ranking = models.SmallIntegerField(
        default=0,
        verbose_name=_('Ranking'))
    duration = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_("Duration"))
    url = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("Video url"))
    enabled = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"))
    poster = models.ImageField(
        upload_to=upload_video_poster,
        storage=OverwriteStorage(),
        blank=False,
        null=False,
        verbose_name=_("Poster"))

    class Meta:
        verbose_name = _('Breeds Video')
        verbose_name_plural = _('Breeds Videos')
