from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class BreedsConfig(AppConfig):
    name = 'breeds'
    verbose_name = _('Horse Breeds')
