from django.conf.urls import url
from breeds import views

urlpatterns = [
    url(r'^breeds$', views.breeds, name='breeds_home'),
    url(r'^breeds/list$', views.list, name='breeds_list'),
    url(r'^breeds/list/(?P<pk>\d+)$', views.breed, name='breeds_breed'),
    url(r'^breeds/breeds_manage_list$', views.breeds_manage_list, name='breeds_manage_list'),
    url(r'^breeds/breeds_manage/(?P<pk>\d+)$', views.breeds_manage_detail, name='breeds_manage_detail'),
    url(r'^breeds/breeds_manage/create$', views.breeds_manage_create, name='breeds_manage_create'),
    url(r'^breeds/breeds_manage/update/(?P<pk>\d+)$', views.breeds_manage_update, name='breeds_manage_update'),
    url(r'^breeds/breeds_manage/delete/(?P<pk>\d+)$', views.breeds_manage_delete, name='breeds_manage_delete'),
    url(r'^breeds/distribution$', views.distribution, name='breeds_distribution'),
    url(r'^breeds/distribution/(?P<pk>[A-Z][A-Z])', views.country_region, name='breeds_country_region'),
    url(r'^breeds/video$', views.video, name='breeds_video'),
    url(r'^breeds/album$', views.BreedsAlbumView.as_view(), name='breeds_album'),
]
