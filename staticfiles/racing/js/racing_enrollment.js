$(function () {
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-dialog").modal("show");
            },
            success: function (data) {
                $("#modal-dialog .modal-content").html(data.html_form);
            }
        });
    };
    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#data-table tbody").html(data.html_list);
                    $("#modal-dialog").modal("hide");
                }
                else {
                    $("#modal-dialog .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    var saveCreateGameForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#data-table").html(data.html_list);
                    $("#modal-dialog").modal("hide");
                }
                else {
                    $("#modal-dialog .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };
    /* Binding */
    // 添加报名项目
    $("#data-table").on('click', '.js-create-game', loadForm);
    $("#modal-dialog").on("submit", ".js-create-game-form", saveCreateGameForm);

    // 删除报名项目
    $("#data-table").on('click', '.js-delete-game', loadForm);
    $("#modal-dialog").on("submit", ".js-delete-game-form", saveCreateGameForm);

    // 锁定报名项目
    $("#data-table").on('click', '.js-enrollment-fixed', loadForm);
    $("#modal-dialog").on("submit", ".js-enrollment-fixed-form", saveCreateGameForm);

    // 会员资格审核
    $("#data-table").on('click', '.js-update-member-qualify', loadForm);
    $("#modal-dialog").on("submit", ".js-update-member-qualify-form", saveForm);

    // 裁判员资格审核
    $("#data-table").on('click', '.js-update-referee-qualify', loadForm);
    $("#modal-dialog").on("submit", ".js-update-referee-qualify-form", saveForm);

    // 骑师资格审核
    $("#data-table").on('click', '.js-update-horseman-qualify', loadForm);
    $("#modal-dialog").on("submit", ".js-update-horseman-qualify-form", saveForm);

    // 马匹审核
    $("#data-table").on('click', '.js-update-horse-qualify', loadForm);
    $("#modal-dialog").on("submit", ".js-update-horse-qualify-form", saveForm);

    // 报名审核
    $("#data-table").on('click', '.js-update-enrollment-qualify', loadForm);
    $("#modal-dialog").on("submit", ".js-update-enrollment-qualify-form", saveForm);


});
