$(function () {

    get_description = function () {
        $.ajax({
            url: "/racing/get_description",
            type: 'get',
            data: {'role': $('#id_role option:selected').val()},
            dataType: 'json',

            success: function (response) {
                $("#description").html(response.description);
            }
        });
    };

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-dialog").modal("show");
            },
            success: function (data) {
                $("#modal-dialog .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#role-table tbody").html(data.html_role_list);
                    $("#add-role-btn").html(data.html_add_role_btn);
                    $("#modal-dialog").modal("hide");
                }
                else {
                    $("#modal-dialog .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };
    /* Binding */
    // add role
    $("#add-role-btn").on('click', '#js-add-role', loadForm);
    $("#modal-dialog").on("submit", ".js-add-role-form", saveForm);


    // abandon role
    $("#role-table").on("click", ".js-role-abandon", loadForm);
    $("#modal-dialog").on("submit", ".js-abandon-role-form", saveForm);
});
