$(function () {
    var addToCart = function () {
        var btn = $(this);
        var product_id = $('#ticket_id').val();
        var quantity = $("#quantity").val();
        var csrfToken = document.getElementById('csrf_token').value;

        $.ajax({
            url: btn.attr("data-url"),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({"pk": product_id, "quantity": quantity}),
            beforeSend: function (xhr) {
                // 向讯服务器发送POST请求之前，先设置CSRF Token
                xhr.setRequestHeader("X-CSRFToken", csrfToken);
            },
            success: function () {
                alert("Thanks!");
            }
        });
        return false;
    };

    var cart = {
        add: function (pk, quantity) {
            quantity = quantity || 1
            $.ajax
            ({
                type: "POST",
                //the url where you want to sent the userName and password to
                url: data - url,
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                //json object to sent to the authentication url
                data: JSON.stringify({"pk": pk, "quantity": quantity}),
                success: function () {

                    alert("Thanks!");
                }
            })
            return false
        },

        remove: function (itemPK) {
            return $.post(URLS.removeItem, {pk: itemPK}, 'json')
        },

        changeQuantity: function (pk, quantity) {
            return $.post(URLS.changeQuantity, {pk: pk, quantity: quantity}, 'json')
        },

        empty: function () {
            $.post(URLS.emptyCart, 'json')
        },
    };


    var URLS = {
        addItem: '{% url "cart-add" %}',
        removeItem: '{% url "cart-remove" %}',
        changeQuantity: '{% url "cart-change-quantity" %}',
        emptyCart: '{% url "cart-empty" %}',
    };


    update_total = function (quantity) {
        price = document.getElementById("price").value;
        var x = document.getElementById("total");
        x.value = quantity * price;
    };

    $("#ticket-item").on("click", "#js-add-to-cart", addToCart);
});
