$(function () {
    get_participant = function () {

        $.ajax({
            url: "/get_participant",
            type: 'get',
            data: {'application': $('#id_application option:selected').val()},
            dataType: 'json',

            success: function (response) {
                $("#participant").html(response.participant);
            }
        });
    };


    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-dialog").modal("show");
            },
            success: function (data) {
                $("#modal-dialog .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#application-table tbody").html(data.html_application_list);
                    $("#add-application-btn").html(data.html_add_application_btn);
                    $("#modal-dialog").modal("hide");
                }
                else {
                    $("#modal-dialog .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // add application
    $("#add-application-btn").on('click', '#js-add-application', loadForm);
    $("#modal-dialog").on("submit", ".js-add-application-form", saveForm);


    // abandon application
    $("#application-table").on("click", ".js-application-abandon", loadForm);
    $("#modal-dialog").on("submit", ".js-abandon-application-form", saveForm);
});
