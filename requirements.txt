defusedxml>=0.5.0                  # 预防xml bomb攻击
django>=1.10.6                     # Web App框架
django-bootstrap3>=8.2.1           # 让django模板输出Bootstrap 3代码以满足响应式设计要求
django-cors-middleware>=1.3.1      # 跨域授权
django-uuidfield>=0.5.0            # 为django models 提供了UUIDField
djangorestframework>=3.6.2         # 基于Django的REST框架
dj-static>=0.0.6                   # django生产环境中提供静态文件服务
gunicorn>=19.7.1                   # 在生产环境中使用专用应用服务器，
                                   # 绿色独角兽Gunicorn是一个被广泛使用的高性能的Python WSGI UNIX HTTP服务器，
                                   # Gunicorn服务器作为wsgi app的容器，能够与各种Web框架兼容（如django）
oauthlib>=2.0.2                    # 开放网络授权（authorization）标准库
python3-openid>=3.1.0              # 分布式网络认证库
requests>=2.13.0                   # HTTP请求
requests-oauthlib>=0.8.0           # 开放网络授权HTTP请求
# django-allauth>=0.31.0           # 用户管理框架，包括注册、登录、账户管理、第三方（社交）账户登录。
                                   # 直接使用源代码，作为本项目的一个App对待，名称为allauth
# django-oauth-toolkit>=0.12.0     # Django OAuth2工具包。直接使用源代码，作为本项目的一个App对待，名称为oauth2_provider
pillow>=4.3.0                      # Python图像处理
reportlab>=3.4.0                   # Python PDF处理
django-widget-tweaks>=1.4.1        # 表单字段渲染
psycopg2-binary>=2.7.3.2                  # postgreSQL数据库Pythony编程语言适配器
sqlparse>=0.2.4                    # SQL语言解析
django-modeltranslation>=0.12.1    # 数据库多语言支持
pypinyin>=0.30.0                   # 支持中文字符串转拼音，以便按拼音排序
sortedcontainers>=1.5.9            # 排序容器
rules>=1.3                         # 对象（记录）权限
python-alipay-sdk>=1.7.0           # 支付宝sdk
