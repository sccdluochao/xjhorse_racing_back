from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required
from certification.decorator import certificate_required
from permission.models import Application, UserApplication
from permission.forms import UserApplicationForm



@login_required  # 首先必须是登录用户
@certificate_required('3')  # 必须通过手机短信认证
def my_application(request):
    applications = Application.objects.all()
    my_applications = UserApplication.objects.filter(user=request.user)
    context = {
        "applications": applications,
        "my_applications": my_applications,
    }
    return render(request, "permission/user_application_list.html", context)


def my_application_add(request):
    data = dict()

    if request.method == 'POST':
        form = UserApplicationForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            my_applications = UserApplication.objects.filter(user=request.user)
            data['html_application_list'] = render_to_string('permission/user_application_partial.html', {
                'my_applications': my_applications
            })
            data['html_add_application_btn'] = render_to_string('permission/user_application_add_btn.html', {
                'my_applications': my_applications
            })
        else:
            data['form_is_valid'] = False
    else:
        form = UserApplicationForm(request.user, initial={'user': request.user})

    context = {'form': form}
    data['html_form'] = render_to_string('permission/user_application_add.html',
                                         context,
                                         request=request
                                         )

    return JsonResponse(data)


def my_application_abandon(request, pk):
    application = get_object_or_404(UserApplication, pk=pk)
    data = dict()
    if request.method == 'POST':
        application.delete()
        data['form_is_valid'] = True
        my_applications = UserApplication.objects.filter(user=request.user)
        data['html_application_list'] = render_to_string('permission/user_application_partial.html', {
            'my_applications': my_applications
        })
        data['html_add_application_btn'] = render_to_string('permission/user_application_add_btn.html', {
            'my_applications': my_applications
        })
    else:
        context = {'application': application}
        data['html_form'] = render_to_string('permission/user_application_abandon.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# 获取每个应用平台的适合人群
def get_participant(request):
    data = dict()
    if request.is_ajax() and request.method == 'GET':
        application_id_str = request.GET.get('application', '')
        if application_id_str is not None and application_id_str != '':
            application_id = int(application_id_str)
            application = Application.objects.get(id=application_id)
            data['participant'] = application.participant
        else:
            data['participant'] = _("Please choose a application!")

    return JsonResponse(data)
