import rules
from django.shortcuts import get_object_or_404

from certification.models import Certification, Corporation
from permission.models import Application, UserApplication


# 使用修饰符@rules.predicate自定义predicates（谓词或判断式），返回True表示有权限，False表示无权限

def get_corporation(request, corporation_id):
    return get_object_or_404(Corporation, pk=corporation_id)


# 是否通过了手机短信认证（3）
@rules.predicate
def is_individual_certificated(user):
    try:
        if user.certification:
            return user.certification.has_certification('3')
    except Certification.DoesNotExist:
        return False


# 是否通过了单位认证（5）
@rules.predicate
def is_corporation_certificated(user):
    try:
        if user.certification:
            return user.certification.has_certification('5')
    except Certification.DoesNotExist:
        return False


# 是否为特定单位管理员
@rules.predicate
def is_corporation_admin(user, corporation):
    if not corporation:
        return False
    try:
        return user.certification.uscc == corporation and corporation.admin == user
    except Certification.DoesNotExist:
        return False


# 是否为特定单位的普通员工
@rules.predicate
def is_corporation_user(user, corporation):
    if not corporation:
        return False
    try:
        return user.certification.uscc == corporation
    except Certification.DoesNotExist:
        return False


# 是否可以使用特定的应用平台
'''
新疆马业
马匹登记
马场管理
马匹竞拍
天马赛事
'''


@rules.predicate
def is_application_user(user, application):
    if not rules.is_authenticated(user):
        return False

    if not application:
        return False
    try:
        UserApplication.objects.get(user=user, application__app_name=application)
        return True
    except UserApplication.DoesNotExist:
        return False


# 设置Permissions
rules.add_perm('permission', rules.always_allow)

# 单位系统管理员权限
rules.add_perm('permission.corporation_admin', is_corporation_admin)

# 通过单位认证用户的权限
rules.add_perm('permission.corporation_certificated', is_corporation_certificated)

# 通过个人认证用户的权限
rules.add_perm('permission.individual_certificated', is_individual_certificated)

# 使用指定应用平台的权限
rules.add_perm('permission.application_user', is_application_user)


# 特定单位普通员工权限
rules.add_perm('permission.corporation_user', is_corporation_user)
