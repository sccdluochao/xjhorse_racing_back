from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class PermissionConfig(AppConfig):
    name = 'permission'
    verbose_name = _('Application')
