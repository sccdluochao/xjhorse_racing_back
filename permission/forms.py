from django import forms
from permission.models import UserApplication, Application


class UserApplicationForm(forms.ModelForm):
    class Meta:
        model = UserApplication
        fields = ('user', 'application',)
        widgets = {
            'user': forms.HiddenInput(),
            'application': forms.Select(attrs={'onchange': 'get_participant();'}),
        }

    def __init__(self, user, *args, **kwargs):
        super(UserApplicationForm, self).__init__(*args, **kwargs)
        application_id_list = UserApplication.objects.filter(user=user).values_list('application_id', flat=True)
        self.fields['application'].queryset = Application.objects.exclude(id__in=application_id_list)
