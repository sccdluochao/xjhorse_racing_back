��          �      l      �     �     �     �               *  S   7     �     �     �     �     �  0   �  	             +      H     i     n       d  �     �     �          ,     9     L  O   Y     �     �     �     �     �  6   �     3     :  '   P  !   x     �     �     �                                                       
      	                                     Abandon Abandon Application Add Application Application Application Name Applications Are you sure you want to abandon <strong>%(application_name)s</strong> application? Close Confirm abandon application Enable application Enabled Applications My Application Notice: You didn't select your applications yet! Operation Participant Please choose a application! Select and enable my application User User Application User Applications Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-08 23:52+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 弃用 弃用应用平台 选择并启用应用平台 应用平台 应用平台名称 应用平台 您确信要放弃使用<strong>%(application_name)s</strong>应用平台吗？ 关闭 弃用应用平台确认 启用应用平台 应用平台 我的应用平台 注意：您尚未选择和启用任何应用平台！ 操作 适合使用的人群 请选择您要使用的应用平台！ 选择并启用我的应用平台 用户 用户的应用平台 用户的应用平台 