-- 加载初始数据
-- makemigrations permission
-- makemigrations permission --empty
-- 编辑makemigrations permission --empty生成的迁移文件，添加以下内容
-- def load_data_from_sql():
--     from xjhorse.settings import BASE_DIR
--     import os
--     sql_statements = open(os.path.join(BASE_DIR, 'permission/sql/init.sql'), 'r').read()
--     return sql_statements
--
-- class Migration(migrations.Migration):
--     dependencies = [
--         ('permission', '0001_initial'),
--     ]
--
--     operations = [
--         migrations.RunSQL(load_data_from_sql()),
--    ]


-- 1、插入应用平台记录
INSERT INTO permission_application (app_name, participant) VALUES
  ('新疆马业', '借助【新疆马业】平台提供的WebService进行二次开发的程序设计人员'),
  ('马匹登记', '从事马匹登记、马匹鉴定、马匹护照登记的人员，包括个人马主、企业马主、马匹登记审核人员、马匹鉴定专家等'),
  ('马场管理', '养马企业的各类管理人员和技术人员'),
  ('马匹竞拍', '需要参与马匹竞价拍卖的人员，包括个人马主、企业马主、马匹卖家、马匹买家、竞拍公司职员等'),
  ('天马赛事', '组织、参与天马节马术比赛的各类人员，包括赛事组织人员、裁判员、运动队领队、运动员、购票观众等');
