from django.conf.urls import url
from permission import views

urlpatterns = [
    url(r'^my_application$', views.my_application, name='my_application'),
    url(r'^my_application_add$', views.my_application_add, name='my_application_add'),
    url(r'^my_application_abandon/(?P<pk>\d+)$', views.my_application_abandon, name='my_application_abandon'),
    url(r'^get_participant$', views.get_participant, name='get_participant'),
]
