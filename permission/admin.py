from django.contrib import admin
from permission.models import Application, UserApplication


class ApplicationAdmin(admin.ModelAdmin):
    list_display = ('id', 'app_name', 'participant')  # 列表


class UserApplicationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'application')  # 列表


admin.site.register(Application, ApplicationAdmin)
admin.site.register(UserApplication, UserApplicationAdmin)
