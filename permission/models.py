from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


# Create your models here.

class Application(models.Model):
    app_name = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_("Application Name")
    )
    participant = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("Participant")
    )

    class Meta:
        verbose_name = _('Application')
        verbose_name_plural = _('Applications')

    def __str__(self):
        return self.app_name


class UserApplication(models.Model):
    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
        verbose_name=_("User")
    )
    application = models.ForeignKey(
        Application,
        null=False,
        blank=False,
        verbose_name=_("Application")
    )

    class Meta:
        verbose_name = _('User Application')
        verbose_name_plural = _('User Applications')
