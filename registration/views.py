from django.shortcuts import render
from django.utils.translation import ugettext as _


def registration(request):
    context = {
        # "carousels": carousels,
        # "title": title,
    }
    return render(request, "registration/index.html", context)
