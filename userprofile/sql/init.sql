
-- 插入userProfile_languages记录
INSERT INTO userprofile_languages(language_name, locale_code) VALUES
('简体中文', 'zh-hans'),
('English', 'en-us'),
('ئۇيغۇر تىلى‎', 'ug-ar'),
('قازاق ٴتىلى', 'kk-ar');
