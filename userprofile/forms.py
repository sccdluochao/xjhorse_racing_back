# 创 建 人：张太红
# 创建日期：2017年04月12日

from django import forms
from .models import UserProfile


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('user', 'locale_code')

        widgets = {'user': forms.HiddenInput()}
