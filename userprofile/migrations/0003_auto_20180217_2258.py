# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-02-17 14:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0002_auto_20180129_0028'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userprofile',
            options={'verbose_name': 'Language Preference', 'verbose_name_plural': 'Languages Preference'},
        ),
    ]
