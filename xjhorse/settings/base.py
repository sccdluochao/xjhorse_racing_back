"""
xjhorse项目Django配置
"""

import os

# 项目目录绝对路径
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
print(BASE_DIR)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# Application definition
AUTHENTICATION_BACKENDS = (
    # 对象（记录）权限
    'rules.permissions.ObjectPermissionBackend',

    # 尽管有了allauth，Django admin仍然需要以下认证模块来完成通过用户名登录
    'django.contrib.auth.backends.ModelBackend',

    # allauth特定的认证方法，如通过e-mail登录
    'allauth.account.auth_backends.AuthenticationBackend',
    # 'guardian.backends.ObjectPermissionBackend',  # 对象（记录）权限
)

# AUTH_USER_MODEL = 'permission.User'

INSTALLED_APPS = [
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # 便于使用bootstrap模板标签
    'bootstrap3',

    #  社交媒体登录，allauth在django需要以下配置
    'django.contrib.sites',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.baidu',  # 1、百度
    'allauth.socialaccount.providers.taobao',  # 2、淘宝
    'allauth.socialaccount.providers.qq',  # 3、QQ
    'allauth.socialaccount.providers.weixin',  # 4、微信
    'allauth.socialaccount.providers.google',  # 5、谷歌
    'allauth.socialaccount.providers.facebook',  # 6、脸谱
    'allauth.socialaccount.providers.github',  # 7、github

    'widget_tweaks',  # 表单渲染
    'corsheaders',  # 跨域授权
    'oauth2_provider',  # 授权服务提供者
    'rest_framework',  # RESTful WebService, Django轻量级Web服务框架
    # 'guardian',  # 对象（记录）权限
    'rules.apps.AutodiscoverRulesConfig',  # 对象（记录）权限
    'DjangoUeditor',                       # 富文本编辑

    'homepage.apps.HomepageConfig',  # 新疆马业首页
    'userprofile.apps.UserprofileConfig',  # 用户个人资料，目前主要为了让用户能够选择自己偏好的语言
    'certification.apps.CertificationConfig',  # 实名认证
    'breeds.apps.BreedsConfig',  # 品种展示
    'disease.apps.DiseaseConfig',  # 马病预防
    'auction.apps.AuctionConfig',  # 马匹竞拍
    'registration.apps.RegistrationConfig',  # 马匹登记
    'horsefarm.apps.HorsefarmConfig',  # 马场管理
    'racing.apps.RacingConfig',  # 天马赛事
    'permission.apps.PermissionConfig',  # 公用权限管理
    'errorhandle.apps.ErrorhandleConfig',  # 自定义错误处理
    'dj_pagination',
]

# 天马赛事门票购物车设置
TICKET_CART_SESSION_ID = 'ticket_cart'

# 指定 Django轻量级Web服务框架使用新的后台认证服务
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.ext.rest_framework.OAuth2Authentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

OAUTH2_PROVIDER = {
    # this is the list of available scopes
    'SCOPES': {'read': 'Read scope', 'write': 'Write scope'}
}

MIDDLEWARE = [
    'dj_pagination.middleware.PaginationMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',  # 跨域授权
    'errorhandle.middleware.HandleBusinessExceptionMiddleware',  # 业务逻辑异常处理
]

CSRF_FAILURE_VIEW = 'errorhandle.views.csrf_failure'

ROOT_URLCONF = 'xjhorse.urls'
CORS_ORIGIN_ALLOW_ALL = True  # 跨域授权（允许来自所有域的请求）
X_FRAME_OPTIONS = 'DENY'  # 点击劫持保护
CSRF_COOKIE_HTTPONLY = True  # 防止恶意JavaScript绕过CSRF保护
SECURE_CONTENT_TYPE_NOSNIFF = True  # 禁止浏览器尝试猜测内容的类型
SECURE_BROWSER_XSS_FILTER = True  # 跨站脚本保护
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')       # 使用https协议
# SECURE_HSTS_SECONDS = 3600
# SECURE_SSL_REDIRECT = True
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True

# allauth在django需要以下配置
SITE_ID = 1
# 登录成功后重定向URL
LOGIN_REDIRECT_URL = '/'

ACCOUNT_AUTHENTICATION_METHOD = 'username_email'  # 使用用户名或邮箱登陆
ACCOUNT_USERNAME_REQUIRED = True  # 必须提供用户名
ACCOUNT_EMAIL_REQUIRED = True  # 必须提供邮箱
ACCOUNT_USERNAME_MIN_LENGTH = 3  # 用户名最少由3个字符组成
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'  # 强制Email验证，未通过Email验证用户无法登陆
ACCOUNT_USER_MODEL_USERNAME_FIELD = "username"  # 用户名数据库字段名称
SOCIALACCOUNT_AUTO_SIGNUP = False  # 禁止社交账号自动登录

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email'],
        'METHOD': 'oauth2'
    },
    'google':
        {'SCOPE': ['profile', 'email'],
         'AUTH_PARAMS': {'access_type': 'online'}
         },
}

CRISPY_TEMPLATE_PACK = "bootstrap3"

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.qq.com'  # SMTP地址
EMAIL_PORT = 25  # SMTP端口
EMAIL_HOST_USER = 'ztaihong@qq.com'  # QQ的邮箱
EMAIL_HOST_PASSWORD = 'fdfpeslrpywygjfh'  # QQ SMTP验证码
EMAIL_SUBJECT_PREFIX = u'新疆马业'  # 为邮件标题前缀
EMAIL_USE_TLS = True  # 与SMTP服务器通信时，是否启动TLS链接(安全链接)
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER  # 发件人邮箱

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, 'templates'), ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'racing.cart_context_ processors.cart',  # 门票购物车
                'racing.cart_context_ processors.order',  # 门票订单
            ],
        }
    },
]

WSGI_APPLICATION = 'xjhorse.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# 网站语言（国际化及多语言支持）
# https://docs.djangoproject.com/en/1.10/topics/i18n/
from django.conf import global_settings

gettext_noop = lambda s: s

LANGUAGE_CODE = 'zh-hans'

LANGUAGES = (
    ('en-us', gettext_noop('English')),
    ('zh-hans', gettext_noop('Chinese')),
    ('ug-ar', gettext_noop('Uyghur')),
    ('kk-ar', gettext_noop('Kazakh')),
)

EXTRA_LANG_INFO = {
    'ug-ar': {
        'bidi': True,  # 从右到左
        'code': 'ug-ar',
        'name': 'Uyghur',
        'name_local': u'\u0626\u06C7\u064A\u063A\u06C7\u0631 \u062A\u0649\u0644\u0649',  # unicode编码
    },
    'kk-ar': {
        'bidi': True,  # 从右到左
        'code': 'kk-ar',
        'name': 'Kazakh',
        'name_local': u'\u0642\u0627\u0632\u0627\u0642\u0634\u0627',  # unicode编码
    },
}
# 添加Django未提供的自定义语言
import django.conf.locale

laninfo = django.conf.locale.LANG_INFO
laninfo.update(EXTRA_LANG_INFO)
django.conf.locale.LANG_INFO = laninfo

# 使用BiDi (从右到左) 布局的语言
LANGUAGES_BIDI = global_settings.LANGUAGES_BIDI + ["kk-ar", "ug-ar", ]

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True

#
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
    os.path.join(BASE_DIR, 'allauth/locale'),
    os.path.join(BASE_DIR, 'auction/locale'),
    os.path.join(BASE_DIR, 'breeds/locale'),
    os.path.join(BASE_DIR, 'certification/locale'),
    os.path.join(BASE_DIR, 'disease/locale'),
    os.path.join(BASE_DIR, 'errorhandle/locale'),
    os.path.join(BASE_DIR, 'homepage/locale'),
    os.path.join(BASE_DIR, 'horsefarm/locale'),
    os.path.join(BASE_DIR, 'racing/locale'),
    os.path.join(BASE_DIR, 'registration/locale'),
    os.path.join(BASE_DIR, 'userprofile/locale'),
)

# 数据库多语言支持
MODELTRANSLATION_DEFAULT_LANGUAGE = 'zh-hans'  # modeltranslation缺省语言
MODELTRANSLATION_AUTO_POPULATE = True  # 自动用初始语言填充翻译字段
MODELTRANSLATION_PREPOPULATE_LANGUAGE = 'en-us'
MODELTRANSLATION_ENABLE_FALLBACKS = True

# GUARDIAN_RAISE_403 = True
# GUARDIAN_RENDER_403 = True
# GUARDIAN_TEMPLATE_403 ='errorhandle/403.html'

# 静态文件 (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'

# 告诉Django除了在每个app中寻找静态文件之外，还可以在项目根目录的static目录中寻找静态文件
# 独立于具体app的共用静态文件夹，位于项目根目录
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# 当运行python manage.py collectstatic时，Django会把所有静态文件收集到项目根目录的staticfiles目录中
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")


AppID="wx417f9321f4e9d37c"
AppSecret="90cb526e02c8e68b53a6a22329f42a72"