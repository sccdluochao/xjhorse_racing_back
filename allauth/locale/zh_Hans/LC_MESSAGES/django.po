# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: django-allauth\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-08 23:52+0800\n"
"PO-Revision-Date: 2014-08-12 00:36+0200\n"
"Last-Translator: jresins <jresins@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: allauth/account/adapter.py:50
msgid "Username can not be used. Please use other username."
msgstr "此用户名不能使用，请改用其他用户名。"

#: allauth/account/adapter.py:54
msgid "Too many failed login attempts. Try again later."
msgstr "登录失败次数过多，请稍后重试。"

#: allauth/account/adapter.py:56
msgid "A user is already registered with this e-mail address."
msgstr "此电子邮箱地址已被其他用户注册。"

#: allauth/account/adapter.py:293
#, python-brace-format
msgid "Password must be a minimum of {0} characters."
msgstr "密码长度不得少于 {0} 个字符。"

#: allauth/account/apps.py:7
msgid "Accounts"
msgstr "账号"

#: allauth/account/forms.py:43 allauth/account/forms.py:378
msgid "You must type the same password each time."
msgstr "两次输入的密码必须相同"

#: allauth/account/forms.py:73 allauth/account/forms.py:348
#: allauth/account/forms.py:456
msgid "Password"
msgstr "密码"

#: allauth/account/forms.py:74
msgid "Remember Me"
msgstr "记住我"

#: allauth/account/forms.py:80
msgid "This account is currently inactive."
msgstr "此账号当前未激活。"

#: allauth/account/forms.py:83
msgid "The e-mail address and/or password you specified are not correct."
msgstr "您提供的电子邮箱地址或密码不正确。"

#: allauth/account/forms.py:86
msgid "The username and/or password you specified are not correct."
msgstr "您提供的账户名或密码不正确。"

#: allauth/account/forms.py:89
msgid "The login and/or password you specified are not correct."
msgstr "您提供的账号或密码错误"

#: allauth/account/forms.py:98 allauth/account/forms.py:252
#: allauth/account/forms.py:406 allauth/account/forms.py:475
msgid "E-mail address"
msgstr "电子邮箱地址"

#: allauth/account/forms.py:100 allauth/account/forms.py:283
#: allauth/account/forms.py:401 allauth/account/forms.py:470
msgid "E-mail"
msgstr "电子邮箱"

#: allauth/account/forms.py:105 allauth/account/forms.py:108
#: allauth/account/forms.py:244 allauth/account/forms.py:248
msgid "Username"
msgstr "用户名"

#: allauth/account/forms.py:115
msgid "Username or e-mail"
msgstr "用户名或电子邮箱"

#: allauth/account/forms.py:118
msgctxt "field label"
msgid "Login"
msgstr "登录"

#: allauth/account/forms.py:272
msgid "E-mail (again)"
msgstr "电子邮箱（再输入一次）"

#: allauth/account/forms.py:276
msgid "E-mail address confirmation"
msgstr "电子邮箱地址确认"

#: allauth/account/forms.py:286
msgid "E-mail (optional)"
msgstr "电子邮箱（可选填项）"

#: allauth/account/forms.py:328
msgid "You must type the same email each time."
msgstr "每次输入的密码必须相同"

#: allauth/account/forms.py:351 allauth/account/forms.py:457
msgid "Password (again)"
msgstr "密码（再输入一次）"

#: allauth/account/forms.py:412
msgid "This e-mail address is already associated with this account."
msgstr "此电子邮箱地址已关联到这个账号。"

#: allauth/account/forms.py:414
msgid "This e-mail address is already associated with another account."
msgstr "此电子邮箱地址已关联到其他账号。"

#: allauth/account/forms.py:436
msgid "Current Password"
msgstr "当前密码"

#: allauth/account/forms.py:437 allauth/account/forms.py:526
msgid "New Password"
msgstr "新密码"

#: allauth/account/forms.py:438 allauth/account/forms.py:527
msgid "New Password (again)"
msgstr "新密码（再输入一次）"

#: allauth/account/forms.py:446
msgid "Please type your current password."
msgstr "请输入您的当前密码"

#: allauth/account/forms.py:484
msgid "The e-mail address is not assigned to any user account"
msgstr "此电子邮箱地址未分配给任何用户账号"

#: allauth/account/forms.py:548
msgid "The password reset token was invalid."
msgstr "重设密码的令牌无效。"

#: allauth/account/models.py:23
msgid "user"
msgstr "用户"

#: allauth/account/models.py:27 allauth/account/models.py:86
msgid "e-mail address"
msgstr "电子邮箱地址"

#: allauth/account/models.py:28
msgid "verified"
msgstr "已验证"

#: allauth/account/models.py:29
msgid "primary"
msgstr "首选电子邮箱"

#: allauth/account/models.py:34
msgid "email address"
msgstr "电子邮箱地址"

#: allauth/account/models.py:35
msgid "email addresses"
msgstr "电子邮箱地址"

#: allauth/account/models.py:88
msgid "created"
msgstr "已建立"

#: allauth/account/models.py:90
msgid "sent"
msgstr "已发送"

#: allauth/account/models.py:91 allauth/socialaccount/models.py:61
msgid "key"
msgstr "码"

#: allauth/account/models.py:96
msgid "email confirmation"
msgstr "电子邮箱确认"

#: allauth/account/models.py:97
msgid "email confirmations"
msgstr "电子邮箱确认"

#: allauth/socialaccount/adapter.py:26
#, python-format
msgid ""
"An account already exists with this e-mail address. Please sign in to that "
"account first, then connect your %s account."
msgstr ""
"已有一个账号与此电子邮箱地址关联，请先登录该账号，然后连接你的 %s 账号。"

#: allauth/socialaccount/adapter.py:131
msgid "Your account has no password set up."
msgstr "您的账号未设置密码。"

#: allauth/socialaccount/adapter.py:138
msgid "Your account has no verified e-mail address."
msgstr "您的账号没有验证电子邮箱地址。"

#: allauth/socialaccount/apps.py:7
msgid "Social Accounts"
msgstr "社交账号"

#: allauth/socialaccount/models.py:49 allauth/socialaccount/models.py:83
msgid "provider"
msgstr "提供商"

#: allauth/socialaccount/models.py:52
msgid "name"
msgstr "用户名"

#: allauth/socialaccount/models.py:54
msgid "client id"
msgstr "客户标志"

#: allauth/socialaccount/models.py:56
msgid "App ID, or consumer key"
msgstr "应用标志或消费码"

#: allauth/socialaccount/models.py:57
msgid "secret key"
msgstr "秘钥"

#: allauth/socialaccount/models.py:59
msgid "API secret, client secret, or consumer secret"
msgstr "应用秘钥、客户秘钥或消费密钥"

#: allauth/socialaccount/models.py:64
msgid "Key"
msgstr "码"

#: allauth/socialaccount/models.py:72
msgid "social application"
msgstr "社交媒体应用"

#: allauth/socialaccount/models.py:73
msgid "social applications"
msgstr "社交应用"

#: allauth/socialaccount/models.py:102
msgid "uid"
msgstr "用户唯一标识符"

#: allauth/socialaccount/models.py:104
msgid "last login"
msgstr "最后一次登录"

#: allauth/socialaccount/models.py:106
msgid "date joined"
msgstr "注册日期"

#: allauth/socialaccount/models.py:108
msgid "extra data"
msgstr "附加数据"

#: allauth/socialaccount/models.py:112
msgid "social account"
msgstr "社交账号"

#: allauth/socialaccount/models.py:113
msgid "social accounts"
msgstr "社交账号"

#: allauth/socialaccount/models.py:139
msgid "token"
msgstr "令牌"

#: allauth/socialaccount/models.py:141
msgid "\"oauth_token\" (OAuth1) or access token (OAuth2)"
msgstr "认证令牌(OAuth1)或访问令牌(OAuth2)"

#: allauth/socialaccount/models.py:144
msgid "token secret"
msgstr "令牌秘钥"

#: allauth/socialaccount/models.py:146
msgid "\"oauth_token_secret\" (OAuth1) or refresh token (OAuth2)"
msgstr "认证令牌秘钥(OAuth1)或刷新令牌(OAuth2)"

#: allauth/socialaccount/models.py:148
msgid "expires at"
msgstr "过期时间"

#: allauth/socialaccount/models.py:152
msgid "social application token"
msgstr "社交应用令牌"

#: allauth/socialaccount/models.py:153
msgid "social application tokens"
msgstr "社交应用令牌"

#: allauth/socialaccount/providers/baidu/provider.py:19
msgid "Baidu"
msgstr "百度"

#: allauth/socialaccount/providers/douban/views.py:36
msgid "Invalid profile data"
msgstr "用户基础信息无效"

#: allauth/socialaccount/providers/facebook/provider.py:50
msgid "Facebook"
msgstr "脸谱"

#: allauth/socialaccount/providers/github/provider.py:28
msgid "GitHub"
msgstr "极特代码托管"

#: allauth/socialaccount/providers/google/provider.py:27
msgid "Google"
msgstr "谷歌"

#: allauth/socialaccount/providers/oauth/client.py:78
#, python-format
msgid "Invalid response while obtaining request token from \"%s\"."
msgstr "从\"%s\"获取请求令牌时响应无效。"

#: allauth/socialaccount/providers/oauth/client.py:109
#, python-format
msgid "Invalid response while obtaining access token from \"%s\"."
msgstr "从\"%s\"获取访问令牌时响应无效。."

#: allauth/socialaccount/providers/oauth/client.py:128
#, python-format
msgid "No request token saved for \"%s\"."
msgstr "没有服务于\"%s\"的请求令牌。"

#: allauth/socialaccount/providers/oauth/client.py:177
#, python-format
msgid "No access token saved for \"%s\"."
msgstr "没有服务于\"%s\"的访问令牌。"

#: allauth/socialaccount/providers/oauth/client.py:197
#, python-format
msgid "No access to private resources at \"%s\"."
msgstr "无权访问私有资源 \"%s\"。"

#: allauth/socialaccount/providers/qq/provider.py:17
msgid "QQ"
msgstr "QQ"

#: allauth/socialaccount/providers/taobao/provider.py:15
msgid "Taobao"
msgstr "淘宝"

#: allauth/socialaccount/providers/weixin/provider.py:18
msgid "Weixin"
msgstr "微信"

#: allauth/templates/account/account_inactive.html:6
#: allauth/templates/account/account_inactive.html:9
msgid "Account Inactive"
msgstr "账号未激活"

#: allauth/templates/account/account_inactive.html:12
msgid "This account is inactive."
msgstr "此账号未激活"

#: allauth/templates/account/email.html:6
msgid "Account"
msgstr "账号"

#: allauth/templates/account/email.html:9
msgid "E-mail Addresses"
msgstr "电子邮箱地址"

#: allauth/templates/account/email.html:13
msgid "The following e-mail addresses are associated with your account:"
msgstr "以下电子邮箱地址已关联到您的帐号："

#: allauth/templates/account/email.html:31
msgid "Verified"
msgstr "已验证"

#: allauth/templates/account/email.html:33
msgid "Unverified"
msgstr "未验证"

#: allauth/templates/account/email.html:35
msgid "Primary"
msgstr "首选电子邮箱"

#: allauth/templates/account/email.html:42
msgid "Make Primary"
msgstr "设置首选电子邮箱"

#: allauth/templates/account/email.html:44
msgid "Re-send Verification"
msgstr "重发验证电子邮箱"

#: allauth/templates/account/email.html:45
msgid "Remove"
msgstr "移除"

#: allauth/templates/account/email.html:53
msgid "Warning:"
msgstr "警告："

#: allauth/templates/account/email.html:53
msgid ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, etc."
msgstr ""
"您当前未设置任何邮件地址。您需要设置一个邮件地址，以便接收通知，重置密码等。"

#: allauth/templates/account/email.html:59
msgid "Add E-mail Address"
msgstr "添加电子邮箱地址"

#: allauth/templates/account/email.html:64
msgid "Add E-mail"
msgstr "添加电子邮箱"

#: allauth/templates/account/email.html:73
msgid "Do you really want to remove the selected e-mail address?"
msgstr "您真的想移除选定的e-mail地址吗？"

#: allauth/templates/account/email/email_confirmation_message.txt:1
#, python-format
msgid ""
"Hello from %(site_name)s!\n"
"\n"
"You're receiving this e-mail because user %(user_display)s has given yours "
"as an e-mail address to connect their account.\n"
"\n"
"To confirm this is correct, go to %(activate_url)s\n"
msgstr ""
"网站%(site_name)s上的用户%(user_display)s将此设为其e-mail地址。\n"
"\n"
"为验证这是正确的，请访问%(activate_url)s\n"

#: allauth/templates/account/email/email_confirmation_message.txt:7
#, python-format
msgid ""
"Thank you from %(site_name)s!\n"
"%(site_domain)s"
msgstr ""
"谢谢您访问%(site_name)s!\n"
"%(site_domain)s"

#: allauth/templates/account/email/email_confirmation_subject.txt:3
msgid "Please Confirm Your E-mail Address"
msgstr "请确认您的E-mail地址"

#: allauth/templates/account/email/password_reset_key_message.txt:1
#, python-format
msgid ""
"Hello from %(site_name)s!\n"
"\n"
"You're receiving this e-mail because you or someone else has requested a "
"password for your user account.\n"
"It can be safely ignored if you did not request a password reset. Click the "
"link below to reset your password."
msgstr ""
"您收到此邮件表示您或者他人在网站 %(site_name)s上为您的账号请求了密码重置。\n"
"若您未请求密码重置，可以直接忽略此邮件。如要重置密码，请点击下面的链接。"

#: allauth/templates/account/email/password_reset_key_message.txt:8
#, python-format
msgid "In case you forgot, your username is %(username)s."
msgstr "作为提示，您的用户名是%(username)s."

#: allauth/templates/account/email/password_reset_key_message.txt:10
#, python-format
msgid ""
"Thank you for using %(site_name)s!\n"
"%(site_domain)s"
msgstr ""
"谢谢您使用%(site_name)s!\n"
"%(site_domain)s"

#: allauth/templates/account/email/password_reset_key_subject.txt:3
msgid "Password Reset E-mail"
msgstr "密码重置邮件"

#: allauth/templates/account/email_confirm.html:7
#: allauth/templates/account/email_confirm.html:11
msgid "Confirm E-mail Address"
msgstr "确认E-mail地址"

#: allauth/templates/account/email_confirm.html:18
#, python-format
msgid ""
"Please confirm that\n"
"            <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail address "
"for user %(user_display)s\n"
"            ."
msgstr ""
"请确认<a href=\"mailto:%(email)s\">%(email)s</a>是否是用户 %(user_display)s的"
"e-mail地址。"

#: allauth/templates/account/email_confirm.html:24
msgid "Confirm"
msgstr "确认"

#: allauth/templates/account/email_confirm.html:31
#, python-format
msgid ""
"This e-mail confirmation link expired or is invalid. Please\n"
"            <a href=\"%(email_url)s\">issue a new e-mail confirmation "
"request</a>."
msgstr ""
"e-mail验证链接失效或无效。请点击 <a href=\"%(email_url)s\">发起新的e-mail验证"
"请求。"

#: allauth/templates/account/login.html:7
#: allauth/templates/account/login.html:13
#: allauth/templates/account/login.html:36
msgid "Sign In"
msgstr "登录"

#: allauth/templates/account/login.html:20
msgid ""
"You can sign in to your account using any of the following third party "
"accounts:"
msgstr "您可以用您的以下任何第三方账号登录"

#: allauth/templates/account/login.html:27
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"                    <a href=\"%(signup_url)s\">sign up</a>first."
msgstr "如果您在本网站还没有账号，请先<a href=\"%(signup_url)s\">注册</a> 。"

#: allauth/templates/account/login.html:37
msgid "Forgot Password?"
msgstr "忘记密码了？"

#: allauth/templates/account/logout.html:6
#: allauth/templates/account/logout.html:9
#: allauth/templates/account/logout.html:19
msgid "Sign Out"
msgstr "注销"

#: allauth/templates/account/logout.html:12
msgid "Are you sure you want to sign out?"
msgstr "您确定要注销登录吗？"

#: allauth/templates/account/messages/cannot_delete_primary_email.txt:2
#, python-format
msgid "You cannot remove your primary e-mail address (%(email)s)."
msgstr "您不能删除您的主e-mail地址 (%(email)s) "

#: allauth/templates/account/messages/email_confirmation_sent.txt:2
#, fuzzy, python-format
msgid "Confirmation e-mail sent to %(email)s."
msgstr "确认e-mail已发往 %(email)s"

#: allauth/templates/account/messages/email_confirmed.txt:2
#, python-format
msgid "You have confirmed %(email)s."
msgstr "您已确认e-mail地址 %(email)s "

#: allauth/templates/account/messages/email_deleted.txt:2
#, fuzzy, python-format
msgid "Removed e-mail address %(email)s."
msgstr "e-mail地址 %(email)s 已删除"

#: allauth/templates/account/messages/logged_in.txt:4
#, python-format
msgid "Successfully signed in as %(name)s."
msgstr "以 %(name)s..身份成功登录"

#: allauth/templates/account/messages/logged_out.txt:2
msgid "You have signed out."
msgstr "您已注销登录。"

#: allauth/templates/account/messages/password_changed.txt:2
msgid "Password successfully changed."
msgstr "密码修改成功。"

#: allauth/templates/account/messages/password_set.txt:2
msgid "Password successfully set."
msgstr "密码设置成功。"

#: allauth/templates/account/messages/primary_email_set.txt:2
msgid "Primary e-mail address set."
msgstr "主e-mail地址已设置"

#: allauth/templates/account/messages/unverified_primary_email.txt:2
msgid "Your primary e-mail address must be verified."
msgstr "您的主e-mail地址必须被验证。"

#: allauth/templates/account/password_change.html:6
#: allauth/templates/account/password_change.html:9
#: allauth/templates/account/password_change.html:15
#: allauth/templates/account/password_reset_from_key.html:5
#: allauth/templates/account/password_reset_from_key.html:8
#: allauth/templates/account/password_reset_from_key_done.html:5
#: allauth/templates/account/password_reset_from_key_done.html:8
msgid "Change Password"
msgstr "修改密码"

#: allauth/templates/account/password_reset.html:7
#: allauth/templates/account/password_reset.html:11
#: allauth/templates/account/password_reset_done.html:7
#: allauth/templates/account/password_reset_done.html:10
msgid "Password Reset"
msgstr "密码重置"

#: allauth/templates/account/password_reset.html:17
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll send you "
"an e-mail allowing you to reset it."
msgstr ""
"忘记密码？在下面输入您的e-mail地址，我们将给您发送一封e-mail，以便重置密码。"

#: allauth/templates/account/password_reset.html:22
msgid "Reset My Password"
msgstr "重置我的密码"

#: allauth/templates/account/password_reset.html:25
msgid "Please contact us if you have any trouble resetting your password."
msgstr "如在重置密码时遇到问题，请与我们联系。"

#: allauth/templates/account/password_reset_done.html:17
msgid ""
"We have sent you an e-mail. Please contact us if you do not receive it "
"within a few minutes."
msgstr "我们已给您发了一封e-mail，如您在几分钟后仍没收到，请与我们联系。"

#: allauth/templates/account/password_reset_from_key.html:8
msgid "Bad Token"
msgstr "Bad Token"

#: allauth/templates/account/password_reset_from_key.html:13
#, python-format
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password reset</"
"a>."
msgstr ""
"密码重置链接无效，可能该链接已被使用。请重新申请<a href="
"\"%(passwd_reset_url)s\">链接重置</a>。"

#: allauth/templates/account/password_reset_from_key.html:19
msgid "change password"
msgstr "修改密码"

#: allauth/templates/account/password_reset_from_key.html:22
#: allauth/templates/account/password_reset_from_key_done.html:11
msgid "Your password is now changed."
msgstr "您的密码现已被修改。"

#: allauth/templates/account/password_set.html:6
#: allauth/templates/account/password_set.html:9
#: allauth/templates/account/password_set.html:15
msgid "Set Password"
msgstr "设置密码"

#: allauth/templates/account/signup.html:6
#: allauth/templates/socialaccount/signup.html:7
msgid "Signup"
msgstr "注册"

#: allauth/templates/account/signup.html:9
#: allauth/templates/account/signup.html:20
msgid "Sign Up"
msgstr "注册"

#: allauth/templates/account/signup.html:11
#, python-format
msgid ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr "已经有一个账号？ 请<a href=\"%(login_url)s\">登录</a>."

#: allauth/templates/account/signup_closed.html:6
#: allauth/templates/account/signup_closed.html:9
msgid "Sign Up Closed"
msgstr "已关闭注册"

#: allauth/templates/account/signup_closed.html:12
msgid "We are sorry, but the sign up is currently closed."
msgstr "非常抱歉，当前已关闭注册。"

#: allauth/templates/account/snippets/already_logged_in.html:5
msgid "Note"
msgstr "注意"

#: allauth/templates/account/snippets/already_logged_in.html:5
#, python-format
msgid "you are already logged in as %(user_display)s."
msgstr "您已以 %(user_display)s的身份登录"

#: allauth/templates/account/verification_sent.html:6
#: allauth/templates/account/verification_sent.html:9
#: allauth/templates/account/verified_email_required.html:6
#: allauth/templates/account/verified_email_required.html:9
msgid "Verify Your E-mail Address"
msgstr "验证您的E-mail地址。"

#: allauth/templates/account/verification_sent.html:12
msgid ""
"We have sent an e-mail to you for verification. Follow the link provided to "
"finalize the signup process. Please contact us if you do not receive it "
"within a few minutes."
msgstr ""
"我们已向您发了一封验证e-mail。点击e-mail中的链接完成注册流程。如果您在几分钟"
"后仍未收到邮件，请与我们联系。"

#: allauth/templates/account/verified_email_required.html:14
msgid ""
"This part of the site requires us to verify that\n"
"        you are who you claim to be. For this purpose, we require that you\n"
"        verify ownership of your e-mail address. "
msgstr ""
"网站的这部分功能要求验证您的真实身份。\n"
"为此，我们需要您确认您是此账号e-mail地址的所有者。"

#: allauth/templates/account/verified_email_required.html:18
msgid ""
"We have sent an e-mail to you for\n"
"        verification. Please click on the link inside this e-mail. Please\n"
"        contact us if you do not receive it within a few minutes."
msgstr ""
"我们已经给您发送了一封e-mail验证邮件。\n"
"请点击e-mail中的链接。若您在几分钟后仍未收到邮件，请联系我们。"

#: allauth/templates/account/verified_email_required.html:22
#, python-format
msgid ""
"<strong>Note:</strong> you can still <a href=\"%(email_url)s\">change your e-"
"mail address</a>\n"
"        ."
msgstr ""
"<strong>注意:</strong> 您仍然能够<a href=\"%(email_url)s\">修改您的e-mail地"
"址 </a>."

#: allauth/templates/bootstrap/bootstrap.html:11
msgid "Xinjiang Horse"
msgstr "新疆马业"

#: allauth/templates/openid/login.html:10
msgid "OpenID Sign In"
msgstr "OpenID登录"

#: allauth/templates/socialaccount/authentication_error.html:6
#: allauth/templates/socialaccount/authentication_error.html:9
msgid "Social Network Login Failure"
msgstr "社交网络登录失败"

#: allauth/templates/socialaccount/authentication_error.html:12
msgid ""
"An error occurred while attempting to login via your social network account."
msgstr "当尝试用您的社交网络账号登录时，发生了一个错误。"

#: allauth/templates/socialaccount/connections.html:6
#: allauth/templates/socialaccount/connections.html:9
msgid "Account Connections"
msgstr "账号绑定"

#: allauth/templates/socialaccount/connections.html:11
msgid "You already logged with this local account:【UserName:"
msgstr "您目前登录的本地账户为：【用户名："

#: allauth/templates/socialaccount/connections.html:11
msgid "Email"
msgstr "电子邮箱"

#: allauth/templates/socialaccount/connections.html:13
msgid ""
"You can sign in to your account using any of the following third party\n"
"            accounts:"
msgstr "您已经绑定了以下第三方账号："

#: allauth/templates/socialaccount/connections.html:41
msgid "Remove binding"
msgstr "解除绑定"

#: allauth/templates/socialaccount/connections.html:49
msgid ""
"You currently have no social network accounts connected to this account, "
"meaning that you can only login in with above local account!"
msgstr ""
"您的本地账号目前还没有和任何第三方社交网络账号绑定，意味着您只能用上面的本地"
"账号进行登录！"

#: allauth/templates/socialaccount/connections.html:52
msgid "Add a 3rd Party Account"
msgstr "您可以选择绑定下列社交网络账号："

#: allauth/templates/socialaccount/login_cancelled.html:6
#: allauth/templates/socialaccount/login_cancelled.html:10
msgid "Login Cancelled"
msgstr "登录已取消"

#: allauth/templates/socialaccount/login_cancelled.html:15
#, python-format
msgid ""
"You decided to cancel logging in to our site using one of your existing "
"accounts. If this was a\n"
"        mistake, please proceed to <a href=\"%(login_url)s\">sign in</a>."
msgstr ""
"您决定取消使用您的已有账号登录我们的网站。如果这是一个误操作，请继续<a href="
"\"%(login_url)s\">登录</a>."

#: allauth/templates/socialaccount/messages/account_connected.txt:2
msgid "The social account has been connected."
msgstr "社交账号已连接"

#: allauth/templates/socialaccount/messages/account_connected_other.txt:2
msgid "The social account is already connected to a different account."
msgstr "社交账号已连接到另一个账号。"

#: allauth/templates/socialaccount/messages/account_disconnected.txt:2
msgid "The social account has been disconnected."
msgstr "社交账号已断开连接"

#: allauth/templates/socialaccount/signup.html:10
#: allauth/templates/socialaccount/signup.html:23
msgid "Map Sign Up"
msgstr "映射注册"

#: allauth/templates/socialaccount/signup.html:13
#, python-format
msgid ""
"You are about to use your\n"
"        %(provider_name)s account to login to\n"
"        %(site_name)s. As a final step, please complete the following form:"
msgstr ""
"%(site_name)s网站需要将您的%(provider_name)s账号映射为一个本站账户，请填写以"
"下表单来完成本站映射注册："
