-- 插入django_site表记录
INSERT INTO django_site (id, name, domain) VALUES
  (1, '新疆马业', 'www.xjhorse.net');

-- 插入socialapp表记录
INSERT INTO socialaccount_socialapp (id, provider, name, client_id, secret, key) VALUES
  (1, 'facebook', '脸谱第三方认证', '1285334538187541', '8ba05edc57fe5f8540db53c43a936052' ,''),
  (2, 'baidu', '百度第三方认证', 'lh8xpyXTlQRoBwUdppUXE5d9', '1dTAfSyTqs8or3I9gGpqjsF2T1OaAHaP', ''),
  (3, 'github', ' GitHub第三方认证', '16090608775de7b92bdb', '8a8cd13124ac7cb7c02a370ebffd75f6b2f69ada', ''),
  (4, 'google', '谷歌第三方认证', '727545488791-l4eo681o035fspulhonkb78a0k9hdaib.apps.googleusercontent.com',
   'C7iWMu408fGEA9SLJAQ2-oc-', ''),
  (5, 'taobao', '淘宝第三方认证', '23717023', 'dca00573f06c81615171ddc6789fc3e8', ''),
  (6, 'weixin', '微信三方认证', 'wxbee37c2ebeda849e', 'add064f87de5d60dd7b72282543e893a', ''),
  (7, 'qq', 'QQ第三方认证', '101388003', '2c4af98f598ed6fd9dde7ad92951f634', '');

-- 插入socialapp_sites表记录
INSERT INTO socialaccount_socialapp_sites (id, socialapp_id, site_id) VALUES
  (1, 1, 1),
  (2, 2, 1),
  (3, 3, 1),
  (4, 4, 1),
  (5, 5, 1),
  (6, 6, 1),
  (7, 7, 1);
